%\section*{Executive Summary}

The 2012 discovery of a new particle, subsequently shown by the
ATLAS and CMS collaborations to be a Higgs boson,
has closed a chapter in particle physics.
Not only on the experimental side, putting an end to a decades-long search,
but also, and perhaps more sharply, by completing the set of predictions by the standard model (SM) for elementary particles.
The challenge that is ahead for the LHC and future machines is now fully in the
BSM realm.

The LHC Run 2, which started in 2015, now has a qualitatively different goal
in what regards the program for measuring the properties of this Higgs boson
and the search for deviations from the SM predictions.

The WG2 contributions to this Yellow Report therefore naturally cluster around two main axes, supplemented by a third aspect:
\begin{enumerate}
  \item How to expand the palette of measurements that can be performed by the experiments.
  \item How to interpret existing measurements to set limits on and constrain new physics and characterize discoveries.
  \item Tools with which to proceed in practice.
\end{enumerate}

The two main axes are complementary to and feed off of each other:
measurements pave the way for different interpretations,
while interpretation frameworks motivate new measurements.
This being said, there are caveats to this interaction.
For instance,
while almost any framework can be used to motivate particular measurements,
the interpretation of a measurement and the definition of (pseudo)-observables
can only be consistently done in a well-defined theory framework.
In other words,
much in the same way that finding a significant deviation
with the kappa framework would clearly point to BSM physics,
its meaning and interpretation would require a well-defined theory,
which the kappa framework alone is not.

Complementing the chapters on measurements and interpretation,
there are also two chapters describing tools
that can be used in the different aspects
of the measurement and interpretation steps.

This Executive Summary provides an overview of the WG2 chapters, comprised in Part~\ref{chap:EFT} and, in collaboration with WG1, in Part~\ref{chap:MO}.
The goal is not to exhaustively review the contents,
but to offer a ``lay of the land'',
providing the reader with the most salient and distinctive features of each chapter and how the different chapters are related and connected
with each other.

\subsection*{Measurements}

In this Yellow Report,
the existing kappa framework for the search of deviations from the SM predictions
is substantially expanded in two, complementary, ways:
simplified template cross-sections (STXS) and
pseudo-observables (PO). 
This dichotomy arises naturally from the fact that at the LHC the Higgs boson interaction
with SM particles is probed at multiple energy scales.
For instance,
while $H \to 4\ell$ probes  
the amplitude coupling the Higgs to four fermions in a region of transferred momenta kinematically bounded by $m_H$,
the associated ZH production  will probe the same amplitude
(or at least part of it) at significantly higher momentum transfer,
possibly even in the multi-TeV region.
That explains the different approaches presented in this Yellow Report, with some chapters focusing on production properties and others on decay properties.

Chapter~\ref{STCS} presents a way to partition the phase-spaces of different Higgs boson production processes into simplified template cross-sections.
The goal of the STXS partitioning is two-fold:
\begin{enumerate}
 \item To separate regions of the phase-space for which theory uncertainties can evolve with time.
 \item To single out parts of the production phase-space where BSM physics predicts large deviations from the SM expectation. In this case, rare corners of SM production can be used to probe for BSM-induced deviations.
\end{enumerate}
The STXS are mostly a tool that generalizes the notion of production process into sub-processes and the result of their use is a measurement of fully-extrapolated and unfolded cross-sections that can be also expressed as signal strengths relative to the SM predictions.
This allows to recombine the measurements and update total cross section measurements ex post facto.
This is for instance the case with jet binning for gluon-fusion cross-sections: measuring the ggH plus 0-jet, 1-jet, and 2-jet sub-processes allows to avoid to commit to a single prescription for jet bin migration that is needed to extract the ggH total cross-section, allowing for the prescription to evolve and be introduced later.

The STXS can be thought of as fully extrapolated and unfolded cross sections that can be inferred differentially in the production properties.
As they are extrapolated from a simultaneous fit, this allows for advanced experimental techniques (including multi-variate observables and discriminants) to be employed in the analyses.
The use of such techniques is not possible, for instance, when measuring fiducial cross-sections, as it is very hard, if not impossible, to define the fiducial volume for a multi-variate observable.

In a completely complementary way, fiducial cross-section (FXS) measurements provide easy-to-reproduce phase-spaces.
Many practical aspects of FXS measurements are discussed in Chapter~\ref{chap:FXS}, paving the way for common extractions of more model-independent quantities that are easy to collect in persistent form, using the HepData database and the Rivet toolkit.
Attention is also paid to the interplay between signal and background processes in a given fiducial volume, as well as to unfolding of experimental effects from the measurements.

Finally, Chapter~\ref{chap:PO} discusses how on-shell Higgs boson decays and production cross sections, close to the threshold region, can be parameterized in terms of pseudo-observables. 
The PO framework builds up on the similar approach introduced for Z-pole observables at LEP. In Higgs physics the formalism is 
a bit more complicated by the multiple poles involved in Higgs boson decays into 3 and 4 bodies, as well as in Higgs boson production cross sections.
This richer kinematical structure is decomposed in terms of independent Lorentz structures, as well as resonant and non-resonant contributions, 
whose form is 
dictated by the general analytic properties of the amplitudes under the  assumption that no BSM particles appear on-shell.
The purpose and the main philosophy of Higgs PO is the same of the Z-pole (pseudo)-observables at LEP:
PO are well-defined quantities from the quantum field theory point of view,
that can be measured by experiments and then interpreted in generic BSM scenarios, 
including effective theory approaches.

\subsection*{Interpretation}

Given that the SM has been completed, the focus on extending the SM is very strong.
There are two fundamentally different ways to go about extending the SM Lagrangian:
via concrete BSM alternatives (such as SUSY), for which different predictions are provided in Part~\ref{chap:BSM}, or through an effective descriptions of sufficiently high-mass (hence partially decoupled) degrees of freedom, as discussed in Part~\ref{chap:EFT}.
The latter approach,
referred to as the effective field theory (EFT) approach to Higgs physics,
is the one discussed in the chapters contributed from WG2.
In this case, the SM Lagrangian is extended by adding higher-dimension operators written in terms of SM fields. 
Such a framework can be used to describe the effects of new heavy
particles on Higgs physics in a large class of models beyond the SM.
The Wilson coefficients of the higher-dimension operators in the EFT
encode information about masses and couplings in the UV theory that
completes the SM.

The EFT approach to Higgs physics is conceptually different from ``top-down'' EFT approaches, such as HQET, where the ultraviolet completion of the theory is known.
In the Higgs EFT case, the full theory is unknown and the working conditions are ``bottom-up''.
This means that, a priori, there is a large range for the possible values of the couplings of the higher-dimensional operators.
The latter can be restricted employing additional dynamical or symmetry assumptions about the overarching BSM model. 
This is why different EFT approaches
(based on different symmetry hypotheses,  
and order of the expansion in the various couplings) are discussed in the chapters from WG2.\\

More generally, two main themes are addressed:\\[2mm]
%\begin{itemize}
%\item 
\hspace{.3cm}-- The definition of the theory frameworks that can be used to extend the SM.\\[2mm]
%\item 
\hspace{.3cm}-- Discussion of the limitations that such effective descriptions have in describing different BSM physics scenarios (UV completions).\\
%\end{itemize}


In terms of Lagrangian formulation, two avenues are explored.
In  Section~\ref{s.nleft} the  chiral Lagrangian relevant to the case  of a possible non-linear realization of electroweak symmetry breaking is presented. 
The largest effort, though, was devoted to the so-called SM EFT where, much as in the SM,  the electroweak symmetry is  realized linearly  and broken spontaneously by the VEV of the Higgs field.  
Within this framework, Sections~\ref{s.eftbasis} and \ref{s.eftnlo} discuss how the Wilson coefficients of dimension-6 operators are related to deformations of the Higgs boson couplings from the SM predictions. 
Furthermore, Chapter~\ref{s.eftbasis} proposes a parameterization of the space of dimension-6 operators, the so-called Higgs basis,  that is convenient for  calculating Higgs observables at the leading order (LO) in the SM EFT.
Section~\ref{s.eftnlo} provides NLO results in the SM EFT after performing the renormalization programme.
Given the differences between the nonlinear formulation, the SM EFT at the LO,  and the NLO formulation, it is important to note that, depending on the exact UV completion realized in nature, there may or
may not be a close correspondence between interpretations of the data in these different frameworks. 
Calculations within the LO EFT are simpler from the theoretical point of view and introduce a minimal number of parameters to describe leading deformations of Higgs observables.
However, there may be physical situations where the LO EFT does not provide an adequate description and going beyond the LO is necessary; they are discussed in Sections~ \ref{s.eftval} and \ref{s.eftnlo}.  
EFT with a non-linearly realized electroweak symmetry is less predictive than the SM EFT, but it may be relevant for certain classes of  UV completions of the SM.

Sections~\ref{s.eftval} and \ref{s.eftmodels} then explore the applicability of the SM EFT construction,
both concluding that the SM EFT enjoys a broad range of validity in the absence of new particles with masses in the hundreds of GeV.
One topic that is particularly difficult to address in interpreting measurements in terms of the underlying theory parameters,
Wilson coefficients in this case, is that of theory uncertainties.
These studies also shed some light into how operators with dimensions higher than 6 can play a role in the interpretation.

One conclusion arising from the discussion in this chapter is that there isn't
a unique EFT approach to be recommended as different EFT approaches have different validity limits.
In principle, the more general, the better, as there are fewer
implicit assumptions about the ultraviolet completion.
However, generality comes at the price of increased complexity and
some compromise between generality and simplicity may be necessary,
especially in the early phases of the LHC Run 2.
This highlights the importance for EFT interpretations of LHC data to always
be pursued in parallel to more general (although less predictive) approaches
such as FXS, STXS, and POs.

Finally, it should be noted that the SM EFT has overarching implications to electroweak physics and even in other sectors in the sense that 
in its full generality, it can be constrained not only from precise measurements of Higgs boson properties but also other measurements, ranging from multiboson production, to top quark properties.
Global fits of EFT parameters may allow for better constraints on BSM
physics and amplify the power of the data.
Indeed, the ability to combine distinct measurements within one
consistent framework is a great strength of the EFT approach and one
of its main motivations.

\subsection*{Tools}

Of course none of the concepts above can be put to practice without computational tools
that allow for the simulation of the different SM deformations,
as well as tools that simplify the practical aspects related to the
statistical inference on the parameters of interest.

Section~\ref{s.efttools} overviews the available frameworks upon which many of the interpretations previously discussed, but also the measurements, can be based on.
The effective Lagrangian formulation is widely used in order to make predictions starting
from Wilson coefficients, but it is also used without loss of generality for
encoding pseudo-observables.

In Section~\ref{s.eftmorphing} the reader can find a practical proposal to
model predictions for different processes in a multidimensional context of SM deformations.
The tools provide for continuous interpolation between parameter-space points, are based on commonly use software at the LHC, and may also find use in other applications.
By providing smooth and continuous interpolations, they allow for simple application
of likelihood ratio methods commonly used by ATLAS and CMS to determine the allowed confidence regions for parameters.
