%\input{./WGx/morphingintro} 
\label{sec:mprinc}
The properties of the newly discovered Higgs boson have been
extensively probed by the ATLAS and CMS experiments using LHC Run\,1
proton-proton collision data at $\sqrt{s}=7$ and
$8$\,TeV \cite{Aad:2015mxa,Khachatryan:2014kca,Aad:2015zhl,Khachatryan:2016vau}.
The studies of the tensor structure of the Higgs boson couplings to
gauge bosons were based on signal models including at most one or two
Beyond the Standard Model coupling parameters at a time, with all
remaining Beyond the Standard Model (BSM) parameters set to zero. For Run\,2, it is envisioned to
have signal models which depend on a larger number of coupling
parameters, in order to account for possible correlations among
them. Additional coupling parameters in the Higgs boson coupling to Standard Model (SM) particles
change the predicted cross section, as well as the shape of differential distributions.
 In this context, it is necessary to revise the existing signal
modelling methods and provide alternatives which are better suited for
 such a multidimensional parameter space.

For this purpose, a morphing method has been developed and
implemented. It provides a continuous description of arbitrary
physical signal observables such as cross sections or differential
distributions in a multidimensional space of coupling parameters. The
morphing-based signal model is a linear combination of a minimal set
of orthogonal base samples (templates) spanning the full coupling
parameter space. The weight of each template is derived from the
coupling parameters  appearing in the signal matrix element.

Morphing is more than a simple interpolation technique, in that it is
not limited to the points in the range spanned by the input
samples. In fact, the choice of the input samples is arbitrary, and
any set of input samples satisfying the required conditions to build
the morphing function will span the entire space, independent of their
precise coordinates.%

A full explanation and validation of this method is shown in reference \cite{LHCHXSWG-INT-2016-004}.



%%%
\subsection{Morphing principles}
%\input{./WGx/morphingderivation}
The morphing procedure is based on the concepts of the morphing of (possibly multi-dimensional) histograms described in Ref~\cite{Baak:2014fta}. It is introduced to describe the dependence of a given physical observable $T$ on an arbitrary configuration of a set of non-SM Higgs boson couplings $\vec{g}_{\text{target}} \equiv \{ g_{\SM}, g_{{\rm BSM} ,1}, .. , g_{{\rm BSM} ,n} \}$ to known particles. This dependence is described by a morphing function
\begin{align}
  T_{\text{out}}(\vec{g}_{\text{target}}) = \sum_{i}w_{i}(\vec{g}_{\text{target}}; \vec{g}_{i})T_{\text{in}}(\vec{g}_{i}),
  \label{eq:derivation_morphingFunction}
\end{align}
which linearly combines the values or differential distributions
$T_{\text{in}}$ at a number of selected discrete coupling
configurations $\vec{g}_i=\{ \tilde {g}_{\SM,i}, \tilde{g}_{{\rm BSM} ,1}, .. , \tilde{g}_{{\rm BSM} ,n}\}$.  The
input distributions $T_{\text{in}}$ are normalized to their expected
cross sections such that $T_{\text{out}}$ includes not only the
correct shape, but also the correct cross section prediction. Here,
$g_{\SM}$ denotes the Higgs boson coupling predicted by the Standard
Model. Morphing only requires that any differential cross section can
be expressed as a polynomial in coupling parameters. For calculation at lowest order and using the narrow-width approximation
for a resonance, this yields a second order polynomial each in
production and decay.


In practice, the template distributions $T_{\text{in}}$ are obtained
from the Monte Carlo (MC) simulation of the signal process for a given
coupling configuration $\vec{g}_i$. The minimal number $N$ of Monte
Carlo samples needed to describe the signal at all possible coupling
configurations, depends on the number $n$ of studied non-SM coupling
parameters. The contribution of each sample $T_{\text{in}}$ is
weighted by a weight $w_i$ based on the assumption that the value of a
physical observable is proportional to the squared matrix element for
the studied process
\begin{align}
T \propto \left\vert \mathcal{M}\right\vert^2.
  \label{eq:derivation_ME}
\end{align}
The weights $w_i$ can therefore be expressed as functions of the coupling parameters in the matrix element $\mathcal{M}$. In this case $T$ can be anything derived from the Matrix element, for example a whole MC sample.

The described procedure allows for a continuous description in an
$n$-dimensional parameter space. A feature-complete implementation
has been developed within the RooFit package \cite{Verkerke:2003ir}, making use
of HistFactory \cite{Cranmer:2012sba}. The provided signal model can
therefore be used in commonly used RooFit workspaces in a
straightforward, black-box-like way. A visual representation of the idea for a simple case is shown in Figure~\ref{fig:morphexample}.

%\begin{figure}[H]
\begin{figure}
\includegraphics[width=\textwidth]{WG2/WG2_9_Morphing/morphing_visualisation.pdf}
\caption{Illustration of the morphing procedure in a simple showcase.\label{fig:morphexample}}
\end{figure}


%%
\subsection{General procedure to construct morphing function}
\label{sec:procedure}
%\input{./WGx/morphingnd} 


A step-by-step explanation on how to construct the morphing function for processes with an arbitrary number of free coupling parameters in two vertices is outlined below.
\begin{enumerate}
\item Construct a general matrix element squared
\begin{align}
\left|{\rm ME}(\vec{g})\right|^{2} = \underbrace{\left(\sum_{x\in p,s}g_{x}\mathcal{O}(g_{x})\right)^{2}}_{\text{production}}\cdot
                      \underbrace{\left(\sum_{x\in d,s}g_{x}\mathcal{O}(g_{x})\right)^{2}}_{\text{decay}},
\end{align}
denoting operators appearing only in the production vertex with $p$,
such only appearing in the decay vertex with $d$, and such shared
between both vertices with $s$, and assuming that production and decay
vertices are uncorrelated, which is the case for a scalar intermediate
particle.
\item Expand the matrix element squared to a 4th degree polynomial in the coupling parameters
\begin{align}
\left|{\rm ME}(\vec{g})\right|^{2}=\sum_{i=1}^{N}X_{i}\cdot P_{i}\left(\vec{g}\right),
%\left|\ME(\vec{g})\right|^2 = \sum_{i=1}^N X_i \cdot \prod_{1..4}g .
\end{align}
$X_i$ is a prefactor, which will be represented by an input distribution. In the 4th degree polynomial $P_i\left(\vec{g}\right) = g_ag_bg_cg_d$ of the coupling parameters $\vec{g}$, the same coupling can occur multiple times (e.g.~$g_\SM^4$ or $g_{{\rm BSM},1}g_{{\rm BSM},2}g_{{\rm BSM},3}^2$). The number of different expressions in the polynomial $N$ is equal to the number of samples needed for the morphing.
\item Next generate input distributions at arbitrary but fixed parameter points $\vec{g}_i$
\begin{align}
T_{\text{in},i}\propto\left|{\rm ME}(\vec{g_{i}})\right|^{2}.
%T_\text{in}(\vec{g}_i) \propto \left|\ME(\vec{g_i})\right|^2.
\end{align}
\item Construct the morphing function with an ansatz
\begin{align}
T_{\text{out}}(\vec{{g}})&=\sum_{i=1}^{N}\underbrace{\left(\sum_{j=1}^{N}A_{ij} P_{j}\left(\vec{g}\right)\right)}_{w_{i}(\vec{{g}})} T_{\text{in},i}.\\
&= \vec{P}\left(\vec{{g}}\right)\cdot A\vec{T},
%T_\text{out}(\vec{g}_\text{target}) = \sum_{i=1}^N \underbrace{\left(\sum_{j=1}^N a_{ij}(\vec{g}_i) \prod_{1..4}g \right)}_{w_i(\vec{g}_\text{target},\vec{g}_i)} \cdot T_\text{in}(\vec{g}_i).
\end{align}
where the second line is the first one recast in matrix notation.  The
matrix $A$ has to be calculated to obtain the full morphing function.
\item Thus, exploit that the output distribution should be equal to the input distribution at the respective input parameters
\begin{align}
T_\text{out}\left(\vec{{g}}_i\right) = T_{\text{in},i} \qquad \text{for}\qquad i=1,\dots ,N.
\end{align}
which can also be cast in matrix notation as
\begin{align}
  \begin{split}
    A \cdot \left(P_j\left(\vec{g}_i\right)\right)_{ij}
    = 1\!\!1\\
    \Leftrightarrow \qquad A\cdot G=1\!\!1.
  \end{split}
\end{align}
\item The unique solution $A=G^{-1}$ requires the input parameters to fulfil the condition $\text{det}(G)\ne 0$.


\end{enumerate}



When the aim is to perform a likelihood fit on some (pseudo-)data $T_d$, the minimization condition is
\begin{align}
  \widehat{\vec{g}}\left(T_d\right) = {\rm argmin}_{\vec{g}} -2 \ln P\left(T_d\mid \mu =
\sum_{i=1}^{N}\left(\sum_{j=1}^{N}A_{ij} P_{j}\left(\vec{g}\right)\right)T_{\text{in},i}\right).
\end{align}
From this it becomes apparent that only the polynomials $P_{j}\left(\vec{g}\right)$ need to be
recalculated during the minimization process, while the non-trivial
quantities $A_{ij}$ and $T_{\text{in},i}$ stay fixed.

The error propagation of statistical uncertainties to the output
$T_\text{out}$ is conceptually straightforward. Since the $\vec{g}_i$
are free parameters, the matrix $A$ carries no uncertainty
besides numerical fluctuations. Thus, uncertainties only propagate via
linear combinations. The question of how the input parameters
$\vec{g}_i$ need to be chosen such that the expected uncertainty of
the output is minimal, within some parameter region of interest, is
non-trivial and will be addressed in future studies.


The number $N$ of input base samples
depends on how many of coupling parameters enter the production and/or the decay vertex.
However, the general morphing principle remains the same and the method can be
generalized to a higher-dimensional coupling parameter space. 


A general expression for the number of input samples $N$ with $n_p$ couplings appearing only in production, 
$n_d$ couplings appearing only in decay and $n_s$ couplings shared in production and decay is given by
\begin{align}
N&=\quad\frac{n_{p}\left(n_{p}+1\right)}{2}\cdot\frac{n_{d}\left(n_{d}+1\right)}{2}+{4+n_{s}-1 \choose 4}\label{eq:p2d2b4}\\
&+\left(n_{p}\cdot n_{s}+\frac{n_{s}\left(n_{s}+1\right)}{2}\right)\cdot\frac{n_{d}\left(n_{d}+1\right)}{2}\label{eq:pbb2d2}\\
&+\left(n_{d}\cdot n_{s}+\frac{n_{s}\left(n_{s}+1\right)}{2}\right)\cdot\frac{n_{p}\left(n_{p}+1\right)}{2}\label{eq:dbb2p2}\\
&+\frac{n_{s}\left(n_{s}+1\right)}{2}\cdot n_{p}\cdot n_{d}+\left(n_{p}+n_{d}\right){3+n_{s}-1 \choose 3}. \label{eq:b2pdb3pb3d}
\end{align}
In this expression the counting is split for (\ref{eq:p2d2b4}) terms pure in production and decay, or pure in shared, (\ref{eq:pbb2d2})
terms pure in decay and mixed in production and shared or purely shared, (\ref{eq:dbb2p2}) terms pure in production
and mixed in decay and shared or purely shared, and (\ref{eq:b2pdb3pb3d} terms mixed in both, and terms mixed in one and
purely shared in the other.

This is a general definition of the number of samples N in terms of
number of coupling parameters $n_p$, $n_d$, and $n_s$.  In case of the gluon
fusion process with subsequent decays to vector bosons, the production
and decay will have a completely disjoint set of couplings, and the
number of input samples will be given by Eq.\,\ref{eq:p2d2b4} by
setting $n_s = 0$. For the VBF Higgs boson production with subsequent
decay into vector bosons, when considering the same set of couplings
in the production and the decay vertex, the number of samples is given
by Eq.\,\ref{eq:p2d2b4} with $n_p = 0$ and $n_d = 0$.



%%
\subsection{Conclusions}
\label{sec:conclusions}
%\input{./WGx/morphingoutlook}

This note describes a method for modelling signal parameters and
distributions in a multidimensional space of coupling
parameters. This method is capable of continuously morphing signal
distributions and rates based on a minimal orthogonal set of
independent base samples. Therefore it allows to directly fit for the
coupling parameters that describe the SM and possibly non-SM
interaction of the Higgs boson with fermions and bosons of the SM.


This method can be utilized to test the properties of the Higgs boson
during the LHC Run 2 data-taking period and beyond and has already
been tested successfully \cite{LHCHXSWG-INT-2016-004}.



\clearpage

