
%\usepackage{booktabs}
%\usepackage{xspace}

%\usepackage[T1]{fontenc} % if needed

% % for signed comments
%\definecolor{darkgreen}{rgb}{0,0.5,0}
%\definecolor{darkpurple}{rgb}{0,0.5,0.5}
%\definecolor{darkblue}{rgb}{0,0,0.7}
%\definecolor{darkred}{rgb}{0.5,0,0.0}
%\definecolor{darkorange}{rgb}{0.8,0.4,0.0}
%\definecolor{green}{rgb}{0.0,0.8,0.4}

%\providecommand{\order}[1]{{\cal O}\left(#1\right)}
%\providecommand{\as}{\alpha_s}
%\providecommand{\aw}{\alpha_w}
%\providecommand{\asCMW}{\alpha_s^{\rm CMW}}
%\providecommand{\fb}{\;\mathrm{fb}}
%\providecommand{\pb}{\;\mathrm{pb}}
%\providecommand{\nb}{\;\mathrm{nb}}
%\providecommand{\GeV}{\;\mathrm{GeV}}
%\providecommand{\TeV}{\;\mathrm{TeV}}
%\providecommand{\ptjv}{p_{\rm t,veto}}
%\providecommand{\cF}{{\cal F}}
%\providecommand{\cO}[1]{{\cal O}\left(#1\right)}
%\providecommand{\LL}{\text{LL}}
\providecommand{\LLR}{\text{LL$_R$}\xspace}
\providecommand{\NNLLNNLO}{\text{NNLO+NNLL}\xspace}
\providecommand{\NNLLNNNLO}{N$^3$LO+NNLL\xspace}
\providecommand{\NNNLO}{\text{N$^3$LO}\xspace}
\providecommand{\NNNLL}{\text{N$^3$LL}\xspace}
%\providecommand{\eff}{\epsilon}
%\providecommand{\asbar}{{\bar \alpha}_s}
%\providecommand{\q}{q_1,\dots,q_n}

%\providecommand{\sigmai}[1]{{\sigma^{(#1)}}}
%\providecommand{\sigmatot}[1]{\sigma_{\rm tot, #1}}


%
In some Higgs boson decay modes (most notably $WW^*$ and $\tau\tau$),
it is standard to perform different analyses depending on the number
of accompanying jets.
%
This is because different jet multiplicities have different dominant
backgrounds.
%
Of particular importance for the $WW$ decay is the zero-jet case, where
the dominant top-antitop background is dramatically reduced.
%
For precision studies it is important to predict accurately the
fraction of signal events that survive the zero-jet constraint, and to
assess the associated theory uncertainty.
%
Jet-veto transverse momentum thresholds used by ATLAS and CMS are
relatively soft ($\ptjv\sim 25-30$ GeV), hence QCD real radiation is
constrained by the cut and the imbalance between virtual and real
corrections results in logarithms of the form $\ln(\ptjv/m_H)$ that
should be resummed to all orders in the coupling constant.
%
This resummation has been carried out to next-to-next-to-leading
logarithmic accuracy (\text{NNLL}, i.e.\ including all
terms $\as^n \ln^k (\ptjv/m_H)$ with $k \ge n-1$ in the logarithm of the
cross section) and matched to next-to-next-to-leading order (\text{NNLO}) in
refs.~\cite{Banfi:2012jm,Stewart:2013faa,Becher:2013xia}
(some of the calculations also included partial \NNNLL{} contributions).
%
At this order one finds that the effect of the resummation is to shift
central predictions only moderately, and to reduce somewhat the
theoretical uncertainties.
%
Yet, the residual theoretical uncertainty remains sizeable, roughly
$10\%$~\cite{Banfi:2012jm}, and the impact of higher-order effects
could therefore be significant.


Since the first \NNLLNNLO{} predictions for the jet-veto, three
important theoretical advances happened: firstly, the \NNNLO{}
calculation of the total gluon-fusion cross
section~\cite{Anastasiou:2015ema}; secondly the calculation of the
\text{NNLO} corrections to the Higgs plus one-jet
cross-section~\cite{Boughezal:2015dra,Boughezal:2015aha,Caola:2015wna};
and finally the LL resummation of logarithms of the jet-radius
$R$~\cite{Dasgupta:2014yra}.
%

All these recent results were merged together in
ref.~\cite{Banfi:2015pju} by extending the matching of the jet-veto
cross-section to N$^3$LO+NNLL+\LLR. The effect of heavy-quark masses
has been considered following the approach of
ref.~\cite{Banfi:2013eda}. The code used to produce the following
results can be downloaded from~\cite{JetVHeto}.


%----------------------------------------------------------------------
\subsubsection{\texorpdfstring{N$^3$LO+NNLL+LL$_{R}$}{N3LO+NNLL+LL_R} cross section and 0-jet efficiency at 13 TeV}
\label{sec:all}


In this section we report predictions for the jet-veto efficiency and
cross section in Higgs boson production in gluon fusion at the
LHC. The results are based on the calculation obtained in
ref.~\cite{Banfi:2015pju}. The reader should refer to the latter work
for additional information.
%
The various ingredients that we use are summarized below:
\begin{itemize}
\item The total N$^3$LO cross section for Higgs boson production in gluon
  fusion~\cite{Anastasiou:2015ema}, obtained in the heavy-top
  limit. The Wilson coefficient is expanded out consistently both in
  the computation of the total and the inclusive one-jet cross
  section.
\item The inclusive one-jet cross section at NNLO taken from the code
  of ref.~\cite{Caola:2015wna}, in the heavy-top limit.
  %
  In this computation the $qq$ channel is included only up to NLO, and
  missing NNLO effects are estimated to be well below scale variation
  uncertainties~\cite{Boughezal:2015aha}.
%
\item Exact top- and bottom-mass effects up to NLO in the jet-veto
  efficiency and cross section~\cite{Spira:1995rr}. Beyond NLO, we use
  the heavy-top result, without any modifications.
%
\item Large logarithms $\ln(m_H/\ptjv)$ resummed to NNLL accuracy
  following the procedure of~\cite{Banfi:2012jm}, with the treatment of
  quark-mass effects as described in ref.~\cite{Banfi:2013eda}.
%
\item Logarithms of the jet radius resummed to LL accuracy,
  following the approach of ref.~\cite{Dasgupta:2014yra}.
\end{itemize}


We consider 13 TeV LHC collisions with a Higgs boson mass of
$m_H = 125$ GeV.
%
For the top and bottom pole quark masses, we use $m_t\,=\,172.5$ GeV and
$m_b\,=\,4.75$~GeV.
%
Jets are defined using the anti-$k_t$
algorithm~\cite{Cacciari:2008gp}, as implemented in \texttt{FastJet
  v3.1.2}~\cite{Cacciari:2011ma}, with radius parameter $R=0.4$, and
perform the momentum recombination in the standard $E$ scheme (i.e.\
summing the four-momenta of the pseudo-particles).
%
We use PDF4LHC15 parton distribution functions at NNLO with
$\alpha_s(m_Z) = 0.118$
(\texttt{PDF4LHC15\_nnlo\_mc})~\cite{Butterworth:2015oua}.
%
The impact of higher-order logarithmic corrections is probed by
introducing a resummation scale $Q$ as shown in ref.~\cite{Banfi:2012jm}.
%
The central prediction for the jet-veto efficiency is obtained by
using the matching scheme $(a)$~\cite{Banfi:2015pju}, setting the
renormalization and factorization scales to $\mu_R=\mu_F = m_H/2$, and
the resummation scale relative to both top and bottom contributions to
$Q=Q_0=m_H/2$.
%
To determine the perturbative uncertainties for the jet-veto
efficiency we follow the Jet-Veto efficiency (JVE) method as outlined
in ref.~\cite{Banfi:2015pju}. This differs from the original method of
refs.~\cite{Banfi:2012yh,Banfi:2012jm} which has been modified to take
into account the excellent convergence observed at the perturbative
order considered here ~\cite{Banfi:2015pju}. The uncertainty band is
determined as described below.
%

We vary $\mu_R$, $\mu_F$ by a factor of 2 in either direction,
requiring $1/2 \le \mu_R/\mu_F \le 2$.
%
Maintaining central $\mu_{R,F}$ values, we also vary $Q$ in the
range $\frac{2}{3}\le Q/Q_0\le\frac{3}{2}$.
%
As far as the small-$R$ effects are concerned, subleading logarithmic
effects are estimated by means of a second resummation scale $R_0$,
which acts as the initial radius for the evolution of the gluon-jet
fragmentation which gives rise to the $\ln R$ terms.  We choose the
default value $R_0=1.0$,\footnote{The initial radius for the small-$R$
  evolution differs from the jet radius used in the definition of
  jets, which is $R=0.4$.} and vary it conservatively by a factor of
two in either direction.
%
Finally, keeping all scales at their respective central values, we
replace the default matching scheme $(a)$ with scheme $(b)$, as
defined in ref.~\cite{Banfi:2015pju}.
%
The final uncertainty band is obtained as the envelope of all the
above variations.
%
We do not consider here the uncertainties associated with the parton
distributions (which mostly affect the cross section, but not the
jet-veto efficiency), the value of the strong coupling or the impact
of finite quark masses on terms beyond NLO. Moreover, our results do
not include electro-weak effects.


%
\begin{figure}
  \centering
  \includegraphics[width=0.49\textwidth]{./WG1/ggF/figs/matched-best-v-n2lo-efficiency.pdf}\hfill
  \includegraphics[width=0.49\textwidth]{./WG1/ggF/figs/matched-best-v-n3lo-efficiency.pdf}
  \caption{N$^3$LO+NNLL+LL$_R$ best prediction for the jet-veto efficiency (blue/hatched)
    compared to NNLO+NNLL (left) and fixed-order at N$^3$LO
    (right).
  }
  \label{fig:bestprediction-efficiency}
\end{figure}
%
In the left plot of Figure~\ref{fig:bestprediction-efficiency} we show
the comparison between our best prediction for the jet-veto efficiency
(N$^3$LO+NNLL+LL$_R$) and the previous NNLO+NNLL accurate prediction,
both including mass effects.
 %
 We see that the impact of the N$^3$LO correction on the central value
 is in the range 1-2\% at relevant jet-veto scales. The uncertainty
 band is significantly reduced when the N$^3$LO corrections are
 included, going from about 10\% at NNLO down to a few per cent at
 N$^3$LO.
%
Figure~\ref{fig:bestprediction-efficiency} (right) shows the
comparison between the N$^3$LO+NNLL+LL$_R$ prediction and the pure
N$^3$LO result. We observe a shift of the central value of the order
of 2\% for $\ptjv > 25\UGeV$ when the resummation is turned on.
%
In that same $\ptjv$ region, the uncertainty associated with the
N$^3$LO prediction is at the 3\% level, comparable with that of the
N$^3$LO+NNLL+LL$_R$ prediction.
%
The fact that resummation effects are nearly of the same order as the
uncertainties of the fixed order calculation suggests that the latter
might be accidentally small.
%
This situation is peculiar to our central renormalization and
factorization scale choice, $\mu_R = \mu_F = m_H/2$, and does not
occur at, for instance, $\mu_R=\mu_F=m_H$ (see
ref.~\cite{Banfi:2015pju} for details).
%

The zero-jet cross section is obtained as
$\Sigma_\text{0-jet}(\ptjv)=\sigma_{\rm tot}\,\epsilon(\ptjv)$, and
the inclusive one-jet cross section is obtained as
$\Sigma_{\ge\text{1-jet}}(p_{t,\rm{min}})=\sigma_{\rm
  tot}\,\left(1-\epsilon(p_{t,\rm{min}})\right)$.
The associated uncertainties are obtained by combining in quadrature
the uncertainty on the efficiency obtained as explained above and that
on the total cross section, for which we use plain scale
variations. The corresponding results are shown in
\refF{fig:bestprediction-sigma}.
%
For this scale choice, we observe that the effect of including
higher-order corrections in the zero-jet cross section is quite
moderate at relevant $\ptjv$ scales. This is because the small $K$
factor in the total cross section compensates for the suppression in
the jet-veto efficiency. The corresponding theoretical uncertainty is
reduced by more than a factor of two.

\begin{figure}
  \centering
  \includegraphics[width=0.49\textwidth]{./WG1/ggF/figs/matched-best-v-n2lo-sigma.pdf}\hfill
  \includegraphics[width=0.49\textwidth]{./WG1/ggF/figs/matched-best-v-n3lo-sigma.pdf}
  \caption{N$^3$LO+NNLL+LL$_R$ best prediction for the jet-veto cross section (blue/hatched)
    compared to NNLO+NNLL (left) and fixed-order at N$^3$LO
    (right).  }
  \label{fig:bestprediction-sigma}
\end{figure}

The predictions for jet-veto efficiency and the zero-jet cross section
are summarized in Table~\ref{tab:13-TeV-0jet}, for two experimentally
relevant $\ptjv$ choices.
%
Results are reported both at fixed-order, and including the various
resummation effects.


\begin{table}
 \caption{Predictions for the jet-veto efficiency and cross
   section at N$^3$LO+NNLL+LL$_{R}$, compared to the N$^3$LO and NNLO+NNLL cross
   sections.
   %
   The uncertainty in the fixed-order prediction is obtained using the
   JVE method. All numbers include the effect of top and bottom quark
   masses, treated as described in the text, and are for a central scale $\mu_0=m_H/2$.}
 \label{tab:13-TeV-0jet}
\begin{center}
%{\renewcommand{\arraystretch}{1.1}
  \begin{tabular}{c|c|c|c|c}
    \toprule
    LHC 13 TeV
    & $\epsilon^{{\rm N^3LO+NNLL+LL_R}}$ & \,$\Sigma^{{\rm
                                N^3LO+NNLL+LL_R}}_\text{0-jet}\,{\rm [pb]}$\,  &
                                                          \,$\Sigma^{{\rm
                                                          N^3LO}}_\text{0-jet}$ &
                                                          \,$\Sigma^{{\rm
                                                          NNLO+NNLL}}_\text{0-jet}\,$
    \\[0.2em] \midrule
    $\ptjv=25\,{\rm GeV}$ & $0.534^{+0.017}_{-0.008}$ & $24.0^{+0.8}_{-1.0}$ & $23.6^{+0.5}_{-1.2}$ & $23.6^{+2.5}_{-3.6}$ \\
    $\ptjv=30\,{\rm GeV}$ & $0.607^{+0.016}_{-0.008}$ & $27.2^{+0.7}_{-1.1}$ & $26.9^{+0.4}_{-1.2}$ & $26.6^{+2.8}_{-3.9}$ \\
   \bottomrule
  \end{tabular}
\end{center}
\end{table}



\begin{figure}
  \centering
  \includegraphics[width=0.49\textwidth]{./WG1/ggF/figs/1jet-matched-v-fixed-JVE.pdf}\hfill
  \includegraphics[width=0.49\textwidth]{./WG1/ggF/figs/1jet-matched-direct-v-JVE.pdf}
  \caption{Matched NNLO+NNLL+\LLR prediction for the inclusive one-jet
    cross section (blue/hatched) compared to fixed-order at NNLO
    (left) and to the matched result with direct scale variation for
    the uncertainty (right), as explained in the text.}
  \label{fig:1jetxs}
\end{figure}

Figure~\ref{fig:1jetxs} shows the inclusive one-jet cross section
$\Sigma_{\ge \text{1-jet}}$, for which the state-of-the-art
fixed-order prediction is
NNLO~\cite{Boughezal:2015dra,Boughezal:2015aha,Caola:2015wna}. The
left-hand plot shows the comparison between the best prediction at
NNLO+NNLL+LL$_R$, and the fixed-order at NNLO. Both uncertainty bands
are obtained with the JVE method outlined in
ref.~\cite{Banfi:2015pju}. We observe that the effect of the
resummation on the central value at moderately small values of $\ptjv$
is at the per cent level. Moreover, the inclusion of the resummation
leads to a slight increase of the theory uncertainty in the small
transverse momentum region.
%

The right-hand plot of \refF{fig:1jetxs} shows our best prediction
for the one-jet cross section with uncertainty obtained with the JVE
method, compared to the case of just scale (i.e.\ $\mu_R$, $\mu_F$,
$Q$) variations. We observe a comparable uncertainty both at small and
at large transverse momentum, which indicates that the JVE method is
not overly conservative in the tail of the distribution. We have
observed that the same features persist for the corresponding
differential distribution.
%
Table~\ref{tab:13-TeV-1jet} contains the predictions for the
inclusive one-jet cross section for two characteristic $p_{t,\text{min}}$ choices.


\begin{table}
 \caption{Predictions for the inclusive one-jet cross section at
   NNLO+NNLL+LL$_{R}$ and NNLO. The uncertainty in the fixed-order
   prediction is obtained using the JVE method. All numbers include
   the effect of top and bottom quark masses, treated as described in
   the text, and are for a central scale $\mu_0=m_H/2$. }
 \label{tab:13-TeV-1jet}
  \begin{center}
%    {\renewcommand{\arraystretch}{1.1}
      \begin{tabular}{c|c|c}
        \toprule
        LHC 13 TeV
        &\, $\Sigma^{{\rm
          NNLO+NNLL+LL_R}}_{\ge \text{1-jet}}\,{\rm [pb]}$\,  &
                                                               \,$\Sigma^{{\rm
                                                               NNLO}}_{\ge\text{
                                                               1-jet}}\,{\rm
                                                               [pb]}$
        \\[0.2em] \midrule
    $p_{\rm t, min}=25\,{\rm GeV}$ & $20.9^{+0.4}_{-1.1}$ & $21.2^{+0.7}_{-1.0}$\\
    $p_{\rm t,min}=30\,{\rm GeV}$ & $17.6^{+0.4}_{-1.0}$ & $17.9^{+0.6}_{-0.8}$ \\
   \bottomrule
  \end{tabular}
\end{center}
\end{table}


%===========================
%
