An accurate modelling of the differential distributions in gluon
fusion production is important since the experimental analyses
typically combine measurements in different phase space regions,
either to improve the sensitivity to a signal or to target other 
Higgs boson production modes to which gluon fusion is a background.  In this
section, comparisons are performed between the predictions of
different parton-level computations and hadron-level event generators,
to assess their compatibilities and the accuracy of the modelling.

Unless otherwise specified, all the predictions correspond to a Higgs boson 
mass of $125$ \UGeV, $\sqrt{s} = 13$ \UTeV, and the choice of SM
input parameters and PDFs in
Sects.~\ref{chapter:input}--~\ref{chap:PDFs}. We will first list the
various codes and calculations used in the benchmarking, and then
discuss predictions for several observables in turn.


\begin{figure}\centering
\begin{center}
  \includegraphics[width=0.8\textwidth]{./WG1/ggF/figs/WG1_benchmarks_Fig1.pdf}
\end{center}
\vspace*{-0.3cm}
\caption{Comparison of cross section in different regions of $p_{T,\,H}$.}
\label{fig:WG1-ggF-differential-parton-pt}
\end{figure}



\subsection{Calculations and codes}



We list here the calculations and codes used in the following
benchmarking, and provide information about settings whenever they
differ from our default.

\subsubsection{Parton level codes }
\label{sec:partonlevelcodes}
\noindent
{\sc \bf HRes}: A parton-level
code~\cite{deFlorian:2012mx,Grazzini:2013mca} to compute differential distributions
in gluon fusion production at NNLO QCD accuracy, with NNLL QCD resummation for small $\pT(\PH)$
and matching to NLO QCD $\PH+1\text{jet}$ at large $\pT(\PH)$.
Finite top, bottom, and charm quark masses are included at NLO QCD accuracy.
In this comparison, predictions were computed with a dynamical scale  
$\mu_R=\mu_F=(\sqrt{m_H^2+p_t^2})/2$ and with resummation scales
$Q_1=m_H/2$, $Q_2=2 m_b$. The overall uncertainties are estimated by
taking the envelope of  a seven-point scale variation of $\mu_R$ and $\mu_F$ for
central $Q_1$ and $Q_2$ and a  
variation of $Q_1$ and $Q_2$ one at a time by a factor two about their
central value, with central  $\mu_R$ and $\mu_F$.\\ 
\noindent
{\sc \bf CuTe}: A parton-level code~\cite{Becher:2012yn}, for the $\pT(\PH)$ differential
distribution, with NNLL QCD resummation at small $\pT(\PH)$ and matching to NLO QCD
$\PH+1\text{jet}$ at large $\pT(\PH)$. The resummation is based on
Soft Collinear Effective Theory (SCET) and NNLL accuracy is
accordingly defined.
The finite top quark mass is included at LO QCD accuracy. 
The scale is chosen as $\mu= p_T + q^*\exp(p_t/q^*)$
with $q^*=7.83$~GeV. Uncertainties are estimated by varying $\mu$ by a
factor two about its central value and 
by varying the unknown coefficient $F_{(3,0)}^g$ of the
anomaly exponent $F^g$ in the range $\pm 2 (4 \pi) F_{(2,0)}^g$, and
also by including one-sigma PDF uncertainties and varying $\alpha_s$
in the range $[0.1165,0.1195]$.
\\
\noindent
{\sc \bf NRV}: A parton-level computation to NNLL resummed accuracy
matched to the fixed-order $O(\alpha_s^4)$
computation~\cite{Neill:2015roa}. Resummation is
performed using a SCET approach and the logarithmic order is defined
accordingly. All quarks but top are assumed massless.  Uncertainties
are estimated as described in Ref.~\cite{Neill:2015roa}.
The 
individual NNLO PDF sets MMHT and NNPDF3.0 entering the PDF4LHC
recommendations have been used, and uncertainties within and between
the sets are 
included in the error estimation.\\
\noindent{\sc \bf MRT}: This code was described in
Sect.~\ref{subsec:MRT}. It is based on a parton-level computation at
NNLL for the Higgs boson $p_T$ distribution matched to the NNLO
($O(\alpha_s^5)$) fixed-order prediction for Higgs+jet. The central
prediction uses $\mu_R = \mu_F = m_H$, and $Q = m_H/2$. The perturbative 
uncertainty for all predictions is estimated by seven-point scale
variation of  $μ_R$ and $μ_F$ with fixed resummation scale, and
varying the resummation 
scale by a factor two for fixed $\mu_R$ and $\mu_F$. 
\\\noindent
{\sc \bf BFGLP}: A parton-level prediction for $\PH+1\text{jet}$ at NNLO
QCD using jettiness  
subtraction~\cite{Boughezal:2015aha}.   The prediction is not matched to inclusive gluon fusion production, and thus the resulting
 $\pT(\PH)$ distribution can be directly compared to the other predictions only well above the
jet $\pT$ threshold used in the computation ($30\UGeV$), but has a higher accuracy because 
of the extra order in QCD.  Predictions shown use the settings of
Ref.~\cite{Heinemeyer:2013tqa} but with NNPDF2.3 parton distributions.
\\
\noindent
{\sc \bf NNLOJET}: This code~\cite{Chen:2016zka} was described in Sect.~\ref{sec:antenna}. It has
the same perturbative 
accuracy as BFGLP. A threshold of $p_T=30$~GeV is adopted for
jet counting. Uncertainties are estimated by three-point scale
variation about $\mu_r=\mu_F=m_H$. The NNLOJET code was validated extensively
    against the H+j NNLO calculation of~\cite{Caola:2015wna}
    (discussed in Section~\ref{sec:jve}), yielding excellent agreement at below one per cent for all
    distributions.
\\
\noindent
{\sc \bf STWZ-BLPTW} This code was described in
Sect.~\ref{subsec:stwz_blptw}. It is a SCET-based resummation for the
jet veto at NNLL$^\prime$+NNLO. The
prescription  used for uncertainty estimation was described in detail
in Sect.~\ref{subsec:stwz_blptw}.
\\
\noindent{\sc \bf JVE} This code was described in Sect.~\ref{sec:jve}. It
performs a N$^3$LO+NNLL+LLR accurate resummation for the jet veto,
including heavy quark mass effects up to NLO. Beyond NLO, the heavy top
result is used as explained in Ref.~\cite{Banfi:2015pju}. The
prescription  used for uncertainty estimation was described in detail
in Sect.~\ref{sec:jve}.
\\
{\sc \bf Gosam + Sherpa}: The predictions used in this comparison
include up to three additional  
jets at NLO QCD
accuracy~\cite{Cullen:2011ac,Cullen:2014yla,Gleisberg:2008ta} in the approximation of an infinitely 
heavy top quark.
%
The predictions presented in this report were computed using sets of
Ntuples with generation cuts set to 
%
%\begin{equation}
$ p_T\;>\;25~\UGeV~, |\eta|\;<\;4.5$.
%\label{cuts:basic}
%\end{equation}
%
%This easily allows to restrict the transverse momentum cut to
%$p_T\;>\;30~\UGeV$, to fulfil the recommendations for this study. 
The cut on the pseudorapidity can not be removed, but
an explicit computation at LO shows that the effect of these cuts is
at the per cent level.
%The jets were clustered according to the recommendations using the
%anti-$k_T$ algorithm~\cite{Cacciari:2005hq,Cacciari:2008gp}
%implemented in the \textsc{FastJet} package~\cite{Cacciari:2011ma}
%with a radius of $R=0.4$. For the partons distribution in the protons
%we use the PDF4LHC15\_nlo\_30~\cite{Butterworth:2015oua} PDFs set.
%
The renormalization and factorization scales were set to
$\frac{\hat{H}^\prime_T}{2}\;=\;\frac{1}{2}\left(\sqrt{m_{\mathrm{H}}^{2}+p_{T,\mathrm{H}}^{2}}+\sum_{i}|p_{T,i}|\right)$, 
where the sum runs over all partons accompanying the Higgs boson in
the event. The theoretical uncertainties were estimated by varying
both of them by factors of $0.5$ and $2$ around this central
value. Further details are provided in Ref.~\cite{Greiner:2015jha}.
\\


\begin{figure}\centering
\begin{center}
  \includegraphics[width=0.7\textwidth]{./WG1/ggF/figs/WG1_benchmarks_Fig4a.pdf}
  \includegraphics[width=0.7\textwidth]{./WG1/ggF/figs/WG1_benchmarks_Fig4b.pdf}
\end{center}
\vspace*{-0.3cm}
\caption{
  Various predictions for the  inclusive and exclusive jet cross
  section for gluon fusion Higgs boson production. 
  } \label{fig:jetbins}
\end{figure}

\subsubsection{Hadron-level event generators}
\noindent
\noindent
{\sc \bf MG5\_ aMC@NLO}: 
The predictions used in this comparison include up to two additional jets at NLO QCD accuracy
with the {\sc FxFx} merging scheme, in the Effective Field Theory approach ($\Mt\to\infty$).
The top quark mass is included via reweighting of the events.  The full mass effects are included
in all multiplicities in all contributions apart from the two-loop
matrix element in the virtual for $\PH+1\text{jet}$ and $\PH+2\text{jets}$ NLO matrix elements, for which
the correction is approximated using that of the Born contribution.
The bottom quark mass and the top-bottom interference are included in $\PH+0\text{jet}$ at NLO
using the resummation scales suggested in \cite{Harlander:2014uea}.
The central value of merging scale is set to 30~GeV, and the deviations from using merging scales of 20~GeV and 50~GeV is taken as an uncertainty. Further details are provided in Ref.~\cite{Frederix:2016cnl}. \\
\noindent
{\sc \bf Powheg NNLOPS}: Predictions have  NNLO QCD accuracy for inclusive
events, and NLO+PS for Higgs+one jet.
Top and bottom quark mass effects are included up to NLO according to
Ref.~\cite{Hamilton:2015nsa}. The central scale choice is
$\mu_F=\mu_R=m_H/2$. Uncertainties are obtained by preforming a
three-point scale variation of $\mu_R=\mu_F$ by a factor two about the
central value for the NNLO part, and using a 9-point variation for the Powheg scale.
These two uncertainties are taken as uncorrelated and are added in quadrature to obtain the total QCD scale variation. Uncertainties are also provided for switching off bottom and top mass effects.


%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}\centering
\includegraphics[width=0.49\textwidth]{./WG1/ggF/figs/WG1_benchmarks_Fig3a.pdf}
\includegraphics[width=0.49\textwidth]{./WG1/ggF/figs/WG1_benchmarks_Fig3b.pdf}

%\includegraphics[width=0.49\textwidth]{./WG1/ggF/figs/WG1_benchmarks_Fig2b.pdf}
\vspace*{-0.3cm}
\caption{Predictions of the Higgs boson rapidity distribution
for gluon fusion production, including both parton-level calculations
and hadron-level MC predictions. The spectra are shown at the
inclusive level (left) and with a jet cut (right).}
\label{fig:Higgs-rapidity}
\end{figure}

\subsection{Observables}
We will first discuss results for integrated cross-sections, then
consider various differential distributions. 

\input{WG1/ggF/benchmarkFractions}


\subsubsection{Cross-sections}
In \refF{fig:WG1-ggF-differential-parton-pt} we compare
predictions for cross-sections in different ranges of the Higgs boson $p_T$. 
Cross-sections are obtained integrating differential
$p_{T,\,H}$ distributions separately for each uncertainty variation provided.
The uncertainties about the central value are obtained using the
prescription associated with each prediction, most commonly quadratic
addition of the difference from the nominal for each effect
considered.

The QCD scale uncertainty is obtained by integrating the spectrum
for each scale choice and taking the envelope around the nominal integral.
The exception is NNLOPS, for which two QCD scale uncertainties are considered:
one uncertainty from the three-point envelope of the NNLO part, and a separate
uncertainty from the nine-point envelope of the Powheg scale. These
uncertainties 
are treated as independent sources and are hence added in quadrature. The
former affects the normalization ("yield") while the latter affects the
migration between low and high Higgs boson (and jet) $p_T$.

It should be pointed out that not all predictions include the same
set of uncertainties. All include QCD scale variations, which is always the
dominant uncertainty. Some also
include other uncertainties, for example from choices of PDF set and
resummation scale.

For the following discussion we find it useful to take the MRT
prediction as a reference, since it has the nominally highest accuracy
both at low and high $p_{T,\,H}$. At low $p_{T,\,H}<20$~GeV we see
that MRT is in good agreement with all codes which include N$^2$LL
resummation (HRes, CuTe and NRV). At high $p_{T,\,H}$ MRT agrees well
with NNLOJET both in central value and (small) uncertainty. We note that
POWHEG NNLOPS agrees well with MRT both at low $p_{T,\,H}$  (even though it
does not have formally NNLL accuracy) and at high $p_{T,\,H}$ (even if
it does not have NNLO corrections to Higgs+1 jet). On the other hand,
we observe that MG5\_aMC@NLO is lower and with rather larger
uncertainty at low $p_T$ since it does not include the NNLO correction
to Higgs boson production. The GoSam+Sherpa prediction at high $p_T$ is
lower.
%, but we observe that top quark mass effects are likely to be
%better approximated at NLO at high $p_{T,\,H}$. 
Finally we remark that
the NRV prediction tends to be lower at medium and large $p_{T,\,H}$,
which also reflect on their total cross section.  This might be
related to the shape function used in the intermediate region by this
group in order to switch to the fixed order result at high  $p_{T,\,H}$.


Next, in \refF{fig:jetbins} we compare predictions for
inclusive (top) and exclusive (bottom) jet-binned cross sections.
The 0-jet inclusive cross-sections obtained integrating the
N$^3$LO+JVE+N$^3$LL construction is by definition the recent N$^3$LO
result. 
Furthermore, for the 0-jet inclusive cross-section Powheg NNLOPS is accurate at
NNLO; STWZ is formally also NNLO accurate, but it is higher (and with
smaller uncertainty) as it
includes $\pi^2$ resummation; MG5\_aMC@NLO is on the other hand lower
and with larger uncertainties as
it is only accurate to NLO. In the inclusive  one-jet bit all
predictions which are NNLO accurate (for Higgs+1 jet) are in good
agreement with each other, while GoSam+Sherpa and  MG5\_aMC@NLO are
lower. Note that even though Powheg NNLOPS does not include NNLO
corrections, it is in good agreement with the NNLO-accurate
predictions.  In the higher inclusive jet bins all results are in
reasonable agreement, with differences most likely due to choices of
scale and the treatment of heavy quarks. We note that for the 3-jet
cross-section the NNLOJET prediction has a very large uncertainty due to
its LO nature. For Powheg NNLOPS the third jet is only provided by
parton showering; it is however known that the uncertainty is
underestimated in this case. 



Exclusive cross sections follow a similar pattern. Note that in this
case the uncertainties shown for MG5\_aMC@NLO and Powheg NNLOPS are
unreliable as jet veto effects are not properly accounted for. 

The fractions of events in the various inclusive and exclusive bins
are tabulated in \refT{tab:ggbenchmarkTable}.
\begin{figure}\centering
\includegraphics[width=0.49\textwidth]{./WG1/ggF/figs/WG1_benchmarks_Fig2a.pdf}

%\includegraphics[width=0.49\textwidth]{./WG1/ggF/figs/WG1_benchmarks_Fig2b.pdf}
\vspace*{-0.3cm}
\caption{ Same as \refF{fig:Higgs-rapidity}, but for the Higgs boson transverse 
momentum distribution at low $p_T$.
}
\label{fig:Higgs-pT}
\end{figure}




%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsubsection{Differential distributions}




In \refF{fig:Higgs-rapidity} we compare Higgs boson rapidity
distributions, both at the inclusive level (left) and with a jet cut
(right) defined as above. At the inclusive level, as before, the NLO
MG5\_aMC@NLO result undershoots the predictions from HNNLO and Powheg
NNLOPS which by construction agree with each other. Note that the
$K$-factor is not flat: NNLO corrections are more important for large rapidity.
Also in the presence of a jet the NLO result from GoSam+Sherpa is
somewhat low. The BFGLP and NNLOJET results substantially differ in shape,
especially at high rapidity, and the BFGLP result seems to have
very small scale  uncertainties.  However, it should be noted that the BFGLP 
setup is different to the default (see Section~\ref{sec:partonlevelcodes}), and 
in particular different PDFs are used.


\begin{figure}\centering
\includegraphics[width=0.49\textwidth]{./WG1/ggF/figs/WG1_benchmarks_Fig2c.pdf}
\includegraphics[width=0.49\textwidth]{./WG1/ggF/figs/WG1_benchmarks_Fig2d.pdf}
\vspace*{-0.3cm}
\caption{Same as \refF{fig:Higgs-pT} for  $p_{T,H}>60\UGeV$.}
\label{fig:WG1-ggF-differential-parton-ptlarge}
\end{figure}




Next, in \refF{fig:Higgs-pT}
we show the Higgs boson transverse momentum distribution at
low  $p_T$. 
Note that all predictions but CuTe have bins with
width of 10~GeV. The  CuTe, MRT and HRes results are in good
agreement throughout the $p_T$ range, with some differences appearing
for $p_T\lsim 10$~GeV. Again the NLO prediction from MG5\_aMC@NLO is
lower and with a somewhat different shape; similar considerations
apply to NRV at high $p_T$ which has the same fixed-order accuracy. At
low $p_T$ NRV does not appear to have a Sudakov peak at the same $p_T$
value as other results. NNLOPS follows closely the HRes result, with
only minor differences in the smallest $p_T$ bin. 


The high $p_T$ region for the same distributions is shown in
\refF{fig:WG1-ggF-differential-parton-ptlarge}. Note that 
predictions should  be taken with care for $p_T\gsim m_T$, as they are
all obtained in the infinite top mass approximation. As above, HRes and
Powheg NNLOPS agree well within uncertainties for all $p_T$ values. At
high $p_T$ MRT and NNLOJET display a somewhat harder spectrum because
they include NNLO corrections  to the one-jet configuration. The CuTe
prediction agrees well with HRes for all $p_T$ values.
The NRV prediction
appears to have a somewhat different shape, and it overshoots the HRes
prediction at the largest $p_T$ despite not including NNLO corrections.  
Uncertainties are all comparable, with the MRT uncertainty smallest as
expected since it includes the N$^3$LO correction to the inclusive
result. 


%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}\centering
\includegraphics[width=0.49\textwidth]{./WG1/ggF/figs/WG1_benchmarks_Fig5a.pdf}
\includegraphics[width=0.49\textwidth]{./WG1/ggF/figs/WG1_benchmarks_Fig5b.pdf}
\vspace*{-0.3cm}
\caption{
  Leading (left) and subleading (right jet $p_T$ distributions.
}
\label{fig:WG1-ggF-differential-hadron-pt1}
\end{figure}


We now turn to the leading and subleading jet $p_T$ distributions,
shown in \refF{fig:WG1-ggF-differential-hadron-pt1}. For the
leading jet (left plot) the NNLOJET result, which is NNLO, is higher and
with smaller uncertainty than GoSam+Sherpa and MG5\_aMC@NLO, which
agree well with each other. The Powheg prediction is affected by large
statistical fluctuations, but the shape can be understood by noting
that at low $p_T$ it agrees with NNLOJET as it includes the NNLO
correction to Higgs boson production, while at high $p_T$ it reproduces the
behaviour of the other NLO Monte Carlos. For the subleading jet
(right plot) all predictions but Powheg NNLOPS have the same NLO
accuracy and agree  within uncertainties. Powheg NNLOPS on the other
hand is leading-order only and its uncertainty is known to be somewhat
underestimated, yet it is in reasonable agreement with the other
results. 




\begin{figure}\centering
\includegraphics[width=0.49\textwidth]{./WG1/ggF/figs/WG1_benchmarks_Fig5c.pdf}
\includegraphics[width=0.49\textwidth]{./WG1/ggF/figs/WG1_benchmarks_Fig5d.pdf}
\vspace*{-0.3cm}
\caption{ The transverse momentum of the 
  third jet  (left) and of the Hjj system (right). }
\label{fig:WG1-ggF-differential-hadron-pt2}
\end{figure}

In \refF{fig:WG1-ggF-differential-hadron-pt2} we show the
 transverse momentum of the 
  third jet  (left) and of the Hjj system (right). These two
 distributions start at $O(\alpha_s^5)$  and thus they coincide in the
 NNLOJET computation, which provides a purely leading-order description
 of these quantities and is thus affected by a large uncertainty. On
 the other hand, the
 $p_T$ of the 
 third jet (left plot) is described by 
 GoSam+Sherpa   at NLO, and 
in this case the
 uncertainty is reduced. The Powheg NNLOPS result for this
 distribution
 agrees well with these computations
 despite the fact that the third jet is only given by the parton
 shower. As far as the Hjj $p_T$ is concerned now GoSam+Sherpa HJJ@NLO
 and NNLOJET both provide a leading-order description while 
GoSam+Sherpa HJJJ@NLO provides  a NLO
 description at high $p_T$. The leading order
 predictions agree well within their large uncertainties. The NLO
 correction is positive at large $p_T$ but the uncertainty is not
 significantly reduced~\cite{Greiner:2015jha}. At lower $p_T$ the HJJJ@NLO becomes unreliable as
 it misses part of the NLO correction since the transverse momentum
 of all jets, including the third one,  is by 
 construction $p_T>30$~GeV.
  The Powheg NNLOPS follows, and in fact exceeds HJJJ@NLO  at high
 $p_T$ but it lies below the LO result  at lower $p_T$. 


\begin{figure}\centering
\includegraphics[width=0.32\textwidth]{./WG1/ggF/figs/WG1_benchmarks_Fig6a.pdf}
\includegraphics[width=0.32\textwidth]{./WG1/ggF/figs/WG1_benchmarks_Fig6b.pdf}
\includegraphics[width=0.32\textwidth]{./WG1/ggF/figs/WG1_benchmarks_Fig6c.pdf}
\vspace*{-0.3cm}
\caption{Two-jet variables typically used in VBF studies: from left to
 right, mass of the dijet system, distance in rapidity, and azimuthal angle between the two
 jets.
}
\label{fig:WG1-ggF-differential-hadron-pt3}
\end{figure}

Finally in \refF{fig:WG1-ggF-differential-hadron-pt3} we show
variables relevant for VBF studies. Cross-sections are also tabulated
in \refT{tab:benchmarkVBF}. All results agree reasonably well
within the large uncertainties. 

\input{WG1/ggF/vbfBenchmark}

