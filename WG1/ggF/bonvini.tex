In this contribution we briefly summarize the impact of threshold
resummation on the inclusive Higgs boson production cross-section, both in
terms of the shift in the central value, as well as a means to
faithfully estimate the theoretical uncertainty from missing higher
orders, $\delta(\text{mho})$, as detailed in
Ref.~\cite{Bonvini:2016frm}.
%
In this context, the proposed best estimate for the Higgs cross section is given by
\begin{equation} \label{eq:master}
\sigma_{\text{N$^3$LO+N$^3$LL}} = \sigma_{\text{N$^3$LO}} + \Delta_3\sigma_{\text{N$^3$LL}},
\end{equation}
where $ \sigma_{\text{N$^3$LO}}$ is the fixed-order cross section at N$^3$LO, as computed in
Refs.~\cite{Anastasiou:2014vaa,Anastasiou:2014lda,Anastasiou:2015ema,Anastasiou:2016cez}.
The second contribution, $\Delta_3\sigma_{\text{N$^3$LL}}$, contains the all-orders resummation of those
contributions that are enhanced in the threshold limit to N$^3$LL
accuracy~\cite{Bonvini:2014joa,Catani:2014uta,Bonvini:2014tea,Schmidt:2015cea},
minus its expansion to fixed N$^3$LO (so this contribution starts at N$^4$LO).
%
The computation of $\Delta_3\sigma_{\text{N$^3$LL}}$ Eq.~\eqref{eq:master} is done through the public code \texttt{TROLL}~\cite{TROLL},
formerly \texttt{ResHiggs}. \texttt{TROLL} does not compute the fixed order, but only the subtracted resummed contribution,
$\Delta_3\sigma_{\text{N$^3$LL}}$, so the fixed order has to be supplied by an external code.
In this section the code \texttt{ggHiggs}~\cite{ggHiggs} has been used.

The resummation is performed in a conjugate (Mellin) space, where the threshold limit corresponds to large $N$.
%
While resummation uniquely determines the coefficients of logarithmically enhanced terms and constants, there is a certain latitude in defining how the soft approximation is constructed, by making choices which differ by terms which vanish as $N\to\infty$.
%
In Refs.~\cite{Bonvini:2014joa, Ball:2013bra}, this freedom was
exploited to construct variants of threshold resummation that have
better perturbative properties. In particular, $\psi$-soft
resummation correctly reproduces, order by order in the strong
coupling, the analytic properties of fixed-order coefficient
functions. 
%
Moreover, as pointed out in Ref.~\cite{Bonvini:2014joa},
$\psi$-soft can be further improved by including in the calculation
more contributions to the soft expansion of the Altarelli-Parisi
splitting function. Two options have been considered:
\begin{itemize}
\item AP2 (default): $P_{gg}$ is retained to second order in $1-z$;
\item AP1: $P_{gg}$ is retained to first order in $1-z$.
\end{itemize}
Varying the order of this soft expansion (AP2 vs.\ AP1) can be used as an estimate of unknown contributions beyond the threshold limit.
%
Finally, in order to assess the impact of subleading contributions beyond N$^3$LL,
one can vary the way we deal with the constant terms, which can be treated in the default setup of Ref.~\cite{Bonvini:2016frm}
or in two extreme configurations:
\begin{itemize}
\item default: those constants coming from the Mellin transform of plus distributions~\cite{Altarelli:1981ax} are in the exponent, the others are not;
\item all constants in the exponent;
\item no constants  in the exponent.
\end{itemize}
%
Up to the working logarithmic accuracy, the position of the constants does not make any difference.
However, beyond the working logarithmic accuracy, moving constants produces, by interference, different subleading contributions.
%
Note that, since constants are known to play an important role for Higgs boson production~\cite{Ahrens:2008qu,Berger:2010xi,Stewart:2013faa},
these variations provide a robust way to estimate the perturbative uncertainty.
%
Combining together the different options for subleading logarithmic terms and subleading threshold terms one gets a total of $3\times2=6$
variants of the resummation.

For simplicity, and for disentangling effects coming from different sources, results are given in the clean environment of the
(rescaled) large-$m_t$ effective theory (rEFT), using the top mass $m_t=172.5$~GeV in the pole scheme,
the Higgs boson mass $m_H=125$~GeV, and the \texttt{PDF4LHC15\_nnlo\_100} PDF set~\cite
{Butterworth:2015oua,Carrazza:2015aoa,Ball:2014uwa,Harland-Lang:2014zoa,Dulat:2015mca}.
The strong coupling $\alpha_s$ is run from $\alpha_s(m_Z^2)=0.118$ (from the PDF set) to $\mu_R$ at four loops.
%
In order to show the stability of the resummed result, four options for the central common factorization and renormalization scale $\mu_0$ are considered:
$\mu_0 = m_H/4$, $\mu_0 = m_H/2$, $\mu_0 = m_H$, $\mu_0 = 2m_H$.
Then, the scales $\mu_R$ and $\mu_F$ are varied about $\mu_0$ by a factor of 2 up and down,
keeping the ratio $\mu_R/\mu_F$ never larger than 2 or smaller than $1/2$ (canonical 7-point scale variation).

In Ref.~\cite{Bonvini:2016frm}, $\psi$-soft with AP2 and with the natural choice for the constants is considered as the best option for threshold resummation. 
%
However, the other variants of $\psi$-soft have the same formal accuracy and are used to estimate the uncertainty from $1/N$ terms and subleading logarithmic corrections.
%
Thus, combining these variations with the 7-point scale variation a robust estimate of the uncertainty coming from unknown missing higher orders is proposed:
\begin{equation*}
\boxed{\delta(\text{mho}): \;\text{envelope of the canonical 7-point scale variations and the 6 variants of $\psi$-soft resummation}}
\end{equation*}
This corresponds to a total of $7\times 6=42$ cross section points from which one takes the highest and the lowest
cross sections as the maximum and minimum of the uncertainty band, and provides the most conservative way of including these uncertainties.
%

\begin{table}
  \caption{Fixed-order results and their scale uncertainty together with resummed results and their uncertainty
    (as given by the envelope of prescription and scale variations)
        for four choices of the central scale. Results are given in
        the (rescaled) large-$m_t$ effective theory with pole top mass
        (see text).}
\begin{center}
  \begin{tabular}{l|llll}
  \toprule
    & $\mu_0=m_H/4$ & $\mu_0=m_H/2$ & $\mu_0=m_H$ & $\mu_0=2m_H$ \\
    \midrule
             LO & $ 18.6^{+5.8}_{-3.9} $ & $ 16.0^{+4.3}_{-3.1} $ & $ 13.8^{+3.2}_{-2.4} $ & $ 11.9^{+2.5}_{-1.9} $ \\[1ex]
            NLO & $ 44.2^{+12.0}_{-8.5} $ & $ 36.9^{+8.4}_{-6.2} $ & $ 31.6^{+6.3}_{-4.8} $ & $ 27.5^{+4.9}_{-3.9} $ \\[1ex]
           NNLO & $ 50.7^{+3.4}_{-4.6} $ & $ 46.5^{+4.2}_{-4.7} $ & $ 42.4^{+4.6}_{-4.4} $ & $ 38.6^{+4.4}_{-4.0} $ \\[1ex]
        N$^3$LO & $ 48.1^{+0.0}_{-7.5} $ & $ 48.1^{+0.1}_{-1.8} $ & $ 46.5^{+1.6}_{-2.6} $ & $ 44.3^{+2.5}_{-2.9} $ \\[1ex]
\midrule
          LO+LL & $ 24.0^{+8.9}_{-6.8} $ & $ 20.1^{+6.2}_{-5.0} $ & $ 16.9^{+4.5}_{-3.7} $ & $ 14.3^{+3.3}_{-2.8} $ \\[1ex]
        NLO+NLL & $ 46.9^{+15.1}_{-12.6} $ & $ 46.2^{+15.0}_{-13.2} $ & $ 46.7^{+20.8}_{-13.8} $ & $ 47.3^{+26.1}_{-15.8} $ \\[1ex]
      NNLO+NNLL & $ 50.2^{+5.5}_{-5.3} $ & $ 50.1^{+3.0}_{-7.1} $ & $ 51.9^{+9.6}_{-8.9} $ & $ 54.9^{+17.6}_{-11.5} $ \\[1ex]
N$^3$LO+N$^3$LL & $ 47.7^{+1.0}_{-6.8} $ & $ 48.5^{+1.5}_{-1.9} $ & $ 50.1^{+5.9}_{-3.5} $ & $ 52.9^{+13.1}_{-5.3} $ \\[1ex]
\bottomrule
  \end{tabular}
  \label{tab:bonviniresults}
  \end{center}
\end{table}

 
Results for the cross section at fixed LO, NLO, NNLO and N$^3$LO accuracy, and its resummed counterpart at
LO+LL, NLO+NLL, NNLO+NNLL and N$^3$LO+N$^3$LL accuracy, are reported in \refT{tab:bonviniresults},
for the four central scale choices. The same results are shown as plots in \refF{fig:13TeV}.
For comparison, results for the ``standard'' threshold resummation (which we call $N$-soft) are also shown in the plots.
For $N$-soft one only keeps non-vanishing contributions in the large $N$ limit:
all the logarithmically enhanced contributions are in the resummed exponent, while the constant terms are not exponentiated.
For completeness, we also provide in \refT{tab:res} the various results at N$^3$LO+N$^3$LL for individual scales and resummation prescription.

\begin{figure}
  \centering
  \includegraphics[width=0.495\textwidth,page=2]{./WG1/ggF/figs/hxswg_hadr_xsec_res_eff_125_13_PDF4LHC15_uncertainty_summary3.pdf}
  \includegraphics[width=0.495\textwidth,page=1]{./WG1/ggF/figs/hxswg_hadr_xsec_res_eff_125_13_PDF4LHC15_uncertainty_summary3.pdf}\\
  \includegraphics[width=0.495\textwidth,page=3]{./WG1/ggF/figs/hxswg_hadr_xsec_res_eff_125_13_PDF4LHC15_uncertainty_summary3.pdf}
  \includegraphics[width=0.495\textwidth,page=4]{./WG1/ggF/figs/hxswg_hadr_xsec_res_eff_125_13_PDF4LHC15_uncertainty_summary3.pdf}
  \caption{Higgs cross section at 13 TeV in the rescaled effective theory (rEFT), for four different choices of the central scale $\mu_F=\mu_R$:
    at the top we show $m_H/2$ and $m_H$, while at the bottom  $m_H/4$ and $2m_H$.
    The uncertainties on the fixed-order predictions and on $N$-soft come solely from scale variation;
    for the $\psi$-soft AP2 results the scale variation is shown as the thick uncertainty band. The 
    thinner bands correspond to the 7-point scale variation
    envelope on the $\psi$-soft AP1 instead, whose central value is not shown.
    The light-red rectangles are the envelope of all $\psi$-soft variants,
    corresponding to the 42-point uncertainty described in the text.}
  \label{fig:13TeV}
\end{figure}

\begin{table}[t]
 \caption{Resummed cross sections at N$^3$LO+N$^3$LL for the different prescriptions as a function of 
  the scales $\mu_F$ and $\mu_R$ over a wide range, for $m_H=125$~GeV and $\sqrt{s}=13$~TeV in the rEFT.}
  \centering
  \begin{tabular}{cc|rrrrrrr}
  \toprule
    & & \multicolumn{6}{c|}{$\psi$-soft} & \\
    & & \multicolumn{2}{c|}{default} & \multicolumn{2}{c|}{all constants in exp} & \multicolumn{2}{c|}{no constants in exp} & \\
    $\mu_F/m_H$ & $\mu_R/m_H$ & AP2 & AP1 & AP2 & AP1 & AP2 & AP1 & \multicolumn{1}{|c}{$N$-soft}\\
    \midrule
$   4$ & $   4$ & $56.8$ & $66.0$ & $56.8$ & $66.0$ & $51.2$ & $58.7$ & $49.4$ \\
$   4$ & $   2$ & $55.1$ & $62.3$ & $54.9$ & $62.0$ & $52.2$ & $58.6$ & $50.5$ \\
$   2$ & $   4$ & $53.2$ & $57.2$ & $53.7$ & $57.9$ & $48.2$ & $51.4$ & $46.0$ \\
$   2$ & $   2$ & $52.9$ & $56.0$ & $52.7$ & $55.8$ & $49.9$ & $52.5$ & $47.9$ \\
$   2$ & $   1$ & $51.2$ & $53.0$ & $50.9$ & $52.6$ & $50.5$ & $52.1$ & $48.9$ \\
$   1$ & $   2$ & $50.2$ & $50.4$ & $50.6$ & $50.9$ & $47.6$ & $47.7$ & $45.6$ \\
$   1$ & $   1$ & $50.1$ & $50.1$ & $49.8$ & $49.8$ & $49.1$ & $49.0$ & $47.5$ \\
$   1$ & $ 1/2$ & $48.5$ & $48.3$ & $48.3$ & $48.0$ & $49.1$ & $48.8$ & $48.3$ \\
$ 1/2$ & $   1$ & $48.4$ & $47.4$ & $48.8$ & $47.7$ & $47.6$ & $46.6$ & $46.3$ \\
$ 1/2$ & $ 1/2$ & $48.5$ & $48.0$ & $48.3$ & $47.8$ & $48.6$ & $48.1$ & $48.0$ \\
$ 1/2$ & $ 1/4$ & $47.0$ & $47.1$ & $47.1$ & $47.2$ & $47.7$ & $47.7$ & $47.9$ \\
$ 1/4$ & $ 1/2$ & $47.8$ & $47.4$ & $48.2$ & $47.7$ & $48.0$ & $47.6$ & $47.6$ \\
$ 1/4$ & $ 1/4$ & $47.7$ & $48.0$ & $47.6$ & $47.9$ & $48.0$ & $48.2$ & $48.2$ \\
$ 1/4$ & $ 1/8$ & $44.7$ & $45.1$ & $45.4$ & $45.7$ & $44.6$ & $45.0$ & $44.9$ \\
$ 1/8$ & $ 1/4$ & $45.5$ & $46.1$ & $46.1$ & $46.6$ & $46.2$ & $46.6$ & $46.5$ \\
$ 1/8$ & $ 1/8$ & $41.0$ & $40.9$ & $41.4$ & $41.2$ & $40.9$ & $40.8$ & $40.9$ \\
\bottomrule
  \end{tabular}
   \label{tab:res}
\end{table}

Several comments are in order:
\begin{itemize}
  
\item The uncertainty on the fixed order reduces to the canonical 7-point variation,
  while at resummed level we have the 42-point variation $\delta(\text{mho})$ detailed above.
  The fixed-order 7-point variation gives an uncertainty similar to $\delta(\text{scale})$ of Ref.~\cite{Anastasiou:2016cez},
  which is based on a 3-point variation of $\mu_0/2<\mu_R=\mu_F<2\mu_0$.
  
  
\item Even ignoring the LO (which contains too few information for being predictive),
  one can consider the convergence pattern of the fixed-order perturbative expansion when going from NLO to NNLO and to N$^3$LO,
  relative to the scale uncertainty.
  %
  The pattern is worse at larger central scales and improves at smaller scales.
  For instance, at the largest central scale considered ($\mu_0=2m_H$), the central result at each order lies outside the band of the previous order;
  conversely, at the smallest central scale considered ($\mu_0=m_H/4$), NNLO is contained in the NLO band, and central N$^3$LO in the NNLO band
  (although the scale error at N$^3$LO is large and very asymmetric).
%
  A similar convergence pattern is observed at $\mu_0=m_H/2$; however, we note that the N$^3$LO band does not overlap with the NLO band.  
  %
  Additionally, the N$^3$LO results at the four central scales shown in Table~\ref{tab:bonviniresults}
  are barely compatible.
  This analysis shows that an estimate of the uncertainty from missing higher orders  that solely relies on $\delta(\text{scale})$,
  i.e.\ a canonical 7-point scale variation, is not reliable at fixed order.
  
\item As far as the resummed results and their uncertainty $\delta(\text{mho})$ are concerned,
 one can note that, for each choice of the central scale $\mu_0$, the uncertainty of the resummed
  results from NLO+NLL onwards covers the central value and at least a portion of the band of the next (logarithmic) order.
  In fact, with the exception of the choice $\mu_0=m_H/4$ (the pathological behaviour of which seems to be driven by the N$^3$LO contribution),
  the NNLO+NNLL band is fully contained in the NLO+NLL band, and the N$^3$LO+N$^3$LL band is fully contained in the NNLO+NNLL band.
  This, together with the observation of the systematic reduction of the scale uncertainty when going from one logarithmic order to the next,
  shows that the proposed $\delta(\text{mho})$ provides one with a very reliable estimate of the uncertainty from missing higher orders.
  % 
  This is further supported by the observation that resummed results at each order are all compatible among the different
  choices of the central scale $\mu_0$.
\item
  Note that the different options for the position of the constants, while giving
  a large spread at NLO+NLL, is of little importance at higher orders, especially at N$^3$LO+N$^3$LL, suggesting a good convergence of the resummed series.
\item
  In many respects, the choice $\mu_0=m_H/2$ seems optimal,
  in full agreement with previous analyses, e.g.~\cite{Anastasiou:2016cez}.
  The convergence of the fixed-order is already good, and the convergence of the resummed result
  is very good. The error band at N$^3$LO+N$^3$LL is smaller than for other central scales,
  but compatible with the results computed at different values of $\mu_0$.
  Moreover, given that the way of estimating the uncertainty is very conservative, and successful at previous orders,
  the uncertainty on the N$^3$LO+N$^3$LL seems reasonably trustful.
\end{itemize}


The result advocated as best result in Ref.~\cite{Bonvini:2016frm} within the rEFT setup considered there is hence
\begin{equation}
\text{rEFT}:\; \sigma_{\text{N$^3$LO+N$^3$LL}} = \sigma_{\text{N$^3$LO}} + \Delta_3\sigma_{\text{N$^3$LL}} = 48.5 \pm1.9\, (4 \%)\, \text{pb},
\end{equation}
where, to be even more conservative, the error has been symmetrized.
Note that the effect of adding the resummation to the N$^3$LO on the central value
is rather small, $+0.4$~pb, corresponding to $+0.8\%$, which, however, is \emph{not} covered by the very asymmetric fixed-order scale uncertainty.
The authors of Ref.~\cite{Bonvini:2016frm} firmly believe that the uncertainty estimate derived from resummation
is much more reliable and trustful than that obtained by simple (asymmetric) scale variation at fixed order.\footnote
{If scale variation error at fixed-order is symmetrized the resulting uncertainty becomes more reasonable.
However, given that the small uncertainty comes from the vicinity of a stationary point in the scale dependence,
it might still underestimate the missing higher order uncertainty.}

In order to go beyond the rEFT approach, we have to discuss the role of quark mass effects on the resummed contribution.
The approach of Ref.~\cite{Schmidt:2015cea} consists of including finite $m_t$ effects to NLL, while treating $m_b$ and $m_c$ at fixed-order. 
Because of the accuracy of the rEFT approach for the top contribution, this leads to a resummed contribution very close to the rEFT one considered so far.
In Ref.~\cite{Bonvini:2014joa} a more aggressive approach was considered and bottom and charm were included to NLL and the top contribution to NNLL,
albeit in the usual $1/m_t$ expansion. However, the interplay between soft logarithms and logarithms of $m_b$ is still to be fully understood
(see for instance Ref.~\cite{Schmidt:2015cea}).
In any case, we believe that the uncertainty $\delta(t,b,c)$ of Ref.~\cite{Anastasiou:2016cez} likely covers the differences between the two approaches. 

In Ref.~\cite{Bonvini:2016frm}, results from resummation have been compared to different methods for estimating the uncertainty from missing higher orders.
First, the Cacciari-Houdeau Bayesian approach~\cite{Cacciari:2011ze} has been considered, which employs
the known perturbative orders to construct a probability distribution for the subsequent unknown order.
%
In its modified incarnation~\cite{Forte:2013mda,Bagnaschi:2014wea},
the method gives an uncertainty of $\pm2$~pb at $95\%$ degree of belief, fully compatible with the estimate obtained from resummation.
%
Second, following an idea by David and Passarino~\cite{David:2013gaa}, several algorithms to accelerate
the convergence of the perturbative series, based on non-linear sequence transformations, have been considered.
%
By performing a survey of different algorithms, it was possible to show that both the fixed-order and resummed series
exhibit good convergence properties at $m_H/2$ (and also at $m_H/4$).
%
Noticeably, the mean of each distribution is very close to the N$^3$LO+N$^3$LL prediction.


In conclusion, these tests  further support the claim that the N$^3$LO+N$^3$LL calculation,
together with its uncertainty $\delta(\text{mho})$, provides the most reliable estimate for the Higgs cross-section in gluon gluon fusion.
In terms of relative contributions, which are likely to remain unchanged when quark-mass and electroweak effects are included in the fixed order,
the results from this section can be summarized as
\begin{equation}
\boxed{\sigma_{\text{N$^3$LO+N$^3$LL}} - \sigma_{\text{N$^3$LO}} = +0.4\,\text{pb},
\qquad
\delta(\text{mho}) = \pm 4\%,}
\end{equation}
which are the recommended shift with respect to the N$^3$LO and recommended uncertainty from missing higher orders
by the Authors of Ref.~\cite{Bonvini:2016frm} for the SM Higgs boson at LHC 13~TeV.

%%% Non-standard bib entries
%@article{ggHiggs,
%      author          = "\href{http://www.ge.infn.it/~bonvini/higgs/}{\texttt{http://www.ge.infn.it/$\sim$bonvini/higgs/}}",
%}
%@article{TROLL,
%      author          = "\href{http://www.ge.infn.it/~bonvini/troll/}{\texttt{http://www.ge.infn.it/$\sim$bonvini/troll/}}",
%}
%%%%%%%
