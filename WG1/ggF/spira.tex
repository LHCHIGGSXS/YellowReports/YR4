The inclusive gluon-fusion cross section for Higgs boson production can
be improved by performing a threshold resummation of soft, virtual and
collinear gluon effects \cite{Kramer:1996iq}. This resummation is
performed in Mellin space according to the conventional formalism used
before for Higgs boson production \cite{Catani:2003zt, Moch:2005ky,
Ravindran:2005vv, Ravindran:2006cg, deFlorian:2009hc, deFlorian:2012yg,
Bonvini:2014joa, Bonvini:2016frm}.  The resummed cross section develops
a factorized kernel structure in Mellin space
\begin{eqnarray}
\tilde G_{gg}^{(res)} \left(N;\alpha_s(\mu_R^2),
\frac{M_H^2}{\mu_R^2}; \frac{M_H^2}{\mu_F^2}; \frac{M_H^2}{m_t^2}
\right)
& = & \alpha_s^2(\mu_R^2) C_{gg} \left( \alpha_s(\mu_R^2),
\frac{M_H^2}{\mu_R^2}; \frac{M_H^2}{\mu_F^2}; \frac{M_H^2}{m_t^2}
\right)
\nonumber \\
& & \times \exp \left\{ \tilde{\cal
G}_H \left( \alpha_s(\mu_R^2), \log N; 
\frac{M_H^2}{\mu_R^2}, \frac{M_H^2}{\mu_F^2} \right) \right\} \, .
\label{eq:gghres}
\end{eqnarray}
We include top mass effects up to the NLL level explicitly in the
coefficient function
\begin{eqnarray}
C_{gg} \left( \alpha_s(\mu_R^2), \frac{M_H^2}{\mu_R^2};
\frac{M_H^2}{\mu_F^2}; \frac{M_H^2}{m_t^2} \right) & = & 1 +
\sum_{n=1}^\infty
\left(\frac{\alpha_s(\mu_R^2)}{\pi}\right)^n C_{gg}^{(n)}
\left( \frac{M_H^2}{\mu_R^2}, \frac{M_H^2}{\mu_F^2}; \frac{M_H^2}{m_t^2}
\right)
\end{eqnarray}
that contains the finite parts of the virtual corrections. The NLO
contribution is given explicitly by ($\tau_Q = 4m_Q^2/M_H^2$)
\cite{deFlorian:2012yg}
\begin{eqnarray}
C_{gg}^{(1)} & = & c_H(\tau_t) + 6\zeta_2 + \frac{33-2N_F}{6}
\log\frac{\mu_R^2}{\mu_F^2} + 6(\gamma_E^2+\zeta_2) -
6\gamma_E \log\frac{M_H^2}{\mu_F^2} \, ,
\end{eqnarray}
where the function $c_H(\tau_t)$ approaches
$11/2$ in the limit of heavy top quarks. The resummed exponential
develops the expansion
\begin{eqnarray}
\tilde{\cal G}_H \left( \alpha_s(\mu_R^2), \log N;
\frac{M_H^2}{\mu_R^2}, \frac{M_H^2}{\mu_F^2} \right) & = & \log
N~g_H^{(1)}(\lambda) \nonumber \\
& + & \left. \sum_{n=2}^\infty \alpha_s^{n-2}(\mu_R^2) g_H^{(n)}\left(
\lambda,
\frac{M_H^2}{\mu_R^2}; \frac{M_H^2}{\mu_F^2} \right)
\right|_{\lambda=b_0 \alpha_s(\mu_R^2) \log N}
\label{eq:resum}
\end{eqnarray}
with $b_0$ denoting the leading order term of the QCD beta function,
\begin{equation}
b_0 = \frac{33-2N_F}{12\pi}
\end{equation} 
where $N_F$ is the number of active flavours that we choose as $N_F=5$
in the following, i.e.~the top quark has been decoupled from the strong
coupling $\alpha_s$ and the PDFs. The individual functions
$g_H^{(i)}~(i=1,\ldots,4)$ can be found e.g.~in \cite{Catani:2003zt,
Vogt:2000ci, Moch:2005ba, Laenen:2005uz}. The leading and subleading
collinear gluon effects have been included by the replacements
\cite{Kramer:1996iq, Catani:2003zt, Catani:2001ic, Schmidt:2015cea}
\begin{eqnarray}
C_{gg}^{(1)} & \to & C_{gg}^{(1)} + 6~\frac{\tilde L}{N} \nonumber \\
C_{gg}^{(2)} & \to & C_{gg}^{(2)} + (48-N_F)~\frac{\tilde L^2}{N}
\label{eq:coll}
\end{eqnarray} 
with the modified logarithm
\begin{eqnarray}
\tilde L = \log \frac{N e^{\gamma_E} \mu_F}{M_H} = \log N + \gamma_E -
\frac{1}{2} \log \frac{M_H^2}{\mu_F^2} \, .
\end{eqnarray}
These replacements reproduce the leading and subleading collinear
logarithms up to N$^3$LO.

The general expression for the inclusive cross section can be cast into
the form
\begin{eqnarray} 
\!\!\!\!\!\!\! && \sigma(s,M_H^2) = \sigma_{tt}^0
\int_{C-i\infty}^{C+i\infty} \frac{dN}{2\pi i}
\left(\frac{M_H^2}{s}\right)^{-N+1} \tilde f_g(N,\mu_F^2) \tilde
f_g(N,\mu_F^2) \nonumber \\
\!\!\!\!\!\!\! && \times \left\{ \tilde G^{(res)}_{gg}
\left(N;\alpha_s(\mu_R^2), \frac{M_H^2}{\mu_R^2};
\frac{M_H^2}{\mu_F^2};0 \right)
- \left[ \tilde G^{(res)}_{gg} \left(N;\alpha_s(\mu_R^2),
  \frac{M_H^2}{\mu_R^2}; \frac{M_H^2}{\mu_F^2};0 \right)
\right]_{(NNLO)} \right\} \nonumber \\
\!\!\!\!\!\!\! && + \sigma_{tt}^0 \int_{C-i\infty}^{C+i\infty}
\frac{dN}{2\pi i} \left(\frac{M_H^2}{s}\right)^{-N+1} \tilde
f_g(N,\mu_F^2) \tilde f_g(N,\mu_F^2) \nonumber \\
\!\!\!\!\!\!\! && \times \left\{ \tilde G^{(res)}_{gg,NLL}
\left(N;\alpha_s(\mu_R^2), \frac{M_H^2}{\mu_R^2}; \frac{M_H^2}{\mu_F^2};
\frac{M_H^2}{m_t^2} \right)
- \tilde G^{(res)}_{gg,NLL} \left(N;\alpha_s(\mu_R^2),
  \frac{M_H^2}{\mu_R^2}; \frac{M_H^2}{\mu_F^2};0 \right) \right.
\nonumber \\
\!\!\!\!\!\!\! && \left. - \left[\tilde G^{(res)}_{gg,NLL}
\left(N;\alpha_s(\mu_R^2), \frac{M_H^2}{\mu_R^2}; \frac{M_H^2}{\mu_F^2};
\frac{M_H^2}{m_t^2} \right)
- \tilde G^{(res)}_{gg,NLL} \left(N;\alpha_s(\mu_R^2),
  \frac{M_H^2}{\mu_R^2}; \frac{M_H^2}{\mu_F^2};0 \right) \right]_{(NLO)}
\right\} \nonumber \\
\!\!\!\!\!\!\! && + \sigma_{t+b+c}^{NNLO}(s,M_H^2)
\label{eq:resmatch}
\end{eqnarray}
with $\sigma_{tt}^0$ denoting top quark contribution to the LO cross
section factor
\begin{eqnarray}
\sigma_0 & = & \frac{G_F}{288\sqrt{2}\pi} \left| \sum_Q
A_Q(\tau_Q)\right|^2 \nonumber \\
A_Q(\tau) & = & \frac{3}{2}\tau[1+(1-\tau) f(\tau)] \nonumber \\
f(\tau) & = & \left\{ \begin{array}{ll}
\displaystyle \arcsin^2 \frac{1}{\sqrt{\tau}} & \tau \ge 1 \\
\displaystyle - \frac{1}{4} \left[ \log \frac{1+\sqrt{1-\tau}}
{1-\sqrt{1-\tau}} - i\pi \right]^2 & \tau < 1
\end{array} \right. 
\label{eq:ggh}
\end{eqnarray}
and $\tilde f_g$ is the Mellin moment of the gluon density. Moreover, in
order to reside to the right of all poles in the complex Mellin plane an
offset $C$ is introduced for the integration contour. The Landau
singularity at large values of $N$ on the other hand is ensured to lie
on the right side of the integration contour \cite{Catani:1996dj,
Catani:1996yz}. The index `$(NNLO)$' in the second line indicates the
fixed-order expansion of the resummed coefficient function $\tilde
G^{(res)}_{gg}$ in Mellin space up to NNLO while the index `($NLO$)'
denotes the perturbative expansion of the NLL resummed coefficient
function $\tilde G^{(res)}_{gg,NLL}$ in Mellin space up to NLO. The
first integral has been convolved with N$^3$LO $\alpha_s$ and NNLO PDFs
according to the discussion about the non-necessity of N$^3$LO PDFs of
Ref.~\cite{Forte:2013mda} and of resummed PDFs of
Ref.~\cite{Bonvini:2015ira} for the SM Higgs boson mass, while the second
integral has been evaluated with NLO $\alpha_s$ and PDFs consistently.
The fixed-order NNLO cross section \cite{Harlander:2002wh,
Anastasiou:2002yz, Ravindran:2003um} of the last term has been derived as
\begin{eqnarray}
\sigma_{t+b+c}^{NNLO}(s,M_H^2) = \sigma_{\infty}^{NNLO}(s,M_H^2) +
\sigma_{t+b+c}^{NLO}(s,M_H^2) - \sigma_{\infty}^{NLO}(s,M_H^2)
\end{eqnarray} 
with the individual parts
\begin{eqnarray}
\sigma_{\infty}^{NNLO}(s,M_H^2) & = & \sigma_{tt}^{LO} K_\infty^{NNLO}
\nonumber \\
\sigma_{\infty}^{NLO}(s,M_H^2) & = & \sigma_{tt}^{LO} K_\infty^{NLO}
\nonumber \\
\sigma_{t+b+c}^{NLO}(s,M_H^2) & = & \sigma_{t+b+c}^{LO} K_{t+b+c}^{NLO}
\end{eqnarray}
where $\sigma_{tt}^{LO}$ denotes the full LO cross section including
only top loops, $\sigma_{t+b+c}^{LO}$ the LO cross section including top
and bottom/charm loops, $K_\infty^{(N)NLO}$ the (N)NLO K-factors
obtained in the limit of heavy top quarks and $K_{t+b+c}^{NLO}$ the full
NLO K-factor including top and bottom/charm loops.  The NNLO parts have
been derived with N$^3$LO $\alpha_s$ and NNLO PDFs and the NLO terms
with NLO $\alpha_s$ and PDFs consistently as implemented in the programs
HIGLU \cite{Spira:1995mt, Spira:1996if} and SusHi
\cite{Harlander:2012pb}. This implementation guarantees that top mass
effects are treated at NLL level and bottom/charm contributions at fixed
NLO respectively.

Since the virtual coefficient of the bottom contributions behaves in the
limit $M_H^2\gg m_b^2$ as \cite{Spira:1995rr} $(C_A = 3, C_F = 4/3)$
\begin{equation}
c_H (\tau_b) \to \frac{C_A-C_F}{12} \log^2 \frac{M_H^2}{m_b^2} - C_F
\log \frac{M_H^2}{m_b^2}
\label{eq:smallmass}
\end{equation}
if the bottom mass is renormalized on-shell, it contains large
logarithms that are not resummed. The resummation of the Abelian part
proportional to $C_F$ has been performed in Ref.~\cite{Kotsky:1997rq,
Akhoury:2001mz, Melnikov:2016emg} up to
the subleading logarithmic level. These logarithms are related to the
Sudakov form factor at the virtual $Hb\bar b$ vertex that generates
these large logarithmic contributions for far off-shell bottom quarks
inside the corresponding loop contributions in the Abelian case. The
resummation of the non-Abelian part proportional to the Casimir factor
$C_A$ has not been considered so far. This type of logarithmic
contributions emerges from a different origin than the soft and
collinear gluon effects discussed so far and is the main source of the
very different size of QCD corrections to the bottom-loop contributions
\cite{Spira:1995rr, Graudenz:1992pv, Harlander:2005rq, Anastasiou:2009kn,
Anastasiou:2006hc, Aglietti:2006tp}. In order to obtain a reliable result
for the bottom contributions a resummation of these types of logarithms
is mandatory so that we do not include these contributions in our soft
and collinear gluon resummation but treat them at fixed NLO. Moreover,
according to the discussion presented about Figure~7a in
Ref.~\cite{Spira:1995rr}
we prefer to introduce quark pole masses also for the bottom and charm
quark, since the finite part of the virtual corrections is then of
moderate size due to an (accidental) cancellation of the logarithms
present in Eq.~(\ref{eq:smallmass}), while this contribution is
significantly larger if using the running $\overline{\rm MS}$ masses at
the scale $M_H/2$ so that the latter choice is disfavoured.

Following the recommendations of the LHC Higgs Cross Section Working
Group \cite{LHCHXSWG-INT-2015-006} our final results for the inclusive cross
section at N$^3$LL including NLO electroweak corrections
\cite{Djouadi:1994ge, Chetyrkin:1996wr, Chetyrkin:1996ke,
Aglietti:2004nj, Aglietti:2006yd, Degrassi:2004mx, Actis:2008ug,
Actis:2008ts} in factorized form are given in Table \ref{tb:cxn} for a
central renormalization and factorization scale choice
$\mu_R=\mu_F=M_H/2$.  Compared to our previous work
\cite{Schmidt:2015cea} the PDF+$\alpha_s$ uncertainties decreased
considerably due to the new PDF4LHC15 sets \cite{Butterworth:2015oua} of
recommended parton densities\footnote{If other PDF sets as ABM12
\cite{Alekhin:2013nda}, HERAPDF2.0 \cite{Abramowicz:2015mha} or JR14
\cite{Jimenez-Delgado:2014twa} are included the PDF+$\alpha_s$
uncertainties will increase considerably with a major part originating
from sizeable differences in the $\alpha_s$ fits at NNLO and different
data sets included in the global fits. Moreover, the proper treatment of
higher-twist effects in the global fits is an open aspect in this
context.}. The scale dependence has been obtained by an independent
variation of the renormalization and factorization scales by factors of
two up and down avoiding a splitting between these two scales by more
than a factor of two. The total uncertainties are evaluated by adding
the scale and PDF+$\alpha_s$ uncertainties linearly (to be
conservative). They range below the 10\%-level. Comparing the resummed
numbers with those of the N$^3$LO expansion of our resummed cross
section one obtains a resummation effect beyond N$^3$LO of less than two
per mille for our central scale choices so that resummation effects are
tiny. For different scale choices they can reach a level of about 2\%.
\begin{table}[hbt]
\caption{\label{tb:cxn} N$^{\,3\!}$LL Higgs boson production cross
sections via gluon fusion for different values of the Higgs boson mass
including the individual and total uncertainties due to the
renormalization and factorization scale dependence and PDF+$\alpha_s$
uncertainties including electroweak corrections using PDF4LHC15
\cite{Butterworth:2015oua} PDFs for a c.m.~energy $\sqrt{s}=13$ TeV. The
renormalization and factorization scales have been chosen as $M_H/2$.}
\renewcommand{\arraystretch}{1.5}
\begin{center}
\begin{tabular}{c|c|c|c|c}
\toprule
$M_H$ [GeV] & $\sigma(pp \to H + X)$ [pb] & scale & PDF+$\alpha_s$
& total \\ 
\midrule
124   & $47.53~pb$ & $^{+4.7\%}_{-5.4\%}$ & $\pm 3.3\%$ &
$^{+8.0\%}_{-8.7\%}$ \\ %\midrule
124.5 & $47.20~pb$ & $^{+4.6\%}_{-5.4\%}$ & $\pm 3.3\%$ &
$^{+7.9\%}_{-8.7\%}$ \\ %\midrule
125   & $46.87~pb$ & $^{+4.6\%}_{-5.4\%}$ & $\pm 3.3\%$ &
$^{+7.9\%}_{-8.7\%}$ \\ %\midrule
125.5 & $46.55~pb$ & $^{+4.6\%}_{-5.4\%}$ & $\pm 3.3\%$ &
$^{+7.8\%}_{-8.7\%}$ \\ %\midrule
126   & $46.22~pb$ & $^{+4.5\%}_{-5.4\%}$ & $\pm 3.3\%$ &
$^{+7.8\%}_{-8.7\%}$ \\ 
\bottomrule
\end{tabular}
\renewcommand{\arraystretch}{1.2}
\end{center}
\end{table}

%----------------------------------------------------------------
Our numbers deviate from the explicit N$^3$LO results given in
Ref.~\cite{Anastasiou:2016cez} due to the different choice of quark mass
scheme for the top, bottom and charm contributions. If these are adopted
as running $\overline{\rm MS}$ quantities at the scale $M_H/2$ the cross
sections increase by 1.4\% compared to those with pole masses as shown
in Table~\ref{tb:cxnrun}. The numbers with $\overline{\rm MS}$ masses
agree with the full N$^3$LO results within about 2 per cent. The
differences are due to the omission of NNLO mass effects and the
virtual+soft+collinear approximation of our N$^3$LL terms in our
results.
\begin{table}[hbt]
\caption{\label{tb:cxnrun} N$^{\,3\!}$LL Higgs boson production
cross sections via gluon fusion for different values of the Higgs boson mass
for two different choices of the scheme for the top, bottom and charm 
quark
masses including electroweak corrections using PDF4LHC15
\cite{Butterworth:2015oua} PDFs for a c.m.~energy $\sqrt{s}=13$ TeV. The
last column shows the central N$^3$LO numbers of
Ref.~\cite{Anastasiou:2016cez}. The renormalization and factorization
scales have been chosen as $M_H/2$.}
\renewcommand{\arraystretch}{1.5}
\begin{center}
\begin{tabular}{c|c|c|c}
\toprule
$M_H$ [GeV] & pole masses & $\overline{m}_{Q}(M_H/2)$ &
Ref.~\cite{Anastasiou:2016cez} \\ 
\midrule
124   & $47.53~pb$ & $48.19~pb$ & $49.27~pb$ \\ 
124.5 & $47.20~pb$ & $47.83~pb$ & $48.92~pb$ \\
125   & $46.87~pb$ & $47.51~pb$ & $48.58~pb$ \\ 
125.5 & $46.55~pb$ & $47.20~pb$ & $48.23~pb$ \\
126   & $46.22~pb$ & $46.86~pb$ & $47.89~pb$ \\ 
\bottomrule
\end{tabular}
\renewcommand{\arraystretch}{1.2}
\end{center}
\end{table}


%--------------------------------------------------------------------------

The whole framework of our resummation approach to the inclusive
gluon-fusion cross sections has also been applied to neutral Higgs boson
production within the MSSM, i.e.~providing resummed predictions at
N$^3$LL for the scalar Higgs bosons and at NNLL for the pseudoscalar
state \cite{Schmidt:2015cea, deFlorian:2007sr}.
