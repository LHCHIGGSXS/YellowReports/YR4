We now summarize the working group recommendation for the total cross-section 
and associated uncertainty for the LHC at~13~TeV.

The recommendation for the central value is to take the pure N$^3$LO
result, evaluated at $\mu_R=\mu_f=m_H/2$. This choice of scale is
motivated by the observation that the perturbative expansion is more
stable both at fixed order and at the resummed level. With this
choice of scale, the effect of the resummation is (at LHC energies)
much smaller than
the uncertainty related to the choice of resummation prescription, and
much smaller than the residual scale uncertainty. Furthermore, the
N$^3$LO EFT result should be rescaled by the $R_{LO}$
Eq.~(\ref{eq:KLO}); charm, bottom and top contributions should be
included exactly up to NLO, and finite top mass effects at NNLO using the
expansion in $1/m_t$ from
Ref.~\cite{Harlander:2009bw,Harlander:2009mq,Harlander:2009my}. Finally,
electroweak corrections~\cite{Actis:2008ug,Actis:2008ts} should be
included multiplicatively using 
complete factorization. The value of the $\overline{\textrm{MS}}$
heavy quark masses and of $\alpha_s$ given in Chapter~\ref{chapter:input} should be used. This leads to the result of Ref.~\cite{Anastasiou:2016cez}, 
given in Eq.~(\ref{eq:finalresult_13TeV}) above. Note that changing
from pole (previous recommendation~\cite{Dittmaier:2011ti}) 
to  $\overline{\textrm{MS}}$ masses leads to an increase of the
cross-section of order 2\%~\cite{Anastasiou:2016cez},

For the treatment of uncertainties, we distinguish parametric
uncertainties and theoretical uncertainties. 


{\bf PDFs and $\alpha_s$} give the parametric uncertainty for this
process. For these, we recommend to follow the PDF4LHC15
  recommendation~\cite{Butterworth:2015oua} summarized in
  Chapters~\ref{chapter:input}-\ref{chap:PDFs}. This leads to an
  absolute 
  uncertainty $\Delta_{{\rm PDF}+\alpha_s}=1.56$~pb, i.e. a relative
  uncertainty of $3.2$~\%. This is in agreement with
  Ref.~\cite{Anastasiou:2016cez}, see also Eq.~(\ref{eq:finalresult_13TeV})
  of Section~\ref{subsec:ADDFGHLM} above.
This is currently the dominant source of uncertainty. It should be
considered Gaussianly distributed, and the above interval provides the
standard deviation of the Gaussian, corresponding to a symmetric 68\% confidence
level. 

The estimation of theoretical uncertainties inevitably involves a
somewhat subjective component. For these we provide two different
procedures, based on two possible different interpretations of
theoretical uncertainties.\begin{itemize}
\item In {\bf Procedure F} the final theoretical uncertainty is
  interpreted as a flat 100\% confidence level. This means that if the
  missing information which provides all the given sources of
  theoretical uncertainty was supplied, then the result is expected to
  lie with certainty within the given interval, but not more likely to
  be in any region within this interval. The interval is constructed
  as the linear sum of individual sources of theoretical uncertainty,
  each estimated as explained below.  Each individual source of
  uncertainty should not be endowed with a statistical interpretation
  and merely concurs to the  determination of  the final overall range. 
\item In {\bf Procedure G} each source of theoretical uncertainty is
  interpreted as a one-sigma range. The final uncertainty is thus
  obtained by combining in quadrature individual sources of
  uncertainty. Because there are many sources of uncertainty, the
  final distribution is expected to be approximately Gaussian, and the
  final combined uncertainty should thus be interpreted as a symmetric
  68\% confidence level.
\end{itemize}

We now provide a list of sources of uncertainty. For each source, we
briefly describe and provide the uncertainty estimate to be used in
either procedure. It is important to stress that individual
uncertainties have a different interpretation in the two procedures:
specifically F-uncertainties are merely components of the final
uncertainty range, while G-uncertainties are one-sigma ranges. Even
when taken to be numerically equal in the two procedures they thus
have a different meaning. 

The various sources of uncertainty and the corresponding
F-uncertainties and G-uncertainties are:

\begin{itemize}
\item{\bf missing higher QCD orders}
In order to estimate the uncertainty related to missing higher order
QCD corrections beyond N$^3$LO various options have been considered:
\begin{itemize}

\item In  Ref.~\cite{Anastasiou:2016cez} it is suggested to  perform a
   scale variation scan of the N$^3$LO result including mass
   effects, in the range  $m_H/4\le \mu_R=\mu_F\le m_H$, (see
   Section~\ref{subsec:ADDFGHLM} above).\footnote{The inclusion of
   mass effects reduces the whole cross section and thus also the
   uncertainty by a factor of about 0.7 in comparison to what one
   would obtain performing a scale variation scan with mass effects
   not included.} 
This procedure gives
   $\Delta_{\rm MHOU}=[-1.2;+0.1]$~pb, i.e. a relative
   uncertainty of $[-2.4;+0.2]$~\%. Note that a three-point scale
   variation, rather than a scan, would give a vanishing upper uncertainty.
% based on numbers send from AL on 2/29/16 11:00 AM
%1            1            43.64        46.77
%1/2            1/2            45.07        47.89
%1/4            1/4            45.03        47.48
%1            1/2            43.35        46.45
%1/2            1/4            44.98        47.81
%1/2            1            45.19        48.03
%1/4            1/2            44.97        47.41
\item A 7-point scale variation can be performed  with $m_H/4\le
   \mu_R, \mu_F\le m_H$ keeping $\frac12 \le \frac{\mu_R}{\mu_F} \le
   2$. This is a standard procedure  
 used to estimate uncertainties
   in fixed-order calculations. It turns out to give a result which is
   similar to the previous one, namely $\Delta_{\rm
       MHOU}=[-1.4;+0.1]$~pb, i.e. a relative uncertainty of
     $[-3.0;+0.2]$~\%. Note that the lower variation is somewhat
   larger than that found in the scan, as the latter was performed
   with fixed ratio $\mu_F/\mu_R=1$.
\item Since scale variation probes only the size of higher-order
    terms, but not their sign, and the scale-variation
    uncertainties quoted above are very asymmetric, one can argue that
    they should be symmetrized while keeping the central
    value fixed.~\footnote{Yet another alternative would be to keep
      the scale-uncertainty band and quote as a central value the
      midpoint.} In the case of the 7-point scale variation this 
    gives $\Delta_{\rm MHOU}=\pm1.4$~pb,
      i.e. a relative uncertainty of $[-3.0;+3.0]$~\%. 
\item While resummation has a minimal impact at central scale, it
  provides a more stable perturbative expansion at all previous
  orders. In Ref.~\cite{Anastasiou:2016cez} a variety of resummation
  schemes were examined and it was found that, within  an equal scale
  variation scan, resummation contributions lie within the fixed order
  scale uncertainty interval in all cases considered. However,  one may argue 
 that seven-point scale variation of the
  resummed result provides a more reliable estimate
  of the perturbative uncertainty. Taking the default N$^3$LL+N$^3$LO
  resummation scheme of Ref.~\cite{Bonvini:2016frm} (see also
  Section~\ref{subsec:bonvini} above) one gets $\Delta_{\rm MHOU}=[-1.6;+1.5]$~pb,
      i.e. a relative uncertainty of $[-3.2;+3.2]$~\%. Note that the
  scale variations reported in \refT{tb:cxn} are larger, but the
  results in that table are at NNLO+N$^3$LL, rather than
  N$^3$LO+N$^3$LL. 
This is very
      close to the symmetrized seven-point scale variation of the
      fixed order result.\footnote{In Ref.~\cite{Bonvini:2016frm} it
      is instead recommended to take the envelope of the seven-point
      scale variations for a variety of different resummation
      prescriptions; this leads to a marginally more conservative
      uncertainty of about $\pm4\%$.}
\end{itemize}
 
The {\bf F-uncertainty} is estimated by performing a
   scale variation scan in the range  $m_H/4\le \mu_R=\mu_F\le m_H$
   following
 the suggestion of
  Ref.~\cite{Anastasiou:2016cez},
   which gives    $\Delta_{\rm MHOU}=[-1.2;+0.1]$~pb, corresponding to a relative
   uncertainty of $[-2.4;+0.2]$~\%.\\
\noindent The {\bf G-uncertainty} is estimated by performing
a  symmetrized
  seven-point scale uncertainty, which leads to a conservative result
  that agrees with the uncertainty based on resummation arguments,
  i.e.
$\Delta_{\rm MHOU}=\pm1.4$~pb, corresponding to
       a relative uncertainty of $[-3.0;+3.0]$\%. 

\item{\bf missing electroweak corrections}
In our recommendation, electroweak (EW) corrections are included assuming
complete factorization. This gives rise to a positive $5\%$ correction
to the pure QCD result. If instead QCD and EW corrections are
combined additively, one gets an enhancement of $1.7\%$. Finally,
mixed QCD-EW corrections computed using an effective field theory
(EFT) with
$m_W,\,m_Z\gg m_H$~\cite{Anastasiou:2008tj} give an enhancement of
$3.2\%$.  

The {\bf F-uncertainty} is estimated as $\Delta_{\rm ew}=0.5$~pb
    corresponding to  $\pm 1\%$ uncertainty, 
which is deemed to be conservative enough and it corresponds to an
intermediate value between various possible estimates, as seen in
Sect.~\ref{sec:breakdown} and 
  Ref.~\cite{Anastasiou:2016cez}.\\
\noindent
The {\bf G-uncertainty} is conservatively
estimated as  the average of
  the difference between our chosen  complete factorization ($5\%$)
  and either of the alternative possibilities (additive:  $1.7\%$, or
  EFT: $3.2\%$). This corresponds to an uncertainty of $\pm 2.5\%$ or
  $\pm1.2$~pb.
\item{\bf bottom and charm interference with top}
Bottom and charm interference with top quark loops are known exactly
only up to NLO. At LO and NLO, bottom-top interference leads to a
correction which is about the same as the finite top mass correction,
but with the opposite sign. At NNLO, including rEFT and $1/m_t$
effects leads to a correction of about 1~pb. Furthermore, the NLO
top-bottom and charm interference correction changes by 0.7~pb if
$\overline{\rm MS}$ or pole heavy quark masses are used. 

The {\bf F-uncertainty} is estimated as $\Delta_{\rm t,bc}=\pm0.4$~pb,
      i.e. a relative uncertainty of $\pm0.8$\%, following
Sect.~\ref{sec:breakdown} and Ref.~\cite{Anastasiou:2016cez}.\\
\noindent
The {\bf G-uncertainty} is estimated taking the scheme dependence of the NLO
  interference as a reasonably conservative estimate. This leads to
$\Delta_{\rm t,bc}=\pm0.7$~pb,
      i.e. a relative uncertainty of $\pm1.5$\%.

\item{\bf finite top mass} 
Both the {\bf F-uncertainty} and 
the {\bf G-uncertainty} are estimated  
as $\Delta_{\rm 1/m_t}=\pm0.49$~pb,
      i.e. a relative uncertainty of $\pm1$\%,   following
      Sect.~\ref{sec:breakdown} and
      Ref.~\cite{Anastasiou:2016cez}. 
\item{\bf missing N$^3$LO PDFs} 
Both the {\bf F-uncertainty} and 
the {\bf G-uncertainty} are estimated  
assigning to lack of knowledge of
  the N$^3$LO PDFs
  an uncertainty of $\Delta_{\rm PDF-TH}=\pm0.56$~pb,
      i.e. a relative uncertainty of $\pm1.2$\%,  following  Sect.~\ref{sec:breakdown} and
      Ref.~\cite{Anastasiou:2016cez}. 
\item{\bf truncation of the soft expansion} Both the {\bf F-uncertainty} and 
the {\bf G-uncertainty} are estimated  assigning to the truncation of the
  soft expansion used to derive the N$^3$LO QCD corrections 
$\Delta_{\rm soft}=\pm0.18$~pb,
      i.e. a relative uncertainty of $\pm0.4$\%, following  Sect.~\ref{sec:breakdown} and
      Ref.~\cite{Anastasiou:2016cez}. 
\end{itemize}

The total uncertainty is thus:
\begin{itemize}
\item {\bf F-uncertainty:} $[-6.7,+4.6]\%$ corresponding to $[-3.3,+2.2]$~pb.
\item {\bf G-uncertainty:}  $\pm4.5$\% corresponding to  $\pm2.2$~pb
\end{itemize}
We recall that the F-uncertainty is a 100\% flat confidence interval,
while the G-uncertainty is a one-sigma Gaussian uncertainty. They can
be compared by noting that a symmetric flat interval has a variance
equal to its half-width divided by $\sqrt3$. The symmetrized
$F$-uncertainty hence corresponds to a variance of $6.7/\sqrt{3}=3.9$,
which is not far from the G-uncertainty. The two estimates are thus
roughly compatible.

The final recommendation for gluon fusion cross-section at the LHC is presented in Chapter~\ref{chap:WG1Summary}.


% This observation suggests the following\\
% \noindent {\bf Recommendation of the Steering Committee and the 
% Convenors of WG1}: use the F-uncertainty
% \begin{equation}\label{eq:thuncf}
% \Delta_{\rm th}=[-6.7,+4.6]\%
% \end{equation}
% which is a
% 100\% flat interval. If it is highly preferred to have Gaussian
% uncertainties, use 
% \begin{equation}\label{eq:thuncg}
% \Delta_{\rm th}=\pm3.9\% .
% \end{equation}
% The corresponding gluon fusion cross-sections expanded to a scan over SM
% Higgs boson masses are presented in \refTs{tab:ggF_XStot_7}--\ref{tab:ggF_XStot_14}.
% Tables~\ref{tab:ggF_XScan_125} and \ref{tab:ggF_XScan_12509} summarize the Standard Model
% gluon fusion cross-sections and the corresponding uncertainties
% for the different proton--proton collision energies
% for a Higgs boson mass $\MH=125\UGeV$ and  $\MH=125.09\UGeV$, respectively.

