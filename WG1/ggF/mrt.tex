\newcommand{\asCMW}{\alpha_s^{\rm CMW}}
\newcommand{\pb}{\;\mathrm{pb}}
\newcommand{\nb}{\;\mathrm{nb}}
\newcommand{\TeV}{\;\mathrm{TeV}}
\newcommand{\mrtpt}{{p_t^{\tiny{\mbox{H}}}}}
\newcommand{\mur}{\mu_{\tiny{\mbox{R}}}}
\newcommand{\muf}{\mu_{\tiny{\mbox{F}}}}
\newcommand{\cF}{{\cal F}}
\newcommand{\dZ}{d{\cal Z}[\{\hat{R}'(\kto), k_i\}]}
\newcommand{\kto}{k_{t,1}}
\newcommand{\kti}{k_{t,i}}
\newcommand{\dki}{\langle dk_{i}\rangle}
\newcommand{\dko}{\langle dk_{1}\rangle}


In the gluon-fusion production mode, the Higgs boson transverse momentum $\mrtpt$
is defined as the inclusive vectorial sum of the transverse momenta of the
recoiling QCD partons radiated off the incoming gluons.
%
The fixed-order perturbative description of its differential
spectrum features large logarithms in the form
$\as^n \ln^m(\mh/\mrtpt)/\mrtpt$, with $m\leq 2n-1$, which spoil the
convergence of the series at small $\mrtpt$.
%
In order to obtain meaningful predictions in that phase-space region,
such terms must be resummed to all orders in $\as$, so that the
perturbative series can be recast in terms of dominant all-order
towers of logarithms. The logarithmic accuracy is commonly defined at
the level of the {\it logarithm} of the cumulative cross section, henceforth
referred to as $\Sigma(\mrtpt)$, where one refers to the dominant terms
$\alpha_s^n \ln^{n+1}(\mh/\mrtpt)$ as leading logarithms (LL), to terms
$\alpha_s^n \ln^{n}(\mh/\mrtpt)$ as next-to-leading logarithms (NLL), to
$\alpha_s^n \ln^{n-1}(\mh/\mrtpt)$ as next-to-next-to-leading logarithms
(NNLL), and so on.
%

The all-order computation of the logarithms of the ratio
$\mh/\mrtpt$ has been performed up to NNLL order in
refs.~\cite{Bozzi:2003jy,Bozzi:2005wk} using the formalism developed
in~\cite{Collins:1984kg,Catani:2000vq}, and in
ref.~\cite{Becher:2010tm} using an effective-field-theory approach.
These resummed results are usually matched to fixed-order predictions
in order to obtain a description of $\mrtpt$ which gives a reliable
coverage of the whole phase space.
%
The recent computations of the differential $\mrtpt$ distribution at next-to-next-to-leading
order (NNLO)~\cite{Boughezal:2015dra,Boughezal:2015aha,
Caola:2015wna,Chen:2016zka}, and of the inclusive gluon-fusion cross section
at next-to-next-to-next-to-leading order (N$^3$LO) in~\cite{Anastasiou:2015ema,
Anastasiou:2016cez}, once combined with state-of-the-art resummation, allow
to obtain a formal NNLL+NNLO accuracy for $d\sigma/d\mrtpt$.
% 

All of the resummation approaches mentioned so far rely on an
impact-parameter-space formulation~\cite{Dokshitzer:1978yd,Parisi:1979se},
which is motivated by the fact that the observable naturally factorizes in this
space as a product of the contributions of each individual emission.
%
Conversely, in $\mrtpt$ space one is unable to find, at a given order
beyond LL, a closed analytic expression for the resummed distribution
which is simultaneously free of logarithmically subleading
corrections and of singularities at finite $\mrtpt$
values~\cite{Frixione:1998dw}.
%
This fact has a simple physical origin: the region of small $\mrtpt$
receives contributions both from configurations in which each of the
transverse momenta of the radiated partons is equally small (Sudakov
limit), and from configurations where $\mrtpt$ tends to zero owing to
cancellations among non-zero transverse momenta of the emissions. The
latter mechanism is in fact the dominant one at small $\mrtpt$ and, as a
result, the cumulative cross section in that region vanishes as
${\cal O}(\mrtpt^2)$ rather than being exponentially
suppressed~\cite{Parisi:1979se}.
%
If these effects are neglected in a resummation performed in
transverse-momentum space, the latter would feature a geometric
singularity at some finite value of $\mrtpt$. The same issue is present
in an impact-parameter-space formulation whenever one tries to obtain a
result in $\mrtpt$ space free of any contamination from subleading
logarithmic terms.
%

However, it has recently been shown~\cite{Monni:2016ktx} that the
problem can be solved also in transverse-momentum space, upon
extending the formalism developed in~\cite{Banfi:2004yd,Banfi:2014sua}
to treat observables that feature such kinematic cancellations.

The method of~\cite{Monni:2016ktx} organizes the computation of the
cumulative cross section $\Sigma(\mrtpt)$ as an ensemble of emissions off
the incoming gluons; the amplitude for each emission, characterized by
a certain transverse momentum $\kti$, is then expanded around the
largest transverse momentum in the ensemble (denoted as $\kto$), and
one just retains the terms in this expansion that contribute to a
given logarithmic accuracy, discarding subleading contributions.
% 
The latter expansion is always justified since, by construction, all
emissions are softer than $\kto$ and, owing to the recursive infrared
and collinear (rIRC) safety of the observable, emissions with $\kti\muchless \kto$
that would invalidate the expansion do not contribute to the
logarithmic structure of the resummed cross
section~\cite{Banfi:2004yd}. The hardest emission is integrated over
all of its natural phase space, including the region $\kto\gg\mrtpt$,
which is regularized by means of the Sudakov exponential.

This formulation ensures that all kinematic effects that contribute to
the $\mrtpt\to0$ limit are properly taken into account, and not only is
$\Sigma(\mrtpt)$ free of singularities at finite $\mrtpt$, but it also
features the correct $\mrtpt^2$ scaling \cite{Parisi:1979se} at low
$\mrtpt$. This procedure can be effectively interpreted as a resummation
of the large logarithms $\ln(\mh/\kto)$, and the logarithmic order is
defined in terms of the latter; the corresponding formal accuracy in
terms of the logarithms $\ln(\mh/\mrtpt)$ is the same as in terms of
$\ln(\mh/\kto)$, and the difference amounts to
logarithmically-subleading corrections.
%

In~\cite{Monni:2016ktx} the cumulative cross section is efficiently
computed with a fast Monte Carlo method where, for each event, the
kinematics of the ensemble of emissions is stochastically generated
with weights that take into account all of the physical effects that
occur at a given logarithmic accuracy.  In particular, at NLL
accuracy, all emissions after the hardest can be generated with equal
weights obtained in the soft-collinear approximation; at NNLL,
one emission weight per event is modified to take into account the
corrections which arise when a single parton in the ensemble is either
emitted collinearly to the beam with a significant fraction of the emitter's
momentum (hard-collinear), or close in rapidity to another parton, and
therefore it is sensitive to its correct rapidity bounds.
The Sudakov-exponential weight associated with the hardest
emission is correspondingly evaluated including terms up to
$\mathcal O(\as^n\ln^{n}(\mh/\kto))$ at NLL, and up to
$\mathcal O(\as^n\ln^{n-1}(\mh/\kto))$ at NNLL, and the overall parton
luminosity incorporates coefficient functions at the relevant order in
the strong coupling -- i.e.~${\cal O}(\as^3)$ and ${\cal O}(\as^4)$
for a NLL and NNLL matching, respectively -- and is evaluated at a
factorization scale of the order of $\kto$.
%

The expansion of the cumulative rate, necessary for the matching with
fixed-order predictions, is performed in a semi-analytic way, where the
contributions to the different orders in $\as$ are reduced
analytically to linear combinations of $\mrtpt$-dependent
master-integral grids that are evaluated numerically with high
accuracy once and for all, and interpolated dynamically at runtime,
ensuring optimal speed performances.
%

The computation is entirely carried out in momentum space, without the
need to transform the observable and the parton luminosities into
impact-parameter space, with benefits in terms of speed.
Moreover, in transverse-momentum space a clear physical picture emerges of the
effects that determine the low-$\mrtpt$ region, where all contributions to a given logarithmic
accuracy can be systematically tracked down and accounted for.
%

The approach used to perform the resummation is fully general and it
can be straightforwardly extended to the entire class of
rIRC-safe~\cite{Banfi:2004yd} observables that feature kinematic
cancellations in the infrared.
%


In the following we report predictions for the Higgs boson transverse
momentum distribution in the heavy-top effective theory at
the $13$\,TeV LHC, with $\mh=125$\,GeV, and {\tt PDF4LHC15}
\cite{Butterworth:2015oua} parton densities at NNLO. The central
prediction uses $\mur=\muf=\mh$, and $Q=\mh/2$, where $Q$ represents
the resummation scale, introduced as usual to estimate the impact on
physical results of the neglected higher-order logarithmic
contributions. The perturbative uncertainty for all predictions is
estimated by varying both $\mur$ and $\muf$ by a factor of two in
either direction while keeping $1/2\leq \mur/\muf \leq 2$.  Moreover,
for central $\mur$ and $\muf$ scales, the resummation scale $Q$ is
varied by a factor of two in either direction.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
 \begin{minipage}[b]{8.0cm}
   \centering
   \includegraphics[width=8.0cm]{./WG1/ggF/figs/jetvheto-v-hqt-v-MC}
   \caption{\footnotesize{Comparison of the Higgs $\mrtpt$ NNLL+NLO
      prediction as obtained in~\cite{Monni:2016ktx} (red) to \texttt{HqT}
      (green). For reference, the predictions obtained with MiNLO at
      NLO (orange), and FxFx (blue) are shown. Lower panel: ratio of
      the various distributions, normalized to their respective
      central-scale inclusive cross sections, to the central NNLL+NLO
      prediction~\cite{Monni:2016ktx}. Uncertainty bands are shown only for the
      resummed results.}}
\label{fig:results1}
 \end{minipage}
 \ \hspace{2mm} \hspace{0mm} \
 \begin{minipage}[b]{8.0cm}
  \centering
   \includegraphics[width=8.0cm]{./WG1/ggF/figs/2matched+fixed-order}
   \caption{\footnotesize{Higgs $\mrtpt$ at NNLL+NNLO (red), NNLL+NLO
    (green), and NNLO (blue). Lower panel: ratio of the
    three predictions to the NNLL+NNLO one.\vspace{22mm}}}
  \label{fig:results2}
 \end{minipage}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%

The matching to the fixed-order prediction is obtained with an
additive scheme, according to the formula
\begin{equation}
\label{eq:ptH_NNLL+NNLO_matching}
\Sigma^{\tiny{\mbox{NNLL+(N)NLO}}}(\mrtpt)=\Sigma^{\tiny{\mbox{(N)NLO}}}(\mrtpt)+
\Sigma^{\tiny{\mbox{NNLL}}}(\mrtpt)-\Sigma^{\tiny{\mbox{EXP}}}(\mrtpt),
\end{equation}
where $\Sigma^{\tiny{\mbox{EXP}}}(\mrtpt)$ represents the expansion of
the resummed formula to $\mathcal O(\as^4)$ for a matching to the NLO-accurate
differential distribution, or to $\mathcal O(\as^5)$ for a matching to the NNLO-accurate
differential distribution. The introduction of modified logarithms of the form
$\ln(Q/\kto)\to \ln\big[(Q/\kto)^p+1\big]/p$ ensures that the matched
cumulative cross section on the left-hand side
of~\eqref{eq:ptH_NNLL+NNLO_matching} reduces to the fixed-order one at
large transverse momentum.
%

In the main panel of \refF{fig:results1} a comparison is shown of the
prediction of ref.~\cite{Monni:2016ktx} at NNLL+NLO to that obtained with
  \texttt{HqT}~\cite{Bozzi:2005wk,deFlorian:2011xf}. As expected, very good
  agreement over the entire $\mrtpt$ range is observed between these two results,
  which have the same perturbative accuracy. The \texttt{HqT} prediction is
  moderately lower in the peak of the distribution, and higher at intermediate
  $\mrtpt$ values, although this pattern may slightly change with different central-scale
  choices. These small differences have to do with the different treatment of
  subleading effects in the two resummation methods.
 The agreement of the two results, both for the central scale and for the
  uncertainty band, is even more evident in the lower inset of \refF{fig:results1},
  which displays the ratio of the various distributions, each normalized to its central-scale 
  inclusive rate, to the normalized central NNLL+NLO curve of~\cite{Monni:2016ktx}.
%

  \refF{fig:results1} also reports the $\mrtpt$ distribution obtained
  with the NLO version of \texttt{POWHEG}+MiNLO
  \cite{Alioli:2010xd,Hamilton:2012rf}, and with the
  \texttt{MG5\_aMC@NLO}+FxFx
  \cite{Alwall:2014hca,Frederix:2012ps} event generators, using
  default renormalization and factorization scales for the two methods
  (in FxFx a merging scale $\mu_Q=\mh/2$ has been employed). Both
  generators are interfaced to \texttt{Pythia\,8.2}
  \cite{Sjostrand:2014zea}, without including hadronization,
  underlying event, and primordial $k_{\perp}$ (whose impact has been
  checked to be fully negligible for this observable), and use {\tt
    PDF4LHC15} parton densities at NLO. By inspecting the normalized
  ratios shown in the lower panel, one observes that the shape of the
  Monte-Carlo predictions deviates significantly from the NNLL+NLO
  results at $\mrtpt \lesssim 60$\,GeV. In order to avoid possible
  misunderstandings between the \texttt{POWHEG}+MiNLO result shown in
  \refF{fig:results1} and the so called ``NNLOPS'' approach, we
  recall that the \texttt{POWHEG}+MiNLO generator was further improved
  to achieve NNLO accuracy for fully inclusive
  observables in ref.~\cite{Hamilton:2013fea}. Although the logarithmic
  accuracy of the two ``\texttt{POWHEG}-based'' results is the same,
  the $\mrtpt$ spectrum obtained with the NNLOPS approach was found to
  be, numerically, in very good agreement with the \texttt{HqT}
  NNLL+NLO result, certainly more than what the \texttt{POWHEG}+MiNLO
  result does. More details can be found in~\cite{Hamilton:2013fea}.
  %
  
  \refF{fig:results2} shows the comparison of the matched
  NNLL+NNLO result to the NNLL+NLO and the fixed-order NNLO
  predictions.  The inclusion of the NNLO corrections leads to a
  $10-15\%$ increase in the matched spectrum for $\mrtpt > 15$\,GeV, and
  to a consistent reduction in the perturbative uncertainty.
%
  The impact of the NNLL resummation on the fixed order becomes
  increasingly important for $\mrtpt \lesssim 40$\,GeV, leading to a
  suppression of the differential spectrum in this phase-space region
  which reaches about $25\%$ at $\mrtpt = 15$\,GeV.  For
  $\mrtpt \gtrsim 40$\,GeV, the matched prediction reduces to the NNLO
  one. The final theory uncertainty is at the $\pm10\%$-level in the
  phenomenologically relevant $\mrtpt$ range.

