\subsubsection{Ingredients of the computation}
This section summarizes our best prediction for the value of the inclusive gluon-fusion cross section and its uncertainties, following Ref.~\cite{Anastasiou:2016cez}. The main ingredient is the recent computation of gluon-fusion cross section through N$^3$LO in the effective theory where the top-quark is integrated out~\cite{Anastasiou:2014lda,Anastasiou:2014vaa,Anastasiou:2015ema}.

The master formula that summarizes all the ingredients entering our prediction for the partonic cross-sections is
\begin{equation}\label{eq:ADDmasteroriginal}
\hat{\sigma}_{ij} \simeq R_{LO}\,\left(\hat{\sigma}_{ij,EFT} + \delta_{t}\hat{\sigma}_{ij,EFT}^{NNLO} + \delta\hat{\sigma}_{ij,EW}\right) + \delta\hat{\sigma}_{ij,ex;t,b,c}^{LO}+\delta\hat{\sigma}_{ij,ex;t,b,c}^{NLO}\,.
\end{equation}
QCD corrections to the production cross-section $\hat{\sigma}_{ij,EFT}$ in the heavy-top limit have been included at NLO~\cite{Dawson:1990zj,Graudenz:1992pv,Djouadi:1991tka}, NNLO~\cite{Anastasiou:2002yz,Harlander:2002wh,Ravindran:2003um} and N$^3$LO~\cite{Anastasiou:2014lda,Anastasiou:2014vaa,Anastasiou:2015ema}. In addition, we also include effects from finite quark masses and electroweak effects, to the extent that they are available.  It was already observed at LO and NLO that the validity of the effective theory can be greatly enhanced by rescaling the effective theory by the exact LO result. We therefore rescale the cross-section $\hat{\sigma}_{ij,EFT}$ in the effective theory by the ratio
\begin{equation}
R_{LO} \equiv \frac{\sigma_{ex;t}^{LO}}{\sigma_{EFT}^{LO}}\,,
\label{eq:KLO}
\end{equation}
where $\sigma_{ex;t}^{LO}$ denotes the exact (hadronic) LO cross-section in the SM with a massive top quark and $N_f=5$ massless quarks. Moreover, at LO and NLO we know the exact result for the production cross-section in the SM, including all mass effects from top, bottom and charm quarks. We include these corrections into our prediction via the terms $\delta\hat{\sigma}_{ij,ex;t,b,c}^{(N)LO}$ in eq.~\eqref{eq:ADDmasteroriginal}, consistently matched to the contributions from the effective theory to avoid double counting. As a consequence, eq.~\eqref{eq:ADDmasteroriginal} agrees with the exact SM cross-section (with massless $u$, $d$ and $s$ quarks) through NLO in QCD. Beyond NLO, we only know the value of the cross-section in the heavy-top effective theory. We can, however, include subleading corrections at NNLO in the effective theory as an expansion in the inverse top mass~\cite{Harlander:2009mq, Pak:2009dg, Harlander:2009bw, Harlander:2009my}. These effects are taken into account through the term $\delta_{t}\hat{\sigma}_{ij,EFT}^{NNLO}$ in eq.~\eqref{eq:ADDmasteroriginal}, with the factor $R_{LO}$ scaled out.  They were originally computed with the top mass at the OS scheme, but their scheme dependence is expected to be at the sub-per mille level, following lower orders, and is hence considered negligible here.
We also include electroweak corrections to the gluon-fusion cross-section (normalized to the exact LO cross-section) through the term $\delta\hat{\sigma}_{ij,EW}$ in eq.~\eqref{eq:ADDmasteroriginal}. Unlike QCD corrections, electroweak corrections have only been computed through NLO in the electromagnetic coupling constant $\alpha$~\cite{Aglietti:2004nj,Actis:2008ug,Actis:2008ts}. Moreover, mixed QCD-electroweak corrections, i.e., corrections proportional to $\alpha\,\alpha_s^3$, are known in an effective 
theory~\cite{Anastasiou:2008tj} valid in the limit where not only the top quark but also the electroweak bosons are much heavier than the Higgs boson. In this limit the interaction of the Higgs boson with the $W$ and $Z$ bosons is described via a point-like vertex coupling the gluons to the Higgs boson. Higher-order corrections in this limit can thus be included into the Wilson coefficient in front of the dimension-five operator describing the effective interaction of the gluons with the Higgs boson.
The validity and limitations of this approximation are discussed in Section~\ref{sec:breakdown}.


\subsubsection{Summary of results}
The numerical results quoted in this section are valid for the following set of input parameters:
\begin{table}[!h]
\begin{center}
\begin{tabular}{cc}
    \toprule
    
    $\sqrt{S}$	&  13~{\textrm{TeV}}                      \\
$m_h$  		&  125~{\textrm{GeV}}                      \\
PDF 			&  {\tt PDF4LHC15\_nnlo\_100}                      \\
$\alpha_s(m_Z)$ 	&  0.118                    \\
$m_t(m_t)$	&  162.7~{\textrm{GeV}} ($\overline{\textrm{MS}}$)\\
$m_b(m_b)$	&  4.18 ~{\textrm{GeV}} ($\overline{\textrm{MS}}$)\\
$m_c(3GeV)$	&  0.986~{\textrm{GeV}}  ($\overline{\textrm{MS}}$)\\
$\mu=\mu_R=\mu_F$	&  62.5~{\textrm{GeV}}  ($=m_H/2$)\\
    \bottomrule
\end{tabular}
\end{center}
\end{table}

Using these input parameters, our current best prediction for the production cross section 
 of a Higgs boson with a mass $m_H = 125$ GeV at the LHC with a centre-of-mass energy of 13 TeV is
\begin{equation}
\label{eq:finalresult_13TeV}
\boxed{
\sigma = 48.58\,{\rm pb} {}^{+2.22\, {\rm pb}\, (+4.56\%)}_{-3.27\, {\rm pb}\, (-6.72\%)} \mbox{ (theory)} 
\pm 1.56 \,{\rm pb}\, (3.20\%)  \mbox{ (PDF+$\alpha_s$)} \,.
}
\end{equation}

The central value in eq.~\eqref{eq:finalresult_13TeV}, computed at the central scale $\mu_F=\mu_R=m_H/2$, is the combination of all the effects
considered in eq.~\eqref{eq:ADDmasteroriginal}. The
breakdown of the different effects is:
\begin{equation}\label{eq:central_value_breakdown}
\begin{array}{rrrl}
48.58\, \textrm{pb} =&  16.00 \, {\rm pb}  & \quad (+32.9\%) & \qquad (\textrm{LO, rEFT})   \\ 
                              & \,+\,  20.84 \,{\rm pb} &  \quad
                                (+42.9\%) & \qquad (\textrm{NLO, rEFT} )   \\
                              &\,-\, \phantom{a}2.05\, {\rm pb}  & \quad (-4.2\%)
                                                              &\qquad  ((t, b, c)\textrm{, exact NLO})  \\
                              & \, + \,\phantom{a} 9.56 \, {\rm pb} & \quad (+19.7\%) & \qquad (\textrm{NNLO, rEFT}) \\
                             &\,+\,\phantom{a} 0.34\, {\rm pb} &  \quad (+0.7\%) &  \qquad (\textrm{NNLO, }1/m_t)  \\
                             &\,+ \,\phantom{a} 2.40\, {\rm pb}  & \quad (+4.9\%) & \qquad
                                                         (\textrm{EW, QCD-EW}) \\
                              &\,+\,\phantom{a}  1.49\, {\rm pb}  & \quad (+3.1\%)
                                                              & \qquad (\textrm{N$^3$LO, rEFT})   
\end{array}
\end{equation}
where rEFT denotes the cross section in the effective field theory approximation rescaled by $R_{LO}$ of \eqref{eq:KLO} .
We note that  the N$^3$LO central value is completely insensitive to threshold resummation effects for $\mu_F=\mu_R=m_H/2$ and the central value obtained from a fixed-order N$^3$LO computation and a resummed computation at N$^3$LO + N$^3$LL are identical for this scale choice. We therefore conclude that threshold resummation does not provide any improvement of the central value, and it is therefore not included in our prediction.

The PDF and $\alpha_s$ uncertainties are computed following the recommendation of the PDF4LHC working group.
The remaining theory-uncertainty in eq.~\eqref{eq:finalresult_13TeV} is obtained by adding linearly various sources of theoretical uncertainty, which affect the different contributions to the cross section in eq.~\eqref{eq:ADDmasteroriginal}. The breakdown of the different theoretical uncertainties whose linear sum produces the theoretical uncertainty in eq.~\eqref{eq:finalresult_13TeV} is
\begin{center}
\begin{tabular}{cccccc}
\toprule
\begin{tabular}{c} $\delta$(scale) \end{tabular} &
\begin{tabular}{c} $\delta$(trunc) \end{tabular} &
\begin{tabular}{c} $\delta$(PDF-TH) \end{tabular} &
\begin{tabular}{c} $\delta$(EW) \end{tabular} & 
\begin{tabular}{c} $\delta(t,b,c)$  \end{tabular} & 
\begin{tabular}{c} $\delta(1/m_t)$ \end{tabular}\\ \midrule
${}^{+0.10\textrm{ pb}}_{-1.15\textrm{ pb}} $ & $\pm$0.18 pb & $\pm$0.56 pb & $\pm$0.49 pb& $\pm$0.40 pb& $\pm$0.49 pb
\\ \midrule
${}^{+0.21\%}_{-2.37\%}$ & $\pm 0.37\%$ & $\pm 1.16\%$ & $\pm 1\%$ & $\pm 0.83\%$ 
& $\pm 1\%$ \\
 \bottomrule
\end{tabular}
\end{center}
In the remainder of this note we address each of the components that enter the final theoretical uncertainty estimate in turn.

\subsubsection{Breakdown of the theoretical uncertainties}
\label{sec:breakdown}

\indent {\bf Uncertainty from missing higher orders: $\delta$(scale)}\\
The uncertainty $\delta$(scale) captures the impact of missing higher order terms in the perturbative expansion of the cross section in the rEFT. We identify this uncertainty with the scale variation when varying the renormalization and factorization scales simultaneously in the interval $\mu_F=\mu_R\in[m_H/4,m_H]$. The N$^3$LO corrections moderately increase ($\sim 3\%$) 
the cross-section for renormalization and factorization scales equal
to  $m_H/2$. In addition, they notably stabilize the scale variation,
reducing it almost by a factor of five compared to NNLO. The N$^3$LO scale-variation band 
is included entirely within the NNLO scale-variation band for scales
in the interval $[{m_H}/{4}, m_H ]$. We note that, while we vary the scales simultaneously, we have checked (see Figure 6 of \cite{Anastasiou:2016cez}) that the factorization scale dependence is flat, and the scale dependence at N$^3$LO is driven by the renormalization scale dependence. 

It is important to assess how well the scale uncertainty captures the uncertainty due to missing higher orders in the perturbative expansion, given that it failed to capture the shift in the central value due to missing perturbative orders at lower orders.  We have found 
good evidence that the N$^3$LO scale variation captures the effects of missing higher 
perturbative orders in the EFT. We base this conclusion on the following observations:
First, we observe that expanding in
$\alpha_s$ separately the Wilson coefficient and matrix-element
factors in the cross-section gives results consistent with expanding
directly their product through N$^3$LO. Second, 
a traditional threshold resummation in Mellin space up to N$^3$LL did not contribute 
significantly to the cross-section beyond N$^3$LO in the range of scales 
$\mu \in [{m_H}/{4}, m_H ]$. 
Although  the effects of threshold resummation are in general sensitive to
ambiguities due to subleading terms beyond the soft limit, we found
that within our preferred range of scales, several variants of the exponentiation formula
gave very similar phenomenological results, which are always consistent with
fixed-order perturbation theory.  Finally, a soft-gluon and
$\pi^2$-resummation using the SCET formalism also gave consistent
results with fixed-order perturbation theory at N$^3$LO. While 
ambiguities in subleading soft terms limit the use of soft-gluon
resummation as an estimator of higher-order effects, and while it is of
course possible that some variant of resummation may  
yield larger corrections, it is encouraging that this does not happen
for the mainstream prescriptions studied here. 

 We conclude this discussion by commenting on the use of resummation to estimate the uncertainty on the cross section. Based on the considerations from the previous paragraph, we are led to conclude that threshold resummation does not provide any improvement over a fixed-order calculation, and we therefore do not include it into our prediction. We base this conclusion on the following facts. First, we have already observed in the previous section that the central value at N$^3$LO for $\mu\equiv\mu_R=\mu_F=m_H/2$ is insensitive to resummation effects, excluding any need for resummation to improve the fixed order prediction. Second, the scale variation for N$^3$LO + N$^3$LL is contained inside the fixed-order N$^3$LO scale-variation band for $\mu\in[m_H/4,m_H]$ for a variety of different formalisms to perform threshold resummation, indicating that threshold resummation is more likely to underestimate the uncertainty from the variation. Finally, we point out that the resummation program itself is plagued by systematic uncertainties coming from  terms that are power suppressed in the threshold variable $(s-m_H^2)$ (or equivalently, in $1/N$ in Mellin space) and  are not controlled by the resummation. Although formally of higher order, these uncontrolled terms can have a substantial impact on the cross section, which is in our opinion not physically motivated. Any conclusion based on varying these uncontrolled power-suppressed and constant terms should therefore be discarded in our opinion, and they are not considered in our prediction.
  
{\bf The truncation uncertainty: $\delta$(trunc)}\\
The truncation uncertainty captures the uncertainty introduced by the fact the N$^3$LO corrections are currently only  known as an expansion around threshold,  i.e., as an expansion in the amount of energy available to QCD radiation, to order 37. We assign an uncertainty due to the truncation of the threshold expansion which is as large 
as 
\begin{equation}\label{eq:trunc_error}
\delta({\rm trunc}) = 10 \times \frac{\sigma^{(3)}_{EFT}(37)  -  \sigma^{(3)}_{EFT}(27)    }{\sigma^{\textrm{N$^3$LO}}_{EFT} } = 0.37\%\,.
\end{equation}
The factor $10$ is a conservative estimator of the progression of the
series beyond the first $37$
%, respectively $37$, 
terms. Note that the complete N$^3$LO cross-section appears in the denominator of eq.~\eqref{eq:trunc_error}, i.e., the uncertainty applies to the complete  N$^3$LO result, not just the coefficient of $\alpha_s^5$.

  
{\bf The uncertainty from missing N$^3$LO PDFs: $\delta$(PDF-TH)}\\
So far, PDFs have only been extracted  by comparing data to theory predictions at NNLO in QCD, and so
an inconsistency may only arise due to the extraction
of the parton densities from data for which there are no N$^3$LO
predictions.
To assess this uncertainty we resort to the experience from the
previous orders and investigate the shift in the NNLO cross section
when it is computed with either NLO or NNLO PDFs.
We observe that as a function of the factorization scale in the range $\mu_F\in[m_H/4,m_H]$
(with the renormalization scale held fixed) 
scale)  
the NNLO cross-section decreases by about $2.2-2.4\%$  when NNLO
PDFs are used instead of NLO PDFs. Given that N$^3$LO
corrections are expected to be milder in general than their
counterparts at NNLO, we anticipate that they will induce a smaller
shift than at NNLO. Based on these considerations, we assign a
conservative
uncertainty estimate due to missing higher orders in the extraction of the
parton densities obtained as
\begin{equation}\label{eq:delta_PDF-TH}
\delta({\rm PDF-TH}) = \frac{1}{2}\,\left|\frac{\sigma_{EFT}^{(2),NNLO}-\sigma_{EFT}^{(2),NLO}}{\sigma_{EFT}^{(2),NNLO}}\right|=\frac{1}{2}\, 2.31\% = 1.16\%\,,
\end{equation}
where $\sigma_{EFT}^{(2),(N)NLO}$ denotes the NNLO cross-section evaluated with (N)NLO PDFs 
at the central scale $\mu_F=\mu_R=m_H/2$.
In the above, the strong coupling was set to its world-average at the $Z$ pole and evolved using three-loop renormalization group running, and we assumed conservatively that the size of the N$^3$LO corrections is
about half of the corresponding NNLO corrections.  
 This estimate is supported by the magnitude of the third-order corrections 
to the coefficient functions for deep inelastic scattering~\cite{Vermaseren:2005qc} and a 
related gluonic scattering process~\cite{Soar:2009yh}, which are the 
only two coefficient functions 
that were computed previously to this level of accuracy. 


{\bf The uncertainty due to missing QCD-EW corrections: $\delta$(EW)}\\
Given the large size of the NLO QCD corrections to the Higgs cross-section, we may expect that also the EW corrections to the NLO QCD cross-section cannot be neglected. Unfortunately, these so-called mixed QCD-EW corrections are at present unknown, and only the contribution from an EFT approximation where the weak bosons are heavier than the Higgs boson are taken into account.
The effective theory method for the mixed QCD-EW corrections 
is of course not entirely satisfactory, because the computation of the EW Wilson coefficient assumes the validity of the $m_H/m_V$ expansion, $V=W,Z$ while clearly $m_H>m_V$. 
We thus need to carefully assess the uncertainty on the mixed QCD-EW corrections due to the EFT approximation. 
In the region $m_H > m_V$, we expect  effects from virtual weak bosons going on shell to be important and one should not expect that a naive application of the  EFT can give a reliable value for the cross-section. However, the EFT is only used to predict 
the relative size of QCD radiative corrections with respect to the leading 
order electroweak corrections, i.e., the dominant electroweak threshold effects from pairs of weak bosons going on shell should already be captured by the leading-order electroweak corrections. This can only vary mildly above and below threshold. For phenomenological purposes, we expect that the rescaling with the exact NLO EW corrections captures the bulk of threshold effects at 
all perturbative orders. To quantify the remaining uncertainty in this approach, we allow the EW Wilson coefficient $C_{1w}$ to vary by a factor of $3$ around its central value. We do this by introducing a rescaling factor $y_{\lambda}$ by
\begin{equation}\label{eq:y_lamda}
\lambda_{EW}\, (1+C_{1w}\,a_s +\ldots) \to \lambda_{EW} \,(1+y_\lambda\, C_{1w}\,a_s +\ldots)\,,
\end{equation}
where $a_s = \alpha_s/\pi$.
Varying $y_{\lambda}$ in the range $[1/3,3]$, we see that the cross-section varies by $-0.2\%$ to $+0.4\% $. Note that the result obtained by assuming complete factorization of EW and QCD corrections lies in the middle of the variation range, slightly higher than the $y_\lambda=1$ prediction.  Finally, we stress that the choice of the range is largely arbitrary of course. It is worth noting, however, that in order to reach uncertainties of the order of $1\%$, one needs to enlarge the range to $y_\lambda\in[-3,6]$.
% which is to our eyes overly conservative. 

An alternative way to assess the uncertainty on the mixed QCD-EW corrections is to note that the factorization of the EW corrections is exact in the soft and collinear limits of the NLO phase space. The hard contribution, however, might be badly captured. At NLO in QCD, the hard contribution amounts to $\sim 40\%$ of the $\mathcal{O}(a_s^3)$ contribution to the cross-section, where we define the \emph{hard contribution} as the NLO cross-section minus its soft-virtual contribution, i.e., the NLO contribution that does not arise from the universal exponentiation of soft gluon radiation. The \emph{hard contribution} is defined as the convolution of the parton-level quantity
\begin{equation}
\frac{\hat{\sigma}_{ij}^{(1),\textrm{hard}}}{z}\equiv\frac{\pi |C_0|^2}{8V}\,a_s^3\,\eta^{(1),\textrm{reg}}_{ij}(z)
\end{equation}
with the PDFs, which
receive contributions from the $gg$, $qg$ and $q\bar{q}$ initial state channels.
The mixed QCD-EW corrections  are $3.2\%$ of the total cross-section. Even if the 
 uncertainty of the  factorization ansatz is taken to be as large as the entire hard contribution, we will obtain
an estimate of the uncertainty equal to $0.4\times 3.2\%=1.3\%$ with respect to the total cross-section. 
  
An alternative way to define the \emph{hard contribution} is to look at the real emission cross-section regulated by a subtraction term in the FKS scheme~\cite{Frixione:1995ms}. We could then exclude the contribution of the integrated subtraction term, which is proportional to the Born matrix element, and hence of soft-collinear nature. We would then estimate the \emph{hard contribution} as $\sim 10\%$ of the $\mathcal{O}(a_s^3)$ contribution to the cross-section, which would lead to an uncertainty equal to $0.1\times 3.2\%=0.32\%$.
  
 We note that the different estimates of the uncertainty range from $0.2\%$ to $1.3\%$.
We therefore assign, conservatively, an uncertainty of $1\%$ due to mixed QCD-EW corrections for 
LHC energies. 



{\bf The missing $b$ and $c$-quark mass effects: $\delta(t,b,c)$}\\
Unlike the case of the top quark, the contributions of the bottom and charm quarks at NNLO are entirely unknown. We estimate the uncertainty of the missing interference between the top and light quarks within the $\overline{ \rm MS }$-scheme as:
\begin{equation}
\label{eq:tbc_uncertainty}
\delta(t,b,c)^{\overline{\rm MS}} = \pm\, \left| \,\frac{\delta\sigma_{ex;t}^{NLO}-\delta\sigma_{ex;t+b+c}^{NLO}}{\delta\sigma_{ex;t}^{NLO}}\,\right|\,
(R_{LO}\delta\sigma_{EFT}^{NNLO}+\delta_{t}\hat{\sigma}_{gg+qg,EFT}^{NNLO}) \simeq \pm 0.31\,\textrm{pb}\,,
\end{equation}
where 
\begin{equation}
\delta\sigma_{X}^{NLO} \equiv \sigma_{X}^{NLO}-\sigma_{X}^{LO}
{\rm~~and~~}
\delta\sigma_{X}^{NNLO} \equiv \sigma_{X}^{NNLO}-\sigma_{X}^{NLO}\,.
\end{equation}

Our preferred scheme is the $\overline{\rm MS}$-scheme {\bf (with $\mu_R=m_H/2$})
  due to the bad convergence of the perturbative series
  for the conversion from an $\overline{\rm MS}$ mass to a pole
  mass for the bottom and charm
  quarks~\cite{Marquard:2016vmy,Marquard:2015qpa}. 
To account for the difference with the OS scheme, we enlarge the uncertainty on 
$\sigma_{t+b+c} - \sigma_t$, as estimated via 
eq.~\eqref{eq:tbc_uncertainty}  within the  $\overline{\rm MS}$
scheme, by multiplying it with a factor of 1.3, %an appropriate  factor
%. At 13 TeV this factor is  1.3, i.e., we have
\begin{equation}
\delta(t,b,c) = 1.3  \, \delta(t,b,c)^{\overline{\rm MS}}\,.
\end{equation}



{\bf Uncertainty from top-mass effects at NNLO: $\delta(1/m_t)$}\\
The corrections due to top-mass effects at NNLO, as an expansion in $1/m_t$, are included through the term $\delta_{t}\hat{\sigma}_{ij,EFT}^{NNLO}$ in eq.~\eqref{eq:ADDmasteroriginal}.
The $1/m_t$ expansion is in fact an expansion in ${s}/m_t^2$, and consequently it needs to be matched to the high-energy limit of the cross-section, known to leading logarithmic accuracy from $k_t$-factorization. The high-energy limit corresponds to the contribution from small values of $z$ to the convolution integral with the parton luminosities. Since this region is suppressed by the luminosity, a lack of knowledge of the precise matching term is not disastrous and induces an uncertainty of roughly $1\%$, which is of the order of magnitude of the net contribution. In conclusion, following the analysis of ref.~\cite{Harlander:2009my}, whose conclusions were confirmed by ref.~\cite{Pak:2009dg}, we assign an overall uncertainty of $1\%$ due to the unknown top-quark effects at NNLO.  


