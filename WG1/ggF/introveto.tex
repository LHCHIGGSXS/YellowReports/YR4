\providecommand{\ptjv}{p_{\rm t,veto}}

In some decay channels
it is common to perform different analyses depending on the number of jets accompanying
the Higgs boson.
This is because the Higgs boson signal in different jet multiplicities is affected
by different backgrounds.
Most notably, when the Higgs boson decays to $WW$, the dominant top background is significantly suppressed by requiring zero jets in the final state. 
Jet veto transverse momentum thresholds used by ATLAS and CMS are of order $25-30$ GeV, and thus substantially smaller than the Higgs boson mass $m_H$. In this case real radiation is suppressed and the imbalance between virtual and real corrections produces logarithms of the form $\ln(\ptjv/m_H)$ which may invalidate the fixed order perturbative expansion. Resummed predictions for the cross section in the 0-jet bin
have been obtained both in full QCD \cite{Banfi:2012jm} and in the framework of
Soft Collinear Effective field Theory (SCET) \cite{Becher:2012qa,Stewart:2013faa}.
Several methods for determining the uncertainties and their correlations across jet-bins were proposed in the past, and used in Run~1 measurements. At the end of Run~1 further improvements were proposed. 
In this section we review different approaches to the treatment of correlated uncertainties in jet bins.

In Section~\ref{subsec:binuncertainties} a general approach to theory uncertainties in kinematic bins is presented. It reduces to the commonly used ST \cite{Stewart:2011cf} or JVE \cite{Banfi:2012jm} methods in particular cases
and is applicable also to treat simplified template cross sections.
In Section~\ref{subsec:run1_method} the ST and JVE methods are compared up to NNLO and results for 13 TeV are presented.
In Section~\ref{subsec:stwz_blptw} an updated calculation of the cross section in the 0-jet, 1-jet and $\geq 2$-jet bins is presented. This calculation, based on the work of Ref.~\cite{Stewart:2013faa}, includes the direct resummation of the logarithmically enhanced terms in the 0-jet bin, and the indirect resummation of the corresponding terms in the 1-jet bin. Heavy quark mass effects are accounted for through an overall rescaling factor.

Recently, the N$^3$LO result for the inclusive cross section (see Section~\ref{subsec:ADDFGHLM}), and the H+jet cross section at NNLO (see Section~\ref{sec:antenna}),  became available.
Both these calculation refer to perturbative corrections at ${\cal O}(\as^5)$ and can be used to improve
the computation of the 0-jet cross section.
In Section~\ref{sec:jve} an updated calculation of the jet vetoed cross section is presented, which consistently includes the above information. The calculation includes the resummation of the $\ln(\ptjv/m_H)$ terms at NNLL and a resummation of the logarithmically enhanced contribution of the jet radius $R$. Heavy-quark mass effects are included according to the approach of Ref.\cite{Banfi:2013eda}.

When the recoiling QCD radiation is integrated over, the NNLO calculation of H+jet provides an NNLO prediction for the $p_T$ spectrum. In Section~\ref{subsec:MRT} an NNLO calculation of the $p_T$ spectrum is combined with an NNLL resummed computation in momentum space \cite{Monni:2016ktx} and NNLL+NNLO predictions for the cumulative distribution are presented, together with a comparison of NNLL+NLO results with available reference predictions for this observable.
