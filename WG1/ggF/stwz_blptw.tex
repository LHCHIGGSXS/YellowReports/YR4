%\providecommand{\GeV}{\,\mathrm{GeV}}
\providecommand{\pTjet}{p_T^{\rm jet}}
\providecommand{\pTjetone}{p_T^{\rm jet1}}
\providecommand{\pTjettwo}{p_T^{\rm jet2}}
\providecommand{\pToff}{p_T^{\rm off}}
\providecommand{\resum}{\mathrm{resum}}
\providecommand{\nons}{\mathrm{nons}}
\providecommand{\Rnons}{{R\mathrm{sub}}}
\providecommand{\muns}{\mu_\mathrm{ns}}
Experimental analyses require a consistent treatment of cross sections and their uncertainties for several jet bins.
In the following we discuss predictions for the $0$-jet,
$1$-jet, and $\ge 2$-jet bins with a resummation of jet-veto logarithms,
and provide updated results for $13\,{\rm TeV}$.
We utilize a theoretical approach that provides flexible control over uncertainties
allowing for the identification of different sources of yield and migration uncertainties.
It is thus well-suited to provide a theoretical description of jet binning, including multiple
jet-bin boundaries.


\subsubsection{Jet \texorpdfstring{$p_T$}{pT} resummation at NNLL\texorpdfstring{$'+$}{prime+}NNLO\SectionAuthor{I.~W.~Stewart, F.~J. Tackmann}}


We discuss the resummed predictions for the $H+0$-jet cross section from gluon fusion with a $p_T$ veto on jets and with the resummation of jet-veto logarithms at NNLL$'+$NNLO order~\cite{Stewart:2013faa}.
We place a particular emphasis on a careful estimate of the perturbative uncertainties and include a detailed discussion of how yield and migration uncertainties are determined. The different contributions to the uncertainty are estimated by appropriate variations of the different scales in virtuality and rapidity space appearing in a factorization theorem.
This allows us to distinguish between and account for the uncertainties due to higher fixed-order corrections as well as higher-order towers of jet-$p_T$ logarithms.

We utilize the framework of soft-collinear effective theory (SCET)~\cite{Bauer:2000ew, Bauer:2000yr, Bauer:2001ct, Bauer:2001yt}
for jet-veto resummation at hadron colliders~\cite{Stewart:2009yx,Berger:2010xi,Becher:2012qa,Tackmann:2012bt,Stewart:2013faa}.
The factorized $pp\to H+0$-jet cross section with a cut on $\pTjet < \pTcut$ is given by
%%%
\begin{align} \label{eq:fact}
\sigma_0(\pTcut)
&= \frac{\sqrt{2} G_F\, m_H^2}{576 \pi \Ecm^2}\, H_{gg} (m_H^2, \mu_H) \int\!d Y
B_g\Bigl(m_H, \pTcut, R, \frac{m_H}{\Ecm}\,e^{Y}, \mu_B, \nu_B \Bigr)
\nn \\ & \qquad
B_g\Bigl(m_H, \pTcut, R, \frac{m_H}{\Ecm}\,e^{-Y}, \mu_B, \nu_B \Bigr)\, S_{gg}(\pTcut, R, \mu_S, \nu_S)
\,U_0(\pTcut, R; \mu_H, \mu_B, \mu_S, \nu_B, \nu_S)
\nn \\ & \qquad
+ \sigma_0^\Rnons(\pTcut, R) + \sigma_0^\nons(\pTcut, R, \muns)
\,.\end{align}
%%%
The first term is the leading contribution containing all the singular logarithmic terms $\alpha_s^i\ln^j(\pTcut/m_H)$. The resummation of the logarithms is performed by renormalization group evolution (RGE) in both virtuality ($\mu$) and rapidity ($\nu$) space, illustrated on the left of \refF{fig:RGscales}. The factorized hard ($H_{gg}$), beam ($B_g$), and soft ($S_g$) functions are evaluated at their own natural virtuality scales $\mu_i$ and rapidity scales $\nu_i$, where they contain no large logarithms and are calculable at fixed order in $\alpha_s$. From there they are evolved to a common (arbitrary) scale, yielding the combined evolution factor $U_0$ which resums the logarithms of the virtuality ratios $\mu_i/\mu_j$ and rapidity ratios $\nu_i/\nu_j$.
The resummation is performed to NNLL$'$ order, which in addition to the NNLL resummation includes the full $\ord{\as^2}$ corrections to $H_{gg}$, $B_g$, and $S_{gg}$ (which includes the $\ord{\as^2}$ effects from jet clustering). These are formally part of the N$^3$LL resummation for which they provide the correct RGE boundary conditions, and incorporate all dominant (singular) NNLO corrections into the resummed result.



\begin{figure}[t!]
\includegraphics[width=0.3\textwidth]{./WG1/ggF/figs/running_pTjet}%
\includegraphics[width=0.35\textwidth]{./WG1/ggF/figs/profile_muvary_lxl}%
\includegraphics[width=0.35\textwidth]{./WG1/ggF/figs/profile_resumvary_lxl}%
\caption{Left panel: Illustration of the RGE in virtuality and rapidity space to resum jet-$p_T$ logarithms.
Middle panel: Profile scale variations contributing to the overall fixed-order (yield) uncertainty.
Right panel: Profile scale variations whose combinations are used to assess the resummation (migration) uncertainty.}
\label{fig:RGscales}
\end{figure}


The second term in Eq.~\eqref{eq:fact}, $\sigma_0^\Rnons(\pTcut, R)$, contains $\ord{R^2}$ contributions. For $R = 0.4$ they are numerically very small and are treated as subleading power corrections. The last term in Eq.~\eqref{eq:fact}, $\sigma_0^\nons(\pTcut, R, \muns)$, contains $\ord{\pTcut/m_H}$ ``nonsingular'' corrections, which vanish for $\pTcut\to0$ but become important at large $\pTcut$.
These terms are added to achieve the full NNLL$'$+NNLO accuracy, which incorporates the complete NNLO cross section for all values of $\pTcut$, including the inclusive NNLO cross section.

The RGE scales $\mu_H, \mu_B, \mu_S, \nu_B$, and $\nu_S$ are chosen as functions of $\pTcut$, which are referred to as profile scales~\cite{Ligeti:2008ac, Abbate:2010xh}. They have to satisfy several constraints and their construction is discussed in detail in Ref.~\cite{Stewart:2013faa}. Essentially, in the resummation region at small $\pTcut$ they have to parametrically follow the canonical scaling dictated by the RGE, while at large $\pTcut \gtrsim m_H/2$ they must approach a common fixed-order scale $\mu_{\rm FO}$ in order to turn off the resummation and avoid unphysical behaviour. The remaining freedom in the choice of the profile scales provides a flexible and powerful way to assess the perturbative uncertainties in the resummed predictions. For this reason, profile scales have been applied by now in a large variety of different contexts and have become an established and reliable method for assessing perturbative uncertainty in resummed predictions.
% (see e.g.~refs.~\cite{Ligeti:2008ac, Abbate:2010xh, Berger:2010xi, Jain:2012uq, Jouttenus:2013hs, Stewart:2013faa, Liu:2013hba, Kang:2013nha, Kang:2013lga, Larkoski:2014uqa, Pietrulewicz:2014qza, Neill:2015roa, Alioli:2015toa, Bonvini:2015pxa,Hornig:2016ahz})
As detailed in Ref.~\cite{Stewart:2013faa}, in the context of jet binning the profile scale variations allow us to identify the different uncertainty sources contributing to the yield and migration uncertainties discussed in Section~\ref{subsec:binuncertainties}.

Before discussing the variations, we stress that the scales are unphysical parameters, and their variations simply provide a convenient way to probe the ``typical'' size of the associated missing higher-order terms. The observed variations in the results must be interpreted as such. In particular, we do not assign any meaning to accidentally small one-sided scale variations that yield asymmetric
uncertainties, which are just the result of nonlinear scale dependence, which is frequently encountered at higher orders and including resummation. Hence, we always consider the maximum absolute deviation from the chosen central scale as the (symmetric) uncertainty. To be explicit, an observed variation of $+|x|$ and $-|y|$ in the cross section is interpreted as uncertainty of $\pm \max\{|x|, |y|\}$.

The first type of variation is a collective variation of all scales by a factor of $1/2$ and $2$. This keeps all scale ratios and thus all logarithms fixed, and at large $\pTcut$ reproduces the uncertainty in the fixed-order cross section. Hence, this corresponds to an overall fixed-order uncertainty (within the resummed prediction), and is naturally identified as a common source for all $\sigma_i$, giving rise to a yield uncertainty $\Delta_{\mu i}$. A second type of variation included in $\Delta_{\mu i}$ is to the profile shape controlling the transition points where the resummation is turned off.
The total of 14 profile variations $V_\mu$ contributing to $\Delta_{\mu 0}$ are displayed in the middle panel of \refF{fig:RGscales}.
For each profile $v_i$ in $V_\mu$ we obtain $\Delta_{\mu i}$ as the maximum absolute deviation from the central value,
%%%
\begin{align} \label{eq:Deltamui}
\Delta_{\mu 0} (\pTcut) = \max_{v_i \in V_\mu} \bigl\lvert \sigma_0^{v_i} (\pTcut) - \sigma_0^{\rm central} (\pTcut) \bigr\rvert
\,,\qquad
\Delta_{\mu \geq 0} = \max_{v_i \in V_\mu} \bigl\lvert \sigma_{\geq 0}^{v_i} - \sigma_{\geq 0}^{\rm central} \bigr\rvert
\,.\end{align}
%%%
For $\Delta_{\mu \geq 0}$, only the variation of $\mu_{\rm FO}$ by a factor $2$ contributes, and $\Delta_{\mu \geq 1}(\pTcut) = \Delta_{\mu \geq 0} - \Delta_{\mu 0} (\pTcut)$.

The profile scale variations $V_\resum$ contributing to the resummation uncertainty, $\Delta^{0/1}_\resum$, are shown in the right panel of \refF{fig:RGscales}. They separately vary each of the beam and soft scales up and down but keep $\mu_{\rm FO}$ fixed. They thus directly probe the intrinsic uncertainty in the resummed logarithmic series. The variation is chosen to approach the conventional factor of 2 for $\pTcut\to 0$. Out of the $80$ possible combinations of all variations, all combinations leading to arguments of logarithms which are more than a factor of 2 different from their central values are not considered. This leaves a total of 35 profile scale variations in $V_\resum$ that are used to estimate
%%%
\begin{equation} \label{eq:Delta01resum}
\Delta^{0/1}_\resum(\pTcut) = \max_{v_i \in V_\resum} \bigl\lvert \sigma_0^{v_i} (\pTcut) - \sigma_0^{\rm central} (\pTcut) \bigr\rvert
\,.\end{equation}
%%%
The $\pTcut$ logarithms are the primary source of uncertainty caused by the jet binning at small $\pTcut$ and we can therefore identify $\Delta_\resum$ as the corresponding migration uncertainty. Furthermore, $\Delta^{0/1}_\resum$ smoothly turns off at large $\pTcut$ where the logarithms become unimportant and the resummation is turned off. This is consistent with the fact that in this limit migration effects become irrelevant since $\sigma_{\geq 1}(\pTcut)$ becomes numerically much smaller than $\sigma_0(\pTcut)$.

Our predictions use a complex hard scale $\mu_H = -i \mu_{\rm FO}$ with $\mu_{\rm FO} = m_H$.
This is the canonical scale at which the hard function
contains no large logarithms and shows a significantly improved perturbative stability
compared to $\mu_H = \mu_{\rm FO}$ (for any value of $\mu_{\rm FO}$).
In the transition from small to large $\pTcut$ we keep the hard scale at its complex value.
In principle, one could contemplate rotating it to the real axis as a function of $\pTcut$ to turn off the resulting
resummation of logarithms of $\ln(\mu_H/\abs{\mu_H})$. However, this would inevitably lead to
an unphysical behaviour of a decreasing cross section with increasing $\pTcut$. Instead, the improved convergence observed in the small $p_T$ region, where the factorization of the hard virtual corrections into the overall factor
$H_{gg}$ is manifest, also translates into the fixed-order cross section at large $\pTcut$, because the majority of the total cross section comes from the small $p_T$ region. Consequently, as one can see from Table~\ref{tab:incl}, the inclusive cross section for $\mu_H = -i m_H$ shows a much better perturbative convergence and as a result is already at NNLO in close agreement with the N$^3$LO result at the default scale $\mu_H = m_H/2$.
The uncertainty related to this resummation is estimated by varying the phase of $\mu_H = \mu_{\rm FO} \exp(-i \varphi)$ as $\varphi = \pi/2\pm\pi/4$. This phase variation is roughly equivalent to the usual factor of 2 since $\pi/4 \simeq \ln 2$. We have
%%%
\begin{equation}
\Delta_{\varphi i} = \max_{\varphi \in \{\pi/4, \pi/2, 3\pi/4\}} \bigl\lvert \sigma_i^{\varphi} - \sigma_i^{\rm central} \bigr\rvert
\,.\end{equation}
%%%
We consider this as an additional independent uncertainty source, and since it affects all cross sections via an overall multiplicative factor, it is treated as a yield uncertainty.

In summary, we have the following three uncertainty amplitudes for $\{\sigma_{\geq 0}, \sigma_0, \sigma_{\geq 1}\}$,
%%%
\begin{align} \label{eq:01nuisance}
\kappa_\mu^{\rm y}: &\quad \{ \Delta_{\mu \geq 0},\, \Delta_{\mu 0},\, \Delta_{\mu \geq 1} \}
\,,\qquad\qquad
\kappa_\varphi^{\rm y}: \quad \{ \Delta_{\varphi \geq 0},\, \Delta_{\varphi0},\, \Delta_{\varphi\geq 1} \}
\,,\nn\\
\kappa^{0/1}_\cut: &\quad \Delta^{0/1}_\resum \times \{ 0,\, 1, -1\}
\,.\end{align}
%%%

\begin{figure}[t!]
\includegraphics[width=0.5\textwidth]{./WG1/ggF/figs/STWZ_ggHsig0pTcut_13_125_R40_conv_imH_PDF4LHC_lxl}%
\includegraphics[width=0.5\textwidth]{./WG1/ggF/figs/STWZ_ggHsig0pTcut_13_125_R40_FOcomp_imH_PDF4LHC_lxl}
\caption{The $0$-jet cross section $\sigma_0(\pTcut)$, comparing different resummation orders (left) and resummed and fixed-order predictions (right). The N$^3$LO result on the right utilizes the results of Refs.~\cite{Anastasiou:2015ema, Caola:2015wna}.}
\label{fig:STWZ0jet}
\end{figure}

\begin{figure}[t!]
\includegraphics[width=0.5\textwidth]{./WG1/ggF/figs/STWZ_ggHsig1inclpTcut_13_125_R40_conv_imH_lxl}%
\includegraphics[width=0.5\textwidth]{./WG1/ggF/figs/STWZ_ggHsig1inclpTcut_13_125_R40_FOcomp_imH_lxl}
\caption{The $\geq 1$-jet cross section $\sigma_{\geq1}(\pTcut)$, comparing different resummation orders (left) and resummed and fixed-order predictions (right). The NNLO$_1$ result on the right utilizes the results of Ref.~\cite{Caola:2015wna}.}
\label{fig:STWZ1jet}
\end{figure}


\begin{table}%[t!]
\caption{The inclusive cross section $\sigma_{\geq0}$, comparing different orders and the conventional scale choices (columns 2 and 3) and with complex hard scale (columns 4 and 5). The uncertainties are the perturbative uncertainties. The last column corresponds
to the default scale choice for $\mu_H$ used for the resummed predictions discussed here.}
\label{tab:incl}
\renewcommand{\arraystretch}{1.1}
\begin{tabular}{l||l|l||l|l}
\toprule
$\sigma_{\geq 0}/\mathrm{pb}$ & $\mu_H = m_H/2$ & $\mu_H = m_H$ & $\mu_H = -i m_H/2$ & $\mu_H = -i m_H$
\\
\midrule
LO & $16.04 \!\pm\! 15.8\%$ & $13.80 \!\pm\! 16.3\%$ & $26.70 \!\pm\! 12.8\%_\mu \!\pm\! 22.9\%_\varphi$ & $23.29 \!\pm\!  14.6\%_\mu \!\pm\! 14.7\%_\varphi $
\\
NLO & $36.94 \!\pm\! 19.7\%$ & $31.61 \!\pm\! 16.9\%$ & $47.40 \!\pm\! 14.1\%_\mu \!\pm\! 10.5\%_\varphi$ & $42.18 \!\pm\! 12.4\%_\mu \!\pm\! 6.8\%_\varphi $
\\
NNLO & $46.55 \!\pm\! 9.1\%$ & $42.42 \!\pm\! 9.7\%$ & $48.76 \!\pm\! 2.8\%_\mu \!\pm\! 1.5\%_\varphi$ & $47.41 \!\pm\! 4.6\%_\mu \!\pm\! 2.0\%_\varphi$
\\
N$^3$LO & $48.03 \!\pm\! 3.2\%$ & $46.51 \!\pm\! 4.9\%$ & $47.85 \!\pm\! 0.6\%_\mu \!\pm\! 0.8 \%_\varphi$ & $47.96 \!\pm\!  1.5\%_\mu \!\pm\! 0.5\%_\varphi $
\\
\bottomrule
\end{tabular}
\end{table}

The numerical results we present are obtained in the top EFT limit rescaled with the exact LO $m_t$ dependence. Bottom quark and EW effects are not yet included. Their numerical effects go in opposite directions and largely cancel each other, so they should be included together.
Jets are defined with a jet radius of $R = 0.4$ and without any cut on the jet rapidity. We use the PDF4LHC15 PDFs with $\alpha_s(m_Z) = 0.118$. In Table~\ref{tab:incl} we compare the corresponding results for the inclusive cross section at different hard scales. We also give the corresponding N$^3$LO results, obtained utilizing the results of Ref.~\cite{Anastasiou:2015ema}.

In \refF{fig:STWZ0jet} we show our results for the resummed and matched $0$-jet cross section, comparing the different consistently matched resummation orders on the left. On the right, we compare our best prediction at NNLL$'+$NNLO with the pure fixed-order results at the corresponding scale with ST uncertainties at NNLO and N$^3$LO (obtained from the results of Refs.~\cite{Anastasiou:2015ema, Caola:2015wna}). The NNLL$'+$NNLO agrees well with the fixed N$^3$LO with ST uncertainties. (The same is also observed for NLL$'+$NLO compared to NNLO.) Since the typical values of $\pTcut \simeq 30\UGeV$ lie in the transition region, it is not unexpected that the resummed result is roughly predicting the next-higher fixed order. This also confirms that the ST method produces reasonably sized uncertainties here. Our resummed predictions still have the advantage of allowing one to separately estimate all types of yield and migration uncertainties, in contrast to the fixed-order predictions where some explicit assumptions on the correlations between cross sections are needed.
The corresponding results for the $\geq 1$-jet cross section are shown in \refF{fig:STWZ1jet}. Here the lowest NLL result is not very meaningful as it does not even contain the correct LO$_1$ result. In the right panel we see that the highest order resummed result agrees well with the corresponding fixed NNLO$_1$~\cite{Boughezal:2015dra, Boughezal:2015aha, Caola:2015wna} result albeit with larger uncertainties toward larger $\pTcut$.

\begin{table}%[t!]
\caption{Predictions for the $0$-jet cross section at $\pTcut = 25\UGeV$ (top) and $\pTcut = 30\UGeV$ (bottom) corresponding to \refF{fig:STWZ0jet} with a breakdown of the perturbative uncertainties.}
\label{tab:STWZ0jet}
\centering
%\renewcommand{\arraystretch}{1.1}
\begin{tabular}{l|l|ccc|c}
\toprule
& $\sigma_0(25\UGeV)/\mathrm{pb}$ & $\Delta_{\mu0}$ & $\Delta_{\varphi0}$ & $\Delta^{0/1}_\resum$ & total pert. unc.
\\ \midrule
NLL & $17.04 \!\pm\! 7.21$ & $18.7\%$ & $12.8\%$ & $35.7\%$ & $42.3\%$
\\
NLL$'+$NLO$_0$ & $22.29 \!\pm\! 3.43$ & $7.7\%$ & $5.1\%$ & $12.3\%$ & $15.4\%$
\\
NNLL$'+$NNLO$_0$ & $26.25 \!\pm\! 1.97$ & $4.7\%$ & $0.6\%$ & $5.8\%$ & $7.5\%$
\\ \bottomrule
% \end{tabular}
% \\[0.5ex]
% \begin{tabular}{l||l|ccc|c}
% \hline\hline
& $\sigma_0(30\UGeV)/\mathrm{pb}$ & $\Delta_{\mu0}$ & $\Delta_{\varphi0}$ & $\Delta^{0/1}_\resum$ & total pert. unc.
\\ \midrule
NLL & $19.10 \!\pm\! 7.52$ & $17.4\%$ & $12.8\%$ & $32.9\%$ & $39.4\%$
\\
NLL$'+$NLO$_0$ & $25.59 \!\pm\! 3.72$ & $7.9\%$ & $5.3\%$ & $11.0\%$ & $14.5\%$
\\
NNLL$'+$NNLO$_0$ & $29.51 \!\pm\! 1.65$ & $3.8\%$ & $0.1\%$ & $4.1\%$ & $5.6\%$
\\ \bottomrule
\end{tabular}
\end{table}

\begin{table}%[t!]
\caption{Predictions for the $\geq 1$-jet cross section at $\pTcut = 25\UGeV$ (top) and $\pTcut = 30\UGeV$ (bottom) corresponding to \refF{fig:STWZ1jet} with a breakdown of the perturbative uncertainties.}
\label{tab:STWZ1jet}
\centering
%\renewcommand{\arraystretch}{1.2}
\begin{tabular}{l|l|ccc|c}
\toprule
& $\sigma_{\geq 1}(25\UGeV)/\mathrm{pb}$ & $\Delta_{\mu\geq 1}$ & $\Delta_{\varphi\geq1}$ & $\Delta^{0/1}_\resum$ & total pert. unc.
\\ \midrule
NLL$'+$NLO$_0$ & $20.69 \!\pm\! 4.88$ & $17.4\%$ & $8.7\%$ & $13.3\%$ & $23.6\%$
\\
NNLL$'+$NNLO$_0$ & $21.16 \!\pm\! 1.96$ & $4.5\%$ & $3.8\%$ & $7.1\%$ & $9.3\%$
\\ \bottomrule
% \end{tabular}
% \\[0.5ex]
% \begin{tabular}{l||l|ccc|c}
% \hline\hline
& $\sigma_{\geq 1}(30\UGeV)/\mathrm{pb}$ & $\Delta_{\mu\geq 1}$ & $\Delta_{\varphi\geq1}$ & $\Delta^{0/1}_\resum$ & total pert. unc.
\\ \midrule
NLL$'+$NLO$_0$ & $17.39 \!\pm\! 4.63$ & $19.1\%$ & $9.1\%$ & $16.2\%$ & $26.6\%$
\\
NNLL$'+$NNLO$_0$ & $17.90 \!\pm\! 1.88$ & $6.0\%$ & $5.2\%$ & $6.8\%$ & $10.5\%$
\\ \bottomrule
\end{tabular}
\end{table}

At this point we should note that matching to the N$^3$LO$_0$ and NNLO$_1$ results without also going to the corresponding N$^3$LL$^{(\prime)}$ resummation order amounts to including unresummed singular logarithms in the nonsingular matching corrections. Doing so would reduce the overall scale dependence but not necessarily improve the accuracy of the final prediction, because in the resummation region there is no guarantee that this would move the result in the right direction. (Thrust in $e^+e^-$ is a known example where it would not.) It would also come at the cost of reentangling the uncertainties sources, since on the one hand it should not reduce the resummation uncertainties while on the other hand it would impact the dominant $\pTcut$ dependence. Given these potential subtleties and given that our results with a complex hard scale already agree very well with the N$^3$LO$_0$ and NNLO$_1$ results, we do not see sufficient reason to add these terms until the corresponding resummation order is available as well.

The numerical results for $\sigma_0(\pTcut)$ and $\sigma_{\geq 1}(\pTcut)$ for $\pTcut = 25\UGeV$ and $\pTcut = 30\UGeV$ with a full breakdown of the uncertainties are given in Tables~\ref{tab:STWZ0jet} and \ref{tab:STWZ1jet}.
For $\pTcut = 30\UGeV$, $\Delta_\mu$ and $\Delta^{0/1}_\resum$ contribute roughly equally at each order, while $\Delta_\varphi$ is subdominant, except at the highest order in $\sigma_{\geq 1}$ where it contributes almost equally.
For $\pTcut = 25\UGeV$, the picture is roughly similar, except that as expected the uncertainties in $\sigma_0$ are somewhat increased compared to $\pTcut = 30\UGeV$. Note that for $\sigma_{\geq1}$ the total uncertainty actually slightly decreases from $\pTcut = 30\UGeV$ to $\pTcut = 25\UGeV$. The reason is that the cross section increases and the logarithms are resummed which results in the relative size of the remaining overall yield uncertainties to decrease.



\subsubsection{Combining resummed predictions for \texorpdfstring{$0/1/2$}{0/1/2}-jet bins \SectionAuthor{R.~Boughezal, X.~Liu, F.~Petriello, F.~J. Tackmann}}

Here, we discuss resummation-improved predictions for $0/1/2$-jet bins. We provide updated numerical results for 13 TeV and PDF4LHC15 PDFs and include the full breakdown of the uncertainties in terms of the parameterization of Eq.~\eqref{eq:3binnuisances}. We follow Ref.~\cite{Boughezal:2013oha} for combining the $0$-jet resummation of Ref.~\cite{Stewart:2013faa}, discussed above, with the $1$-jet resummation of Refs.~\cite{Liu:2012sz, Liu:2013hba}, which was documented in Ref.~\cite{Heinemeyer:2013tqa}, and for the estimation of yield and migration uncertainties.


\begin{figure}[t!]
\centering
\includegraphics[width=0.4\textwidth]{./WG1/ggF/figs/jetbin_012_lxl}%
\caption{Illustration of the $\pTjetone$-$\pTjettwo$ plane relevant for the $0/1/2$-jet binning. The orange jet boundaries are treated in resummed perturbation theory, while the blue boundary is treated at fixed order. The dependence of the final results on the arbitrary $\pToff$ parameter is negligible compared to the perturbative uncertainties.}
\label{fig:012jetbins}
\end{figure}


We denote the $p_T$ of the leading and 2nd leading jet by $\pTjetone$ and $\pTjettwo$ and by definition $\pTjettwo<\pTjetone$. The different jet bins are illustrated in \refF{fig:012jetbins}. We define
\begin{align}
\sigma_{\geq 0} &: \text{ the inclusive cross section}
\,, \\
\sigma_0 (\pTcut) &: \text{ the 0-jet cross section, with } \pTjetone\! < \pTcut
\,, \nn \\
\sigma_{\ge 1} (\pTcut) &: \text{ the inclusive 1-jet cross section, with } \pTjetone\! \geq \pTcut
\,, \nn \\
\sigma_1 ([p_{Ta}, p_{Tb}]; \pTcut) &: \text{ the exclusive 1-jet cross section, with } p_{Ta} \leq \pTjetone\! < p_{Tb}
\,,\quad \pTjettwo\! < \pTcut
\,, \nn \\
\sigma_{\ge 2} ([p_{Ta}, p_{Tb}]; \pTcut) &: \text{ the inclusive 2-jet cross section, with } p_{Ta} \leq \pTjetone\! < p_{Tb}
\,,\quad \pTjettwo\! \geq \pTcut
\,. \nn
\end{align}

The exclusive 1-jet bin is theoretically quite nontrivial as it is affected by both the $0/1$-jet and $1/2$-jet boundaries. To construct a resummation-improved expression for it, we introduce a parameter $\pToff > \pTcut$ to separate the low and high $\pTjetone$ regions, shown by the dotted line in \refF{fig:012jetbins},
\begin{equation}
\sigma_1(\pTcut) \equiv \sigma_1 ([\pTcut, \infty]; \pTcut) = \sigma_1 ([\pTcut, \pToff]; \pTcut) + \sigma_1 ([\pToff, \infty]; \pTcut)
\,.\end{equation}
In practice, $\pToff$ is taken to be around $m_H/2$. The second term above contains logarithms of $\pTcut/Q$ with $Q \sim \pToff \sim m_H$.
These can be resummed to NLL$'+$NLO using the direct resummation of Refs.~\cite{Liu:2012sz, Liu:2013hba}, which is valid for $\pTjetone \sim m_H$ much larger than the $\pTcut$ on the 2nd jet.
The first term above contains the $0/1$ jet boundary. To improve it by the corresponding resummation, we can write it as
\begin{equation}
\label{eq:sigma1lowpTJ}
\sigma_1 ([\pTcut, \pToff]; \pTcut) = \bigl[ \sigma_0 (\pToff) - \sigma_0 (\pTcut) \bigr] - \sigma_{\ge 2} ([\pTcut, \pToff], \pTcut)
\,,\end{equation}
where the first term in brackets is equivalent to $\sigma_0 (\pToff) - \sigma_0 (\pTcut) = \sigma_{\ge 1} (\pTcut) - \sigma_{\ge 1} (\pToff)$.
We can then use the NNLL$'+$NNLO resummation of Ref.~\cite{Stewart:2013faa} for resumming the logarithms of $\pTcut / m_H$ associated with the $0/1$-jet boundary. The additional 2-jet terms in Eq.~\eqref{eq:sigma1lowpTJ} are calculated at fixed NLO$_2$. The validity of this indirect resummation approach is discussed and studied in Ref.~\cite{Boughezal:2013oha}. It essentially relies on the fact that in the region of interest, the $0$-jet terms dominate while the 2-jet terms are numerically small.

The combination of the indirect resummation for $\pTjetone < \pToff$ and the direct resummation for $\pTjetone \geq \pToff$ thus allows for a resummation-improved description of the complete 1-jet bin, i.e.,
\begin{equation}
\label{eq:1jetmaster}
\sigma_1 ([\pTcut, \infty]; \pTcut) = \sigma_1^{\rm indirect} ([\pTcut, \pToff]; \pTcut) + \sigma_1^{\rm direct} ([\pToff, \infty]; \pTcut) \,.\end{equation}
An important consistency check of this method is that the final result should be largely independent of the precise choice of $\pToff$. More precisely, the residual dependence on $\pToff$ should be much smaller than the estimated perturbative uncertainties. As shown in Ref.~\cite{Boughezal:2013oha}, by using a complex scale hard scale in the $0$-jet resummation as well as including the $H+1$ jet NNLO$_1$ virtual corrections in the direct resummation, the results become practically independent of $\pToff$.

To estimate the perturbative uncertainties, we use profile scale variations in both the $0$-jet and $1$-jet resummations, using the same physical interpretations to identify the different uncertainties sources.
The first is the overall fixed-order uncertainty, $\Delta_{\mu i}$, as discussed above Eq.~\eqref{eq:Deltamui}, which is treated as a yield uncertainty. It is estimated by collectively varying all scales that appear in the resummed predictions and reduces to the usual fixed-order scale variations in the respective limits of large $\pTcut$. The uncertainties for $\{\sigma_{\geq 0}, \sigma_0, \sigma_{\geq 1}, \sigma_1, \sigma_{\geq 2}\}$ are
\begin{equation}
\kappa_\mu^{\rm y}: \quad \{ \Delta_{\mu \geq 0},\, \Delta_{\mu 0},\, \Delta_{\mu \geq1},\, \Delta_{\mu 1},\, \Delta_{\mu \geq 2} \}
\,,\end{equation}
%%%
where the first three are as in Eqs.~\eqref{eq:Deltamui} and \eqref{eq:01nuisance} and
\begin{align} \label{eq:Delta1mu}
\Delta_{\mu 1} &\equiv \Delta_{\mu 1}([\pTcut, \infty]; \pTcut)
= \Delta_{\mu 0}(\pToff) - \Delta_{\mu 0}(\pTcut) + \Delta_{\mu1}([\pToff, \infty]; \pTcut)
\,,\nn\\
\Delta_{\mu \geq 2} &\equiv \Delta_{\mu \geq0} - \Delta_{\mu0}(\pTcut) - \Delta_{\mu 1}([\pTcut, \infty]; \pTcut)
\,.\end{align}
Since the yield uncertainties are fully correlated, the yield uncertainty in the 1-jet bin is the linear sum from the two regions. For the region below $\pToff$ we use $\Delta_{\mu0}(\pToff) - \Delta_{0\mu}(\pTcut)$ from the $0$-jet resummation in Eq.~\eqref{eq:Deltamui}. For the region above $\pToff$, $\Delta_{\mu1} ([\pToff, \infty]; \pTcut)$ is determined by the overall profile scale variations in the $1$-jet resummation~\cite{Liu:2013hba}. The result for $\Delta_{\mu \geq 2}$ then follows from consistency and e.g. ensures that $\Delta_{\mu\geq1} = \Delta_{\mu 1} + \Delta_{\mu \geq2}$.

Next, the resummation uncertainties induced by the $0/1$-jet and $1/2$-jet boundaries are used to estimate the respective migration uncertainties. Parameterizing the migration uncertainties by two independent nuisance parameters as in Eq.~\eqref{eq:3binnuisances}, the uncertainties for $\{\sigma_{\geq 0}, \sigma_0, \sigma_{\geq 1}, \sigma_1, \sigma_{\geq 2}\}$ are
%%%
\begin{align}
\kappa_\cut^{0/1}: &\quad \Delta^{0/1}_\cut \times \{ 0,\, 1,\, -1,\, -(1 - x_1),\, -x_1 \}
\,,\nn\\
\kappa_\cut^{1/2}: &\quad \Delta^{1/2}_\cut \times \{ 0,\, 0,\, 0,\, 1,\, -1 \}
\,,\end{align}
%%%
where we set $x_2 = 0$ and
\begin{align}
\Delta^{0/1}_{\cut} &= \Delta^{0/1}_\resum(\pTcut)
\,,\qquad
x_1 = \Delta^{0/1}_\resum (\pToff) / \Delta^{0/1}_\resum (\pTcut)
\,,\nn\\
\Delta^{1/2}_\cut
&= \Bigl\{ \bigl[\Delta^{\rm FO}_{\ge 2} ([\pTcut, \pToff], \pTcut) \bigr]^2 + \bigl[ \Delta^{1/2}_\resum ([\pToff, \infty]; \pTcut) \bigr]^2
\Bigr\}^{1/2}
\,.\end{align}
Here, $\Delta^{0/1}_\resum$ is the $0$-jet resummation uncertainty from Eq.~\eqref{eq:Delta01resum} which accounts for the $0/1$-jet boundary (vertical orange line in \refF{fig:012jetbins}).
The nonzero value for $x_1$ arises from the region below $\pToff$, which is computed from the difference of the 0-jet cross sections at $\pTcut$ and $\pToff$ as shown in Eq.~\eqref{eq:sigma1lowpTJ}. Hence, the contribution of the $0$-jet resummation uncertainty to $\sigma_1$ is $\Delta^{0/1}_\resum (\pToff) - \Delta^{0/1}_\resum(\pTcut)$ (where the two are treated as correlated). Comparing this to the parameterization above, this should be equal to $-\Delta^{0/1}_\cut(1 - x_1)$, which determines $x_1$.

The $1/2$-jet boundary above $\pToff$ (the horizontal orange line in \refF{fig:012jetbins}) is accounted for by the $1$-jet resummation uncertainty $\Delta_\resum^{1/2}([\pToff, \infty]; \pTcut)$, which is determined by resummation profile scale variations in Ref.~\cite{Liu:2013hba}. For the $1/2$-jet boundary below $\pToff$ (blue line in \refF{fig:012jetbins}), we follow the original ST method and use the inclusive 2-jet uncertainty $\Delta^{\rm FO}_{\ge 2}$, given by the usual scale variation of the 2-jet fixed-order contributions in Eq.~\eqref{eq:sigma1lowpTJ}. The two are considered independent and added in quadrature.


\begin{table}%[t!]
\caption{Predictions for the $0/1/2$-jet bins for $\pTcut = 25\UGeV$ (top) and $\pTcut = 30\UGeV$ (bottom).}
\label{tab:BLPTW12jet}
\centering
%\renewcommand{\arraystretch}{1.2}
\begin{tabular}{l||c|cccc|c}
\toprule
$\pTcut = 25\UGeV$ & $\sigma/\mathrm{pb}$ & $\Delta_\mu$ & $\Delta_\varphi$ & $\Delta^{0/1}_\cut$ & $\Delta^{1/2}_\cut$ & total pert. unc.
\\ \midrule
$\sigma_{\geq 0}$ &  $47.41 \!\pm\! 2.40 $ &  $4.6\%$ & $2.0\%$ & - & - & $5.1\%$
\\
$\sigma_0$ & $26.25 \!\pm\! 1.97$ & $4.7\%$ & $0.6\%$ & $5.8\%$ & - & $7.5\%$
\\
$\sigma_{\geq 1}$ & $21.16 \!\pm\! 1.96$ & $4.5\%$ & $3.8\%$ & $7.1\%$ & - & $9.3\%$
\\
$\sigma_{1}$ & $13.28 \!\pm\! 1.76$ & $4.2\%$ & $3.3\%$ & $9.8\%$ & $7.2\%$ & $13.3\%$
\\
$\sigma_{\geq 2}$ & $\,\,\,7.88 \!\pm\! 1.12$ & $5.1\%$ & $4.6\%$ & $2.7\%$ & $12.2\%$ & $14.3\%$
\\ \bottomrule
% \end{tabular}
% \\[0.5ex]
% \begin{tabular}{l||c|cccc|c}
% \hline\hline
$\pTcut = 30\UGeV$ & $\sigma/\mathrm{pb}$ & $\Delta_\mu$ & $\Delta_\varphi$ & $\Delta^{0/1}_\cut$ & $\Delta^{1/2}_\cut$ & total pert. unc.
\\ \midrule
$\sigma_{\geq 0}$ &  $47.41 \!\pm\! 2.40 $ &  $4.6\%$ & $2.0\%$ & - & - & $5.1\%$
\\
$\sigma_0$ & $29.51 \!\pm\! 1.65$ & $3.8\%$ & $0.1\%$ & $4.1\%$ & - & $5.6\%$
\\
$\sigma_{\geq 1}$ & $17.90 \!\pm\! 1.88$ & $6.0\%$ & $5.2\%$ & $6.8\%$ & - & $10.5\%$
\\
$\sigma_{1}$ & $11.94 \!\pm\! 1.58$ & $5.5\%$ & $4.8\%$ & $8.4\%$ & $7.2\%$ & $13.2\%$
\\
$\sigma_{\geq 2}$ & $\,\,\,5.96 \!\pm\! 1.05$ & $7.1\%$ & $6.1\%$ & $3.6\%$ & $14.5\%$ & $17.6\%$
\\ \bottomrule
\end{tabular}
\end{table}


For our numerical results we use the same input parameters as for the $0$-jet resummation in the previous subsubsection. The results are presented in Table~\ref{tab:BLPTW12jet} for $\pTcut = 25\UGeV$ and $\pTcut = 30\UGeV$ with a complete breakdown of all uncertainty contributions. The results present a very consistent picture. The underlying parameters entering the migration uncertainties are given by $\Delta^{0/1}_\cut(25\UGeV) = 1.51\,\mathrm{pb}$, $x_1(25\UGeV) = 0.141$, $\Delta^{1/2}_\cut(25\UGeV) = 0.96\,\mathrm{pb}$, and
$\Delta^{0/1}_\cut(30\UGeV) = 1.21\,\mathrm{pb}$, $x_1(30\UGeV) = 0.175$, $\Delta^{1/2}_\cut(30\UGeV) = 0.86\,\mathrm{pb}$. As one might expect, $x_1$ is small and most of the $0/1$-migration uncertainty enters in $\sigma_1$. We also see that $\sigma_1$ is dominated by the migration uncertainties with similar contributions from each boundary, while the uncertainty in $\sigma_{\geq 2}$ is dominated by the $1/2$ boundary. However, due to the resummation improvement the total uncertainties in all bins are substantially smaller than in the pure fixed-order predictions.
