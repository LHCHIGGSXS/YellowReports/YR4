The Higgs Effective Field Theory (HEFT) framework for perturbative
calculations of the gluon fusion Higgs boson production process is a well
established tool that allows a significant reduction of complexity
in higher-order QCD calculations.  In this approach, the
heavy-quark-loop induced Higgs-gluon coupling of the Standard Model
(SM) is approximated by taking into account only the top quark
contribution and by calculating production amplitudes in the limit of
an infinite top quark mass. This is typically achieved by deriving the
relevant amplitudes from the effective Lagrangian
\begin{align}
\mathcal{L}_\mathrm{HEFT} = -\frac{C_1}{4v} H G_{\mu\nu}G^{\mu\nu}\, ,
\end{align}
with the gluon field strength tensor $G_{\mu\nu}$, the Higgs field
$H$, and a perturbatively calculable Wilson coefficient $C_1$. This
Lagrangian gives rise to tree-level couplings that replace the
loop-induced SM couplings between gluons and the Higgs boson, effectively
reducing the number of loops in any calculation by one.

When considering the total inclusive Higgs boson production cross section,
finite top quark mass effects remain very moderate even at higher
orders in QCD~\cite{Marzani:2008az,Pak:2009bx,Pak:2009dg,
  Harlander:2009my,Harlander:2009mq,Harlander:2009bw}.
In the tail of the transverse momentum spectrum of the Higgs boson
or for heavy Higgs boson (virtual) masses, however, the corrections
can become very large, indicating a complete breakdown of the HEFT
approximation~\cite{Baur:1989cm,Ellis:1987xu}. It has also
been known for a long time that the bottom quark loops, which are
entirely neglected in the HEFT, affect the spectrum in the
small-$p_\perp^H$ region \cite{Ellis:1987xu,Keung:2009bs}. In this
region, an all-order resummation of QCD corrections is
required. Standard techniques need to be adapted in order to achieve
this due to the bottom quark mass that introduces an additional scale
into the calculation \cite{Grazzini:2013mca}.

Several fully differential Monte Carlo codes have therefore been
developed that take into account the full heavy quark mass dependence
at NLO~\cite{Langenegger:2006wu,Anastasiou:2009kn,Bagnaschi:2011tu,
  Grazzini:2013mca}.
NLO results for Higgs boson production in association with a jet are not
available for finite heavy quark masses due to missing two-loop
amplitudes for this process.

In this note, we present an approximate treatment of finite top mass
effects at NLO based on one-loop amplitudes only. This allows us to
calculate Higgs plus $n$-jet processes at NLO, while retaining finite
top mass effects in an approximate way. Using this approximation, we
employ multijet merging techniques~\cite{Hoeche:2012yf} to
merge higher-multiplicity NLO processes matched to a parton shower
into one exclusive event sample, extending similar
approaches~\cite{Catani:2001cc,Krauss:2002up,Lonnblad:2001iq,Alwall:2011cy}
in terms of jet multiplicity and $\alpha_s$ accuracy.
Based on leading order merging, we also suggest a method to address
the issues raised in \cite{Grazzini:2013mca} concerning the inclusion
of bottom quark contributions in the low-$p_\perp^H$ region.

\subsection{Implementation of quark mass corrections}

In order to take into account the full heavy quark mass effects in the
hard scattering at leading order, we replace the approximate HEFT
tree-level matrix elements provided by Sherpa's matrix element
generator Amegic++~\cite{Krauss:2001iv} with the exact one-loop matrix
elements provided by OpenLoops~\cite{Cascioli:2011va} in combination
with Collier~\cite{Denner:2014gla}. This allows the calculation of
processes with up to three additional jets in the final state at
leading order, with the full top and bottom quark mass dependency
taken into account.

At NLO, the cross section for the production of a Higgs boson accompanied by
a certain number $m-1$ of jets receives contributions from two
integrals of different phase space dimensionality.
\begin{align}
  \sigma = \int (B+V+I)d\phi_m + \int (R-S)d\phi_{m+1} \label{gghmq_nloxs}
\end{align}
The born term $B$ and the real emission term $R$ are present already
at leading order for processes of the respective jet multiplicity and
can be corrected as in the leading order calculation. $I(\phi_m)$ and
$S(\phi_{m+1})$ denote the integrated and differential Catani-Seymour
subtraction terms, respectively~\cite{Catani:1996vz}. They render both
integrals separately finite and are built up from leading-order
$m$-particle matrix elements dressed with appropriate splitting
kernels and can henceforth be corrected by using the full one-loop
matrix elements instead of the tree-level HEFT approximation. Note
that because we correct $R$ and $S$ with matrix elements of different
final state multiplicity, the mere convergence of the corresponding
integral already provides a crucial test for our implementation.

The IR-subtracted virtual correction $V$ receives contributions from
two-loop diagrams when taking into account the full heavy quark mass
dependencies. Since these amplitudes are available only for the Higgs boson 
plus zero-jet final state, we employ an ad-hoc approximation that only
involves one-loop matrix elements (even for the Higgs boson plus zero-jet
final state). We assume a factorization of the $\alpha_s$-corrections
from the quark mass corrections and set
\begin{align}
V = V_\mathrm{HEFT}\frac{B}{B_\mathrm{HEFT}}\,.\label{gghmq_approx}
\end{align}
In this approximation, we can straightforwardly apply finite top mass
corrections in simulations employing CKKW multi jet merging at NLO in
the MEPS@NLO scheme~\cite{Hoeche:2012yf}.

We expect the approximation~\eqref{gghmq_approx} to give reasonable
results only if the HEFT-approximation is valid. For any contribution
involving the bottom Yukawa coupling $y_b$ , it cannot be used due to
the small bottom quark mass. This applies to the interference terms
proportional to $y_ty_b$ as well as the squared bottom contributions
proportional to $y_b^2$. We therefore calculate terms that involve
$y_b$ as separate processes at leading order. The NLO corrections to
the total inclusive cross sections for the $y_ty_b$ contributions and
the $y_b^2$ contributions are only ${\cal{O}}{1}\%$ and ${\cal{O}}{20}\%$, 
respectively~\cite{Bagnaschi:2015bop}.  Furthermore, the $y_b^2$ 
contributions featuring the slightly larger NLO K-factor are 
significantly suppressed compared to the $y_ty_b$ 
terms~\cite{Bagnaschi:2015bop}. We therefore consider a treatment at
leading order sufficiently accurate. Any terms proportional to $y_t^2$
will however be calculated at NLO in the approximation described
above.

\subsection{Finite top mass effects}

As mentioned in the introduction, the total inclusive cross section is
only mildly affected by finite top mass effects. The low-$p^H_\perp$
region, where the bulk of the cross section is located, can therefore
be expected to exhibit only a moderate dependence on the top quark
mass. In kinematic regimes where any invariant significantly exceeds
$m_t$, however, we expect the HEFT approximation to break down. The
$p_\perp^H$ distributions in \refF{gghmq_meps} (left) exemplify
this picture. We show Higgs boson transverse momentum distributions for
final states with one, two, and three jets calculated at fixed leading
order. Jets are reconstructed using the anti-$k_T$ algorithm with a
radius parameter of $R=0.4$ and a minimum jet $p_\perp$ of
$\sim{30}$~GeV except in the 1-jet case, where we apply
only a small minimum $p_\perp$-cut of $\sim{1}$~GeV. The
distributions for all three jet multiplicities exhibit a very similar
pattern when comparing the full SM result to the HEFT approximation.
Below $p_\perp^H\approx m_H$, we observe a flat excess of around
$\sim{6}\%$ that recovers the correction factor to the total
inclusive Higgs boson production cross section at leading order. The
deviations become very large when $p_\perp^H$ significantly exceeds
$m_t$, as expected. The similarity of the top mass dependency of the
$p_\perp^H$ spectrum for all jet final multiplicities confirms similar
findings for one- and two-jet configurations in
\cite{Buschmann:2014twa}.

In \refF{gghmq_meps} (right), we show analogous results obtained 
from the MEPS@NLO simulation. We included NLO matrix elements for the 
zero- and one-jet final states as well as leading order matrix elements 
for the two-jet final state in the merged setup and set $Q_\mathrm{cut}$
to $\sim{30}$~GeV. From the ratio plot in \refF{gghmq_meps} it is evident that in our approximation we recover
the same suppression patterns as in the respective fixed leading order
calculations for all jet multiplicities. This is a nontrivial
observation as an $m$-jet configuration receives corrections from
$m$-jet matrix elements as well as from $m+1$-jet matrix elements
through the real emission corrections $R$ in \eqref{gghmq_approx}.

\begin{figure}
  \centering
    \includegraphics[width=0.45\textwidth]{./WG1/ggF/figs/fo_ratios}
%    \label{gghmq_fo_ratios}
  \hfill
    \includegraphics[width=0.45\textwidth]{./WG1/ggF/figs/mepsnlo}
    \caption{  The Higgs boson transverse momentum spectrum in gluon fusion. We
    show individual curves for the HEFT approximation (dashed) and the
    full SM result taking into account the mass dependence in the top
    quark loops. The lower panel shows the ratio of the SM results to
    the HEFT approximation.
    Left: LO fixed order calculation for up to three jets. The
    error bands indicate the uncertainties obtained from variations
    of the factorization and renormalization scales.  
    Right: Multijet merged calculation. We include the zero and one jet 
    final states at NLO as well as the two jet final state at leading order. 
    The individual curves show inclusive $N$-jet contributions.}
    \label{gghmq_meps}
\end{figure}

\subsection{Nonzero bottom mass effects}

\begin{figure}
  \centering
  \includegraphics[width=.48\textwidth]{./WG1/ggF/figs/tb_fo}
  \caption{Bottom quark mass effects at fixed leading order. The
      minimum jet $p_\perp$ is set to $\sim{1}$~GeV in
      order to display map out the low $p_\perp^H$ region as well.}
    \label{gghmq_tb_fo}
  \end{figure}
  
As pointed out already in~\cite{Ellis:1987xu,Keung:2009bs}, the
inclusion of the bottom quark in the loops affects the $p_\perp^H$
distribution only at small values of $p_\perp^H$ around $m_b$. In
\refF{gghmq_tb_fo} we reproduce these findings for the process
$pp\rightarrow H+j$ at fixed order. In the $p_\perp$ range around
$m_b$ where the bottom effects are large, a fixed order prediction is
of course unreliable due to the large hierarchy of scales between
$m_H$ and the transverse momentum. This large separation of scales
induces Sudakov logarithms $\ln(m_H/p_\perp)$ that spoil any fixed
order expansion and require resummation.

It was argued in~\cite{Grazzini:2013mca} that the resummation of these
logarithms is complicated by the presence of the bottom quark in loops
that couple to the Higgs boson. The bottom quark introduces $m_b$ as
an additional scale above which the matrix elements for additional QCD
emissions do not factorize. Since a factorization is essential for the
applicability of resummation techniques, it was proposed to use a
separate resummation scale of the order of $m_b$ for the contributions
involving $y_b$, thereby restricting the range of transverse momenta
where resummation is applied to the phase space where factorization is
guaranteed. Two quantitative prescriptions have been proposed for the
determination of a specific numerical value for the resummation scale
of the bottom
contributions~\cite{Bagnaschi:2015qta,Harlander:2014uea}. These two
methods yield numerical values of $\sim{9}$~GeV and
$\sim{31}$~GeV~\cite{Bagnaschi:2015bop} for the dominant
top-bottom interference terms. In addition to $m_b$, we will therefore
consider these values for our numerical studies. The pure top quark
contributions proportional to $y_t^2$ will be treated as usual, with
the resummation scale set to $m_H$.

While reference~\cite{Grazzini:2013mca} was concerned with analytical
resummation techniques, similar approaches were studied in the context
of NLO-matched parton shower Monte Carlos
\cite{Bagnaschi:2015qta,Harlander:2014uea,Bagnaschi:2015bop}. Our
discussion will be restricted to the leading order as the
approximation used for the NLO calculation of the top quark
contributions \eqref{gghmq_approx} is invalid for the bottom quark
terms. The equivalent of the resummation scale in analytic
calculations is the parton shower starting scale $\mu_\mathrm{PS}$
because it restricts parton shower emissions to the phase space below
this scale and because this scale enters as the argument in the
Sudakov form factors. Using separate parton shower starting scales for
the top and the bottom contributions, respectively, requires to
generate and shower them separately as well. A corresponding
separation of terms in the one-loop matrix elements can be achieved
with OpenLoops. By means of this separation into terms proportional to
$y_t^2$ and the remainder, we can generate an MC@NLO sample for the
top quark contributions while calculating the terms involving $y_b$ at
leading order. \refF{gghmq_tb_meps} (left) shows the $p_\perp^H$
spectrum obtained this way. We show results with $\mu_\mathrm{PS}^b$
set to $m_b$, $\sim{9}$~GeV, and
$\sim{30}$~GeV as motivated above. The parton shower
starting scale for the top quark contributions will be
$\mu_\mathrm{PS}^t=m_H$ throughout. For small $\mu_\mathrm{PS}^b$, the
suppression in the low $p_\perp$ region below $m_b$ is much more
pronounced than in the fixed order result in \refF{gghmq_tb_fo}.
This is because, pictorially speaking, without changing the cross
section of the individual contributions, the parton shower simulation
spreads the $y_t^2$ part over a much wider range, up to
$\mathcal{O}(m_H)$, than for the negative $y_ty_b$, up to
$\mathcal{O}(m_b)$ only. The spectrum in this region is therefore
extremely sensitive to variations of $\mu_\mathrm{PS}^b$. When varying
$\mu_\mathrm{PS}^b$ to sufficiently low values, the differential cross
section may even become negative, clearly an unphysical result. We
stress, again, that this is not a physical effect but an artefact of
the unitary nature of the parton shower. Setting the value of
$\mu_\mathrm{PS}^b$ to a small value, the entire leading order bottom
cross section contributions will be distributed in a phase space with
Higgs boson transverse momenta not significantly exceeding
$\mu_\mathrm{PS}^b$. Since this cross section is negative, the
spectrum must become negative at some point when lowering
$\mu_\mathrm{PS}^b$.

\begin{figure}
  \centering
    \includegraphics[width=0.45\textwidth]{./WG1/ggF/figs/tb_lops_yr}
%    \label{gghmq_tb_lops}
  \hfill
    \includegraphics[width=0.45\textwidth]{./WG1/ggF/figs/tb_meps_yr}
    \caption{Left: Bottom quark mass effects at LO+PS accuracy with small
      parton shower starting scales $\mu_\mathrm{PS}^b$. The specific
      values chosen for $\mu_\mathrm{PS}^b$ are motivated in the
      text.
      Right: Bottom quark mass effects taken into account by means of
      CKKW merging with a small merging scale
      $Q^b_\mathrm{cut}=\mathcal{O}(m_b)$. The red error band shows
      variations of this scale as indicated.}
    \label{gghmq_tb_meps}
\end{figure}

We therefore suggest another approach at taking into account the
bottom quark contributions in a parton shower Monte Carlo simulation.
We account for the non-factorization of the real emission matrix
elements above some scale $Q^b_\mathrm{cut}$ by correcting parton
shower emissions harder than this scale with the appropriate fixed
order matrix elements. This can be done consistently in the CKKW
merging scheme~\cite{Catani:2001cc,Hoeche:2009rj}.  Setting the
merging scale for the bottom contributions $Q_\mathrm{cut}^b$ to $m_b$
allows the correction of the parton shower in the regime where the
matrix elements involving $m_b$ do not factorize (without restricting
all emissions to the phase space below).  Above $Q_\mathrm{cut}$, the
fixed-order accuracy of the real emission matrix
elements is thereby retained. Since any NLO prediction of the
inclusive process describes the $p_\perp$ spectrum only at leading
order, our approach retains the same parametric fixed order accuracy
when considering the $p_\perp^H$ distribution. Beyond fixed order, the
differences should be small since the NLO corrections to the inclusive
cross section are at the per cent level for the $y_ty_b$ interference
terms.

In \refF{gghmq_tb_meps} (right) we show the bottom quark effects on 
the $p_\perp^H$ spectrum in this approach. We include matrix elements with
up to one jet in the merging such that a leading order accuracy in
$\alpha_s$ is guaranteed for both the top and the bottom contributions
to the $p_\perp^H$ spectrum. This allows a comparison to 
\refF{gghmq_tb_fo}. The effects of the bottom quarks lead to a very
similar suppression pattern over the entire displayed range of
$p_\perp^H$. The large NLO K-factor that appears in the MC@NLO
calculations of the top contributions however affects the overall
relative normalization of the bottom quark effects. They are
correspondingly smaller by roughly $\sim{50}\%$ in 
\refF{gghmq_tb_meps} when compared to \refF{gghmq_tb_fo}. The
sensitivity to variations of the scale in the calculation that
effectively accounts for the presence of the bottom mass in the
problem is drastically reduced. \refF{gghmq_tb_meps} includes an
error band corresponding to variations of $Q_\mathrm{cut}^b$ in the
large interval between $m_b$ and $\sim{31}$~GeV. On the
displayed scale, these variations are hardly visible.

\subsection{Conclusions}

We presented in this section an implementation of heavy quark mass
effects in gluon fusion Higgs boson production that allows to systematically
include finite top mass effects in an approximate way at NLO for in
principle arbitrary jet multiplicities in the final state. Based on
this approximation, we presented results for the Higgs boson transverse
momentum distributions obtained from NLO matched and merged samples.
When comparing the top quark mass dependence in one-, two-, and
three-jet final states, we observed a universal suppression pattern
that agrees very well with the corresponding leading order results.

Our treatment of contributions involving the bottom Yukawa coupling is
based on $m_b$- and $m_t$-exact leading order matrix elements in
combination with tree-level multijet merging techniques. We argued
that this approximation is appropriate since it allows to retain
leading order accuracy for the corresponding contributions in the
$p_\perp^H$-spectrum and it also allows to account for the
non-factorization of real emission amplitudes at scales above $m_b$.
In this approach, the uncertainty associated with the appearance of
$m_b$ as an additional scale in the calculation is drastically
reduced.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
