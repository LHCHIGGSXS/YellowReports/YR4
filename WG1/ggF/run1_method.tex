
During Run-1 of LHC data taking at 7 and 8 TeV the two methods reviewed
in Sect.~\ref{subsec:binuncertainties}  were used to compute uncertainties on the jet bin 
acceptance for the Higgs boson signal: the so called Stewart-Tackmann method (ST) \cite{Stewart:2011cf} and the Jet Veto Efficiency method  (JVE) \cite{Banfi:2012jm}.
The first method has been used from the Higgs boson discovery paper
\cite{Aad:2012tfa}, \cite{Chatrchyan:2012xdj} while the second method
has been used in the  Run-1 ATLAS paper on the $h \to WW^*$ channel
\cite{ATLAS:2014aga}.
 In the present section we will present results obtained using both
 methods at 13~TeV, providing all details
 on the inputs used for the computation of the uncertainties.

\subsubsection{The Stewart-Tackmann method}

%{\colour{red} The uncertainties should be symmetrized. If a cross section is at the edge of the scale scan, the error is doubled}


The ST method can be applied to all jet bins and assumes that inclusive jet bin cross sections, that is the cross section to have at least N jets,
($\sigma_{\ge N}$) are uncorrelated for all values of $N$. In this case the $N-$jet cross section can be written as:
\[
\sigma_N = \sigma_{\ge N} - \sigma_{\ge N+1}
\]
and the uncertainty on $\sigma_N$ is computed using the relation:
\[
\Delta^2 \sigma_{N} = \Delta^2 \sigma_{\ge N} + \Delta^2 \sigma_{\ge N+1}
\]
 The values of $\Delta^2\sigma_{\ge N, \ge N+1}$ are evaluated as the envelope
 of all cross sections obtained by changing the renormalization
 ($\mu_r$) and factorization ($\mu_f$) scales
by  a factor two around the central scale of $\mu = m_H/2$, excluding the values $\mu_f/\mu_r = 4$ and  $\mu_f/\mu_r = 1/4$. The values of $\sigma_{\ge 0}$
are computed at NNLO using the HNNLO program~\cite{Catani:2007vq}, and
 the process $pp \to h \to WW^{*} \to  e^+ \nu e^- \bar{\nu}$ is used
 for the computation of the cross section. The branching fraction to $W$ pairs used in HNNLO is Br$(h \to W^+W^-)
=0.2054$, and it is used, together with the PDG~\cite{Agashe:2014kda} value of Br$(W\to
e\nu) = 0.1070$, to correct the HNNLO cross section by the total branching
fraction, in order to extract the $h$ production cross section. The
value used is:
\[
{\rm Br}(h \to W^+ W^-){\rm Br}^2(W\to e \nu) = 0.002356
\]
The  obtained values are tabulated in Table~\ref{tab:inclusive}.


The $H+1$ jet and $H+2$ jet cross sections have been computed
at LO and NLO for the  $pp \to h \to WW^{*} \to  e^+ \nu e^-
\bar{\nu}$ process using the MCFM program~\cite{Campbell:2006xx},
the branching fraction to $W$ pairs used in MCFM is Br$(h \to W^+W^-)
=0.214$, and it is used to correct the MCFM cross section by the total branching
fraction:
\[
{\rm Br}(h \to W^+ W^-){\rm Br}^2(W\to e \nu) = 0.00245
\]
In Table \ref{tab:Hplus1jet} and Table
\ref{tab:Hplus2jet} we show the Higgs boson production cross section after
having corrected the MCFM results for the branching fraction
above. The jet $p_T$ thresholds of 25 GeV and 30 GeV are used.

\begin{table}
\caption{\label{tab:inclusive} Inclusive cross sections
($\sigma_{\ge0}$) in pb for the process
  $pp \to H$ for several renormalization and
  factorization scale values,  the cross section is evaluated with
  HNNLO at $\sqrt{s}=13$ TeV, the central value scale is set to 
$m_H/2$. The Higgs boson mass is set at $m_H = 125.09$ GeV. PDF4LHC15 NNLO MC PDFs are
used. The error quoted is the statistical error of the computation. The
computation is performed in the infinite top quark mass approximation.
The cross section is reported at LO, NLO and NNLO.}
\centering
\begin{tabular}{cc|ccc}
\toprule
 & & \multicolumn{3}{c}{Inclusive cross section (HNNLO)} \\ 
\cmidrule{3-5}
$\mu_{F}/m_H$ & $\mu_{R}/m_H$ & LO & NLO & NNLO \\
\midrule
1&1&12.697$\pm$0.003&30.30$\pm$0.15&41.50$\pm$0.15\\
1&1/2&15.519$\pm$0.003&36.51$\pm$0.26&46.14$\pm$0.26\\
1/4&1/4&16.691$\pm$0.003&42.82$\pm$0.34&50.16$\pm$0.34\\
1/4&1/2&13.354$\pm$0.003&34.38$\pm$0.20&45.33$\pm$0.20\\
1/2&1&11.958$\pm$0.003&29.43$\pm$0.16&40.99$\pm$0.16\\
1/2&1/4&18.267$\pm$0.004&43.99$\pm$0.35&49.65$\pm$0.35\\
1/2&1/2&14.615$\pm$0.003&35.61$\pm$0.25&45.64$\pm$0.25\\
\bottomrule
\end{tabular}
\end{table}


\begin{table}
\caption{\label{tab:Hplus1jet} H+1 jet inclusive cross sections
($\sigma_{\ge1}$) in pb for  the process
  $pp \to H$ for several renormalization and
  factorization scale values,  the cross section is evaluated with
  MCFM at $\sqrt{s}=13$ TeV, the central value scale is set to $m_H/2$. 
The Higgs boson mass is set at $m_H = 125.09$ GeV. PDF4LHC15 NNLO MC PDFs are
used. }
\centering
\begin{tabular}{cc|cc|cc}
\toprule
 & & \multicolumn{4}{c}{ H+1jet inclusive cross section (MCFM)} \\ 
\cmidrule{3-6}
\multicolumn{2}{c}{}&\multicolumn{2}{|c|}{$p_T > 25$
                      GeV}&\multicolumn{2}{c}{$p_T > 30$ GeV} \\
%\midrule
$\mu_{F}/m_H$ & $\mu_{R}/m_H$ & LO & NLO & LO & NLO \\
\midrule
1 & 1 & 9.303$\pm$0.002&16.48$\pm$0.05&7.947$\pm$0.002&14.11$\pm$0.02 \\
1 & 1/2 & 12.764$\pm$0.002&19.80$\pm$0.05&10.900$\pm$0.002&16.87$\pm$0.03 \\
1/4 & 1/4 & 18.373$\pm$0.004&22.04$\pm$0.08&15.836$\pm$0.003&18.96$\pm$0.06 \\
1/4 & 1/2 & 12.874$\pm$0.002&19.18$\pm$0.05&11.097$\pm$0.002&16.47$\pm$0.05 \\
1/2 & 1 & 9.398$\pm$0.002&16.42$\pm$0.02&8.060$\pm$0.0016&14.04$\pm$0.03 \\
1/2 & 1/4 & 18.396$\pm$0.003&22.36$\pm$0.10&15.777$\pm$0.003&19.29$\pm$0.06 \\
1/2 & 1/2 & 12.893$\pm$0.002&19.42$\pm$0.10&11.056$\pm$0.002&16.77$\pm$0.05 \\
\bottomrule
\end{tabular}
\end{table}

\begin{table}
\caption{\label{tab:Hplus2jet} H+2 jet cross-section ($\sigma_{\ge2}$)
in pb for
 the process
  $pp \to H$ for several renormalization and
  factorization scale values.
  The values are computed with MCFM  at $\sqrt{s}=13$ TeV, R=0.4
  for jet thresholds of  $p_{T} > 25$ and $p_{T} > 30$  GeV. The central value scale is chosen to be
  $m_H/2$. PDF4LHC15 NNLO MC  pdfs are used.}
\centering
\begin{tabular}{cc|cc}
\toprule
 \multicolumn{4}{c}{gg$\to$H+2jets cross section (MCFM)} \\ 
\midrule
$\mu_{F}/m_H$ & $\mu_{R}/m_H$ & LO & NLO \\
%\midrule
\multicolumn{2}{c}{} & \multicolumn{2}{|c}{$p_T > 25$ GeV} \\
\midrule
1 & 1 & 5.250$\pm$	0.002&	6.96$\pm$
0.03 \\
1 & 1/2 & 8.003	$\pm$0.003	&6.90$\pm$	0.06\\
1/4 & 1/4 & 14.565$\pm$	0.005	&2.26$\pm$	0.14\\
1/4 & 1/2 &  9.068$\pm$	0.003	&4.73$\pm$	0.09\\
1/2 & 1 & 5.586	$\pm$0.002	&5.67	$\pm$0.05\\
1/2 & 1/4 & 13.679$\pm$	0.005	&4.10	$\pm$0.11\\
1/2 & 1/2 & 8.514	$\pm$0.003	&5.55	$\pm$0.07\\
\midrule
\multicolumn{2}{c}{} & \multicolumn{2}{|c}{$p_T > 30$ GeV} \\
\midrule
1 & 1 & 3.980	$\pm$0.001&	5.20	$\pm$0.03\\
1 & 1/2 & 6.064	$\pm$0.002&	5.27	$\pm$0.04\\
1/4 & 1/4 & 11.192	$\pm$0.004&	-1.6	$\pm$0.5\\
1/4 & 1/2 & 6.966	$\pm$0.002&	3.52	$\pm$0.05\\
1/2 & 1 & 4.262	$\pm$0.002&	4.28	$\pm$0.04\\
1/2 & 1/4 & 10.434	$\pm$0.004&	2.81	$\pm$0.10\\
1/2 & 1/2 & 6.496	$\pm$0.002&	4.12	$\pm$0.05\\
\bottomrule
\end{tabular}
\end{table}

From Table \ref{tab:inclusive} and Table \ref{tab:Hplus1jet} we can
compute $\Delta \sigma_{\ge 0}$, $\Delta \sigma_{\ge 1}$,
$\Delta \sigma_{\ge 2}$ and $\sigma_0$, $\sigma_1$ using the
definitions:\footnote{Note that $\sigma_{\ge 2}$ and its uncertainty
are computed here at leading order in order to match the power of
$\alpha_s$ with   the NLO computation of $\sigma_{\ge
1}$. Alternatively, one could choose to evaluate $\sigma_{\ge 2}$ at
the highest known order, namely, NLO. }
\[
\sigma_0 = \sigma_{\ge 0} - \sigma_{\ge 1} ,
\, \sigma_1 = \sigma_{\ge 1} - \sigma_{\ge 2}^{\rm LO}.
\]
where the cross section corresponding to the scale choice  $\mu_{F} =
\mu_{R} = m_H/2$ is used as central value and the LO value of
$\sigma_{\ge 2}$ is used in order to preserve the $\alpha_s$ power
counting in $\sigma_1$, being $\sigma_{\ge 1}$ computed up to NLO.
The central values of the exclusive cross sections and their
uncertainties are summarized in Table \ref{tab:ST_unc} together with
the fractional error on $\sigma_0$ and $\sigma_1$. 
The upward and downward fractional errors are obtained using the following
formulae:
\[
\frac{\Delta^+ \sigma_0}{\sigma_0} = \frac{\sqrt{\Delta^{+2} \sigma_{\ge 0} + \Delta^{-2} \sigma_{\ge 1}}}{\sigma_0} \quad
\frac{\Delta^- \sigma_0}{\sigma_0} = \frac{\sqrt{\Delta^{-2} \sigma_{\ge 0} + \Delta^{+2} \sigma_{\ge 1}}}{\sigma_0} 
\]

 
\[
\frac{\Delta^+ \sigma_1}{\sigma_1} = \frac{\sqrt{\Delta^{+2} \sigma_{\ge 1} + \Delta^{-2} \sigma_{\ge 2}}}{\sigma_1} \quad
\frac{\Delta^- \sigma_1}{\sigma_1} = \frac{\sqrt{\Delta^{-2} \sigma_{\ge 1} + \Delta^{+2} \sigma_{\ge 2}}}{\sigma_1} 
\]
where the ${}^+$ sign indicates the upward uncertainty and the ${}^-$
sign the downward uncertainty. In the same table, the symmetrized
number are reported, using the maximum of the upward and downward
errors. We recommend to use the symmetrized  values as final uncertainties.


\begin{table}
\caption{\label{tab:ST_unc} Summary of jet-bin uncertainties on the $0$ and
  $1$ jet exclusive cross sections obtained using the ST method. The
  last two lines show symmetrized uncertainty intervals from the $7^{th}$ and $8^{th}$ row.}
\centering
\begin{tabular}{c|c|c}
\toprule
%& \multicolumn{2}{|c}{	Summary of jet-bin uncertainties related to the
%  					S.T. method, the last two rows} \\
%& \multicolumn{2}{|c}{	are obtained by simply symmetrizing
%  					the uncertainty intervals shown at the $7^{th}$ and $8^{th}$ row.}\\
%\midrule
$\Delta \sigma_{\ge 0}$ &\multicolumn{2}{c}{ $[-4.6,+4.6]  $ pb} \\
\midrule
& $p_T > 25$ GeV & $p_T > 30$ GeV \\
\midrule
$\Delta \sigma_{\ge 1}$ & $[-3,+2.9]  $ pb & $[-2.7,+2.5 $ pb\\
$\Delta \sigma_{\ge 2}^{\rm LO}$ & $[- 3.3, +6.0] $ pb &  $[-2.5,+4.7] $ pb\\
$\sigma_{0}$ & 26.2 pb & 28.9 pb  \\
$\sigma_{1}$ &10.9 pb & 10.3 pb \\
\midrule
$\Delta \sigma_0 / \sigma_0$ S.T &[-0.22, +0.22]  & [-0.18,+0.18]  \\
$\Delta \sigma_1 / \sigma_1$ S.T &[-0.62, +0.40]  & [-0.53,+0.34]  \\
\midrule
$\Delta \sigma_0 / \sigma_0$ S.T &[-0.22, +0.22]  & [-0.18,+0.18]  \\
$\Delta \sigma_1 / \sigma_1$ S.T &[-0.62, +0.62]  & [-0.53,+0.53]  \\
\bottomrule
\end{tabular}
\end{table}


\subsubsection{The Jet Veto Efficiency method.}

The first version of Jet Veto Efficiency method, presented in
Ref.~\cite{Banfi:2012jm}, 
computes the jet veto acceptance uncertainties using three different definitions of the jet veto efficiency.
Such definitions differ among each other for terms beyond $\alpha_s^2$ and the related efficiencies show different behaviour as a function of the vetoed jet $p_T$. 
The uncertainty is computed, in this case, by doing the envelope of the
naive scale uncertainty of the reference method and the central values
obtained with the three jet veto efficiency definitions. The definitions are commonly referred as ``schemes'' $a,b,c$ and are reported below:
\begin{equation}
 \epsilon_N^{a}= 1-\frac{\sigma_{\ge N+1}^{\rm NLO}}{\sigma_{\ge N}^{\rm NNLO}}     \quad \epsilon_N^{b} = 1 -
\frac{\sigma_{\ge N+1}^{\rm NLO}}{\sigma^{\rm NLO}_{\ge N}} \quad \epsilon_N^{c} = 1 - \frac{\sigma_{\ge
    N+1}^{\rm NLO}}{\sigma_{\ge N}^{\rm NLO}} + \left ( \frac{\sigma_{\ge N}^{\rm
      NLO}}{\sigma_{\ge N}^{\rm LO}} - 1 \right ) \frac{\sigma_{\ge
    N+1}^{\rm LO}}{\sigma_{\ge N}^{\rm LO}}  \label{eq:schemes}
\end{equation}
The $N$ value represents the number of jets of the exclusive
selection.

\subsubsubsection{The JVE method for zero jet}
In the 0-jet case the large logs that are produced by the
 introduction of the jet $p_T$ threshold can be resummed using the
 JetVHeto  program.
Inputs to the JetVHeto computation are the LO, NLO and NNLO inclusive
cross sections, that are shown in Table \ref{tab:inclusive} and the
$\sigma_{\ge 1}$ cross section shown in Table \ref{tab:Hplus1jet}. The
resummed computation can be matched to the fixed order result using
three different schemes  that can be
considered equivalent to the three different Jet Veto Efficiency
schemes listed above. In the resummed case, all scales, including the resummation scale, are varied by a factor two,
and the envelope built using such uncertainty band together with the central value
obtained with the three different matching schemes is quoted as final uncertainty.
We report results using both the fixed-order computation and  the
resummed one.


In Table \ref{table:JVE0} we show the 0-jet JetVHeto efficiencies obtained for the factorization, renormalization and resummation scale considered in the envelope,
and the central value of each  scheme. The envelope is built using
the scale variations of the ``scheme a'' only, therefore only for this
scheme  are detailed values for each scale choice  shown.
The efficiencies are reported for the 25 and 30 GeV $p_T$ threshold
using fixed order, resummed only and resummed results matched with the
fixed order computation at NNLO+NNLL.

%\begin{landscape}
\begin{table}
\centering
%\rotatebox{90}{
%\begin{sidewaystable}
\caption{\label{tab:JVE0} $H+0$ jet efficiencies of the process
  $pp \to H \to e^+ \nu e^- \bar{\nu}$ for several renormalization and
  factorization scale values, matching schemes and resummation scales.
  The values are computed with JetVHeto  at $\sqrt{s}=13$ TeV, R=0.4
  and $p_{T} >25, 30$ GeV. The central value scale is chosen to be
  $m_H/2$. The Higgs boson mass is set at $m_H = 125.09$ GeV. PDF4LHC15 NNLO MC  are
used. } \label{table:JVE0}
\footnotesize
\begin{tabular}{cc|c|c|ccc|ccc}
\toprule
\multicolumn{10}{c}{0-jet JetVHeto efficiencies for different $p_T$ thresholds} \\
\midrule
\multicolumn{4}{c|}{} & \multicolumn{3}{c|}{$p_T > 25$ GeV} & \multicolumn{3}{c}{$p_T > 30$ GeV} \\ 
\cmidrule{5-10}
$\mu_F/m_H$ & $\mu_R/m_H$ & $Q_{\rm res}/m_H$ & Scheme &  NNLO+NNLL &
                                                                      NNLL&
                                                                            NNLO & NNLO+NNLL & NNLL & NNLO \\
\midrule
1 & 1& 1/2 & a & 0.5841 & 0.5727 & 0.6028 & 0.6509 & 0.6289 &0.6600\\
1 & 1/2& 1/2 & a & 0.5810 & 0.5513 & 0.5708  & 0.6472 & 0.6025 & 0.6344\\
1/4 & 1/4& 1/2 & a & 0.5792 & 0.5176 & 0.5606  & 0.6431 & 0.5642 & 0.6221 \\
1/4 & 1/2& 1/2 & a & 0.5549 & 0.5290 & 0.5768 & 0.6248 & 0.5845 & 0.6366\\
1/2 & 1& 1/2 & a & 0.56790 & 0.5586 & 0.5995& 0.6379 & 0.6169 & 0.6575\\
1/2 & 1/4& 1/2 & a & 0.5866 & 0.5207 & 0.5495  & 0.6462 & 0.5656 & 0.6114 \\
1/2 & 1/2& 1/2 & a & 0.5726 & 0.5413 & 0.5744  & 0.6367 & 0.5940 & 0.6325 \\
1/2 & 1/2& 1/4 & a & 0.5650 & 0.5295 & 0.5744 & 0.6273 & 0.5686 & 0.6325\\
1/2 & 1/2& 1 & a & 0.6336 & 0.6387 & 0.5744  & 0.6987 & 0.6938 & 0.6325\\
1/2 & 1/2& 1/2 & b & 0.5147 & 0.5413 & 0.4544  & 0.5760 & 0.5940 & 0.5289\\
1/2 & 1/2& 1/2 & c & 0.7557 & 0.5413 & 0.9379 & 0.8207 & 0.5940 & 0.9389\\
\bottomrule
\end{tabular}
%}
%\end{sidewaystable}
\end{table}
%\end{landscape}

\subsubsubsection{The JVE method for one jet}
The $JVE$ method in his fixed order form can be easily extended to the
1-jet bin. Such approach has been used by the ATLAS collaboration for
the publication of the  Run-1 paper on the $h \to WW$ channel~\cite{ATLAS:2014aga}. This channels is, at the
moment, the decay channel that provides the most accurate
measurements of the Higgs boson production cross section and its couplings. 
Equations~(\ref{eq:schemes}) can be used also for  the 1-jet bin where $\epsilon_1$ represents the ratio of events with exactly one
jet over the number of events with at least one jet.
In scheme $a$  the NNLO $H+1$ jet cross section
is used. Such value has been nowadays evaluated~\cite{Caola:2015wna} but
software tools are still not publicly available, therefore the same
approach of~\cite{ATLAS:2014aga} will be followed in the following, assuming  that scheme $a$  lies between scheme $b$ and scheme $c$. Such assumption has been tested up to NLO,
and at NNLO for the $gg$ induced process at the $\sqrt{s}$ of 8 TeV.
Therefore, the average of schemes $b$ and $c$ is used instead of
the scheme $a$. Using the cross sections reported in Table \ref{tab:inclusive}, Table \ref{tab:Hplus1jet} and
Table \ref{tab:Hplus2jet} , the $\epsilon_1$ values are computed for schemes
$b$, $c$ and their average and tabulated in Table \ref{tab:i}. The uncertainty is evaluated as the envelope
of the average of schemes $b$ and $c$, computed for all renormalization
and factorization scales,  and the central values of the schemes  $b$
and $c$.


\begin{table}
\caption{\label{tab:i} H+1 jet efficiency of the process
  $pp \to H \to e^+ \nu e^- \bar{\nu}$ for several renormalization and
  factorization scale values.
  The values are computed with MCFM  at $\sqrt{s}=13$ TeV, R=0.4
  and and $p_{T} >25, 30$ GeV. The central value scale is chosen to be
  $m_H/2$. The Higgs boson mass is set at $m_H = 125.09$ GeV. PDF4LHC15 NNLO MC  are
used. }
\centering
\begin{tabular}{cc|ccc|ccc}
\toprule
\multicolumn{8}{c}{$\epsilon_1$ NLO} \\
\midrule
\multicolumn{2}{c}{} & \multicolumn{3}{|c|}{$p_T > 25$ GeV} &\multicolumn{3}{c}{$p_T > 30$ GeV} \\
\cmidrule{3-8}
$\mu_F/m_H$ & $\mu_R/m_H$ & $\epsilon_1^{b}$ & $\epsilon_1^{c}$ &
                                                                  $(\epsilon_1^b+\epsilon_1^c)/2$
                                             & $\epsilon_1^{b}$ & $\epsilon_1^{c}$ & $(\epsilon_1^b+\epsilon_1^c)/2$\\
\midrule
1 & 1 & 0.5777 & 0.6874 & 0.6326 &  0.6311 & 0.7335 & 0.6823\\
1 & 1/2 & 0.6517 & 0.8054 & 0.7285 & 0.6878 & 0.8215 & 0.7546\\
1/4 & 1/4 & 0.8976 & 1.035 & 0.9665 & 1.086 & 1.242 & 1.164
  \\
1/4 & 1/2 & 0.7533 & 0.9776 & 0.8654 & 0.7860 & 0.9865 & 0.8862\\
1/2 & 1 & 0.6549 & 0.8411 & 0.7480  & 0.6952 & 0.8615 &
                                                                  0.778337 \\
1/2 & 1/4 & 0.8167 & 0.9375 & 0.8771 & 0.8543 & 0.9692 &
                                                                   0.9117 \\
1/2 & 1/2 & 0.7142 & 0.9040 & 0.8091 & 0.7541 & 0.9308 & 0.8424\\

\bottomrule
\end{tabular}
\end{table}

\subsubsubsection{The JVE method final results}
 In Table \ref{tab:JVE_Final} we
summarize the jet veto efficiencies and their uncertainties for the 0-jet and
1-jet bin indicating,  in the 0-jet case,  both the resummed and  the fixed order results. 
We recommend to use the resummed result as reference values. 

\begin{table}
\caption{\label{tab:JVE_Final} Jet veto efficiency, exclusive jet bin
  cross sections and their  uncertainties for the $0$ and
  $1$ jet bin using the JVE method. The labels F.O. and RES. refer to
  the usage of fixed order and resummed inputs, respectively. The last four lines of the table
  show symmetrized uncertainty intervals.}
\centering
\begin{tabular}{c|c|c}
\toprule
\multicolumn{3}{c}{Summary of JVE related uncertainties.}\\
\midrule
& $p_T > 25$ GeV & $p_T > 30$ GeV\\
\midrule
$\epsilon_0$ RES.   &  0.57  & 0.64 \\
$\epsilon_0$ FO.   &  0.57  & 0.63 \\
$\Delta \epsilon_0$ RES. & [- 0.06,+0.18]  &[-0.06,  +0.18 ]  \\
$\Delta \epsilon_0$ F.O. & [-0.12, +0.36]  &[-0.10, +0.31]    \\
$\Delta \epsilon_0/\epsilon_0  $ RES. &[-0.11 , +0.32]  &[-0.09,
                                                          +0.28]    \\
$\Delta \epsilon_0/\epsilon_0  $ F.O. &[-0.21 , +0.63]  &[-0.16, +0.49]    \\

\midrule
$\epsilon_1$ F.O. & 0.81 & 0.84 \\
$\Delta \epsilon_1$ F.O.  &[ -0.18, +0.16] &[-0.16, +0.32] \\
$\Delta \epsilon_1/\epsilon_1$ F.O.  &[-0.22, +0.20]  &[-0.19, +0.38] \\
\midrule
$\Delta \sigma_0/\sigma_0$ F.O. &[-0.18, +0.37]  &[-0.12, +0.33]\\
$\Delta \sigma_1/\sigma_1$ F.O. &[-0.87, +0.36]  &[-0.86, +0.48]\\
$\Delta \sigma_0/\sigma_0$ RES. &[-0.13, +0.32]  &[-0.12, +0.29]\\
$\Delta \sigma_1/\sigma_1$ RES. &[-0.48, +0.25]  &[-0.54, +0.42]\\
\midrule
$\Delta \sigma_0/\sigma_0$ F.O. &[-0.37, +0.37]  &[-0.33, +0.33]\\
$\Delta \sigma_1/\sigma_1$ F.O. &[-0.87, +0.87]  &[-0.86, +0.86]\\
$\Delta \sigma_0/\sigma_0$ RES. &[-0.32, +0.32]  &[-0.29, +0.29]\\
$\Delta \sigma_1/\sigma_1$ RES. &[-0.48, +0.48]  &[-0.54, +0.54]\\
\bottomrule
\end{tabular}
\end{table}


\noindent In order to compare the JVE results with the ST ones, in the same
table we report the
0 ($\sigma_0$) and 1 ($\sigma_1$)  jet cross sections with the Jet Veto method.
They are computed from the Higgs  total cross section and the
jet veto efficiencies $\epsilon_0$ and $\epsilon_1$ according to the
following formulae:
\[
\sigma_0  = \sigma_{\rm tot} \cdot \epsilon_0 \quad \sigma_1 = \sigma_{\rm tot} \cdot (1 - \epsilon_0) \cdot \epsilon_1
\]

\noindent In this expression, the best available computation of the Higgs total cross section can be used because full factorization between total cross section and
 acceptance is assumed  in this approach. The cross section value computed using the De Florian-Grazzini method for resummation and
including the complex pole scheme is:
\[
 \sigma_{\rm tot} = 43.92 \Upb {}^{+7.4 \%}_{- 7.9 \%} {\rm (scale)}
 {}^{+7.1 \%}_{-6.0 \%} {\rm (PDF)}
\]
the PDF error is usually accounted independently in experimental analyses,
therefore only the scale variation related  error is accounted
for. Using simple error propagation, the central values and their
uncertainties have been computed and  summarized in Table \ref{tab:JVE_Final}.
