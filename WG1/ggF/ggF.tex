%\chapter{Gluon-gluon fusion}
\providecommand{\pTcut}{p_T^{\rm cut}}
\providecommand{\cut}{{\rm cut}}

We present here an update of developments since the publication of
YR3.   First,  recent results on the inclusive
cross-section are discussed, and we provide recommendations for the
computation of its value and uncertainty. Then, jet-binned cross sections are
examined:
we provide
benchmarks for differential transverse-momentum distributions in the
effective theory. Finally the effects of heavy-quark
masses on these distributions are examined.

%**************
\section{The inclusive cross-section}
The inclusive gluon fusion Higgs boson production cross-section has a slowly
convergent perturbative expansion in QCD with large corrections at NLO
and NNLO. Therefore, uncertainties due to missing higher orders have
always been large and comparable to PDF uncertainties. Recently,
N$^3$LO QCD corrections have been computed in the effective theory as
an expansion around threshold in Ref.~\cite{Anastasiou:2016cez}, along with the evaluation of threshold resummation in
different schemes. In
this section the results of
Ref.~\cite{Anastasiou:2016cez} are reviewed; then, results on
threshold resummation
at the N$^3$LL matched  first with the NNLO (Sect.~\ref{subsec:spira}) , and then
with the N$^3$LO  (Sect.~\ref{subsec:bonvini}) fixed-order results are
presented.
fixed or level and its matching to the fixed-order result.
Finally, we present the summary for the computation of the
central value including all known corrections and the associated
total uncertainty.

\subsection[The N\texorpdfstring{$^3$}{3}LO cross section]{The N\texorpdfstring{$^3$}{3}LO cross section\SectionAuthor{C.~Anastasiou, C.~Duhr, F.~Dulat, E.~Furlan, T.~Gehrmann, F.~Herzog, A.~Lazopoulos, B.~Mistlberger}}
\label{subsec:ADDFGHLM}
\input{./WG1/ggF/ADDFGHLM}

\subsection[N\texorpdfstring{$^3$}{3}LL resummation]{N\texorpdfstring{$^3$}{3}LL resummation\SectionAuthor{T.~Schmidt, M.~Spira}}
\label{subsec:spira}
\input{./WG1/ggF/spira}

\subsection[Combined fixed order and resummed results at N\texorpdfstring{$^3$}{3}LO+N\texorpdfstring{$^3$}{3}LL]{Combined fixed order and resummed results at N\texorpdfstring{$^3$}{3}LO+N\texorpdfstring{$^3$}{3}LL\SectionAuthor{M.~Bonvini, S.~Marzani}}
\label{subsec:bonvini}
\input{./WG1/ggF/bonvini}

\subsection[Summary for the total cross-section]{Summary for the total cross-section\SectionAuthor{S.~Forte, D.~Gillberg, C.~Hays, A.~Lazopoulos, G.~Petrucciani, A.~Massironi, G.~Zanderighi}}
\label{subsec:recommendation}
\input{./WG1/ggF/recommendation}

%**************
\section{Differential and jet-binned cross sections}
\input{./WG1/ggF/introveto.tex}

\subsection[General treatment of theory uncertainties in kinematic bins]{General treatment of theory uncertainties in kinematic bins\SectionAuthor{F.J.~Tackmann, K.~Tackmann}}
\label{subsec:binuncertainties}
\input{./WG1/ggF/binuncertainties}


\subsection[Exclusive fixed-order cross sections and jet-veto efficiencies at NNLO]{Exclusive fixed-order cross sections and jet-veto efficiencies at NNLO\SectionAuthor{B.~Di~Micco}}
\label{subsec:run1_method}
\input{./WG1/ggF/run1_method}


\subsection[Combined resummed predictions for the \texorpdfstring{$0$-jet, $1$-jet, and $\geq 2$-jet}{0-jet, 1-jet, and 2-or-more-jet} bins]{Combined resummed predictions for the \texorpdfstring{$0$-jet, $1$-jet, and $\geq 2$-jet}{0-jet, 1-jet, and 2-or-more-jet} bins\SectionAuthor{R.~Boughezal, X.~Liu, F.~Petriello, I.W.~Stewart, F.J.~Tackmann}}
\label{subsec:stwz_blptw}
\input{./WG1/ggF/stwz_blptw}

\subsection[Jet-vetoed Higgs cross section in gluon fusion at N\texorpdfstring{$^3$}{3}LO+NNLL]{Jet-vetoed Higgs cross section in gluon fusion at N\texorpdfstring{$^3$}{3}LO+NNLL with small-\texorpdfstring{$R$}{R} resummation\SectionAuthor{A.~Banfi, F.~Caola, F.A.~Dreyer, P.F.~Monni, G.P.~Salam, G.~Zanderighi, F.~Dulat}}
\label{sec:jve}
\input{./WG1/ggF/jve_n3lonnll}

\subsection[Higgs-\texorpdfstring{$\pt$}{pT} resummation in momentum space at NNLL+NNLO in gluon fusion]{Higgs-\texorpdfstring{$\pt$}{pT} resummation in momentum space at NNLL+NNLO in gluon fusion\SectionAuthor{P.F.~Monni, E.~Re, P.~Torrielli}}
\label{subsec:MRT}
\input{./WG1/ggF/mrt}

\subsection[NNLOJET: \texorpdfstring{$H + j$}{H+j} at NNLO using Antenna subtraction]{NNLOJET:: \texorpdfstring{$H + j$}{H+j} at NNLO using Antenna subtraction\SectionAuthor{X.~Chen, E.W.N.~Glover, T.~Gehrmann, M.~Jaquier}}
\label{sec:antenna}
\input{./WG1/ggF/antenna}

%**************
\section[Benchmarks for cross sections and differential distributions]{Benchmarks for cross sections and differential distributions\SectionAuthor{S.~Forte, D.~Gillberg, C.~Hays, A.~Lazopoulos, G.~Petrucciani, A.~Massironi, G.~Zanderighi}}
\label{sec:jbenchmarks}
\input{./WG1/ggF/benchmark}

%**************
\section[Effects of heavy-quark masses]{Effects of heavy-quark masses\SectionAuthor{F.~Krauss, S.~Kuttimalai, P.~Maierh\"ofer, M.~Sch\"onherr}}
\input{./WG1/ggF/heavyquark}

%**************
%\section*{Acknowledgements}

\vspace{0.5cm}
\noindent
{\bf Acknowledgements}

The editors are grateful to the following people who have provided  numbers used for
the benchmarks for cross sections and differential distributions presented in Section~\ref{sec:jbenchmarks}: Thomas Becher, Massimiliano Grazzini, 
Nicolas Greiner, Thomas L\"ubbert, Gionata Luisoni, Duff Neill, Matthias Neubert, Hayk Sargsian, Ira Rothstein, Varun Vaidya, Daniel Wilhelm and Jan Winter.
