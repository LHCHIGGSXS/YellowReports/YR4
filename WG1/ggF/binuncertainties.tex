Whenever the experimental measurements are separately performed in different kinematic regions (or bins),
the theoretical predictions and their uncertainties must also be evaluated separately for each
kinematic region. This is necessary also when the information from all measured bins is eventually combined in the interpretation,
since different bins can in general have different sensitivities and therefore contribute with different relative weights
to the final result. In this context, the correlations of the theoretical uncertainties for
different bins must be taken into account. This is particularly important whenever a binning cut induces an important
additional source of perturbative uncertainties that affects each bin but should cancel in their sum. This is precisely what happens in the context of jet binning, and it requires one to treat the uncertainties induced by the binning as anti-correlated between the bins~\cite{Berger:2010xi, Stewart:2011cf}. In general, to properly treat the theoretical uncertainties one should thus try to identify and distinguish different sources of uncertainties and take into account the correlation implied by each source.

\subsubsection{Single bin boundary}

We first review the case where the cross section is split into two bins by a single perturbatively nontrivial binning cut. To be concrete, we use the $0$-jet cross section as an important example.

In this case, the total inclusive cross section, $\sigma_\tot \equiv \sigma_{\geq 0}$, is divided into an exclusive $0$-jet bin, $\sigma_0(\pTcut)$, where the $p_T$ of the leading jet is restricted to $p_T^{\rm jet} < \pTcut$ and the remaining inclusive $1$-jet bin, where the leading jet is required to have $p_T^{\rm jet} \geq \pTcut$,
%%%
\begin{equation}
\sigma_{\geq 0} = \sigma_0(\pTcut) + \sigma_{\geq 1}(\pTcut)
\,.\end{equation}
%%%
Typically, the experimentally used $\pTcut$ values are smaller than the hard-interaction scale $\sim m_H$. In this case, the $\pTcut$ restriction induces Sudakov double logarithms~\cite{Tackmann:2012bt} $\ln(p_T^{\rm cut}/m_H)$ at each order in $\alpha_s$, which grow as $\pTcut$ is lowered. When $\pTcut$ is small enough for $\sigma_{\geq1}(\pTcut)$ to contain a nonnegligible fraction of the total cross section, this implies that the cut-induced perturbative corrections have a nontrivial influence on the perturbative series of $\sigma_0$, corresponding to an additional and a priori nonnegligible source of uncertainty that is not present in $\sigma_{\geq 0}$. (Note that this is irrespective of whether the cut effects are computable in fixed-order or resummed perturbation theory.)

The uncertainties involved in the jet binning can be described in general in terms of fully correlated and fully anticorrelated components, which amounts to parameterizing the uncertainty matrix for $\{\sigma_0, \sigma_{\geq 1}\}$ as
%%%
\begin{equation} \label{eq:Cgeneral}
C(\{\sigma_0, \sigma_{\geq 1}\}) =\!
\begin{pmatrix}
(\Delta^{\rm y}_0)^2 &  \Delta^{\rm y}_0\,\Delta^{\rm y}_{\geq 1}  \\
\Delta^{\rm y}_0\,\Delta^{\rm y}_{\geq 1} & (\Delta^{\rm y}_{\geq 1})^2
\end{pmatrix}
\!+
\begin{pmatrix}
 \Delta_\cut^2 &  - \Delta_\cut^2 \\
-\Delta_\cut^2 & \Delta_\cut^2
\end{pmatrix}
\!.\end{equation}
%%%
While this form was originally utilized in the context of the ST method~\cite{Stewart:2011cf, Gangal:2013nxa}, it
is simply a general parameterization of a $2\times2$ symmetric matrix, and not specific to a particular calculation or
framework for determining theory uncertainties. That is, the uncertainties obtained with any prescription can always be written in this form, provided sufficient information or assumptions on the correlations are available.

The parameterization in \eqref{eq:Cgeneral} proves convenient for two reasons:
First, the separation into independent components that are 100\% correlated or anticorrelated between the different observables allows for a straightforward implementation in terms of independent nuisance parameters for each component. That is, one has two nuisance parameters $\kappa^{\rm y}$ and $\kappa_\cut$ whose uncertainty amplitudes for $\{\sigma_{\geq 0}, \sigma_0, \sigma_{\geq 1}\}$ are
%%%
\begin{equation}
\kappa^{\rm y}: \quad \{ \Delta^{\rm y}_{\geq 0},\, \Delta^{\rm y}_{0},\, \Delta^{\rm y}_{\geq 1} \}
\qquad\qquad
\kappa_\cut: \quad \{ 0,\, \Delta_\cut, -\Delta_\cut \}
\,,\end{equation}
%%%
where $\Delta_{\geq 0}^{\rm y} = \Delta^{\rm y}_{0} + \Delta^{\rm y}_{\geq 1}$.
Hence, this provides a baseline for the experimental implementation, independent of a particular theoretical prediction.
Second, this parameterization admits a simple physical interpretation, which is very useful to identify and estimate each component for a given theory calculation: The first correlated component, denoted with a superscript ``y'', can be interpreted as an overall yield uncertainty of a common source for all bins. The second anticorrelated component can be interpreted as a migration uncertainty between the two bins, which is introduced by the binning and drops out in their sum.

The existing prescriptions for estimating perturbative uncertainties in jet binning and their justifications have been documented extensively before~\cite{Stewart:2011cf, Banfi:2012yh, Gangal:2013nxa, Dittmaier:2012vm, Heinemeyer:2013tqa}. Here, we only give a brief summary in order to illustrate the above for the case of the $0/1$-jet boundary. In fixed-order predictions, there is no way to unambiguously identify different sources for $\Delta^{\rm y}$ and $\Delta_{\rm cut}$, so one has to make some assumptions. Using a naive correlated scale variation for all jet bins amounts to setting $\Delta_i^{\rm y} \equiv \Delta_i^{\rm FO}$, where $\Delta_{i}^{\rm FO}$ are the default perturbative uncertainties estimated by the usual scale variations in the fixed-order predictions, while $\Delta_\cut \equiv 0$ is neglected. As mentioned above, once the binning effects become important, the associated migration uncertainty should not be neglected, otherwise this can easily lead to an underestimate. In the ST method~\cite{Stewart:2011cf}, this is avoided by taking
%%%
\begin{equation}  \label{eq:ST}
\text{ST}:\qquad
\Delta_0^{\rm y} = \Delta_{\geq 0}^{\rm y} = \Delta_{\geq 0}^{\rm FO}
\,,\quad
\Delta_{\geq 1}^{\rm y} = 0
\,,\qquad\qquad
\Delta_\cut = \Delta_{\geq 1}^{\rm FO}
\,.\end{equation}
%%%
That is, the migration uncertainty is approximated by the perturbative uncertainty of $\sigma_{\geq 1}(\pTcut)$, which is motivated by the structure of the perturbative series at small $\pTcut$. Maintaining as the total uncertainty for $\sigma_{\geq 1}$ its usual fixed-order uncertainty then requires setting $\Delta_{\geq 1}^{\rm y} = 0$. As a result, one effectively treats the usual fixed-order perturbative uncertainties in $\sigma_{\geq 0}$ and $\sigma_{\geq 1}$ as independent sources. This can also be generalized~\cite{Gangal:2013nxa}, by using an additional parameter $\rho$ to separate $\Delta_{\geq 1}^{\rm FO}$ into yield and migration parts, which then effectively determines the correlation between $\Delta_{\geq 0}^{\rm FO}$ and $\Delta_{\geq 1}^{\rm FO}$,
%%%
\begin{equation}  \label{eq:STrho}
\text{ST($\rho$)}:\qquad
\Delta_0^{\rm y} = \Delta_{\geq 0}^{\rm FO}
\,,\quad
\Delta_{\geq 1}^{\rm y} = \rho\, \Delta_{\geq 1}^{\rm FO}
\,,\qquad\qquad
\Delta_\cut = \sqrt{1 - \rho^2}\, \Delta_{\geq 1}^{\rm FO}
\,.\end{equation}
%%%

Another prescription is the JVE method~\cite{Banfi:2012yh}, which typically yields similar uncertainties for the same perturbative inputs. It relies on the assumption that the perturbative uncertainty in the $0$-jet fraction $\epsilon_0 = \sigma_0(\pTcut)/\sigma_{\geq 0}$ is uncorrelated with that of the inclusive cross section $\sigma_{\geq 0}$, i.e., the perturbative uncertainties in $\epsilon_0$ and $\sigma_{\geq 0}$ are treated as the independent sources. This amounts to taking
%%%
\begin{equation}  \label{eq:JVE}
\text{JVE}:\qquad
\Delta_{\geq 0}^{\rm y} = \Delta_{\geq 0}^{\rm FO}
\,,\quad
\Delta_0^{\rm y} = \epsilon_0\, \Delta_{\geq 0}^{\rm FO}
\,,\quad
\Delta_{\geq 1}^{\rm y} = (1 - \epsilon_0)\, \Delta_{\geq 0}^{\rm FO}
\,,\qquad
\Delta_\cut = \sigma_{\geq 0}\, \Delta(\epsilon_0)
\,.\end{equation}
%%%
This means that the relative yield uncertainties for all bins are equal to the relative uncertainty of the total cross section, $\Delta_i^{\rm y}/\sigma_i = \Delta_{\geq 0}^{\rm FO}/\sigma_{\geq 0}$.
In the migration uncertainty, $\Delta(\epsilon_0)$ is the perturbative uncertainty of $\epsilon_0$, which is obtained by considering its scale variation together with several variations of how to write its perturbative series that differ by higher-order terms. This also means that the total uncertainty for $\sigma_{\geq 1}$ is not just given by the usual $\Delta_{\geq 1}^{\rm FO}$. When used together with a resummed calculation, the fixed-order estimate of $\Delta(\epsilon_0)$ is replaced by its resummed counterpart, see Section~\ref{subsec:stwz_blptw}.

\subsubsection{Multiple bin boundaries}

We now move on to discuss the more general case where the total cross
section is split into multiple mutually exclusive bins, as is the case
in most experimental analyses. Specifically, this applies to the case
of
the simplified template cross section framework discussed in
Section~\ref{STCS}. A full implementation of the
theory-independent parameterization discussed here in the simplified
template cross sections would allow utilizing theoretical predictions
in a very flexible manner. In particular, it would enable easily
switching between different theory predictions in the interpretation
of the experimental measurements. 

With multiple bins, each bin can have more than one boundary and vice versa any given boundary can be shared by different bins.
To make this tractable in a systematic fashion, we first note that Eq.~\eqref{eq:Cgeneral} applies in an obvious way to any single bin boundary and binning cut when all additional subdivisions are removed. That is, a given binning cut, labelled ``$a/b$'', separates the cross section as $\sigma_{ab} = \sigma_a + \sigma_b$ with an associated migration uncertainty $\Delta_\cut^{a/b}$, which is anticorrelated between $\sigma_a$ and $\sigma_b$. (Note that $\sigma_{ab}$ is not necessarily the total cross section but can itself correspond to a bin; what is relevant is that the $a/b$ cut does not act outside of $\sigma_{ab}$.)
Including additional cuts that further subdivide $\sigma_a$ and $\sigma_b$, we have $\sigma_a = \sum_i \sigma_a^i$ and $\sigma_b = \sum_j \sigma_b^j$, where we have labelled the individual sub-bins according to whether they are part of $\sigma_a$ or $\sigma_b$. Since we interpret the $a/b$ boundary as a common uncertainty source, we can consider it as fully correlated among each set of sub-bins and implement it via a single nuisance parameter $\kappa_\cut^{a/b}$. The corresponding uncertainty amplitudes for all the bins are given as
%%%
\begin{equation}
\kappa_\cut^{a/b}: \quad \Delta_\cut^{a/b} \times \bigl\{\{ x_a^i \}, - \{ x_b^j \} \bigr\}
\qquad\text{with}\qquad
\sum_i x_a^i = \sum_j x_b^j = 1
\,,\end{equation}
%%%
where the parameters $x_a^i$ and $x_b^j$ specify how $\Delta_\cut^{a/b}$ gets distributed among the sub-bins. (With this information it is also straightforward to construct a corresponding uncertainty matrix, but this is not actually necessary.)

In this way, we can consider each binning cut (or bin boundary) as a potential source of an uncertainty with an associated nuisance parameter.
(Of course, in practice with sufficiently complicated bin boundaries, one has to apply some theoretical judgement in how one chooses the relevant independent binning cuts.) In addition, we can have an overall yield uncertainty correlated among all bins.

To illustrate this for a simple example, consider the case of 3 mutually exclusive jet bins $\{\sigma_0, \sigma_1, \sigma_{\geq 2}\}$. In this case, we can easily identify two bin boundaries as the two relevant sources of migration uncertainties, namely the cut on the leading jet separating $\sigma_{\geq 0} = \sigma_0 + \sigma_{\geq 1}$ and the cut on the 2nd jet separating $\sigma_{\geq 1} = \sigma_1 + \sigma_{\geq 2}$. In addition, we have an overall yield uncertainty. Considering the five (interdependent) observables $\{\sigma_{\geq 0}, \sigma_0, \sigma_{\geq 1}, \sigma_1, \sigma_{\geq 2} \}$, the three nuisance parameters and their respective uncertainty amplitudes are
%%%
\begin{align} \label{eq:3binnuisances}
\kappa^{\rm y}: &\quad \{ \Delta^{\rm y}_{\geq 0},\, \Delta^{\rm y}_{0},\, \Delta^{\rm y}_{\geq 1},\, \Delta^{\rm y}_{1},\, \Delta^{\rm y}_{\geq 2} \}
\quad\text{with}\quad
\Delta^{\rm y}_{\geq 0} = \Delta^{\rm y}_{0} + \Delta^{\rm y}_{\geq 1}
\,,\qquad
\Delta^{\rm y}_{\geq 1} = \Delta^{\rm y}_{1} + \Delta^{\rm y}_{\geq 2}
\,,\nn\\
\kappa_\cut^{0/1}: &\quad \Delta^{0/1}_\cut \times \{ 0,\, 1,\, -1,\, -(1 - x_1),\, -x_1 \}
\,,\nn\\
\kappa_\cut^{1/2}: &\quad \Delta^{1/2}_\cut \times \{ 0,\, x_2,\, -x_2,\, 1 - x_2,\, -1 \}
\,,\end{align}
%%%
Here, $x_1$ determines how $\Delta^{0/1}_\cut$ is split between $\sigma_1$ and $\sigma_{\geq 2}$, and $x_2$ determines how $\Delta_\cut^{1/2}$ is split between $\sigma_0$ and $\sigma_1$.
For $x_1\neq 0$, the $0/1$ migration into $\sigma_{\geq 1}$ can affect both $\sigma_1$ and $\sigma_{\geq 2}$. For
$x_1 = 0$, the $0/1$ migration happens entirely between $\sigma_0$ and $\sigma_1$, so the binning is effectively treated as $\sigma_{0+1} = \sigma_0 + \sigma_1$. Similarly, for $x_2 = 0$ the $1/2$ migration is contained within $\sigma_{\geq 1} = \sigma_1 + \sigma_{\geq 2}$. On the other hand, allowing for $x_2 \neq 0$ corresponds to the more general case $\sigma_{\geq 0} = \sigma_{<2} + \sigma_{\geq 2}$.

In terms of the uncertainty matrices for the 3 quantities $\{\sigma_0, \sigma_1, \sigma_{\ge 2}\}$, Eq.~\eqref{eq:3binnuisances} corresponds to
%%%
\begin{align}
C(\{\sigma_0, \sigma_1, \sigma_{\ge 2}\})
&= C^{\rm y}(\{\sigma_0, \sigma_1, \sigma_{\ge 2}\}) + C_\cut^{0/1}(\{\sigma_0, \sigma_1, \sigma_{\ge 2}\}) + C_\cut^{1/2}(\{\sigma_0, \sigma_1, \sigma_{\ge 2}\})
\,, \\
C^{\rm y}(\{\sigma_0, \sigma_1, \sigma_{\ge 2}\})
&= \begin{pmatrix}
(\Delta_0^{\rm y})^2 \;&\; \Delta_0^{\rm y} \Delta_1^{\rm y} \;&\; \Delta_0^{\rm y} \Delta_{\ge2}^{\rm y} \\
\Delta_0^{\rm y} \Delta_1^{\rm y} \;&\; (\Delta_1^{\rm y})^2 \;&\; \Delta_1^{\rm y} \Delta_{\ge2}^{\rm y} \\
\Delta_0^{\rm y} \Delta_{\ge2}^{\rm y} \;&\; \Delta_1^{\rm y} \Delta_{\ge2}^{\rm y} \;&\; (\Delta_{\ge2}^{\rm y})^2
\end{pmatrix}
\,, \\
C_\cut^{0/1}(\{\sigma_0, \sigma_1, \sigma_{\ge 2}\})
&= \bigl(\Delta_\cut^{0/1} \bigr)^2\begin{pmatrix}
1 \;&\; -(1-x_1) \;&\; -x_1 \\
-(1 - x_1) \;&\; (1 - x_1)^2 \;&\; x_1(1 - x_1) \\
-x_1  \;&\; x_1(1 - x_1) \;&\; x_1^2
\end{pmatrix}
\,, \\
C_\cut^{1/2}(\{\sigma_0, \sigma_1, \sigma_{\ge 2}\})
&= \bigl(\Delta_\cut^{1/2} \bigr)^2\begin{pmatrix}
x_2^2 \;&\; x_2(1 - x_2) \;&\; -x_2 \\
x_2(1 - x_2) \;&\; (1 - x_2)^2\;&\; -(1 - x_2) \\
-x_2 \;&\; -(1 - x_2) \;&\; 1
\end{pmatrix}
\,.\end{align}
Note that for the most generic case with 3 bins, one could in principle add an analogous third migration component $0/2$ for $\sigma_{\neq 1} = \sigma_0 + \sigma_{\geq 2}$. In this jet-binning example, this is clearly artificial, since $\sigma_0$ and $\sigma_{\geq 2}$ do not actually share a boundary and the migration between them is only indirect and already captured by the other two migration components.

The ST method applied to the case of 3 jet bins is equivalent to using
$\Delta_0^{\rm y} = \Delta_{\geq 0}^{\rm FO}$,  $\Delta_{1}^{\rm y} = \Delta_{\geq 2}^{\rm y} = 0$, 
$\Delta_\cut^{0/1} = \Delta_{\geq 1}^{\rm FO}$, $\Delta_\cut^{1/2} = \Delta_{\geq 2}^{\rm FO}$, and taking $x_1 = x_2 = 0$. This effectively considers $\sigma_{\geq 0}$, $\sigma_{\geq 1}$, and $\sigma_{\geq 2}$ as the independent sources, and in particular one can directly identify the corresponding nuisance parameters $\kappa_{\geq i}$ in existing implementations of the ST method as $\kappa^{\rm y} \equiv \kappa_{\geq 0}$, $\kappa_\cut^{0/1} \equiv \kappa_{\geq 1}$, and $\kappa_\cut^{1/2} \equiv \kappa_{\geq 2}$.
Section~\ref{subsec:stwz_blptw} discusses how estimates for the above parameters can be obtained when both jet boundaries are treated in resummed perturbation theory.

Mathematically speaking, the above generic parameterization has some redundancy, as it has more than the minimal number of six parameters that would be required to describe a general 3x3 symmetric matrix.
This is desired and makes it flexible enough to accommodate different scenarios while maintaining the simple physical interpretation in terms of the underlying sources. In contrast, using a mathematically minimal parameterization one would inevitably be forced to reexpress the contributions from several physically independent sources in terms of a minimal number of mathematically independent components and thus lose their physical meaning. In general, there can be several (independent) sources of theory uncertainties that give rise to each type of component. As for any other systematic uncertainty, these should then be implemented via separate nuisance parameters, which preserves their physical origin and in particular allows for the possibility to correlate them with other observables if necessary. For example, when electroweak and QCD corrections are treated as factorized, one could consider separately estimating the perturbative uncertainties of each, with each having their own set of nuisance parameters. (In practice, when the electroweak corrections are applied as an overall correction factor, they give rise to a yield uncertainty and a single nuisance parameter.)
