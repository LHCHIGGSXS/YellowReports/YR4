
%%%%%%%%%%%%%%%%%%
\begin{figure}[!b]
  \centering
  \includegraphics[width=0.49\textwidth]{WG1/HH/plot_m.pdf}
  \hfill
  \includegraphics[width=0.49\textwidth]{WG1/HH/plot_m2.pdf}
  \caption{\label{fig:nnlodiff} $\sqrts=8~\tev$ and $13~\tev$
    NNLO+NNLL cross section distribution for $\mt\to \infty$
    calculation detailed in the previous section, also showing the
    scale uncertainty\cite{deFlorian:2015moa}. For details see text.}
\end{figure}
%%%%%%%%%%%%%%%%%%

\section{Differential distributions}
\label{sec:hhdif}
The di-Higgs differential mass distribution of the NNLO+NNLL
calculation detailed in the previous section is shown in
\refF{fig:nnlodiff}.  This figure includes the higher order corrections
in the $\mt\rightarrow\infty$ limit.
Due to the intricate destructive interplay of the trilinear and box
contributions depicted in \refF{fig:feyndiag}, however,  the top mass
threshold significantly impacts the differential distributions for the
gluon fusion process, and the invariant di-Higgs boson mass differential cross section in particular.  
On the one hand, the momentum-dependent distributions of the di-Higgs system 
are exploited in phenomenological analyses (either implicitly
or explicitly), as they exhibit a highly sensitive response to
BSM-induced modifications of the SM coupling pattern (see below).  On
the other hand, experimental characteristics of a particular set of
selection cuts motivated from the desire to enhance signal over
background strongly depend on the transverse Higgs momentum (and
therefore on $\mhh$) selection thresholds; 
boosted Higgs kinematics~\cite{Dolan:2012rv,Gouzevitch:2013qca,Papaefstathiou:2012qe,Behr:2015oqq} are a
particularly drastic example of this. Both the theoretical appeal and
the experimental necessity of studying non-inclusive fiducial cross
sections have far-reaching consequences for di-Higgs analyses when we
extrapolate the findings of the previous section to realistic
selection criteria.

%\afterpage{
%\begin{landscape}
%
%%%%%%%%%%%%%%%%%%%
  \begin{figure}[!p]
    \centering
%$    \parbox{.72\textwidth}{ \includegraphics[height=.76\textwidth]{WG1/HH/pth8k.pdf}\hfill \caption{\label{fig:diffxsec1a}
    	\includegraphics[width=0.50\textwidth]{WG1/HH/pth8k.pdf}
	\caption{\label{fig:diffxsec1a}    
        Transverse  momentum distribution of the leading Higgs boson in \gev\ for $pp\to \hsm \hsm$
        using {\tt MG5\_aMC@NLO + Pythia8} at NLO with the {{\FTapp}}~approximation, for $\mhsm=125~\gev$, $\mt=172.5~\gev$, and
        $\sqrts =8~\tev$. The scales are chosen to be
        $\mu_R=\mu_F=\mhh/2$.  Scale and PDF uncertainties are added
        linearly in the distribution. The $K-$factor is defined as the ratio between NLO and LO cross sections. } %}
%    \hfill
%    \parbox{.72\textwidth}{ \includegraphics[height=.76\textwidth]{WG1/HH/mhh8k.pdf}\hfill \caption{\label{fig:diffxsec1b}
    	\includegraphics[width=0.50\textwidth]{WG1/HH/mhh8k.pdf} 
	\caption{\label{fig:diffxsec1b}    
        Invariant di-Higgs boson mass differential distribution in \gev\ for $pp\to
        \hsm \hsm$ using {\tt MG5\_aMC@NLO + Pythia8} at NLO
        with the {{\FTapp}}~approximation, for $\mhsm=125~\gev$,
        $\mt=172.5~\gev$, and $\sqrts =8~\tev$. The scales are chosen
        to be $\mu_R=\mu_F=\mhh/2$.  Scale and PDF uncertainties are
        added linearly in the distribution. The $K-$factor is defined as the ratio between NLO and LO cross sections. } %}
  \end{figure}
%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%
  \begin{figure}[!p]
    \centering
%    \parbox{.72\textwidth}{ \includegraphics[height=.76\textwidth]{WG1/HH/pth13k.pdf}\hfill \caption{\label{fig:diffxsec2a}
	\includegraphics[width=0.50\textwidth]{WG1/HH/pth13k.pdf}
	\caption{\label{fig:diffxsec2a}
        Transverse momentum  distribution of the leading Higgs boson in \gev\ for $pp\to \hsm \hsm$
        using {\tt MG5\_aMC@NLO + Pythia8} at NLO with the {{\FTapp}}~approximation, for $\mhsm=125~\gev$, $\mt=172.5~\gev$, and
        $\sqrts =13~\tev$. The scales are chosen to be
        $\mu_R=\mu_F=\mhh/2$.  Scale and PDF uncertainties are added
        linearly in the distribution.  The $K-$factor is defined as the ratio between NLO and LO cross sections.} %}
%    \hfill
%    \parbox{.72\textwidth}{ \includegraphics[height=.76\textwidth]{WG1/HH/mhh13k.pdf}\hfill \caption{\label{fig:diffxsec2b}
	\includegraphics[width=0.50\textwidth]{WG1/HH/mhh13k.pdf}
	\caption{\label{fig:diffxsec2b}
        Invariant di-Higgs boson mass differential distribution in \gev\ for $pp\to
        \hsm \hsm$ using {\tt MG5\_aMC@NLO + Pythia8} at NLO
        with the {{\FTapp}}~approximation, for $\mhsm=125~\gev$,
        $\mt=172.5~\gev$, and $\sqrts =13~\tev$. The scales are chosen
        to be $\mu_R=\mu_F=\mhh/2$.  Scale and PDF uncertainties are
        added linearly in the distribution.  The $K-$factor is defined as the ratio between NLO and LO cross sections.} %}
  \end{figure}
%%%%%%%%%%%%%%%%%%
%\end{landscape}
%}

In this section we present some distributions obtained using the {{\FTapp}}~approximation for the NLO results to establish to which extent
approximate NLO event generators can be used, and to which extent this
calculation provides a guideline to relate fiducial cross sections to
inclusively modeled quantities. We also comment on the impact of
higher order corrections beyond the \FTapp~approximation that have
become available \cite{Borowka:2016ehy,Borowka:2016ypz} while this report was
completed.  Technically, the  {{\FTapp}} calculation is
performed using matched {\tt MG5\_aMC@NLO + Pythia8}
simulations~\cite{Alwall:2014hca,Frederix:2014hta}. As already mentioned, this
approximation contains exact, full $\mt$-dependent real emission
contributions combined with a finite $\mt$ Born-reweighted
$\mt\to\infty$ calculation of the virtual loop corrections to obtain
an approximation of the fully differential NLO cross section.

We show in Figs.~\ref{fig:diffxsec1a} - \ref{fig:diffxsec2b} the \FTapp~distributions for the $\hsm\hsm$ invariant mass and the leading
Higgs boson $p_T$, including PDF and scale uncertainties for centre-of-mass energies of
$8~\tev$ and $13~\tev$. These distributions also rely on the
{\tt PDF4LHC15\_nlo\_mc} sets with 30 replicas. The scale variation dominates over the PDF
uncertainty leading to a rather flat $+30\%,~ -25\%$ uncertainty over a
broad, phenomenologically interesting energy regime, calculated from a
scale variation that is again $\mu_0/2<\mu<2 \mu_0$, for
$\mu_0=\mhh/2$. 
The scale uncertainty is similar when considering the full NLO QCD corrections, see Figs.~\ref{fig:mhhfull} - \ref{fig:pthardfull100}. 

For some observables, for example all rapidity distributions, the differential QCD
corrections are simple rescalings of the LO distribution with the
total K factor in the \FTapp~scheme. 
%These include the di-Higgs boson invariant mass $\mhh$, the
%subleading Higgs boson transverse momentum $p_{T,h_2}$, and all rapidity
%distributions. 
For most distributions, however, the discrepancy between  the \FTapp~and the full result moves out of the scale variation uncertainty band in the tail of the distributions.
This is shown for the di-Higgs boson invariant mass $\mhh$-distribution in \refF{fig:mhhfull} and for the
leading Higgs boson transverse momentum $p_{T,h_1}$ in Figures~\ref{fig:pthardfull14} and \ref{fig:pthardfull100}.

While the \FTapp~scheme provides a reasonable approximation in the low energy regime within uncertainties, the virtual corrections in the finite $m_t$ limit significantly soften the high energy events compared to the \FTapp~approximation, see \refF{fig:mhhfull}. These corrections can be as big as 30\% and a corresponding reweighting or associated uncertainty needs to be included in analyses that particularly focus on the phase space region $m_{hh}\gtrsim 400~\text{GeV}$.
The full NLO QCD corrections have a significant effect on the $p_T$ distribution (both the distribution of the leading-$p_T$ Higgs boson, $p_{T,h_1}$,  
and the one of any Higgs boson, $p_{T,h}$), as can be seen in 
Figs.~\ref{fig:pthardfull14} - \ref{fig:pthfull}.  
Both the HEFT and \FTapp  approximation fail to reproduce the distribution above the top mass. 

The leading jet transverse momentum and the transverse momentum of
the di-Higgs system are also  not well approximated
by a rescaling of the LO results.  It should be noted that the leading jet transverse momentum distribution is only
known to leading order precision and should be considered with care by
including LO uncertainties. For the $p_{T}(hh)$ distribution, resummed results at NLL+NLO, including the full NLO mass dependence in the $p_T(hh)\to 0$ region, 
have been obtained recently~\cite{Ferrera:2016prr}.

%%%%%%
\begin{figure}[t]
\centering
\includegraphics[width=0.50\textwidth]{WG1/HH/mhh.pdf}
\caption{\label{fig:mhhfull} Invariant di-Higgs boson mass distribution for
  various approximations, taken from~\cite{Borowka:2016ehy}. The red curves include the complete
  $\mt$ dependent NLO calculation. The uncertainty is computed by varying the scales by a factor 2 around $\mhh/2$. The Higgs boson mass is chosen to be $\mhsm=125~\gev$ and $\mt=173~\gev$, the centre of mass energy is $\sqrts=14~\tev$.}
\end{figure}
%%%%%%

%%%%%%%%%%%%%%%%%%%
  \begin{figure}[!p]
    \centering
%    \parbox{.5\textwidth}{ \includegraphics[height=.49\textwidth]{WG1/HH/pth_hard_14TeV_Kfac.pdf}\hfill \caption{\label{fig:pthardfull14}
     	\includegraphics[width=0.50\textwidth]{WG1/HH/pth_hard_14TeV_Kfac.pdf}
     	\caption{\label{fig:pthardfull14}   
        Transverse momentum  distribution of the leading Higgs boson in \gev\ for $pp\to \hsm \hsm$ 
         at NLO with the full top mass dependence, taken from~\cite{Borowka:2016ypz}, for $\mhsm=125~\gev$, $\mt=173~\gev$, and
        $\sqrts =14~\tev$. The scales are chosen to be
        $\mu_R=\mu_F=\mhh/2$.} %}
%    \hfill
%    \parbox{.5\textwidth}{ \includegraphics[height=.49\textwidth]{WG1/HH/pth_hard_100TeV_Kfac.pdf}\hfill \caption{\label{fig:pthardfull100}
   	\includegraphics[width=0.50\textwidth]{WG1/HH/pth_hard_100TeV_Kfac.pdf}
	\caption{\label{fig:pthardfull100}    
         Transverse momentum  distribution of the leading Higgs boson in \gev\ for $pp\to \hsm \hsm$ 
         at NLO with the full top mass dependence, taken from~\cite{Borowka:2016ypz}, for $\mhsm=125~\gev$, $\mt=173~\gev$, and
        $\sqrts =100~\tev$. The scales are chosen to be
        $\mu_R=\mu_F=\mhh/2$.} %}
  \end{figure}
%%%%%%%%%%%%%%%%%%


\begin{figure}[t]
\centering
\includegraphics[width=0.49\textwidth]{WG1/HH/pth_kfac.pdf}
\caption{\label{fig:pthfull} $p_{T,h}$  distribution for
  various approximations, taken from~\cite{Borowka:2016ehy}. The red curves include the complete
  $\mt$ dependent NLO calculation. The uncertainty is computed by varying the scales by a factor 2 around $\mhh/2$. The Higgs boson mass is chosen to be $\mhsm=125~\gev$ and $\mt=173~\gev$, the centre of mass energy is $\sqrts=14~\tev$.}
\end{figure}

Other uncertainties of equal importance become particularly apparent from investigating different
shower and matching/merging approaches that we detail in the
following. The comparison we detail in the following is based on the MC@NLO
calculation that appears in~\cite{Maltoni:2014eza} generated using
\texttt{MG5\_aMC@NLO}~\cite{Alwall:2011uj, Alwall:2014hca}
and the merged calculation of~\cite{Maierhofer:2013sha} using
\texttt{OpenLoops} matrix elements~\cite{Cascioli:2011va}. All the
results use the \texttt{HERWIG++} general-purpose Monte Carlo for the
parton shower~\cite{Bahr:2008pv, Gieseke:2011na,
  Arnold:2012fq,Bellm:2013hwb,Bellm:2015jjp}. The Higgs bosons are
taken to be stable and hadronization and the underlying event
simulation is turned off. The central
factorization/renormalization scale for both calculations is chosen
to be $\mu_0 = \mhh/2$. This scale is varied between $\mu = \mu_0/2$ and
$\mu = 2 \mu_0$ in the merged calculation. Furthermore, the merging
scales used for the MLM procedure were varied in the range 40-90~GeV in steps of 10~GeV and a
uniform smooth function, as described in~\cite{Maierhofer:2013sha}, of
widths $\epsilon = 10, 20, 30$~GeV is used.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
  \centering
  \parbox{0.49\textwidth}{\includegraphics[width=0.49\textwidth]{WG1/HH/H_pT.pdf}}\hfill
  \parbox{0.49\textwidth}{\includegraphics[width=0.49\textwidth]{WG1/HH/H_y.pdf}}\\
  \parbox{0.49\textwidth}{\includegraphics[width=0.49\textwidth]{WG1/HH/jet_pT_1.pdf}}\hfill
  \parbox{0.45\textwidth}{\vspace{0.6cm}\caption{{{Comparisons of
      distributions of observables in di-Higgs boson production. We
      show the transverse momentum of (any) Higgs boson, $p_{T,h}$, in
      the top left figure, the rapidity of (any) Higgs boson, $y_h$,
      in the top right figure and the transverse momentum of the hardest
      jet $p_T(\text{jet}_1)$, in the
      figure at the bottom. The uncertainty band corresponds to the scale uncertainties described above.}}\label{fig:dihiggs1}}}\\[0.2cm]
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[htb]
  \centering
  \parbox{0.49\textwidth}{\includegraphics[width=0.49\textwidth]{WG1/HH/HH_mass.pdf}}\hfill
  \parbox{0.49\textwidth}{\includegraphics[width=0.49\textwidth]{WG1/HH/HH_dR.pdf}}
  \parbox{0.49\textwidth}{\includegraphics[width=0.49\textwidth]{WG1/HH/HH_pT.pdf}}\hfill
  \parbox{0.45\textwidth}{\vspace{0.6cm}\caption{{{Comparisons of
      distributions of di-Higgs observables. We show the invariant
      mass of the di-Higgs system, $\mhh$, in the top left figure, the
      separation between the two Higgs bosons, $\Delta R(h,h)$, in the
      top right figure and the transverse momentum of the di-Higgs system, 
      $p_T({hh})$ in the lower
      figure. The uncertainty band corresponds to the scale uncertainties described above.}}\label{fig:dihiggs2}}}\\[0.6cm]
\end{figure} 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We present comparisons of the MC@NLO samples, labelled `mc@nlo', the
merged calculation, labelled `mlm' and the pure leading-order
calculation, labelled `shower', all showered with \texttt{HERWIG++} in
Figs.~\ref{fig:dihiggs1} and~\ref{fig:dihiggs2}. We show,
respectively, distributions for the transverse momentum and rapidity
of (any) single Higgs boson, the transverse momentum of the hardest
jet, the invariant mass of the di-Higgs boson pair, the di-Higgs boson transverse
momentum and the separation between the Higgs bosons. All
distributions are normalized to unity. The di-Higgs boson invariant
mass and separation between the Higgs bosons, as well as the single
Higgs observables, are in good agreement within the uncertainties of
the merged calculation {{indicated by the band}}. 
There exists a discrepancy in the di-Higgs
transverse momentum and the transverse momentum of the hardest jet.
One of the reasons that could explain this effect is the difference in the choice
of the dynamical starting scale of the shower $Q$.
\refF{fig:scales} provides the differential distribution of the $\hsm\hsm$ production cross section
as function of $Q$. We can clearly see that the choice of the scale in the MC@NLO sample
differs from MLM sample. 
This observation suggests that further assessment of the
systematics owing to the parton shower should be performed, a task left
to future studies.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h!tb]
\centering
 \centering
  \includegraphics[width=0.6\textwidth]{WG1/HH/scale.pdf}
  \caption{The dynamical starting scale for the \texttt{HERWIG++} shower used in
    each of the calculations. }\label{fig:scales}
\end{figure} 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
