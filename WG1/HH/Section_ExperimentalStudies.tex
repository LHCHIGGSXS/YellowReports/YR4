\clearpage
\section{Experimental results}

In Run~1 ATLAS and CMS performed searches for BSM di-Higgs boson production in gluon-gluon fusion process assuming resonant and nonresonant hypotheses. Taking into account the Higgs bosons decays, four different final states were explored.
One search requires both Higgs bosons to decay to $b\bar{b}$, %~\cite{Aad:2015uka, Khachatryan:2015year, Khachatryan:2016cfa}, 
that is the largest decay branching fraction within the SM.
In other two the second Higgs boson decays to $\gamma\gamma$ or $\tau\tau$ final states that help to reduce the SM background. 
The fourth channel, explored by ATLAS, features one  Higgs boson decaying to $WW^*$ with a subsequent leptonic decay and the other to $\gamma\gamma$.
A summary of the searches, obtained assuming a di-Higgs boson production through a spin-0 resonance in s-channel with a negligible natural width, is shown in \refF{fig:combined_plot}\footnote{One may notice than for some of the analyses a spin-2 interpretation is also available as well as an interpretation assuming a significant natural width~\cite{Aad:2015uka, Khachatryan:2015yea}.}. To compare different final states the decays branching fractions of the Higgs boson are assumed to be those of the SM.
Limits are provided from $\mXsz = 260$~GeV to $\mXsz = 3$~TeV and span over 3 orders of magnitude from typically 1-10 pb around the lowest edge and 1-10 fb around the highest edge. They are interpreted in the context of two simplified scenarios of the Minimal Supersymmetric Standard Model, 2 Higgs Doublet Model and Warped Extra Dimensions.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h!tb]
\centering
 \centering
  \includegraphics[width=0.9\textwidth]{WG1/HH/Combined_HH_plot_RunI.pdf}
  \caption{Comparison of the observed and expected 95\% confidence level (CL) upper limits on the product of cross section and the
  branching fraction $\sigma(pp \rightarrow \Xsz) \times \mathcal{B}(\Xsz \rightarrow \hsm\hsm)$. We assuming a narrow-width approximation for $\Xsz$ and SM branching fractions for Higgs boson decays. Results are provided by ATLAS and CMS collaborations based on results from Run~1 data taking period.\label{fig:combined_plot}}
\end{figure} 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The ATLAS collaboration performed searches at $\sqrt{s} = 8$~TeV
using an integrated luminosity of $20.3$~fb$^{-1}$~\cite{Aad:2014yja, Aad:2015uka, Aad:2015xja} and subsequently combined them for $\mXsz < 1$~TeV hypothesis in Ref.~\cite{Aad:2015xja}. The latter result is shown on \refF{fig:combined_plot} complemented with  $b\bar{b}b\bar{b}$ results for $\mXsz > 1$~TeV hypothesis.

Similar searches were performed by the CMS collaboration in $\gamma\gamma b\bar{b}$~\cite{Khachatryan:2016sey}, $\tau\tau b\bar{b}$~\cite{Khachatryan:2015tha, CMS:2016zxv, CMS:2015zug}, $b\bar{b} b\bar{b}$ \cite{Khachatryan:2015yea, Khachatryan:2016cfa} using a data sample of $17.9$ to $20.3$~fb$^{-1}$ depending on the analysis. The results obtained by different analyses looking at an identical final state are shown in \refF{fig:combined_plot} with the same colour. In particular in the case of $\tau\tau b\bar{b}$ final state, Ref.~\cite{Khachatryan:2015tha} was optimized to look for low-mass region $\mXsz < 350$~GeV; Ref.~\cite{CMS:2016zxv} concentrates its efforts on middle-mass region $300 < \mXsz < 1000$~GeV using boosted taus; Ref.~\cite{CMS:2015zug} extends the search up to 2.5 TeV looking at boosted Higgs bosons decaying to nearby taus and jets. Similar logic divides the low-mass~\cite{Khachatryan:2015yea}, $\mXsz < 1100$~GeV, and high-mass~\cite{Khachatryan:2016cfa}, $\mXsz > 1150$~GeV, searches in $b\bar{b} b\bar{b}$ final state.

The properties discussed below are valid for the results from both ATLAS and CMS collaborations.
In general, we observe that at low mass, $\mXsz \lesssim 350$~GeV, $\gamma\gamma b\bar{b}$ channel is the most sensitive one. It benefits from a good trigger efficiency looking online for a pair of photons, a good reconstruction efficiency of this pair and a low SM background. In contrast, above 500 GeV the most sensitive channel is $b\bar{b} b\bar{b}$. At high mass the trigger efficiency looking for 3 or 4 b-tagged jets with high $p_T$ improves compared to the low mass. Therefore the branching fraction of this channel provides a decisive advantage. The properties of the $\tau\tau b\bar{b}$ channel are intermediate between these two. The sensitivity of different channels crosses around $\mXsz \approx 400-500$~GeV depending on the exact details of each analysis. Finally the $\gamma\gamma WW^*$ channel is the less sensitive one since it benefits from all the advantages of $\gamma\gamma b\bar{b}$ channel, but suffers from a significantly lower branching fraction and reconstruction efficiency of $h \rightarrow WW^*$ compared to $h \rightarrow b\bar{b}$.

The searches for nonresonant Higgs boson pair production assuming SM like kinematics was performed by ATLAS in all four channels followed by a subsequent combination~\cite{Aad:2015xja} and by CMS in  $\gamma\gamma b\bar{b}$~\cite{Khachatryan:2016sey} and $\tau\tau b\bar{b}$~\cite{CMS:2016zxv} channels. The results are shown in Table \ref{tab:nonresonant}. The observed limits exceeds by at least a factor of 40 the total SM cross section provided in Section \ref{sec:hhxsec}. 
The CMS collaboration performed also a first generic nonresonant search within the framework of EFT using the $\gamma\gamma b\bar{b}$ final state~\cite{Khachatryan:2016sey}. This search was designed to exploit the shape properties of the nonresonant $m_{hh}$ spectrum already discussed in Section \ref{sec:hhbsm}. The limits were interpreted in term of parameters $\kappa_\lambda$, $\kappa_t$, and $c_2$ excluding $|c_2| > 3$ and $\kappa_\lambda < - 17.5$ or $\kappa_\lambda > 22.5$.

Although the Run~1 results are still far away from being sensitive to the SM $hh$ production we can already drive interesting conclusions for future analyses. The sensitivity of $\gamma\gamma b\bar{b}$, $\tau\tau b\bar{b}$ and $b\bar{b} b\bar{b}$ is similar for the SM like nonresonant search and for the resonant search with $\mXsz \approx 400$~GeV. This is not a coincidence, indeed the SM $m_{hh}$ spectrum exhibits a broad peak around 400 GeV. This means that a measurement of the SM $hh$ production would equally benefit from a combination of those three channels. This observation confirms the prospect from the ATLAS~\cite{ATL-PHYS-PUB-2014-019, ATL-PHYS-PUB-2015-046} and CMS~\cite{Butler:2020886} collaborations for the HL-LHC program. One shall notice that those prospects includes also $WW^*b\bar{b}$ as a promising channel~\cite{Butler:2020886} .

\begin{table}%[!t]
\caption{\label{tab:nonresonant} Comparison of the observed and expected 95\% CL upper limits on the nonresonant cross section $\sigma(pp \rightarrow \hsm\hsm)$ assuming SM-like kinematics.}
\begin{center}
\def\arraystretch{1.3}
\begin{tabular}{cc|ll}
\toprule
Channel &  Experiment &  Observed (pb) &  Expected (pb) \\
\midrule
$\gamma\gamma b\bar{b}$ & ATLAS & 2.2 &  1.0 \\
$\gamma\gamma b\bar{b}$ & CMS & 0.71 & 0.60 \\
$\tau\tau b\bar{b}$  & ATLAS & 1.6 & 1.3 \\
$\tau\tau b\bar{b}$  & CMS & 0.59 & 0.94\\
$b\bar{b} b\bar{b}$  & ATLAS & 0.62 & 0.62 \\
$\gamma\gamma WW^*$  & ATLAS & 11.0 & 6.7 \\
\midrule
Combination & ATLAS & 0.69 & 0.47 \\
\bottomrule
\end{tabular}
\end{center}
\end{table}

