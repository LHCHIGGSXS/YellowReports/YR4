
\section{Introduction}
\label{sec:hhintro}
In the SM, the Higgs self-couplings are uniquely determined by the structure of the scalar
potential,
\begin{equation}
V={\mhsm^2\over 2}\hsm^2+\lambda_3 v \hsm^3 +{\lambda_4\over 4}\hsm^4\, ,
\end{equation}
where $\lambda_3=\lambda_4={\mhsm^2/(2 v^2)}$.  Experimentally measuring $\lambda_3$ and
$\lambda_4$ is thus a crucial test of the mechanism of electroweak symmetry breaking.  A measurement of $\lambda_3$
requires double Higgs boson production while $\lambda_4$ is first probed in the production of $3$ Higgs bosons.

The phenomenology of multi-Higgs boson final states will provide
complementary information to that found from single Higgs physics at the LHC. Due to generically
small inclusive cross sections and a difficult signal vs. background
discrimination, the best motivated multi-Higgs final states at the Large
Hadron Collider are Higgs boson pair final states, of which gluon fusion
$gg\to \hsm\hsm$ is the dominant production mode.  

%{\bft{REPLACE THIS WITH EXPERIMENTAL SUMMARY: Analyses by ATLAS~\cite{Aad:2015xja}
% and CMS based on Run 1 data imply that
%the rate is no larger than $70$ times the SM prediction.}}
%In Run 1 ATLAS has searched for Higgs boson pair production with the 
%$bb\gamma\gamma$, $bbbb$, $ bb\tau\tau$, $\gamma\gamma WW*$ final states~\cite{Aad:2015xja}. 
%Combining 20.3 fb of data in four channels, the upper limits of 0.69 pb 
%have been set on the production cross-section, corresponding to 70 times 
%the SM di-Higgs boson production.
%The data have been also interpreted in the context of two simplified 
%scenarios of the Minimal Supersymmetric Standard Model.  
%Cross-section limits of Higgs boson pair production from a heavy narrow-width 
%Higgs boson decay are set as a function of the heavy Higgs boson mass. 
%The observed (expected) limits range from 2.1 (1.1) pb at 260 GeV to 0.011 (0.018) pb at 1000 GeV assuming SM Higgs boson decay branching fractions.

%Similar searches was performed by the CMS collaboration in $bb\gamma\gamma$~\cite{CMS:2014ipa}, $bbbb$~\cite{Khachatryan:2015year, Khachatryan:2016cfa},
%$bb\tau\tau$~\cite{Khachatryan:2015tha} and in multilepton and diphoton~\cite{Khachatryan:2014jya} final states. Cross-section lim%its of Higgs boson pair production from a heavy narrow-width Higgs boson decay was set for each of those channels. The most sensitive channel at 260 GeV was observed to be $bb\gamma\gamma$ with an observed (expected) limit of 1.2 (0.8) pb. It benefits in the low mass region from a good trigger and reconstruction efficiency and low background. In contrast at 1000 GeV the most sensitive channel was $bbbb$ with an observed (expected) limit of 0.023 (0.034) pb. The highest exclusion point is provided at 3 TeV with an observed (expected) limit of 0.008 (0.010) pb. At high mass the branching fraction of this channel provides a decisive advantage. The properties of the $bb\tau\tau$ channel are intermediate between these two. The sensitivity of different channels cross around 400 GeV.

Many models of physics beyond the Standard Model with SM-compatible single
Higgs boson signal strengths can exhibit a di-Higgs phenomenology vastly different from
the SM expectation. In this sense, a successful discovery of Higgs boson pair
production at the LHC and the subsequent measurement of potential
deviations from the SM constitute an important avenue in the search for
physics beyond the SM. In particular, modifications of the Higgs trilinear 
couplings (e.g. via a modified Higgs self interaction) can
only be directly observed in Higgs boson pair production.  In the
gluon fusion process this occurs via the
interference of the box and triangle diagrams shown in
\refF{fig:feyndiag}~\cite{Plehn:1996wb,Djouadi:1999rca,Glover:1987nx}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[!b]
  \begin{center}
    \includegraphics[scale=1.1]{WG1/HH/feyndiag.pdf}
    \caption{\label{fig:feyndiag} Feynman diagrams contributing to Higgs boson pair
      production via gluon fusion at leading order.}
  \end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

To facilitate such a measurement, it is crucial to establish the Higgs boson pair 
production cross section in the SM to the best theoretical
accuracy possible and to provide BSM benchmarks that reflect the phenomenology of Higgs boson pairs at the LHC in a consistent and concise fashion. 

This report summarizes the results of the HH cross section group of the 2014-2015 LHC Higgs Cross Section
working group that aims to establish SM predictions for a range of dominant and subdominant Higgs boson pair production modes at the LHC at the highest available theoretical precision. In addition, we provide benchmarks for resonant and non-resonant extensions of the SM phenomenology of Higgs boson pairs in light of current single Higgs property measurements which are aligned with the efforts of other subgroups.
This note is structured as follows: In \refS{sec:hhxsec} we provide an update on the dominant Higgs boson pair production modes at the LHC; special care is devoted to the dominant gluon fusion production mode in \refS{sec:hhgf}.  Section~\ref{sec:hhdif} contains representative distributions at NLO for the SM gluon fusion pair production channel. Section~\ref{sec:hhbsm} discusses benchmarks of motivated BSM scenarios. The BSM phenomenology of multi-Higgs final states can be divided into resonant and non-resonant extensions of the SM. The latter is discussed in \refS{sec:hheft} using the language of Effective Field Theory. Benchmarks for resonant di-Higgs final state searches are discussed using the singlet-extended SM and the 2 Higgs doublet model in \refS{sec:hhsinglet}, which provide theoretically clean avenues to introduce new resonant physics into Higgs boson pair production.


