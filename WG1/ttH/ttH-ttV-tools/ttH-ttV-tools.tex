\subsection{Comparison of NLO QCD predictions for differential distributions}
\label{sec:ttH-ttV-tools}

In this section we provide a comparison among fixed-order NLO QCD predictions 
of differential distributions for $\ttv$ ($V=Z,W^\pm$) production at
the LHC with $\sqrt{s}=13\UTeV$, obtained using the following tools:
\begin{itemize}
\item \Sherpa~2.2.0 ~+~\Openloops~1.2.3,
\item \MGfiveamcnlo~2.3.2,
\item \Powhel.
\end{itemize}
The impact of EW corrections has been discussed in
Section~\ref{sec:ttH-ttV-QCD+EW}. Here the main goal is only to assess
the agreement and compatibility of existing fixed-order NLO QCD
calculations at the level of differential cross sections. Future
studies will test the compatibility of different implementations of
these calculations which also include parton-shower effects. In this
context, all the tools listed above have been used as fixed-order QCD
Monte Carlo generators.

\Sherpa+\Openloops\, uses \Openloops~\cite{Cascioli:2011va} as a
one-loop generator, and relies on the \Cuttools\,
library ~\cite{Ossola:2007ax} for the numerically stable
evaluation of tensor integrals. Real-emission
contributions, infrared subtractions based on the Catani-Seymour
technique~\cite{Catani:1996vz,Catani:2002hc}, and phase-space
integration are handled by
\Sherpa~\cite{Schumann:2007mg,Hoeche:2011fd,Hoche:2012wh}.

In \MGfiveamcnlo~\cite{Hirschi:2011pa,Alwall:2014hca}, fixed-order
NLO QCD results are obtained by adopting the FKS
method~\cite{Frixione:1995ms,Frixione:1997np} for the subtraction of
the infrared divergences of the real-emission matrix elements
(automated in the module \MadFKS~\cite{Frederix:2009yq}), and the OPP
integral-reduction procedure~\cite{Ossola:2006us} for the computation
of the one-loop matrix elements (automated in the module
\Madloop~\cite{Hirschi:2011pa}).

Finally, the \Powhel\, generator~\cite{Garzelli:2012bn} uses the \Helacnlo\,
package~\cite{Bevilacqua:2011xh} for the computation of all matrix
elements provided as input to the \Powhegbox.  The \Powhegbox\,
framework~\cite{Nason:2004rx,Frixione:2007vw,Alioli:2010xd} adopts the
FKS subtraction scheme~\cite{Frixione:1995ms,Frixione:1997np} to
factor out the infrared singularities of the real-emission cross
section, while the the virtual one-loop matrix elements can be
provided with different methods.

For this comparison all input parameters are chosen according to the
HXSWG recommendations~\cite{LHCHXSWG-INT-2015-006}, and the hadronic cross
section is obtained using the PDF4LHC15~\cite{Butterworth:2015oua}
and a five-flavour scheme (5FS).  Renormalization ($\muR$) and
factorization ($\muF$)
scales have been fixed to a dynamical central value
$\mu_0=H_{\mathrm{T}}$, where $H_{\mathrm{T}}$ is the sum of the
transverse energies of the $\ttv$ final state ignoring extra jet
emission. The  theoretical uncertainty has been
estimated by varying both $\muR$ and $\muF$ by a factor of 2 about $\mu_0$.

Results for the following distributions:
\begin{itemize}
\item transverse momentum ($p_{\mathrm{T}}$)  of the top quark,
  vector boson, $t\bar{t}$ system, and $\ttv$ system;
\item pseudorapidity ($\eta$) of the top quark and
  vector boson,
\end{itemize}
are presented in Figs.~\ref{fig:ttWp_stable_NLOPS_1}-\ref{fig:ttz_stable_NLOPS_2}, for the case of $V=W^+,W^-$, and
$Z$, respectively.


%\section*{Plots for $\ttbar W^+$ production}

\providecommand{\ttwpplot}[2]{
\includegraphics[width=0.85\textwidth, trim=0 #1 0 0,clip]{\ttHttvtoolspath/ttwp/#2}}

\providecommand{\ttwpplotfig}[1]{
\begin{minipage}{0.5\textwidth}
\ttwpplot{0.115\textwidth}{main/#1}\\
\ttwpplot{0.115\textwidth}{sherpaopenloops/#1}\\
\ttwpplot{0.115\textwidth}{mg5amc/#1}\\
\ttwpplot{0\textwidth}{powhel/#1}
\end{minipage}}


\begin{figure}
\ttwpplotfig{ETA_TOP}
\ttwpplotfig{PT_TOP}\\[2mm]
\ttwpplotfig{ETA_V}
\ttwpplotfig{PT_V}\\
\caption{\label{fig:ttWp_stable_NLOPS_1} 
Fixed-order NLO predictions for differential $\ttwp$ observables at 13\,TeV.
Each ratio plot shows all results normalized to one particular 
NLO+PS prediction and the scale variation band of the reference prediction.
}
\end{figure}


\begin{figure}
\ttwpplotfig{PT_TT}
\ttwpplotfig{PT_TTV}
\caption{\label{fig:ttWp_stable_NLOPS_2} 
Fixed-order NLO predictions for differential $\ttwp$ observables at 13\,TeV.
Ratio plots as in \refF{fig:ttWp_stable_NLOPS_1}. 
}
\end{figure}


%\begin{figure}
%\ttwpplotfig{ETA_ATOP}
%\ttwpplotfig{PT_ATOP}\\[2mm]
%\ttwpplotfig{PT_J1}
%\caption{\label{fig:ttWp_stable_NLOPS_3} 
%$\ttbar W^+$ production at 13\,TeV: discarded observables}
%\end{figure}




%\section*{Plots for $\ttbar W^-$ production}

\providecommand{\ttwmplot}[2]{
\includegraphics[width=0.85\textwidth, trim=0 #1 0 0,clip]{\ttHttvtoolspath/ttwm/#2}}

\providecommand{\ttwmplotfig}[1]{
\begin{minipage}{0.5\textwidth}
\ttwmplot{0.115\textwidth}{main/#1}\\
\ttwmplot{0.115\textwidth}{sherpaopenloops/#1}\\
\ttwmplot{0.115\textwidth}{mg5amc/#1}\\
\ttwmplot{0\textwidth}{powhel/#1}
\end{minipage}}


\begin{figure}
\ttwmplotfig{ETA_TOP}
\ttwmplotfig{PT_TOP}\\[2mm]
\ttwmplotfig{ETA_V}
\ttwmplotfig{PT_V}\\
\caption{\label{fig:ttWm_stable_NLOPS_1} 
Fixed-order NLO predictions for differential $\ttwm$ observables at 13\,TeV.
Each ratio plot shows all results normalized to one particular 
NLO+PS prediction and the scale variation band of the reference prediction.
}
\end{figure}


\begin{figure}
\ttwmplotfig{PT_TT}
\ttwmplotfig{PT_TTV}
\caption{\label{fig:ttWm_stable_NLOPS_2} 
Fixed-order NLO predictions for differential $\ttwm$ observables at 13\,TeV.
Ratio plots as in \refF{fig:ttWm_stable_NLOPS_1}. 
}
\end{figure}


%\begin{figure}
%\ttwmplotfig{ETA_ATOP}
%\ttwmplotfig{PT_ATOP}\\[2mm]
%\ttwmplotfig{PT_J1}
%\caption{\label{fig:ttWm_stable_NLOPS_3} 
%$\ttbar W^-$ production at 13\,TeV: discarded observables}
%\end{figure}



%\section*{Plots for $\ttbar Z$ production}

\providecommand{\ttzplot}[2]{
\includegraphics[width=0.85\textwidth, trim=0 #1 0 0,clip]{\ttHttvtoolspath/ttz/#2}}

\providecommand{\ttzplotfig}[1]{
\begin{minipage}{0.5\textwidth}
\ttzplot{0.115\textwidth}{main/#1}\\
\ttzplot{0.115\textwidth}{sherpaopenloops/#1}\\
\ttzplot{0.115\textwidth}{mg5amc/#1}\\
\ttzplot{0\textwidth}{powhel/#1}
\end{minipage}}


\begin{figure}
\ttzplotfig{ETA_TOP}
\ttzplotfig{PT_TOP}\\[2mm]
\ttzplotfig{ETA_V}
\ttzplotfig{PT_V}\\
\caption{\label{fig:ttz_stable_NLOPS_1} 
Fixed-order NLO predictions for differential $\ttz$ observables at 13\,TeV.
Each ratio plot shows all results normalized to one particular 
NLO+PS prediction and the scale variation  of the reference prediction.
}
\end{figure}


\begin{figure}
\ttzplotfig{PT_TT}
\ttzplotfig{PT_TTV}
\caption{\label{fig:ttz_stable_NLOPS_2} 
Fixed-order NLO predictions for differential $\ttz$ observables at 13\,TeV.
Ratio plots as in \refF{fig:ttz_stable_NLOPS_1}. 
}
\end{figure}


%\begin{figure}
%\ttzplotfig{ETA_ATOP}
%\ttzplotfig{PT_ATOP}\\[2mm]
%\ttzplotfig{PT_J1}
%\caption{\label{fig:ttz_stable_NLOPS_3} 
%$\ttbar Z$ production at 13\,TeV: discarded observables}
%\end{figure}






