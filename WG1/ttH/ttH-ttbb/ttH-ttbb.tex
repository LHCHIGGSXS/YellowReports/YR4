\section{NLO+PS simulations of \texorpdfstring{$t\bar t b\bar b$}{ttbb} production}
\label{sec:ttH-ttbb}

The production of $\ttbar$ pairs  in association with two $b$-jets constitutes
a large irreducible background to $\tth$ production in the $H\to\bbbar$ channel,
and the rather large uncertainty of
Monte Carlo simulations of $\ttbar+b$-jets  production
is one of the main limitations of current $\tth(\bbbar)$ searches at the LHC.
A reliable theoretical description of $\ttbar$ production
in association with two $b$-jets requires
hard-scattering cross sections for the relevant partonic
processes $q\bar q/gg\to \ttbb$ at
NLO QCD~\cite{Bredenstein:2009aj,Bevilacqua:2009zn,Bredenstein:2010rs}.
The inclusion of NLO QCD effects reduces scale uncertainties from the 70--80\% level at LO
to about 20--30\%.
To become applicable in the context of experimental analyses,
NLO calculations need to be matched to parton showers.
A NLO+PS simulation of $\ttbb$ production
based on the five flavour number scheme (5FNS), where b-quarks are treated as massless partons,
was presented in~\cite{TROCSANYI:2014lha,Garzelli:2014aba},
while an alternative NLO+PS simulation that includes b-quark mass effects
in the four flavour number scheme (4FNS)
was published in~\cite{Cascioli:2013era}.

Finite b-quark masses permit to extend $\ttbb$ matrix elements to the full
phase space, including regions where b-quark pairs become collinear and
matrix elements with $m_b=0$ would be divergent.  Thus, using $\ttbb$ matrix
elements with $m_b>0$ in the 4FNS it is possible to simulate $\ttbar+b$-jets
production in a fully inclusive way, including also signatures where a b-quark remains
unresolved and a single b-jet is observed.\footnote{Here and in the
following we consistently exclude top-decay products from the counting of
$b$ jets.}
In contrast, the applicability of
5FNS calculations is limited to phase-space regions where the two
$b$-quarks in $\ttbb$ matrix elements have sufficient transverse momentum
and angular separation in order to avoid the breakdown of the $m_b=0$ approximation
in the collinear regions.
Such a requirement needs to be imposed at the
level of generation cuts, i.e.~before matching matrix elements to the parton
shower, and one might expect that the resulting NLO+PS predictions for observables with
two or more hard b-jets should be insensitive to generation cuts.
However, this is not the case, since events with multiple hard $b$-jets
can result from collinear
$g\to \bbbar$ splittings in $\ttbb$
matrix elements combined with the conversion of hard gluons into
$b$-jets via $g\to \bbbar$ parton shower splittings.
In fact, as pointed out in~\cite{Cascioli:2013era}, this so-called double-splitting mechanism
can lead to a sizeable enhancement of the
$\ttbb$ background in the Higgs-signal region, $M_{bb}\sim M_H$.

Given the importance of (quasi) collinear $g\to \bbbar$ splittings,
the choice of the flavour number scheme and the inclusion of b-quark mass effects
play a critical role.
For what concerns 5FNS calculations,
while it is clear that setting $m_b=0$ and omitting the
singular phase-space regions leads to
a logarithmic sensitivity to the unphysical
generation cuts for observables with a single hard b-jet,
double-splitting (or multiple-splitting)
contributions imply such a sensitivity also
for observables with two or more hard b-jets.
Such a logarithmic dependence can naturally be avoided in
the framework of NLO merging~\cite{Hoeche:2012yf,Frederix:2012ps,Lonnblad:2012ix},
where the singular phase space regions,
defined in terms of an appropriate merging cut,
are populated by the parton shower combined with matrix elements
for $\ttbar+0,1$\,jet production.
However, applying NLO merging to $\ttbar+0,1,2$\,jet
production~\cite{Hoeche:2014qda} is technically much more challenging as compared to
NLO+PS simulations of $\ttbb$ production.  Moreover, in the merging
approach all $b$-quarks produced via double-splitting contributions
would tend to arise from the parton shower,
which implies a strong dependence on parton-shower modelling.
In contrast, 4FNS simulations with $m_b>0$ have the advantage that one of
the $g\to \bbbar$ splittings is entirely described in terms of $\ttbb$
matrix elements at NLO,
while the additional hard $b$-jet arises from $\ttbb g$ tree amplitudes
matched to the parton shower via $g\to \bbbar$ splittings that can take
place at any stage of the shower evolution.
%
In conventional 4FNS calculations as the ones presented in this study, the
number of active quark flavours is limited to four both in the evolution of
the PDFs and $\alpha_s$.  Thus, renormalization group logarithms associated
with $b$- and $t$-quark loops are included only at fixed-order NLO.\footnote{In the 4FNS,
$b$-quark contributions to the running of $\alpha_s$ are consistently restored at NLO
accuracy by  including $b$-quark loops in the matrix elements
and renormalizing them via zero-momentum subtraction.  Also top-quark loop
contributions to $\alpha_s$ have been renormalized via zero-momentum
subtraction in the 4FNS.}
%
As
discussed in~\cite{Cascioli:2013era}, such logarithms can be easily resummed
by using modified 4FNS PDF sets that include all relevant quark flavours in
the running of $\alpha_s$.  At $\sqrt{s}=8$\,TeV it was found that
higher-order contributions of this type increase the NLO 4FNS $\ttbb$ cross
section by about $9\%$~\cite{Cascioli:2013era}, while in the 5FNS they are
naturally included.

Finally, we observe that simulations of $\ttbb$ production in the 4FNS can be
combined with fully inclusive $\ttbar+$jets samples based on the 5FNS in a
rather straightforward way~\cite{Moretti:2015vaa}.  In fact, in order to
avoid the double counting of $b$-quark production in the $\ttbb$ and
$\ttbar+$jets sample it is sufficient to veto events that involve $b$
quarks in the $\ttbar+$jets sample.  This prescription has to be applied after
parton showering, and the $b$-quark veto should be restricted to showered
$\ttbar+$jets matrix elements before top decays, i.e.~it should not be
applied to $b$ quarks that arise from (showered) top decays or from the
underlying event.


From the above discussion it is clear that the parton shower and the
choice of the flavour number scheme
play a critical role
in the description of $\ttbb$ production, and
the thorough understanding of the related uncertainties is of prime importance for the success of
$\ttbar H(\bbbar)$ searches. As a first step in this direction, in the following we present a systematic
comparison of various NLO+PS simulations of $\ttbb$ production
based on different parton showers,
matching schemes,
and flavour number schemes.



\subsection{NLO+PS tools and simulations}
Three different NLO+PS simulations of $\ttbb$ production at $\sqrt{s}=13$\,TeV based on \Sherpa+\Openloops \cite{Gleisberg:2008ta,Cascioli:2011va,Cascioli:2013gfa},
\MGfiveamcnlo+\Pythiaeight~\cite{Alwall:2014hca,Hirschi:2011pa,Skands:2014pea,Sjostrand:2014zea}
and
\Powhel+\Pythiaeight~\cite{Kardos:2013vxa,Bevilacqua:2011xh,Skands:2014pea,Sjostrand:2014zea}
have been compared.
The various NLO matching methods, parton showers and flavour number
schemes employed in the three simulations are summarized
in~\refT{tab:ttbb_tools}.
The \Sherpa+\Openloops\, and \MGfiveamcnlo\, simulations employ the
4FNS and the \Mcnlo\, matching
method\footnote{More precisely, \MGfiveamcnlo\, employs the original
  formulation of \Mcnlo\,
  matching~\cite{Frixione:2002ik,Frixione:2003ei}, while
  \Sherpa+\Openloops\, implements an alternative
formulation~\cite{Hoeche:2011fd,Hoche:2012wh}
denoted as \Smcnlo, which
  is characterized by an improved treatment of colour correlations but
  is otherwise equivalent to the method
  of~\cite{Frixione:2002ik,Frixione:2003ei}.} to combine matrix
elements with the \Sherpa\, and \Pythiaeight\, parton showers,
respectively.  Therefore possible differences between
\Sherpa+\Openloops\, and \MGfiveamcnlo\, predictions
can be attributed to parton shower effects or to differences in the two
implementations of the MC@NLO approach and related technical parameters.
Instead, the \Powhel\, simulation differs
from the other two simulations in at least two aspects: it is
performed in the 5FNS and it employs the
\Powheg\, matching method~\cite{Nason:2004rx,Frixione:2007vw}.  Moreover, when comparing
\Sherpa+\Openloops\, and \Powhel\, predictions one should keep in mind
that also the respective parton showers (\Sherpa\, and \Pythiaeight) are
different.


Since the \Powhel\, simulation is performed with massless
$b$ quarks,\footnote{To be precise, in the \Powhel\, simulation
$b$-mass effects are neglected at the matrix element level but are taken
into account in the parton shower.}
in order to avoid collinear $g\to\bbbar$ singularities,
the hard matrix elements need to be restricted to
phase space regions where both $b$ quarks remain resolved.  This is achieved
through the generation cuts
\begin{equation}
\label{ttbb:5Fgencuts}
m_{bb}> 2 \xi_m m_b,\qquad
p_{T,b}>\xi_T m_b.
\end{equation}
%
Technically, in order to guarantee infrared
safety, in the case of real emission events the cuts in \Eref{ttbb:5Fgencuts}
are applied after a projection onto the Born phase space.
The above cuts are chosen in a way that
mimics, at LO in $\alpha_S$,  the  $\log(m_b)$ dependence
that arises when one $b$-quark is integrated out.
Thus they can be regarded as an
heuristic approach in order to obtain reasonable 5FNS predictions also for
$\ttbar+b$-jet observables with a single resolved $b$ jet.
However, as discussed above,
NLO+PS $\ttbb$ predictions in the 5FNS are sensitive to the
generation cuts in \Eref{ttbb:5Fgencuts}. In particular they depend on the
unphysical parameters $\xi_m$ and $\xi_T$,
which have been set to one in the present study.\footnote{Variations of these parameters
will allow to quantify up to which extent,
in practice, predictions in conditions typically met in experimental
analyses depend on the choice of these technical cuts.}
Moreover, one should keep in mind that $\log(m_b)$ terms beyond LO and finite terms of order $m_b$
are not consistently included in the 5FNS.



\begin{table}\footnotesize
\caption{Employed tools, matching methods, parton showers, flavour number scheme (FNS) and
generation cuts in the NLO+PS simulations of $\ttbb$ production.
The \Powhel~generator implements matrix elements with massless $b$-quarks but
includes $b$-mass effects through \Pythia.}
\label{tab:ttbb_tools}
\setlength{\tabcolsep}{1.0ex}
\begin{center}
%%\tabcolsep5pt
\renewcommand{\arraystretch}{1.2}
\begin{tabular}{llllll}
\toprule
Tools                                    & Matching method     & Shower   & FNS   & $m_b$\,[GeV] & Generation cuts \\
\midrule
\Sherpa~2.2.1+\Openloops~1.2.3              & \Smcnlo           & \Sherpa   & 4FNS & 4.75         & fully inclusive  \\
\MGfiveamcnlo~2.3.2+\Pythiaeight~2.1.0  & \Mcnlo              & \Pythiaeight   & 4FNS & 4.75         & fully inclusive  \\
\Powhel+\Pythiaeight~2.1.0                     & \Powheg              & \Pythiaeight   & 5FNS & 0            & $m_{bb}> 2 m_b$,\; $p_{T,b}>m_b$ \\
\bottomrule
\end{tabular}
\end{center}
\end{table}


\subsection{Parton showers, PDF, and \texorpdfstring{$\alpha_s$}{alpha_s}}

Full Monte Carlo simulations of $\ttbb$ production
involve hard $\ttbb$ cross section at NLO,
top-quark decays, parton showering,
hadronization, hadron decays, and the underlying event.  The main
source of theoretical uncertainty in this involved simulation
framework is given by the mechanism that governs $b$-quark production
in association with top-quark pairs.  Thus, in order to obtain a
sufficiently transparent picture of the nontrivial QCD dynamics
of $b$-quark production, it was decided to reduce the complexity
that results from the presence of the additional b-quarks that arise from
top-quark decays via well-understood weak interactions.
To this end, top quarks have been treated as stable particles in the simulations.
Moreover, all NLO+PS simulations have been performed at the parton level,
including only the perturbative phase of parton shower evolution, and
neglecting hadronization as well as any other non-perturbative aspect.
The quantitative importance of hadronization and the
  possible bias that can result from switching off hadronization in
  the comparison of two $\ttbar+b$-jet simulations based on different
  parton showers was assessed by comparing \Sherpa~2.1 and \Pythia~8.2
  LO+PS simulations of $pp\to H+$jets (including
  $b$ jets) at 14\,TeV.  Thanks to the colour neutral nature of the
  Higgs boson, this process allows one to assess the impact of
  hadronization by turning it on and off.  The effects of
  hadronization increase with decreasing jet transverse momenta. Thus
  they predominantly arise in the vicinity of the jet-$p_T$
  threshold. For the production of $b$-jets with $p_T>25\UGeV$ and
  $|\eta|<2.5$ they amount to about $-2\%(-4\%)$ per $b$ jet in
  \Pythiaeight\, (\Sherpa).  This suggests that the bias that results from
  turning off hadronization should be well below the typical NLO+PS
  uncertainties in $\ttbb$ production.


In order to reduce uncontrolled sources of bias related to shower modelling
in the comparison of NLO+PS simulations based on \Sherpa\, and \Pythiaeight, those
free parton-shower parameters related to the strong coupling have
been chosen in a uniform way. Specifically, the rescaling factors $x$ that are
applied to the strong coupling terms $\alpha_S(x\,k_T^2)$
for each shower emission have been set to $x=1$ both for initial- and final-state radiation.
%
Furthermore the option of resumming
subleading logarithms
of Catani-Marchesini-Webber kind~\cite{Catani:1990rr}
was deactivated.
%
Note that these choices neither correspond to the \Sherpa\, default nor to the
\Pythiaeight\, default settings.  Moreover they are not meant to provide an
optimal description of data.  They are only aimed at a consistent comparison of
the two showers, where simple parametric differences are avoided, and the
remaining deviations can be attributed to intrinsic shower features, such as
the different definition of the shower evolution variables.


Since parton-shower tunes and PDFs are intimately connected, it is not
trivial to identify a common PDF set that is optimal for all parton
showers.  For the present study the NNPDF3.0 NLO PDF set was adopted,
keeping in mind that this choice might bias the comparison of
\Sherpa\, against \Pythiaeight. The specific PDF set was chosen according to
the employed flavour number scheme (4FNS or 5FNS), while the value of
$\alpha_s(M_Z)$ in NLO matrix elements and for the first shower
emission was chosen consistently with the PDF. The same holds for the
running of $\alpha_s$, whose evolution is implemented at 2-loops both
in matrix elements and parton showers.  For subsequent shower
emissions the 4FNS (5FNS) together with the corresponding value of
$\alpha_s(M_Z)$ was used in \Sherpa\,~(\Pythiaeight).


\subsection{Input parameters and scale choices}
To simulate $\ttbb$ production at 13\,TeV the input parameters
$m_t=172.5$~GeV, $m_b=4.75$~GeV and $\alpha_s^{(5F)}(M_Z)=0.118$
have been used together with NNPDF3.0 parton distributions at NLO,
as discussed above.\footnote{Note that the employed NNPDFs
and related $\alpha_s(M_Z)$ value in the 4FNS are
derived from variable-flavour-number NNPDFs with $\alpha_s^{(5F)}(M_Z)=0.118$
via appropriate backward and forward evolution with five and four active flavours,
respectively.}
%
The central values of the renormalization and factorization scales have been chosen as
\begin{equation}
\label{ttbb:scales}
\mu_{R,0}=\left(\prod_{i=t,\bar t,b, \bar b} E_{T,i}\right)^{1/4},
\qquad
\mu_{F,0}=\frac{H_T}{2} =\frac{1}{2}\sum_{i=t, \bar t,b, \bar b,j} E_{T,i},
\end{equation}
where $E_{T,i}=\sqrt{M_i^2+p^2_{T,i}}$ denotes the transverse
energy of top and bottom quarks, defined at parton level. Note that also
extra parton emissions contribute to the total transverse energy $H_T$ in
\Eref{ttbb:scales}.
Theoretical uncertainties have been assessed by means of standard
variations $\mu_R=\xi_R \mu_{R,0}$, $\mu_F=\xi_F \mu_{F,0}$ with
$0.5< \xi_R, \xi_F <2$ and $0.5< \xi_R/\xi_F <2$.

The CKKW inspired renormalization scale choice in \Eref{ttbb:scales}
is based on~\cite{Cascioli:2013era} and takes into account the fact
that top and bottom quarks are produced at widely different scales
$E_{T,b}\muchless E_{T,t}$. This turns out to improve the perturbative
convergence as compared to a hard global scale of order $m_t$.
%
In particular, in the 4FNS it was checked that using $\mu_{R}=H_T/2$ instead
of $\mu_{R}=\mu_{R,0}$ increases the $K$-factor by 0.25 and reduces the NLO
cross section by about 40\%, which is only barely consistent with the level
of uncertainty expected from factor-two scale variations.  Moreover,
computing LO and NLO cross sections using PDFs and $\alpha_s$ values at NLO
throughout\footnote{With this approach $K$-factors are much less dependent
on the employed PDF sets and reflect the convergence of the perturbative
expansion in a more realistic way as compared to using LO inputs for the LO
cross section.} yields $K$-factors around 2 with $\mu_R=\mu_{R,0}$ and about
0.25 higher with $\mu_R=H_T/2$.  Thus both scale choices seem to be
suboptimal, and in order to improve the convergence of the perturbative
expansion, a scale even softer than~\Eref{ttbb:scales} should be considered
in the future.  In any case a hard scale of type $\mu_R=H_T/2$ is not
recommended.


\def\ximin{\xi_{\mathrm{min}}}
\def\ximax{\xi_{\mathrm{max}}}

In the context of the \Mcnlo\, matching approach, where the
resummation scale $\mu_Q$, i.e.~the parton shower starting scale, is a
free parameter, it is natural to identify this scale with the
factorization scale.  Thus $\mu_Q=\mu_{F,0}=H_T/2$ was used in the
\Sherpa+\Openloops\, simulation.  In the case of \MGfiveamcnlo\, a
different choice had to be adopted since only resummation scales of
the form $\mu_Q=\xi \sqrt{\hat{s}}$ are supported, where the prefactor $\xi$
is randomly distributed in the freely adjustable range
$[\ximin,\ximax]$ with a distribution that is strongly peaked at
$(\ximin+\ximax)/2$~\cite{Alwall:2014hca}. Comparing the $H_T/2$ and $\mu_Q=\xi \sqrt{\hat{s}}$
distributions it was observed that the
respective peaks lie around $200\UGeV$ and $400\UGeV$
when the default \MGfiveamcnlo\, settings
$(\ximin,\ximax)=(0.1,1)$ are used, i.e.~the default $\mu_Q$
in \MGfiveamcnlo\, is much harder.

Given that \Mcnlo\,
predictions for $\ttbb$ production are quite sensitive to $\mu_Q$,
it was decided to lower the $\xi$ upper bound to $\ximax=0.25$, which
brings the $\mu_Q$ reasonably close to $H_T/2$.  We note that this
choice is also supported by the study of an \MGfiveamcnlo\, simulation
of $H\bbbar$ production in the 4FNS~\cite{Wiesemann:2014ioa}, where it
was found that reducing $\ximax$ from 1 to 0.25 strongly improves the
convergence of NLO+PS and NLO distributions at large transverse
momenta.


\def\hdamp{h_{\mathrm{damp}}}

In the \Powheg\, matching method, the resummation scale is not a freely
adjustable parameter, since the first emission on top of $\ttbb$ events is
entirely described by matrix elements, and the corresponding transverse momentum
scale sets the upper bound for subsequent shower emissions.
%
Nevertheless, \Powheg\, simulations involve a parameter $\hdamp$ that
separates the first-emission phase space into a singular region, where the
first emission is resummed and corrected with a local $K$-factor, and a
remnant region, where it is handled as at fixed-order NLO.
%
Given the analogy with the separation of
soft and hard events in the \Mcnlo\, approach,
and given that $\mu_Q$ represents the upper bound for
emissions off soft events,
it is natural to chose
$\hdamp$ of the same order of $\mu_Q$.  Thus the choice $\hdamp=H_T/2$ was
adopted in the \Powhel\, simulation.

Variations of the resummation scale and of the $\hdamp$ parameter
have not been considered in this study.


\subsection{NLO+PS predictions for \texorpdfstring{$t\bar t+b$}{tt+b}-jets cross sections in \texorpdfstring{$b$}{b}-jet bins}

In the following we compare integrated and differential NLO+PS
predictions for $\ttbar+b$-jets production with a certain minimum
number of $b$ jets, $n_b>N_b$.  In particular we focus on the
bins with $n_b\ge 1$ or $n_b\ge 2$, which are the most relevant ones
for $\ttbar H(\bbbar)$ analyses.  For the jet definition the anti-$k_T$
algorithm with $R=0.4$ is adopted, and jets that involve one or more
$b$-quark constituents are classified as $b$-jets.  Note that also
jets that result from collinear $g\to\bbbar$ splittings are handled as
$b$ jets.  Moreover no requirement is imposed on the minimum transverse
momentum of $b$ quarks inside $b$ jets.  Events are categorized
according to the number $n_b$ of resolved $b$ jets within the
acceptance region,
%
\begin{equation}
p_{T,b}>25\,\UGeV\,,
\qquad
|\eta_{b}|<2.5\,.
\end{equation}
%
Let us recall that top quarks are treated as stable particles, thus
the two $b$ quarks that arise from top decays as well as possible
extra $b$ quarks from the showering of top-decay products are not
included in $n_b$.  Apart from the requirement $n_b\ge N_b$ no additional cut
will be applied.\footnote{To be more precise, the \Sherpa+\Openloops\,
  and \MGfiveamcnlo\, samples are fully inclusive, while in the case
  of \Powhel\, the technical cuts \Eref{ttbb:5Fgencuts} are applied as
  discussed above.} In order to illustrate the importance of parton
shower effects, the various NLO+PS predictions presented in the
following are also compared to fixed-order NLO predictions.  The
latter are based on \Sherpa+\OpenLoops and are obviously independent
of the employed parton shower and matching scheme.

All quoted theoretical uncertainties correspond to factor-two
variations of the renormalization and factorization scales. In
\refFs{fig:ttbb_1}--\ref{fig:ttbb_9} they are shown as bands, and, to
improve readability, three different ratio plots are shown, where all results are normalized to
one particular NLO QCD+PS prediction and the corresponding scale
variation band is shown.

\newcommand{\xs}[2]{#1}
\def\TTB{\mathrm{ttb}}
\def\TTBB{\mathrm{ttbb}}
\def\TTBBB{\mathrm{tt+3b}}
\def\TTBBBB{\mathrm{tt+4b}}



\begin{table}
\caption{Fixed-order NLO and NLO+PS predictions for integrated $\ttbar+b$-jets cross sections
at 13\,TeV in bins with $n_b\ge 1$ and $n_b\ge 2$ $b$ jets.}
\label{tab:XS}
\vspace*{0.3ex}
\begin{center}
\renewcommand{\arraystretch}{1.2}
\begin{tabular}{llll@{\hspace{12mm}}l@{\hspace{12mm}}l}
\toprule
Selection
&  Tool
&  {$\sigma_{\mathrm{NLO}}\,[\mathrm{fb}]$} %\phantom{\huge{X}}
&  {$\sigma_{\mathrm{NLO+PS}}\,[\mathrm{fb}]$} %\phantom{\huge{X}}
&  {$\sigma_{\mathrm{NLO+PS}}/\sigma_{\mathrm{NLO}}$}  %\phantom{\huge{X}}
\\ %[.5mm] 
\midrule
$n_b\ge 1$
& \Sherpa+\OpenLoops
& $12820^{+35\%}_{-28\%}$ % SOL ttb NLO
& $12939^{+30\%}_{-27\%}$ % SOL ttb NLOPS
& $1.01$ % SOL ttb NLOPS/NLO
\\%[.5mm]
& \MGfiveamcnlo
& % MGfiveamcnlo ttb NLO
& $13833^{+37\%}_{-29\%}$ % MGaMC ttb NLOPS
& $1.08$ % MGaMC ttb NLOPS/NLO
\\%[.5mm]
& \Powhel
& % POWHEL ttb NLO
& $10073^{+45\%}_{-29\%}$ % POWHEL ttb NLOPS
& $0.79$ % POWHEL ttb NLOPS/NLO
\\%[.5mm] 
\midrule
$n_b\ge 2$
& \Sherpa+\OpenLoops
& $2268^{+30\%}_{-27\%}$ % SOL ttbb NLO
& $2413^{+21\%}_{-24\%}$ % SOL ttbb NLOPS
& $1.06$ % SOL ttbb NLOPS/NLO
\\%[.5mm]
& \MGfiveamcnlo
& % MGaMC ttbb NLO
& $3192^{+38\%}_{-29\%}$ % MGaMC ttbb NLOPS
& $1.41$ % MGaMC ttbb NLOPS/NLO
\\%[.5mm]
& \Powhel
& % POWHEL ttbb NLO
& $2570^{+35\%}_{-28\%}$ % POWHEL ttbb NLOPS
& $1.13$ % POWHEL ttbb NLOPS/NLO 
\\%[.5mm]
\midrule
\end{tabular}
\end{center}
\end{table}

Results for the $\ttbar+b$-jets cross sections with $n_b\ge N_b$ b
jets for various values of $N_b$ are presented in \refT{tab:XS} and
\refF{fig:ttbb_1}.  In the following we will refer to the results for
$N_b=1,2,3,4$ as $\TTB$, $\TTBB$, $\TTBBB$ and $\TTBBBB$ cross
sections, respectively.  For the $\TTB$ and $\TTBB$ cross sections,
which are described at NLO accuracy, the various NLO+PS predictions
turn out to be in decent mutual agreement.  More precisely, $\TTB$
predictions based on the 4FNS (\Sherpa+\Openloops\, and \MGfiveamcnlo)
agree very well with each other and with fixed-order NLO, and the 5FNS
$\TTB$ simulation (\Powhel) lies only 20\% lower, despite that
it was not designed to describe final states with a single
b-jet (due to the generation cuts).


For the $\TTBB$ cross section one finds excellent agreement between
fixed-order NLO, \linebreak
\Sherpa+\Openloops\, and \Powhel.  This seems to
suggest that this observable has little sensitivity to parton shower
effects and to the choice of the flavour number scheme.  However this
interpretation is challenged by the fact that the \MGfiveamcnlo\,
$\TTBB$ result lies more than $30\%$ above the other predictions.
The only significant differences between \MGfiveamcnlo\, and
\Sherpa+\Openloops\, simulations lie in the employed parton showers and details
of MC@NLO matching, thus the origin of the observed discrepancy is likely to lie in the choice
of shower starting scale in \MGfiveamcnlo\, combined with the higher
intensity of QCD radiation in \Pythiaeight\, with respect to \Sherpa.
This is confirmed by the further enhancement of the \MGfiveamcnlo\, cross section in
the bins with $n_b\ge 3$ and $n_b\ge 4$ $b$-jets (see
\refF{fig:ttbb_1}), where the additional b quarks arise from
$g\to \bbbar$ parton-shower splittings, which results in a much
stronger sensitivity to shower effects.  Note that this kind of
uncertainty for $N_b=3,4$ is not included in the quoted scale
variations.
In the \Sherpa+\Openloops\, simulation, the size of scale uncertainties and
the difference between NLO and NLO+PS predictions are fairly similar to what
observed at $\sqrt{s}=8$\,TeV in~\cite{Cascioli:2013era}.  In particular,
NLO+PS scale uncertainties range between 20--30\% in all $b$-jet bins and
are smaller as compared to the case of fixed-order NLO.  Scale variations in
\MGfiveamcnlo\, and \Powhel\, tend to be larger and agree well with each other
for $N_b=2$, while \Powhel\, features a larger scale dependence in the
other bins, especially for $N_b=3,4$.  These various differences can be
attributed to the employed flavour-number schemes and
to technical aspects of the implementation of scale variations in the
three different NLO+PS Monte Carlo tools.



\subsection{\texorpdfstring{$\TTB$}{ttb} differential analysis}

Various differential observables for an inclusive $\TTB$ analysis with
$n_b\ge 1$ $b$-jets are presented in \refFs{fig:ttbb_2}--\ref{fig:ttbb_4}.
For all distributions that are inclusive with respect to extra light-jet emissions
one observes a rather similar behaviour as for the $\TTB$ cross section,
i.e.~\Sherpa+\OpenLoops, \MGfiveamcnlo\, and fixed-order NLO predictions
agree well, while \Powhel\, lies about 20\% lower. Only \Powhel\, features significant shape
distortions with respect to fixed-order NLO in the region of low rapidity and/or
low $p_T$ for the leading top and bottom quarks and for the $\ttbar$ system
(\refFs{fig:ttbb_2}--\ref{fig:ttbb_3}).
Observables that explicitly involve the first light-jet emission
(\refF{fig:ttbb_4}) turn out to behave differently. While
for \Sherpa+\OpenLoops, \Powhel\, and fixed-order NLO
there is mutual agreement within scale variations,
the \MGfiveamcnlo\, prediction turns out to lie up to
50\% higher at $p_{T,j}\sim 50$\,\UGeV.
This enhancement of QCD radiation
in \MGfiveamcnlo+\Pythiaeight\,
disappears at $p_{T,j}\sim 150$\,\UGeV.
It is most likely related to what was observed above
in $b$-jet bin cross sections with $N_b\ge 2$.




\subsection{\texorpdfstring{$\TTBB$}{ttbb} differential analysis}

Various differential observables for an inclusive $\TTBB$ analysis with
$n_b\ge 2$ $b$-jets are presented in \refFs{fig:ttbb_5}--\ref{fig:ttbb_9}
Observables that depend on the top-quark and b-jet kinematics
but are inclusive with respect to extra jet emission are presented
in \refFs{fig:ttbb_5}--\ref{fig:ttbb_7}. For all such distributions
a fairly good agreement between \Sherpa+\OpenLoops, \Powhel\, and fixed-order NLO is
observed, both at the level of shapes and normalization.
The most significant shape differences show up in the $p_T$
of the 2nd b-jet and do not exceed 20\%.
%
In \MGfiveamcnlo\ the matching to the  \Pythiaeight\ shower increases the $\TTBB$
rates by about 35\% with respect to \Sherpa+\OpenLoops,
and turns out to have an non-trivial dependence on the
top and b-jet kinematics.  In particular it tends to enhance distributions in
the the regions with small top-quark and b-jet $p_T$ and at large
$\Delta R$ separation between the two $b$-jets.


For the distribution in the invariant mass of the $b$-jet pairs, which corresponds to the
mass of the $H\to \bbbar$ candidate, it turns out that all NLO+PS results
are in reasonably good mutual agreement. The results also confirm the presence of an NLO+PS
distortion of the invariant-mass distribution, which was
attributed to double-splitting effects in~\cite{Cascioli:2013era}.
More precisely, in the vicinity of the Higgs boson resonance
the NLO+PS enhancement w.r.t.~NLO is close to 20\% and thus less pronounced
to what was observed in~\cite{Cascioli:2013era} at $\sqrt{s}=8$\,TeV,\footnote{This can be due to the
different collider energy and to the different scale choices in this study and in~\cite{Cascioli:2013era}.}
while the \Powhel\,and \MGfiveamcnlo\, distributions
feature an additional enhancement of about 10\% and 35\%, respectively,
w.r.t.~\Sherpa+\OpenLoops in the Higgs boson signal region.

Various observables that are directly sensitive to the emission of
an additional jet are shown in \refFs{fig:ttbb_8}--\ref{fig:ttbb_9}.
Despite the intrinsic LO nature and stronger shower dependence of such distributions,
\Sherpa+\OpenLoops\, and \Powhel\, remain in good
agreement: the most important deviations, which show up in the $p_T$
tail of the first light jet, do not exceed 40\%.
In contrast, the excess of \MGfiveamcnlo\, w.r.t.~the other predictions
grows by about a factor two, reaching about 70\% in average, and gives rise
to more pronounced shape distortions as compared to the case of
inclusive $\TTBB$ observables. Similarly as for the $\TTBB$ analysis, the enhancement is
concentrated at light-jet momenta between 50--150\UGeV,
where it reaches up to 100\%. A similarly strong
increase shows up also in the region of central light-jet rapidity,
as well as in angular and mass distributions that involve light and $b$-jets.


\subsection{Summary and conclusions}
In summary, we have presented a systematic study of Monte Carlo simulations of $pp\to
\ttbar+b$-jets at 13\,TeV that compares various NLO+PS
predictions based on different matching methods, parton showers and matching
schemes.  While the inclusion of $b$-mass effects is the only fully
consistent way of describing inclusive $\ttbar+b$-jets production in terms
of $\ttbb$ matrix elements, the observed agreement between
\Sherpa+\OpenLoops and \Powhel\, predictions indicates that also
simulations with massless b-quarks and appropriate generation cuts
provide predictions in agreement well within the scale uncertainties.

The various NLO+PS simulations considered in this study confirm
that the invariant mass of the b-jet pair receives significant NLO+PS corrections
that can reach 20-30\% in the $H\to \bbbar$ signal region~\cite{Cascioli:2013era}.
Based on standard variations of the renormalization and factorization scales,
the expected accuracy of NLO predictions should be at the 25--35\% level.
However in various phase-space regions the differences between the various NLO+PS simulations
tend to be larger.


In particular, some of the distributions generated with
\MGfiveamcnlo+\Pythiaeight\,
have significantly different shapes, resulting in larger predictions for up
to 100\%, compared to the other NLO+PS simulations.  These are probably
related to the high intensity of the QCD radiation
in \Pythiaeight\, and are quite sensitive to the choice of the
shower starting scale in the MC@NLO matching framework.
These findings should be regarded as a first step towards a thorough
investigation of NLO matching and parton shower effects,
including all relevant sources
of uncertainty, in the Monte Carlo modelling of $\ttbar+b$-jets production.
In the future also top-quark decays should be investigated.





\providecommand{\ttbbtableplot}[2]{
\includegraphics[width=0.85\textwidth, trim=0 #1 0 0,clip]{\ttHttbbpath/#2}}

\providecommand{\ttbbfig}[1]{
\begin{minipage}{0.5\textwidth}
\ttbbtableplot{0.115\textwidth}{plots/main/#1}\\
\ttbbtableplot{0.115\textwidth}{plots/sherpaopenloops/#1}\\
\ttbbtableplot{0.115\textwidth}{plots/mg5amc/#1}\\
\ttbbtableplot{0}{plots/powhel/#1}
%\ttbbtableplot{0}{plots/powheg/#1}
\end{minipage}}



\begin{figure} % TTBB 1
\bce
\ttbbfig{NBj_XS}
\ece
\caption{\label{fig:ttbb_1} %ttbb 1
  Fixed-order NLO and NLO+PS predictions for integrated
  $\ttbar+$b-jets cross sections at 13\,TeV in inclusive bins with
  $n_b>N_b$ $b$ jets.  Each ratio plot shows all results normalized to
  one particular NLO QCD+PS prediction and the corresponding scale
  variation band.  }
\end{figure}




\begin{figure}
\ttbbfig{1_PT_T1}
\ttbbfig{1_PT_T2}
\\[2mm]
\ttbbfig{1_ETA_T1}
\ttbbfig{1_ETA_T2}
\caption{\label{fig:ttbb_2} Fixed-order NLO and NLO+PS top-quark
 distributions for $pp\to\ttbar+\ge 1\,b$ jets at 13\,TeV.  Ratio
  plots like in \refF{fig:ttbb_1}.  }
\end{figure}




\begin{figure}% TTBB 3
\ttbbfig{1_PT_T1T2}
\ttbbfig{1_PT_B1}
\\[2mm]
\bce
\ttbbfig{1_ETA_B1}
\ece
\caption{\label{fig:ttbb_3} Fixed-order NLO and NLO+PS
top-quark and b-jet
distributions for $pp\to\ttbar+\ge 1\,b$ jets at 13\,TeV.
  Ratio plots like in \refF{fig:ttbb_1}.  }
\end{figure}




\begin{figure}% TTBB 4
\ttbbfig{1_ETA_J1}
\ttbbfig{1_PT_J1}
\\[2mm]
\bce
\ttbbfig{1_M_J1B1}
\ece
\caption{\label{fig:ttbb_4}
Fixed-order NLO and NLO+PS light-jet distributions
for $pp\to\ttbar+\ge 1\,b$ jets
at 13\,TeV.
Ratio plots like in \refF{fig:ttbb_1}.
}
\end{figure}



\begin{figure} % TTBB 5
\ttbbfig{2_PT_T1}
\ttbbfig{2_PT_T2}
\\[2mm]
\ttbbfig{2_ETA_T1}
\ttbbfig{2_ETA_T2}
\caption{\label{fig:ttbb_5} Fixed-order NLO and NLO+PS top-quark
distributions for $pp\to\ttbar+\ge 2\,b$ jets at 13\,TeV.  Ratio
  plots like in \refF{fig:ttbb_1}.  }
\end{figure}





\begin{figure} % TTBB 6
\ttbbfig{2_ETA_B1}
\ttbbfig{2_ETA_B2}
\\[2mm]
\ttbbfig{2_PT_B1}
\ttbbfig{2_PT_B2}
\caption{\label{fig:ttbb_6} Fixed-order NLO and NLO+PS b-jet
distributions for $pp\to\ttbar+\ge 2\,b$ jets at
13\,TeV.  Ratio plots like in \refF{fig:ttbb_1}.  }
\end{figure}




\begin{figure} % TTBB 7
\ttbbfig{2_M_B1B2}
\ttbbfig{2_DR_B1B2}
\\[2mm]
\ttbbfig{2_PT_T1T2}
\ttbbfig{2_PT_B1B2}
\caption{\label{fig:ttbb_7} (7) Fixed-order NLO and NLO+PS
 distributions of the $\bbbar$ and $\ttbar$ systems for $pp\to\ttbar+\ge 2\,b$ jets at
  13\,TeV.  Ratio plots like in \refF{fig:ttbb_1}.  }
\end{figure}




\begin{figure} % TTBB 8
\ttbbfig{2_M_J1B1}
\ttbbfig{2_M_J1B2}
\\[2mm]
\bce
\ttbbfig{2_PT_J1}
\ece
\caption{\label{fig:ttbb_8} Fixed-order NLO and NLO+PS
  light-jet mass and transverse momentum distributions for $pp\to\ttbar+\ge 2\,b$ jets at
  13\,TeV.  Ratio plots like in \refF{fig:ttbb_1}.  }
\end{figure}



\begin{figure} % TTBB 9
\ttbbfig{2_DR_J1B1}
\ttbbfig{2_DR_J1B2}
\\[2mm]
\bce
\ttbbfig{2_ETA_J1}
\ece
\caption{\label{fig:ttbb_9} Fixed-order NLO and NLO+PS
  light-jet angular distributions for $pp\to\ttbar+\ge 2\,b$ jets at
  13\,TeV.  Ratio plots like in \refF{fig:ttbb_1}.  }
\end{figure}
