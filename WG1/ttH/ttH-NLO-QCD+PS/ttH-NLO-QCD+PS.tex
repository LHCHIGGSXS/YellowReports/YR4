\section{Comparison of NLO QCD+Parton Shower simulations for \texorpdfstring{$t\bar{t}H(b\bar{b})$}{ttH(bb)}}
\label{sec:ttH-nlo-qcd+ps}

In recent years fixed-order NLO QCD calculations of $t\bar{t}H$ have been interfaced
with parton-shower (PS) Monte-Carlo generators
(\Herwig~\cite{Marchesini:1991ch,Corcella:2000bw,Bahr:2008pv},
\Pythia~\cite{Sjostrand:2006za,Sjostrand:2007gs,Skands:2014pea,Sjostrand:2014zea},
and \Sherpa~\cite{Gleisberg:2008ta}) using one of the methods proposed in
the literature, namely \Mcnlo~\cite{Frixione:2002ik,Frixione:2003ei},
\Powheg~\cite{Nason:2004rx,Frixione:2007vw,Frixione:2007nw}, and
\Smcnlo~\cite{Hoeche:2011fd,Hoeche:2012ft}, and are nowadays
implemented in a variety of tools, from
\MGfiveamcnlo \cite{Frederix:2011zi,Alwall:2014hca,Hirschi:2011pa},
to \Powhel~\cite{Bevilacqua:2011xh,Garzelli:2011vp},
\Powhegbox~\cite{Alioli:2010xd,Hartanto:2015uka},
\Sherpa+\Openloops~\cite{Gleisberg:2008ta,Cascioli:2011va,Cascioli:2013gfa},
and \Herwigseven~\cite{Bellm:2015jjp,Bahr:2008pv}.

The accurate description of the $\tth$ signal, from the energy
scale of the hard scattering to the hadronization energy scale,
crucially relies on these tools and their use in experimental analyses
is highly recommended. Due to the prominent role that $\tth$
production will play in the Higgs-physics program of Run~2 of the
LHC, it is crucial to validate different implementations against each
other and verify their compatibility. Given the multiplicity and
diversity of NLO QCD parton-shower Monte-Carlo generators available to
calculate $\tth$ observables (total cross section and
distributions), a systematic comparison requires to define a common
set-up that takes into account the technical aspects of different
matching schemes between fixed-order NLO QCD calculation and parton
shower.  It is the first necessary step towards a better control of
the theoretical accuracy of $\tth$ predictions, and has been the
purpose of a dedicated study in the context of the $\tth$
working group. Previous
studies~\cite{Dittmaier:2012vm,Andersen:2014efa} have shown
compatibility among different subsets of these tools, but different
choices made in each existing study prevent to derive from them a
more uniform comparison.

In this section we present details and outcomes of a new comprehensive
comparison of the most up-to-date tools currently available for Run~2 
studies, and compare them using a common choice of input parameters
for the fixed-order NLO QCD calculation and the PS. Some arbitrariness
in the choice of PS-specific parameters can still be present, as will
be manifest in the comparison of observables that are more sensitive
to regions of phase space that are dominated by the PS. We recommend
that the comparison presented in this section serves as the main
reference to anybody interested in using any of the NLO QCD+PS tools
that will be discussed in the following for the production of official
samples of $\tth$ showered events.

We have compared five NLO QCD calculations of $\tth$ consistently
interfaced with either \Sherpa, \Pythiaeight, or \Herwigseven. Namely,
we have compared results from:
\begin{itemize}
\item \Smcnlo\, using \Openloops~1.2.3~+~\Sherpa~2.2.0,
\item \MGfiveamcnlo~2.3.2~+~\Pythiaeight~2.1.0,
\item \Powhel~+~\Pythiaeight~2.1.0,
\item \Powhegbox~+~\Pythiaeight~2.1.0,
\item  \Herwigseven\, using \Openloops~1.2.4+~\MGfiveamcnlo~2.3.0+~\Herwigseven.
\end{itemize}
\Sherpa+\Openloops\, uses \Openloops~\cite{Cascioli:2011va} as a
one-loop generator, and relies on the \Cuttools\,
library ~\cite{Ossola:2007ax} for the numerically stable
evaluation of tensor integrals. Real-emission
contributions, infrared subtractions based on the Catani-Seymour
technique~\cite{Catani:1996vz,Catani:2002hc}, and phase-space
integration are handled by \Sherpa. The NLO corrections are matched to
the \Sherpa\, PS generator~\cite{Schumann:2007mg} using
the \Sherpa\, formulation~\cite{Hoeche:2011fd,Hoche:2012wh} of the
\Mcnlo~\cite{Frixione:2002ik,Frixione:2003ei} method, also dubbed
\Smcnlo.

Within \MGfiveamcnlo~\cite{Hirschi:2011pa,Alwall:2014hca}, fixed-order
NLO QCD results are obtained by adopting the FKS
method~\cite{Frixione:1995ms,Frixione:1997np} for the subtraction of
the infrared divergences of the real-emission matrix elements
(automated in the module \MadFKS~\cite{Frederix:2009yq}), and the
OPP integral-reduction procedure~\cite{Ossola:2006us} for the
computation of the one-loop matrix elements (automated in the module
\Madloop~\cite{Hirschi:2011pa}). Matching with parton showers is
achieved by means of the \Mcnlo\,
formalism~\cite{Frixione:2002ik,Frixione:2003ei}.

The \Powhegbox\,
framework~\cite{Nason:2004rx,Frixione:2007vw,Alioli:2010xd} adopts the
FKS subtraction scheme~\cite{Frixione:1995ms,Frixione:1997np} to
factor out the infrared singularities of the real-emission cross
section, while the the virtual one-loop matrix elements can be
provided with different methods. In the public \Powhegbox~(V2)
distribution of $\tth$, the NLO QCD virtual corrections are
implemented using the one-loop routines from the NLO QCD calculation
of Ref.~\cite{Reina:2001sf,Dawson:2002tg,Dawson:2003zu}. The
corresponding results are labelled as \Powhegbox\, in this section.  On
the other hand, the \Powhel\, generator~\cite{Garzelli:2011vp} uses the \Helacnlo\,
package~\cite{Bevilacqua:2011xh} for the computation of all matrix
elements provided as input to the \Powhegbox. Within the \Powhegbox\,
the matching with parton showers is obtained implementing the \Powheg\,
matching scheme~\cite{Nason:2004rx,Frixione:2007vw,Frixione:2007nw}.
The matched results from the NLO computations are interfaced to
\Pythiaeight\, via Les-Houches event (LHE) files.

\Herwigseven, based on extensions of the previously developed \Matchbox\,
module~\cite{Platzer:2011bc,Bellm:2015jjp}, implements the
Catani-Seymour dipole subtraction
method~\cite{Catani:1996vz,Catani:2002hc} for the infrared divergences
of the real-emission matrix elements, provides for the final-state phase-space
integration, and can interface to a variety
of LO and NLO matrix elements providers, either at the level of
squared matrix elements, based on extensions of the BLHA
standard~\cite{Binoth:2010xt,Alioli:2013nda,Andersen:2014efa}, or at
the level of colour--ordered subamplitudes, where the colour bases are
provided by an interface to the \ColorFull~\cite{Sjodahl:2014opa} and
\CVolver~\cite{Platzer:2013fha} libraries. For this study the relevant
tree-level matrix elements are provided by
\MGfiveamcnlo~\cite{Alwall:2011uj,Alwall:2014hca} (at the level of
colour-ordered subamplitudes), whereas the relevant
tree-level/one-loop interference terms are provided by
\Openloops~\cite{Cascioli:2011va,Cascioli:2014wya} (at the level of
squared matrix elements).  Fully automated NLO matching algorithms are
available, henceforth referred to as subtractive (NLO$\oplus$) and
multiplicative (NLO$\otimes$) matching -- based on the
\Mcnlo~\cite{Frixione:2002ik} and \Powheg~\cite{Nason:2004rx}
formalism respectively -- for the systematic and consistent
combination of NLO QCD calculations with both shower variants (an
angular--ordered parton shower~\cite{Gieseke:2003rz} and a dipole
shower~\cite{Platzer:2009jq}) in \Herwigseven. For this study the
subtractive matching in combination with the angular--ordered parton
shower has been chosen.

For the purpose of the comparison presented in this section, the NLO
QCD calculation has been performed using $N_{\mathrm{F}}=5$ light flavours,
$\sqrt{s}=13$~\UTeV\, for the centre-of-mass energy, $\mh=125$~\UGeV\, for the
Higgs boson mass, and $\mt=172.5$~\UGeV\, for the top-quark mass. The
top-quark Yukawa coupling has been defined in terms of the Fermi
constant as $y_{\mathrm{t}}=(\sqrt{2}G_F)^{1/2}\mt$. We have followed the
recommendation of the Higgs Cross-Section Working
Group~\cite{LHCHXSWG-INT-2015-006} for all other parameters that are not
explicitly given here. We have used a dynamical renormalization
($\muR$) and factorization ($\muF$) scale defined as the geometric
mean of the transverse energies ($E_T$) of the final-state particles
($t$, $\bar{t}$, and $H$). The central value of both scales is then
set to $\mu_0=(E_{\mathrm{T}}(t)E_{\mathrm{T}}(\bar{t})E_{\mathrm{T}}(H))^{1/3}$, where
$E_{\mathrm{T}}=\sqrt{M^2+p_{\mathrm{T}}^2}$ for $M$ the mass of a given particle and $p_{\mathrm{T}}$
its corresponding transverse momentum. Finally, following the 
Higgs Cross Section Working Group recommendation, we have used the PDF4LHC15
parton distribution functions (PDF)~\cite{Butterworth:2015oua}, and
more specifically the central set of PDF4LHC15\_nlo\_30, with NLO
$\als(\mu)$ and $\als(\mz)=0.118$.

The different parton-shower generators have all been set up not to
include hadronization, underlying events, and QED effects in the shower.  
%The shower resummation scale has been set to $H_T/2$ where
%$H_{\mathrm{T}}$ is defined as the sum of the $\tth$ final-state transverse
%energies ($H_{\mathrm{T}}=E_{\mathrm{T}}(t)+E_{\mathrm{T}}(\bar{t})+E_{\mathrm{T}}(H)$). In particular, this
%corresponds to setting $\mu_{\mathrm{Q}}=H_{\mathrm{T}}/2$ in \Smcnlo\, and \MGfiveamcnlo,
%while in \Powhegbox\, it is implemented by setting $h=H_{\mathrm{T}}/2$ in the
%definition of $h_{\mathrm{damp}}=h^2/(h^2+p_{\mathrm{T}}^2)$, where $p_{\mathrm{T}}$ is the
%transverse momentum of the hardest parton in the $O(\als)$ QCD real emission. 
The shower resummation scale has been chosen as consistently as possible among different generators. This corresponds to setting $\mu_Q=H_T/2$ in \Smcnlo\, where $H_{\mathrm{T}}$ is defined as the sum of the $\tth$ final-state transverse energies ($H_{\mathrm{T}}=E_{\mathrm{T}}(t)+E_{\mathrm{T}}(\bar{t})+E_{\mathrm{T}}(H)$),
while using $h=H_{\mathrm{T}}/2$ in the
definition of $h_{\mathrm{damp}}=h^2/(h^2+p_{\mathrm{T}}^2)$ in \Powhegbox\,
where $p_{\mathrm{T}}$ is the transverse momentum of the hardest parton in the $O(\als)$ QCD real emission. In the case of \MGfiveamcnlo\ a different choice had to be adopted since only resummetion scales of the form $\mu_Q=\xi\sqrt{\hat{s}}$ are supported, where the prefactor $\xi$ is randomly distributed in a freely adjustable
$(\xi_{\mathrm{min}},\xi_{\mathrm{max}})$ range. In particular
we have adopted the default recommendation and used
$(\xi_{\mathrm{min}},\xi_{\mathrm{max}})=(0.1,1)$. 
In the case of \Herwigseven\, the hard shower
scale, similarly to the renormalization and factorization scale, has been
set to be the geometric mean of the transverse energies (see
above). Internal studies have shown that a different scale choice for
the hard shower scale results in only small differences in the distributions.


The theoretical uncertainty bands have been calculated purely from the
renormalization and factorization scale dependence, estimated by
varying these scales independently by a factor of two about their
central value ($\muR=\mathswitch{\xi_{\ssR}}\mu_0$ and
$\muF=\mathswitch{\xi_{\ssF}}\mu_0$, with
$\mathswitch{\xi_{\ssR,\ssF}}=1/2,1,2$). For the purpose of this
comparison we have used a common set of PDF and therefore we have not
included in the theoretical error any uncertainty from PDF variation (since it
would have been the same for all results).
No uncertainty from the parton shower has been included. It has been
our goal to investigate if, under physically equivalent choices of the
parton-shower setup, and having eliminated the differences that can
come from different treatment of hadronization and underlying events,
all the tools considered in this comparison give results that are
compatible within the scale uncertainty for all observables
that are not directly affected by parton-shower effects (see discussion
of Figs.~\ref{fig:ttH_stable_NLOPS_1}-\ref{fig:ttH_decays_NLOPS_4}).
Differences that are
observed in regions of phase space dominated by the PS
should be resolved by properly including parton-shower uncertainties,
and this study should serve as solid ground to investigate these
parton-shower specific effects.

Finally, we have considered two scenarios: without and with decays of
the $\tth$ final-state particles. In the second case, we have let
the Higgs boson decay to $b$ quarks, $H\rightarrow b\bar{b}$, while the
top and antitop quarks decay leptonically to $t\rightarrow b e^+\nu_{\mathrm{e}}$
and $\bar{t}\rightarrow \bar{b}\mu^-\bar{\nu}_\mu$, respectively.
Notice that, for the purpose of this comparison, the results presented
include only this specific decay chain, i.e. correspond to the process
$t\bar{t}H\rightarrow e^+\mu^-\nu_{\mathrm{e}}\bar{\nu}_\mu b\bar{b}b\bar{b}$.
Events are required to contain one $e^+$ and one $\mu^-$ with
transverse momentum $p_{\mathrm{T}}^l>20$~\UGeV\, and pseudorapidity $|\eta^l|<2.5$
($l$=lepton), as well as missing transverse energy
$E_{\mathrm{T}}\!\!\!\!\!\!/>30$~\UGeV, and four $b$ jets.  $b$ jets are defined
using the anti-$k_T$ jet algorithm with $R=0.4$, and requiring that
the jet contains at least one $b$ or $\bar{b}$ quark and has
transverse momentum $p_{\mathrm{T}}^{\mathrm{b}}>25$~\UGeV\, and pseudorapidity $|\eta^{\mathrm{b}}|<2.5$.
Finally, spin-correlation effects have been taken into account using
the built-in implementations of each package, like \Madspin~\cite{Artoisenet:2012st} for
\MGfiveamcnlo, \Decayer~\cite{Garzelli:2014dka} for \Powhel, and analogous modules for
\Powhegbox, \Smcnlo, and \Herwigseven, all based on the approach
originally proposed in Ref.~\cite{Frixione:2007zp}.  The results from the five NLO
QCD+PS simulations listed above have been processed through a common
\Rivet\, analysis that implements the selection cuts described above.

In Figs.~\ref{fig:ttH_stable_NLOPS_1}-\ref{fig:ttH_stable_NLOPS_2} we
present results for the comparison of the on-shell case (no decay of
$t$, $\bar{t}$, and $H$ included), while in
Figs.~\ref{fig:ttH_decays_NLOPS_1} -\ref{fig:ttH_decays_NLOPS_4} we
present results for the case of
$t\bar{t}H \rightarrow e^+\mu^-\nu_{\mathrm{e}}\bar{\nu}_\mu b\bar{b}b\bar{b}$.
In order to minimize the effect of treating decays at LO the
corresponding branching ratios have been normalized to
$\mathrm{Br}(H\rightarrow b\bar{b})=57.7\%$ (from
Ref.~\cite{Heinemeyer:2013tqa}, for $\mh=125$~\UGeV), and
$\mathrm{Br}(t\rightarrow b e^+ \nu_{\mathrm{e}})=\mathrm{Br}(W^+\rightarrow
e^+\nu_{\mathrm{e}})=10.83\%$ (from~\cite{Agashe:2014kda}), and similarly for
$\bar{t}\rightarrow\bar{b}\mu^-\bar{\nu}_\mu$.

In each case we compare results for various standard differential
observables. Each plot shows in the upper window the comparison of
results obtained using the five different NLO QCD+PS tools used in our
study, as well as the pure NLO QCD fixed-order results (see
Table~\ref{tab:sm13tev}) which have been used for validation. The
lower windows of each plot illustrate the theoretical uncertainty from
renormalization- and factorization-scale dependence calculated as
previously explained. More specifically, each lower window shows all
results normalized to a particular one, together with the uncertainty
band of the latter. For NLO distributions this uncertainty is of the
order of 10-15\%, but can grow to 20\% or more in the tails of
distributions. On the other hand, a much larger uncertainty affects
distributions like the $p_{\mathrm{T}}$ of the hardest light jet since the
underlying hard process is LO in nature.

The comparison of both total cross sections and distributions shows in
general full compatibility among all sets of results within the
theoretical uncertainty considered in this study.  This is in
particular true for the on-shell case, when decays of the final-state
particles are not considered. This validates the set-up chosen for the
comparison, both at the level of the fixed-order NLO QCD calculation
and at the level of the matching with the PS.

In the case in which the decays of both top-quarks and Higgs boson are
implemented we still see overall very good agreement. We notice some
moderate discrepancies in the distribution in the number of $b$ jets
($d\sigma/dN_{\mathrm{b-jets}}$). In the case of
\Powhel+\Pythiaeight\, the excess in the low $N_{\mathrm{b-jets}}$
bins and the deficit in the high $N_{\mathrm{b-jets}}$ bins are mainly due
to having considered the bottom quarks as massless in the decays of
the top quarks, matched to a parton shower that uses massive bottom 
quarks. As all other distributions in
Figs.~\ref{fig:ttH_decays_NLOPS_1}-\ref{fig:ttH_decays_NLOPS_4} are
obtained from events with exactly four $b$ jets, the difference in the
exclusive $b$-jet multiplicity distribution at $N_{{\rm b-jets}} = 4$
affects the normalization of the distributions, but leaves their
shapes intact.

On the other hand, the overall discrepancy between most
implementations considered for $N_{\mathrm{b-jets}}>4$ is mainly of
parton-shower origin.  Indeed, since the $N_{\mathrm{b-jets}}>4$ bins
are mainly populated by $b$ jets originating in the parton shower,
these effects depend on the specific set up of the parton-shower
algorithm used in each case and should be considered as part of the
theoretical uncertainty coming from the parton shower, which we have
not explicitly quantified in this study. A dedicated study of
parton-shower effects acquires more meaning in the context of specific
experimental analyses, if, for instance, observables like
$d\sigma/dN_{\mathrm{b-jets}}$ for large numbers of $b$ jets had to
become relevant. Having provided a sound comparison of a broad variety
of main NLO QCD+PS frameworks, we have laid the foundation for further
dedicated studies that will likely happen in the context of specific
experimental analyses.


\providecommand{\higgsstableplot}[2]{
\includegraphics[width=0.75\textwidth, trim=0 #1 0 0,clip]{\ttHnlopspath//#2}}

\providecommand{\higgsstablefig}[1]{
\begin{minipage}{0.5\textwidth}
\higgsstableplot{0.115\textwidth}{figs_stable/main/#1}\\
\higgsstableplot{0.115\textwidth}{figs_stable/sherpaopenloops/#1}\\
\higgsstableplot{0.115\textwidth}{figs_stable/mg5amc/#1}\\
\higgsstableplot{0.115\textwidth}{figs_stable/powhel/#1}\\
\higgsstableplot{0.115\textwidth}{figs_stable/powheg/#1}\\
\higgsstableplot{0}{figs_stable/herwig/#1}
\end{minipage}}

\begin{figure}
%\vspace{-2cm}
\higgsstablefig{PT_TOP}
\higgsstablefig{ETA_TOP}\\[2mm]
\higgsstablefig{PT_H}
\higgsstablefig{ETA_H}
\caption{\label{fig:ttH_stable_NLOPS_1} NLO QCD+PS and fixed-order NLO
  QCD predictions for differential $\tth$ observables at 13~\UTeV.
  Each ratio plot shows all results normalized to one particular NLO
  QCD+PS prediction and the scale variation band of the latter.  }
\end{figure}

\begin{figure}
%\vspace{-2cm}
\begin{tabular}{cc}
	\higgsstablefig{PT_TT}	&	\higgsstablefig{PT_TTH}\\[2mm]
	\multicolumn{2}{c}{\higgsstablefig{PT_J1}} \\
\end{tabular}
\caption{\label{fig:ttH_stable_NLOPS_2} NLO QCD+PS and fixed-order NLO
  QCD predictions for differential $\tth$ observables at 13~\UTeV.
  The ratio plots are defined as in \refF{fig:ttH_stable_NLOPS_1}.  }
\end{figure}


\providecommand{\higgsdecaysplot}[2]{
  \includegraphics[width=0.75\textwidth, trim=0 #1 0 0,clip]{\ttHnlopspath/#2}}

\providecommand{\higgsdecaysfig}[1]{
\begin{minipage}{0.49\textwidth}
\higgsdecaysplot{0.115\textwidth}{figs_decays/main/#1}\\
\higgsdecaysplot{0.115\textwidth}{figs_decays/sherpaopenloops/#1}\\
\higgsdecaysplot{0.115\textwidth}{figs_decays/mg5amc/#1}\\
\higgsdecaysplot{0.115\textwidth}{figs_decays/powhel/#1}\\
\higgsdecaysplot{0.115\textwidth}{figs_decays/powheg/#1}\\
\higgsdecaysplot{0}{figs_decays/herwig/#1}
\end{minipage}}


\begin{figure}
%\vspace{-2cm}
\centering
\higgsdecaysfig{PT_EL}
\higgsdecaysfig{ETA_EL}\\[2mm]
\higgsdecaysfig{PT_MU}
\higgsdecaysfig{ETA_MU}
\caption{\label{fig:ttH_decays_NLOPS_1}
NLO QCD+PS predictions for differential $\tth$ observables with
$t\bar{t}H\to e^+\mu^-\nu_{\mathrm{e}}\bar{\nu}_\mu+b\bar{b}b\bar{b}$
at 13~\UTeV. The ratio plots are defined as in \refF{fig:ttH_stable_NLOPS_1}.}
\end{figure}

\begin{figure}
%\vspace{-2cm}
\centering
\higgsdecaysfig{MET}
\higgsdecaysfig{DPHI_LL}\\[2mm]
\higgsdecaysfig{NBj_Exc_XS}
\higgsdecaysfig{NBj_Inc_XS}
\caption{\label{fig:ttH_decays_NLOPS_2}
NLO QCD+PS predictions for differential $\tth$ observables with
$t\bar{t}H\to e^+\mu^-\nu_{\mathrm{e}}\bar{\nu}_\mu+b\bar{b}b\bar{b}$ at 13~\UTeV.
The ratio plots are defined as in \refF{fig:ttH_stable_NLOPS_1}.}
\end{figure}

\begin{figure}
%\vspace{-2cm}
\centering
\higgsdecaysfig{PT_B1}
\higgsdecaysfig{PT_B2}\\[2mm]
\higgsdecaysfig{PT_B3}
\higgsdecaysfig{PT_B4}
\caption{\label{fig:ttH_decays_NLOPS_3}
NLO QCD+PS predictions for differential $\tth$ observables with
$t\bar{t}H\to e^+\mu^-\nu_{\mathrm{e}}\bar{\nu}_\mu+b\bar{b}b\bar{b}$ at 13~\UTeV.
The ratio plots are defined as in \refF{fig:ttH_stable_NLOPS_1}.}
\end{figure}


\begin{figure}
%\vspace{-2cm}
\centering
\higgsdecaysfig{ETA_B1}
\higgsdecaysfig{ETA_B2}\\[2mm]
\higgsdecaysfig{ETA_B3}
\higgsdecaysfig{ETA_B4}
\caption{\label{fig:ttH_decays_NLOPS_4}
NLO QCD+PS predictions for differential $\tth$ observables with
$t\bar{t}H\to e^+\mu^-\nu_{\mathrm{e}}\bar{\nu}_\mu+b\bar{b}b\bar{b}$ at 13~\UTeV.
The ratio plots are defined as in \refF{fig:ttH_stable_NLOPS_1}.}
\end{figure}
