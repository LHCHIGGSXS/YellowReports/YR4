\providecommand{\ttv}{t\bar t V}
\providecommand{\ttvv}{t\bar t VV}
\providecommand{\ttz}{t\bar t Z}
\providecommand{\ttwpm}{t\bar t W^{\pm}}
\providecommand{\ttwp}{t\bar t W^{+}}
\providecommand{\ttwm}{t\bar t W^{-}}


The production of a $\ttbar$ pair in association with electroweak vector bosons represent an important source of background
to $\tth$ production in the $H\to \bbbar$, $H\to WW^*$ and $H\to \tau\tau$ channels.
In this section we present NLO QCD+EW predictions for inclusive and differential $\ttz$ and $\ttwpm$
cross sections, a comparison of NLO QCD distributions obtained with various automated tools,
as well as NLO QCD predictions for $\ttbar$ production in association with two vector bosons.


\subsection{NLO QCD+EW predictions for \texorpdfstring{$t\bar{t}Z$}{ttZ} and \texorpdfstring{$t\bar{t}W^\pm$}{ttW} production}
\label{sec:ttH-ttV-QCD+EW}

Predictions for $\ttbar$ production in association with a vector boson
$V=Z,W^\pm$ at NLO QCD have been presented in~\cite{Lazopoulos:2008de,Kardos:2011na,Campbell:2012dh} and matched to the parton shower
in~\cite{Garzelli:2011is,Garzelli:2012bn}, while the first calculation of electroweak corrections
for this class of processes has been completed more
recently~\cite{Frixione:2015zaa}.  In the following we present NLO QCD+EW
predictions for inclusive $\ttz$ and $\ttwpm$ production
at $\sqrt{s}=13\,\UTeV$ and $14\,\UTeV$.
All input parameters  are chosen according to the HXSWG recommendations~\cite{LHCHXSWG-INT-2015-006}, and the
hadronic cross section is obtained using the PDF4LHC15~\cite{Butterworth:2015oua}
and NNPDF2.3QED~\cite{Ball:2013hta} parton distributions as explained below.
The top-quark and the electroweak vector bosons are treated as stable particles,
and for the renormalization of the respective mass paramaters the on-shell scheme is used.
The Higgs boson mass is set to $\mh=125\UGeV$.
The electroweak couplings and mixing angle are evaluated in the $G_\mu$-scheme using the
Fermi constant and the vector-boson masses as input parameters.
The central value for renormalization and factorization
scales is set to
\begin{equation}
\mu_0 = \mt + M_{\mathrm{V}}/2\,.
\end{equation}
For the NLO QCD part of the calculation scale uncertainties are
estimated by independent variations of renormalization and factorization
scales in the range $ \mu/2 \le \muR, \muF \le 2\mu$, with $1/2 \le \muR
/ \muF \le 2$, while for PDF and $\als$
uncertainties the  PDF4LHC15 prescription is used.
The resulting uncertainties are applied also to NLO EW correction effects.


The NLO QCD+EW predictions presented in the following,
\begin{equation}
\sigmaNLOqcdew=\sigmaNLOqcd+\dsigmaew\,,
\label{eq:ttvqcdew}
\end{equation}
result from the combination of various contributions.  The usual NLO QCD cross section,
\begin{equation}
\sigmaNLOqcd=\sigmaLOqcd+\delta\sigmaNLOqcd,
\end{equation}
comprises LO terms of $\mathcal{O}(\als^2\alpha)$ and NLO QCD corrections of
$\mathcal{O}(\als^3\alpha)$, which involve $gg$, $q\bar q$ and $gq$
partonic channels. Note that the $gg$ channel
starts contributing only at NNLO QCD in the case of
$\ttwpm$ production.
The remaining EW corrections, denoted as $\dsigmaew$,
include three types of terms:
%
\begin{enumerate}

\item LO EW terms of
$\mathcal{O}(\alpha^3)$ that result from squared EW tree amplitudes in the
$q\bar q$ and $\gamma\gamma$ channels.

\item LO mixed terms  of
$\mathcal{O}(\als\alpha^2)$ that result from the interference of EW and
QCD tree diagrams in the $b\bar b$ and $\gamma g$ channels. Other $q\bar q$
channels do not contribute at this order due to the vanishing interference
of the related colour structures. Thus $\ttwpm$ production does not receive any
$\mathcal{O}(\als\alpha^2)$ contribution.


\item NLO EW corrections of
$\mathcal{O}(\als^2\alpha^2)$ in the $q\bar q$, $gg$ and $\gamma g$
channels.  Subleading NLO terms of $\mathcal{O}(\als\alpha^3)$ and
$\mathcal{O}(\alpha^4)$ are not included as they are expected to be strongly
suppressed.
\end{enumerate}
At $\sqrt{s}=13$--$14\,\UTeV$, LO EW contributions
to $\ttz$ and $\ttwpm$ production
correspond to $+1.2\%$ and $+0.6\%$ of the respective NLO QCD cross sections,
while LO mixed effects are at the sub-per mille level.
Order $\als^2\alpha^2$  NLO EW corrections to the
$\ttz$, $\ttwp$ and $\ttwm$ cross sections
amount to $-0.9\%$, $-3.3\%$ and $-2.6\%$, respectively.\footnote{Here $\mathcal{O}(\alpha)$ effects related
to the QED evolution of PDF (see below) are not included.}
Photon-induced channels have a non-negligible impact ($+0.8\%$) only
in the $\ttz$ cross section at $\mathcal{O}(\als\alpha^2)$. However
their effect is almost completely cancelled by the contribution of the $b\bar b$ channel
at the same order.


Effects of $\mathcal{O}(\alpha)$ related to the QED evolution of PDF and initial-state photons
are included in the same way as discussed in Sect.~\ref{sec:ttH-XS}
for $\tth$ production: (i) all NLO QCD+EW
predictions for partonic channels with initial-state gluons and quarks are
computed using the PDF4LHC15 set with 30+2 members; (ii) for $\gamma$-induced
channels NNPDF2.3QED set (with $\als(\mz)=0.118$) is used;
(iii) the effect of the QED evolution of quark PDF is estimated from the
difference between NNPDF2.3QED and NNPDF2.3 parton densities
as indicated in~\refE{eq:tthpdfqed}, and
for a more detailed discussion we refer to Sect.~\ref{sec:ttH-XS}.
Similarly as for $\tth$ production, the effect of QED PDF evolution is
negative and ranges from $-0.6\%$ to $-0.8\%$.

Corrections of order $\alpha^2\als^2$ resulting from the emission of and
extra heavy boson have been discussed in~\cite{Frixione:2015zaa}.  Although
they enter at the same perturbative order as the NLO EW corrections to
$\ttv$ production, such contribution represent
separate physics processes, namely $\ttbar$ production in associated with
two vector bosons. Corresponding
predictions at NLO QCD are presented in Sect.~\ref{sec:ttH-ttVV}.


Predictions for inclusive $\ttv$ cross sections at NLO QCD+EW for $\sqrt{s}=13$ and $14\,\UTeV$ are listed
in \refTs{tab:ttvXSa}--\ref{tab:ttvXSb}. The
impact of QCD and EW corrections is shown in the form of a QCD correction
factor
\begin{equation}
\label{eq:ttvqcd}
K_{\mathrm{QCD}}=\frac{\sigmaNLOqcd}{\sigmaLOqcd}\,,
\end{equation}
and a relative EW correction factor
\begin{equation}
\delta_{\mathrm{EW}}=\dsigmaew/\sigmaNLOqcd\,.
\label{eq:ttvdsiew}
\end{equation}
Electroweak corrections include all LO and NLO EW
effects discussed above, i.e.~also those arising form the QED evolution of PDF.
All quantities in \refE{eq:ttvqcd}--\refE{eq:ttvdsiew}
are obtained using NLO QCD PDF.
Results in \refTs{tab:ttvXSa}--\ref{tab:ttvXSb} have been obtained with
\MGfiveamcnlo~\cite{Alwall:2014hca}.  A cross check against an independent
calculation based on \Sherpa+\Openloops~\cite{Kallweit:2014xda} has
confirmed the correctness of NLO QCD+EW predictions at the level of the
quoted statistical accuracy (0.1--0.2\%).



%\begin{table}
%\centering
%\begin{tabular}{ccccccccc}
%\hline
%Process &  $\sqrt{s}$ &  $\sigmaLOqcd$    &  $\sigmaNLOqcd$ &  $\sigmaNLOqcdew$  & $K_{\mathrm{QCD}}$ & $\deltaEW$[\%]&  Scale[\%]            &  PDF+$\alpha_S$[\%] \\   \hline
%$\ttz$  &  $13$       &  $603.5(\pm0.2)$  &  $841.3(\pm 1.6)$ &  $839.3(\pm 1.6)$  & $1.39$             & $-0.2$        &  $+9.6\%\;\;-11.3\%$  &  $+2.8\%\;\;-2.8\%$ \\
%$\ttwp$ &  $13$       &  $277.4(\pm0.09)$ &  $412.0(\pm0.32)$ &  $397.6(\pm 0.32)$ & $1.49$             & $-3.5$        &  $+12.7\%\;\;-11.4\%$ &  $+2.0\%\;\;-2.0\%$ \\
%$\ttwm$ &  $13$       &  $137.8(\pm 0.03)$&  $208.6(\pm0.16)$ &  $203.2(\pm 0.16)$ & $1.51$             & $-2.6$        &  $+13.3\%\;\;-11.7\%$ &  $+2.1\%\;\;-2.1\%$ \\
%\end{tabular}
%\caption{Inclusive $\ttbar V$ cross sections at the LHC for $\sqrt{s}=13$\UTeV~with $\mu_0 = m_t + m_V/2$.
%Collider energy and cross sections are in TeV and femtobarn, respectively.
%Statistical uncertainties are quoted in parenthesis.}
%\label{tab:ttvXSa}
%\end{table}


\begin{table}\footnotesize
\caption{Inclusive $\ttv$ cross sections at NLO QCD and NLO QCD+EW accuracy for $\sqrt{s}=13$~\UTeV.
NLO QCD+EW results represent the best predictions and should be used in experimental analyses.
Scale, PDF, and $\als$ uncertainties are quoted in per cent. Absolute statistical uncertainties
are indicated in parenthesis. We also quote the NLO QCD+EW $\ttwm +
\ttwp$  combined cross
sections where correlation effects have been
consistently included in the estimate of the corresponding uncertainties.  Collider energy and cross sections are in \UTeV\, and femtobarn, respectively.}
\label{tab:ttvXSa}
\setlength{\tabcolsep}{1.0ex}
\centering
\renewcommand{\arraystretch}{1.2}
\begin{tabular}{lcccccccc}
\toprule
Process &  $\sqrt{s}$ &  $\sigmaNLOqcd$   &  $\sigmaNLOqcdew$  & $K_{\mathrm{QCD}}$ & $\deltaEW$[\%]&  Scale[\%]              &  PDF[\%]   &  $\alpha_S$[\%]   \\   
\midrule
$\ttz$  &  $13$       &  $841.3( 1.6)$ &  $839.3( 1.6)$  & $1.39$             & $-0.2$        &  $+9.6\%\;\;-11.3\%$  &  $+2.8\%\;\;-2.8\%$   &  $+2.8\%\;\;-2.8\%$   \\
$\ttwp$ &  $13$       &  $412.0(0.32)$ &  $397.6( 0.32)$ & $1.49$             & $-3.5$        &  $+12.7\%\;\;-11.4\%$ &  $+2.0\%\;\;-2.0\%$   &  $+2.6\%\;\;-2.6\%$   \\
$\ttwm$ &  $13$       &  $208.6(0.16)$ &  $203.2( 0.16)$ & $1.51$             & $-2.6$        &  $+13.3\%\;\;-11.7\%$ &  $+2.1\%\;\;-2.1\%$   &  $+2.9\%\;\;-2.9\%$   \\
$\ttwm + \ttwp$ &  $13$       &  $620.6(0.36)$ &  $600.8(0.36)$ &  $1.50$     & $-3.2$      &  $+12.9\%\;\;-11.5\%$ &  $+2.0\%\;\;-2.0\%$   &  $+2.7\%\;\;-2.7\%$   \\
\bottomrule
\end{tabular}
\end{table}


%\begin{table}
%\centering
%\begin{tabular}{ccccccccc}
%\hline
%Process &  $\sqrt{s}$ &  $\sigmaLOqcd$    &  $\sigmaNLOqcd$ &  $\sigmaNLOqcdew$ & $K_{\mathrm{QCD}}$ & $\deltaEW$[\%]&  Scale[\%]              &  PDF+$\alpha_S$[\%] \\   \hline
%$\ttz$  &  $14$       &  $729.3(\pm0.3)$  &  $1018(\pm2.2)$   &  $1015(\pm2.2)$   & $1.40$             & $-0.3$        &  $+9.6\%\;\;-11.2\%$  &  $+2.7\%\;\;-2.7\%$ \\
%$\ttwp$ &  $14$       &  $313.5(\pm0.07)$ &  $474.9(\pm0.36)$ &  $458.2(\pm0.36)$ & $1.51$             & $-3.5$        &  $+13.2\%\;\;-11.6\%$ &  $+1.9\%\;\;-1.9\%$ \\
%$\ttwm$ &  $14$       &  $159.0(\pm0.04)$&   $244.5(\pm0.17)$ &  $238.0(\pm0.17)$ & $1.54$             & $-2.7$        &  $+13.8\%\;\;-11.8\%$ &  $+2.0\%\;\;-2.0\%$ \\
%\end{tabular}
%\caption{Inclusive $\ttbar V$ cross sections at the LHC for $\sqrt{s}=14$\UTeV~with $\mu_0 = m_t + m_V/2$.
%Collider energy and cross sections are in TeV and femtobarn, respectively.
%Statistical uncertainties are quoted in parenthesis.}
%\label{tab:ttvXSb}
%\end{table}



\begin{table}
\footnotesize
\caption{Inclusive $\ttv$ cross sections at NLO QCD and NLO QCD+EW accuracy for $\sqrt{s}=14$~\UTeV.
NLO QCD+EW results represent the best predictions and should be used in experimental analyses.
Scale, PDF, and $\als$ uncertainties are quoted in per cent. Absolute statistical uncertainties
are indicated in parenthesis. Collider energy and cross sections are in \UTeV\, and femtobarn, respectively.}
\label{tab:ttvXSb}
\setlength{\tabcolsep}{1.0ex}
\centering
\renewcommand{\arraystretch}{1.2}
\begin{tabular}{lcccccccc}
\toprule
Process &  $\sqrt{s}$ &  $\sigmaNLOqcd$ &  $\sigmaNLOqcdew$ & $K_{\mathrm{QCD}}$ & $\deltaEW$[\%]&  Scale[\%]               &  PDF[\%] &  $\alpha_S$[\%] \\   
\midrule
$\ttz$  &  $14$       &  $1018(2.2)$   &  $1015(2.2)$   & $1.40$             & $-0.3$        &  $+9.6\%\;\;-11.2\%$   &  $+2.7\%\;\;-2.7\%$ &  $+2.8\%\;\;-2.8\%$   \\
$\ttwp$ &  $14$       &  $474.9(0.36)$ &  $458.2(0.36)$ & $1.51$             & $-3.5$        &  $+13.2\%\;\;-11.6\%$  &  $+1.9\%\;\;-1.9\%$ &  $+2.6\%\;\;-2.6\%$   \\
$\ttwm$ &  $14$       &   $244.5(0.17)$ &  $238.0(0.17)$ & $1.54$             & $-2.7$        &  $+13.8\%\;\;-11.8\%$ &  $+2.0\%\;\;-2.0\%$ &  $+2.9\%\;\;-2.9\%$   \\
\bottomrule
\end{tabular}
\end{table}



The size of the corrections as well as the scale and PDF+$\als$
uncertainties vary very little form 13 to 14~\UTeV. For $\ttwpm$
production the QCD and EW corrections as well as the NLO scale
uncertainties turn out to be slightly more pronounced as compared to
$\ttz$ production.  Scale variations range from 10 to 13\% and
represent the dominant source of uncertainty.  It was checked that
replacing the fixed scale $\mu=\mt+M_{\mathrm{V}}/2$ by a dynamic
scale $\mu=H_{\mathrm{T}}/2$ shifts all $\ttv$ cross sections by
$-7\%$, which is consistent with the scale uncertainties quoted
in~\refTs{tab:ttvXSa}--\ref{tab:ttvXSb}.  The combined PDF+$\als$
uncertainty amounts to $2$--$3\%$, and EW correction effects turn out
to be similarly small (between $-0.2\%$ and $-3.5\%$).

In the tails of transverse-momentum distributions NLO electroweak
effects can become more sizeable due to the appearance of Sudakov
logarithms. This is illustrated in
\refFs{fig:ttZEW}--\ref{fig:ttWmEW}, which display differential NLO
QCD+EW predictions obtained with \MGfiveamcnlo\, for $\ttz$ , $\ttwp$
and $\ttwm$ production, respectively, using the same setup and fixed
scale choice as described above.


%TODO: short discussion of NLO EW effects in distributions.


\begin{figure}
	\centering
	\includegraphics[page=1, trim=80 100 40 80, clip,scale=0.32]{\ttHttvqcdewpath/Figures/ttZ.pdf}
	\includegraphics[page=2, trim=80 100 40 80, clip,scale=0.32]{\ttHttvqcdewpath/Figures/ttZ.pdf}
	\includegraphics[page=3, trim=80 100 40 80, clip,scale=0.32]{\ttHttvqcdewpath/Figures/ttZ.pdf}
	\caption{Transverse momentum ($p_{\mathrm{T}}$) distribution of the top quark, anti-top quark, and $Z$ boson.}
\label{fig:ttZEW}
\end{figure}


\begin{figure}
	\centering
	\includegraphics[page=1, trim=80 100 40 80, clip,scale=0.32]{\ttHttvqcdewpath/Figures/ttWp.pdf}
	\includegraphics[page=2, trim=80 100 40 80, clip,scale=0.32]{\ttHttvqcdewpath/Figures/ttWp.pdf}
	\includegraphics[page=3, trim=80 100 40 80, clip,scale=0.32]{\ttHttvqcdewpath/Figures/ttWp.pdf}
	\caption{Transverse momentum ($p_{\mathrm{T}}$) distribution of the top quark, anti-top quark, and $W^+$ boson.}
\label{fig:ttWpEW}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[page=1, trim=80 100 40 80, clip,scale=0.32]{\ttHttvqcdewpath/Figures/ttWm.pdf}
	\includegraphics[page=2, trim=80 100 40 80, clip,scale=0.32]{\ttHttvqcdewpath/Figures/ttWm.pdf}
	\includegraphics[page=3, trim=80 100 40 80, clip,scale=0.32]{\ttHttvqcdewpath/Figures/ttWm.pdf}
	\caption{Transverse momentum ($p_{\mathrm{T}}$) distribution of the top quark, anti-top quark, and $W^-$ boson.}
\label{fig:ttWmEW}
\end{figure}
