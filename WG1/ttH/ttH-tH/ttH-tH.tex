\section{\texorpdfstring{$tH$}{tH} production at NLO in QCD}
\label{sec:tH_NLO_QCD}

The production of a Higgs boson in association with a single top quark ($tH$)
at hadron colliders shares important analogies with the
electroweak production of a single top quark.
At LO one can organize the Feynman diagrams into three
independent (non-interfering) sets, based on the virtuality
of the $W$ boson coupled to the heavy-quark $b{-}t$ current:
$t$-channel production features a virtual space-like $W$,
$s$-channel production a virtual time-like $W$,
and $W$-associated production an on-shell $W$ boson in the final state.
This classification is useful for event generation, but is valid
only at LO and in the five-flavour scheme (5FS); when adding higher-order
corrections, or when employing the four-flavour scheme (4FS),
the picture becomes fuzzy because some amplitudes can interfere.

In the 5FS the separation between $t$-channel, $s$-channel
and $W$-associated production is exact up to NLO in QCD
(channels start to interfere at NNLO), and calculations are
typically simpler than in the 4FS.
In the 4FS, instead, the $t$-channel process
can interfere already at NLO in QCD both with (NNLO) $s$-channel
and with $W$-associated production (only if the $W$ boson decays hadronically);
nevertheless, such interference is tiny at NLO and can be neglected if
the aim is to compute the dominant $t$-channel process.
Therefore, the separate simulation of $t$-channel and $s$-channel $tH$
processes is not a problem up to NLO accuracy in QCD.
A detailed study of these two channels at the LHC
has been presented recently in \Bref{Demartin:2015uha}, including NLO QCD
corrections and addressing many sources of uncertainty
(notably the flavour-scheme dependence of the $t$-channel process).
We will review $t$-channel $tH$ production in \Section~\ref{sec:tH_tcha},
and for $s$-channel $tH$ production in \Section~\ref{sec:tH_scha}.

Associated $tWH$ production, on the other hand, interferes also
with the much larger $\tth$ process starting from NLO in QCD in the 5FS,
and this happens already at LO in the 4FS.
In general, such interference is large and cannot be neglected,
thus NLO simulations of the $tWH$ channel are not straightforward to carry out. For a recent NLO computation of this process in the 5FS, see \Ref~\cite{Demartin:2016axk}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  t-channel subsection
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{\texorpdfstring{$t$}{t}-channel \texorpdfstring{$tH$}{tH} production}
\label{sec:tH_tcha}

\begin{figure}
\center
\includegraphics[width=0.6\textwidth]{\ttHtHpath/tH_tchannel_4F.pdf}
\hspace{4em}
\includegraphics[width=0.24\textwidth]{\ttHtHpath/tH_tchannel_5F.pdf}\\[1ex]
\caption{LO Feynman diagrams for $t$-channel $tH$ production in the 4FS
 (left) and in the 5FS (right).}
\label{fig:tH_tcha_diagrams}
\end{figure}

Being a process initiated by bottom quarks,
the $t$-channel $tH$ production can be computed either with the 4FS
or with the 5FS approach.
In the 4FS the hard matrix element and the phase space are computed taking
into account all the bottom-quark (pole) mass effects, but there is no
$b$ parton distribution function (PDF), thus $b$ quarks are generated
perturbatively in the hard matrix element via initial-state gluon splitting into
a $b\bar b$ pair (see \refF{fig:tH_tcha_diagrams}, left).
In the 5FS potentially large logarithms associated with
such $g \to b \bar b$ splitting are resummed
to all orders into an initial-state bottom-quark PDF
(see \refF{fig:tH_tcha_diagrams}, right),
while all other bottom-quark mass effects are neglected
($M_{\mathrm{b}}=0$), and a correct description of the process transverse dynamics is included only at NLO.
Both approaches feature advantages and shortcomings at the LHC.
Their difference mainly consists of what kind of terms are kept in the
perturbative expansion or pushed into
missing higher-order corrections, thus it becomes milder when including
more terms in perturbative QCD.
Therefore, NLO accuracy is mandatory to reduce the flavour-scheme dependence
of theoretical predictions. Moreover, a comparison of 4FS and 5FS results
can offer some insights on the relevance of missing higher-order
corrections for processes that are more sensitive to large
initial-state logarithmic corrections, and should therefore be taken
into account in estimating the theoretical uncertainty for a given process.

The results presented in this section have been obtained
in the \MGfiveamcnlo\, framework~\cite{Alwall:2014hca},
where the NLO simulation can be generated
automatically in both the 4FS and 5FS~\cite{Demartin:2015uha}.
The top quarks can then be decayed with \Madspin~\cite{Artoisenet:2012st},
thereby keeping spin correlations.


\subsubsection{Total cross section at NLO}

In this section we address the inclusive cross section at NLO accuracy in QCD.
The numerical results presented here have been obtained using
the input parameters listed below, which follow the prescriptions given
in \Brefs{LHCHXSWG-INT-2015-006,Butterworth:2015oua}.
The pole mass of the top quark and its Yukawa coupling\footnote{In the adopted convention the Feynman rule of
  the $q\bar{q}H$ vertex is $(-iy_{\mathrm{q}})$ for a generic quark $q$.}
(renormalized on shell) are
\begin{equation}
\label{eq:mt_yt}
\mt = 172.5\UGeV \,, \hspace{4em}
y_{\mathrm{t}}=\frac{\mt}{v} = \big( \sqrt{2} G_\mu \big)^{1/2} \mt \,,
\end{equation}
where $v \simeq 246\UGeV$ is the EW vacuum expectation value.
The bottom-quark pole mass in the 4FS (left) and 5FS (right) is set to
\begin{equation}
M_{\mathrm{b}}^\mathrm{(4FS)} = 4.92\UGeV \,,
\hspace{4em}
M_{\mathrm{b}}^\mathrm{(5FS)} = 0 \,,
\end{equation}
while the bottom-quark Yukawa coupling is always set to zero,
$y_{\mathrm{b}}=0 $\,,
because its impact on the
total cross section amounts to less than $0.1\%$.
Actually, to speed up the 4FS code, the corresponding diagrams
are not even generated.
%, setting the bottom-quark Yukawa coupling to zero before loading the
%\texttt{loop\_sm} model).
The EW parameters are
\begin{equation}
G_\mu = 1.166379 \cdot 10^{-5}\UGeV^{-2} \,, \hspace*{3em}
\mz= 91.1876\UGeV \,, \hspace*{3em}
\mw = 80.385\UGeV \,,
\end{equation}
which in turn fix the electromagnetic coupling (no running) and the on-shell
weak mixing angle to
\begin{equation}
\alpha = \sqrt{2} G_\mu \mw^2 (1 - \mw^2/\mz^2) / \pi \simeq 1/132.233  \,,
\hspace*{3em}
\sin^2 \theta_{\mathrm{W}} = 1 - \mw^2/\mz^2 \simeq 0.2229 \,.
\end{equation}
We assume $V_{\mathrm{tb}}=1$ and, for simplicity, the whole CKM matrix to be diagonal%
\footnote{The only important assumption here is $V_{\mathrm{tb}}=1$;
once the third generation is decoupled from the first two, and if one is
inclusive over the first two generations, then the result doesn't depend on the
mixing between the first two generations (i.e. the Cabibbo angle) due to unitarity.}
\begin{equation}
 V_{\mathrm{CKM}} = \mathrm{diag} \big\{ V_{\mathrm{ud}}, V_{\mathrm{cs}}, V_{\mathrm{tb}} \big\} = \mathrm{diag} \big\{ 1,1,1 \big\} \,.
\end{equation}
The proton content in terms of parton distribution functions (PDFs)
is evaluated by using the NLO \textsc{PDF4LHC15} sets
in the corresponding flavour-number scheme.
The PDFs also determine the reference value of the strong coupling
used in the simulation, which then is automatically run at 2-loop accuracy.
In the 5FS this value and its uncertainty are
\begin{equation}
\label{eq:tH_tcha_alphas}
\als^\mathrm{(5FS)}(\mz) = 0.1180 \pm 0.0015 \,,
\end{equation}
while in the 4FS $\als(\mz)$ is slightly smaller and consistent with
a four-flavour running~\cite{Butterworth:2015oua}.
The combined PDF+$\als$ uncertainty is computed from the
Hessian set with 30 (PDF) + 2 ($\als$) members, accordingly to
\Eq~(28) in \Ref~\cite{Butterworth:2015oua}.
The renormalization $\muR$ and factorization $\muF$ scales
are both set equal to the reference value
\begin{equation}
\label{eq:tH_tcha_scale}
\mu_0^{(t-\mathrm{channel})} = (\mh+\mt)/4 \,,
\end{equation}
while the scale dependence in each flavour scheme is estimated from the
maximum and minumum variations of the cross section
among six scale points with
\begin{equation}
\label{eq:tH_tcha_scalevariations}
1/2 < \mu_{\mathrm{R,F}}/\mu_0 < 2 \,, \qquad 1/2 < \muR/\muF < 2 \,.
\end{equation}
The reference scale choice in \Eq~\eqref{eq:tH_tcha_scale} is motivated
by physical arguments in the 4FS description~\cite{Maltoni:2012pa}. In particular,
it ensures that the discrepancy between the 4FS and 5FS results
is not unreasonably large, and that the 5FS uncertainty is not underestimated,
which might happen when using a very high scale
(see Figure~3 in \Ref~\cite{Demartin:2015uha}).

In \Trefs{tab:tH_tcha_SM_lhc7}, \ref{tab:tH_tcha_SM_lhc8},
\ref{tab:tH_tcha_SM_lhc13} and \ref{tab:tH_tcha_SM_lhc14}
we collect the results for the combined $t$-channel $pp \to tH + \bar tH$ production
at the LHC, at centre-of-mass energies of
$\sqrt{s} = 7,8,13,$ and $14\UTeV$, respectively, and for various Higgs boson masses
in the range $120{-}130\UGeV$.
In the third column we report the reference cross section (in\Ufb),
$\sigma_{tH+\bar tH}$, computed at NLO and in the 5FS, while in the fourth
column we report the NLO $K_{\mathrm{QCD}}$ factor, defined as
\begin{equation}
K_{\mathrm{QCD}}= \sigma^{\mathrm{NLO~QCD}}_{tH + \bar tH} / \sigma^{\mathrm{LO}}_{tH + \bar tH} \,,
\end{equation}
where both the LO and NLO cross sections are computed with the same inputs.
In the fifth column we report the combined scale plus flavour-scheme (FS)
uncertainty, expressed as upper and lower per cent variations
with respect to the reference 5FS prediction.
The combined scale+FS uncertainty band is the largest source of theoretical
uncertainty, and it is computed from the maximum
and minimum variations of the cross section among the 6+6 scale points
according to \Erefs{eq:tH_tcha_scalevariations}
in the two flavour schemes.
This translates into the following equations
\begin{equation}
 \sigma^{+} = \max \limits_{
 \substack{(\mu_R,\mu_F)~\mathrm{points} \\[0.2ex] \mathrm{4FS,~5FS}} }
 \, \sigma^{(\mathrm{FS})}_{tH + \bar tH}(\muR,\muF)  \,,
 \hspace*{4em}
 \sigma^{-} = \min \limits_{
 \substack{(\muR,\muF)~\mathrm{points} \\[0.2ex] \mathrm{4FS,~5FS}} }
 \, \sigma^{(\mathrm{FS})}_{tH + \bar tH}(\mu_R,\mu_F)  \,,
\end{equation}
\begin{equation}
 \mathrm{Scale+FS~[\%]} =
100\left( \sigma^{+} / \sigma^{(\mathrm{5FS})}_{tH+\bar tH} -1 \right)
 \hspace*{2em}
 100 \left( \sigma^{-} / \sigma^{(\mathrm{5FS})}_{tH+\bar tH} -1 \right) \,.
\end{equation}
In the sixth, seventh, and eighth columns we report the $\als$, PDF, and
combined PDF+$\als$ uncertainty in the 5FS,
which is the second-largest source of theoretical uncertainty.
We recall that it is computed employing the \textsc{PDF4LHC15}
Hessian set with 30 (PDF) + 2 ($\als$) members, with the $\als$
uncertainty given in \Eq~\eqref{eq:tH_tcha_alphas}, and combining
the two uncertainties in quadrature accordingly to the
\textsc{PDF4LHC15} prescription.
Finally, in the last two columns we report the separate top ($tH$)
and anti-top ($\bar tH$) contributions to the 5FS cross section (in\Ufb).
In \Tref{tab:tH_tcha_SM_lhc6to15} we repeat the exercise, this time
keeping the Higgs boson mass fixed to $\mh=125\UGeV$ and varying instead
the collider energy in the range $\sqrt{s} = 6{-}15\UTeV$, to show
the gain in the cross section and the reduction of uncertainties.
The numbers in \Trefs{tab:tH_tcha_SM_lhc7} to \ref{tab:tH_tcha_SM_lhc6to15},
relevant for the SM Higgs boson, are summarized in the plots of
\refF{fig:tH_tcha_SM_plots} where the blue uncertainty band
is computed summing the scale+FS and PDF+$\als$ uncertainties.

We conclude the discussion of results relevant for the SM Higgs boson
by commenting on two minor uncertainties, namely the ones associated
with the bottom-quark and top-quark masses.
According to \Ref~\cite{LHCHXSWG-INT-2015-006}, we take the uncertainty
on the bottom-quark mass to be $M_{\mathrm{b}} = 4.92 \pm 0.13\UGeV$. At $13\UTeV$,
and for a $125\UGeV$ Higgs boson mass, this translates into a 4FS cross section
of $\sigma^\mathrm{(4FS)}_{tH + \bar tH} = 67.4^{+0.7}_{-0.5}\Ufb$,
which corresponds to an uncertainty of about $1\%$.
Since no \textsc{PDF4LHC15} set with heavy-quark mass variations
has been published yet, we estimate the impact on the 5FS cross section
using the numbers in \Ref~\cite{Demartin:2015uha},
where previous-generation PDF sets have been used.

The $\pm 0.25\UGeV$ bottom-mass uncertainty quoted in \Ref~\cite{Demartin:2015uha} returned an uncertainty in the 5FS cross section of about $2\%$.
A crude rescaling to $\pm 0.13\UGeV$ results in an uncertainty of roughly  $1\%$, comparable to the one in the 4FS.

%A crude rescaling of the $\pm 0.25\UGeV$ (about $2\%$) bottom-quark mass uncertainty
%quoted in \Ref.~\cite{Demartin:2015uha} gives
%a 5FS cross-section uncertainty of $\pm 0.13\UGeV$, i.e. about $1\%$.
%tells us that the $M_{\mathrm{b}}$ uncertainty should be of order
%in the 5FS as well.


Similarly, we consider a top-quark mass uncertainty of
$\mt = 172.5 \pm 1.0\UGeV$, which returns a 5FS cross section of
$\sigma^\mathrm{(5FS)}_{tH + \bar tH} = 74.3^{+0.4}_{-0.3}\Ufb$ at
$13\UTeV$.  Thus, the $\mt$ uncertainty in the total cross section is
below $1\%$, since increasing the top-quark mass causes a reduction of
the available phase space which is however partly compensated by the
larger top-quark Yukawa coupling.

Associated $tH$  production in the $t$-channel  is known for having
maximal destructive interference in the SM between $H-W$ interactions
on the one hand, and $H-t$ interactions on the other hand:
deviations from the SM top-quark Yukawa coupling can result in a large enhancement
of the cross section.
This has prompted the LHC experiments to perform
searches for the $125\UGeV$ Higgs boson in this process~\cite{Aad:2014lma,Khachatryan:2015ota}
assuming that the sign of the top-quark Yukawa coupling is opposite to the SM coupling
in \Eq~\eqref{eq:mt_yt},
\begin{equation}
\label{eq:-yt}
y_{\mathrm{t}}=-y_{\mathrm{t}}^{\mathrm{(SM)}}=-\mt/v \,,
\end{equation}
which results in maximally constructive interference between the two subsets of diagrams.
Given the interest in experimental searches, in \Tref{tab:tH_tcha_-ytSM}
we provide reference cross sections and uncertainties for this scenario at 13 and $14\UTeV$.
For further applications of this process to constrain deviations from the SM
interactions of the $125\UGeV$ particle,
see also \refS{s.efttools}.
%{\colour{red} (put here a reference to HEFTtools section, and get a cross reference from that section)}.


Finally, we extend our investigation to Higgs boson masses in the range
$\mh=10{-}3000\UGeV$ keeping the Higgs boson as stable particle and neglecting Higgs boson width effects,
which might provide a useful reference for BSM Higgs searches.
The results at 13 and $14\UTeV$ are
% collected in  \Trefs{tab:tH_tcha_BSM_lhc13} and \ref{tab:tH_tcha_BSM_lhc14} respectively
% (analogous to \Trefs{tab:tH_tcha_SM_lhc13} and \ref{tab:tH_tcha_SM_lhc14}) and also
plotted in \refF{fig:tH_tcha_BSM_plots}.
These results should be taken with care, since an hypothetical BSM
Higgs boson may contribute to the same $tH$ final state through
different interactions than the ones described by SM-like diagrams.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Differential distributions at NLO+PS}


\begin{figure}
\center
\includegraphics[width=0.32\textwidth]{\ttHtHpath/tH_tchannel_HpT_13tev.pdf}
\hspace*{0.2em}
\includegraphics[width=0.32\textwidth]{\ttHtHpath/tH_tchannel_bj1pT_13tev.pdf}
\hspace*{0.2em}
\includegraphics[width=0.32\textwidth]{\ttHtHpath/tH_tchannel_jetcount_13tev.pdf}
\caption{Differential distributions in $t$-channel $pp \to tH + \bar tH$
production at the $13\UTeV$ LHC, computed at NLO matched to \Pythiaeight,
in the 4FS (blue) and in the 5FS (red).
We show the transverse momentum of the Higgs boson $p_{\mathrm{T}}(H)$ on the left,
of the hardest $b$-tagged jet $p_{\mathrm{T}}(j_{\mathrm{b},1})$ in the centre,
and the light-jet and $b$-jet multiplicities on the right.
Plots are taken from \Ref~\cite{Demartin:2015uha}.}
\label{fig:tH_tcha_distributions}
\end{figure}


In this section we briefly address NLO differential distributions
matched to a parton shower (NLO+PS).
To generate events for distributions, we recommend to use a dynamic
event-by-event scale instead of the static one in \Eq~\eqref{eq:tH_tcha_scale}.
In \Ref~\cite{Demartin:2015uha} we have employed the fraction of transverse energy
(restricted to the set of $H$, $t$, and $b$ particles) given by the formula
\begin{equation}
\label{eq:tH_tcha_dynscale}
\mu_0^{(t-\mathrm{channel,~dynamic})} = \sum_{\mathrm{i=H,t,b}} M_{\mathrm{T}}(i)/6 \,,
\hspace*{4em}
\mathrm{where} \quad M_{\mathrm{T}} = \sqrt{M^2+p_{\mathrm{T}}^2} \,,
\end{equation}
is the transverse mass of a particle of mass $M$.
We have found that this choice of dynamic scale returns a total cross section very close
to the one computed with the static scale in \Eq~\eqref{eq:tH_tcha_scale}.
On top of that, there is a remarkable agreement at NLO+PS between the 4FS and 5FS
predictions for many observables, such as those related to the
Higgs boson (see left plot in \refF{fig:tH_tcha_distributions}),
the top quark, and the forward jet.
This is non trivial, especially in the light of the inadequacy of
the 5FS predictions at LO, which can suffer of large differential $K$ factors
after the inclusion of bottom-quark transverse dynamics,
see central plot in \refF{fig:tH_tcha_distributions}.
On the other hand, the 4FS gives in general more stable results
(flatter $K$ factors, smaller scale dependence in the tails)
and is able to provide accurate predictions for a wider set of observables,
including those related to the spectator $b$-quark and the extra jets.
Finally, we find the choice of shower starting scale, $Q_{\mathrm{sh}}$, to have a tiny impact on NLO+PS results,
see right-hand-side plot in \refF{fig:tH_tcha_distributions}.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  s-channel subsection
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{\texorpdfstring{$s$}{s}-channel \texorpdfstring{$tH$}{tH} production}
\label{sec:tH_scha}

\begin{figure}
\center
\vspace*{0.08\textwidth}
\includegraphics[width=0.33\textwidth]{\ttHtHpath/tH_schannel.pdf}
\hspace*{0.63\textwidth} \\
\vspace*{-0.18\textwidth} \hspace*{0.38\textwidth}
\includegraphics[width=0.28\textwidth]{\ttHtHpath/tH_schannel_HtDeta_13tev.pdf}
\hspace*{0.2em}
\includegraphics[width=0.28\textwidth]{\ttHtHpath/tH_schannel_bjetcount_13tev.pdf}
\caption{On the left: LO Feynman diagrams for $s$-channel $tH$ production.
At the centre: pseudorapidity distance between the Higgs boson and the top quark
$\Delta \eta (H,t)$ in the $s$-channel process (blue)
compared to the $t$-channel one (red).
On the right: multiplicity of $b$-tagged jets in the $s$-channel process (blue)
compared to the $t$-channel one (red).
Plots are taken from \Ref~\cite{Demartin:2015uha}.}
\label{fig:tH_scha_diagrams}
\end{figure}

Unlike the $t$-channel process, $s$-channel $tH$ production is not
affected by flavour-scheme ambiguities, since at LO it is initiated by
quark-antiquark annihilation (see left-hand-side of
\refF{fig:tH_scha_diagrams}).  Therefore, one can simply employ
the 5FS for simulating this process.  Results presented in this section have been obtained
in the \MGfiveamcnlo\, framework. Once again, spin correlations can be kept
by decaying the top quarks with \Madspin.
%issuing the following commands:
%%
%\begin{verbatim}
% > import model loop_sm-no_b_mass
% > generate p p > w+ > h t b~ [QCD]
% > add process p p > w- > h t~ b [QCD]
% > output
% > launch
%\end{verbatim}
%%
%where the \texttt{> w >} syntax selects only diagrams with an $s$-channel $W$
%propagator.
The same input parameters as for the $t$-channel process in the 5FS
have been used, with the exception of the reference scale choice,
which in this case is
\begin{equation}
\mu_0^{(s-\mathrm{channel})} = (\mh+\mt)/2 \,.
\end{equation}

In \Trefs{tab:tH_scha_SM_lhc7}, \ref{tab:tH_scha_SM_lhc8},
\ref{tab:tH_scha_SM_lhc13} and \ref{tab:tH_scha_SM_lhc14}
we collect the results for the combined $s$-channel $pp \to tH + \bar tH$ production
at the LHC, at centre-of-mass energies of
$\sqrt{s} = 7,8,13$, and $14\UTeV$ respectively, and for various Higgs boson masses
in the range $120{-}130\UGeV$.
These tables are analogous to the ones presented in the previous section for the
$t$-channel process:
in the third column we report the reference cross section;
in the fourth the QCD $K$ factor, defined in \Eq~\eqref{eq:tH_tcha_scalevariations};
in the fifth the scale dependence, computed from the maximum and minimum variations
of the cross section among the 6 points listed in \Eq~\eqref{eq:tH_tcha_scalevariations};
in the sixth, seventh, and eight the $\als$, PDF, and combined PDF+$\als$ uncertainty,
computed employing the 30+2 \textsc{PDF4LHC15} Hessian set;
and finally, in the last two columns we report the separate top and anti-top contributions
to the cross section.
In \refT{tab:tH_scha_SM_lhc6to15} we show the cross-section results
obtained varying the
LHC energy in the range $\sqrt{s} = 6{-}15\UTeV$ and keeping the Higgs boson mass fixed
to $\mh=125\UGeV$.
All these numbers are summarized in the plots
of \refF{fig:tH_scha_SM_plots}, where the the blue uncertainty band
is produced summing the scale and PDF+$\als$ uncertainties.

%We also collect the analogous cross-section results in the extended Higgs boson mass range
%$10{-}3000\UGeV$, at 13 and $14\UTeV$, in \Trefs{tab:tH_scha_BSM_lhc13} and
%\ref{tab:tH_scha_BSM_lhc14} respectively,
%and we plot these numbers in\refF{fig:tH_scha_BSM_plots}.
%Finally, we remark that even though the $s$-channel cross section is very tiny
%(around $25-30$ times smaller than the $t$-channel cross section at $13-14\UTeV$),
%this process features kinematical distributions with shapes rather different than the
%$t$-channel ones, see central and right plots in\refF{fig:tH_scha_diagrams}.


We also plot in
\refF{fig:tH_scha_BSM_plots} the analogous cross-section results in the extended Higgs boson mass range
$10{-}3000\UGeV$, at 13 and $14\UTeV$.
Finally, we remark that even though the $s$-channel cross section is very tiny
(around $25{-}30$ times smaller than the $t$-channel cross section at $13{-}14\UTeV$),
this process features kinematical distributions with shapes rather different than the
$t$-channel ones, see central and right plots in \refF{fig:tH_scha_diagrams}.



\begin{figure}
\centering
\includegraphics[width=.4\textwidth]{\ttHtHpath/tH_tchannel_lhc7}
\includegraphics[width=.4\textwidth]{\ttHtHpath/tH_tchannel_lhc8}\\
\includegraphics[width=.4\textwidth]{\ttHtHpath/tH_tchannel_lhc13}
\includegraphics[width=.4\textwidth]{\ttHtHpath/tH_tchannel_lhc14}\\
\includegraphics[width=.4\textwidth]{\ttHtHpath/tH_tchannel_lhc_energy_scan}
\caption{Cross sections for $t$-channel $tH$ and $\bar t H$ production.}
\label{fig:tH_tcha_SM_plots}
\end{figure}

\begin{table}\footnotesize
\caption{Cross section for $t$-channel $tH$ and $\bar t H$ production at the 13 and 14\UTeV~LHC, for $y_t=-y_t^{\mathrm{(SM)}}$.}
\label{tab:tH_tcha_-ytSM}
\setlength{\tabcolsep}{1.0ex}
\centering
\begin{tabular}{cccccccccc}
\toprule
$\sqrt{s}$[\UTeV] &
$\mh$[\UGeV] &
$\sigma_{tH+\bar tH}$[\Ufb] &
$K_{\mathrm{QCD}}$ &
Scale+FS  [\%] &
$\alpha_S$ [\%] &
PDF [\%] &
PDF+$\alpha_S$ [\%] &
$\sigma_{tH}$[\Ufb] &
$\sigma_{\bar tH}$[\Ufb]  \\   \midrule
\input{\ttHtHpath/table_-ytSM_tchannel.dat} 
\bottomrule
\end{tabular}
\end{table}


%\clearpage


\begin{figure}
\centering
\includegraphics[width=.4\textwidth]{\ttHtHpath/tH_BSM_tchannel_lhc13}
\includegraphics[width=.4\textwidth]{\ttHtHpath/tH_BSM_tchannel_lhc14}
\caption{Cross sections for $t$-channel $tH$ and $\bar t H$ production
in the extended Higgs boson mass range.}
\label{fig:tH_tcha_BSM_plots}
\end{figure}



\begin{figure}
\centering
\includegraphics[width=.4\textwidth]{\ttHtHpath/tH_schannel_lhc7}
\includegraphics[width=.4\textwidth]{\ttHtHpath/tH_schannel_lhc8}\\
\includegraphics[width=.4\textwidth]{\ttHtHpath/tH_schannel_lhc13}
\includegraphics[width=.4\textwidth]{\ttHtHpath/tH_schannel_lhc14}\\
\includegraphics[width=.4\textwidth]{\ttHtHpath/tH_schannel_lhc_energy_scan}
\caption{Cross sections for $s$-channel $tH$ and $\bar t H$ production.}
\label{fig:tH_scha_SM_plots}
\end{figure}



\begin{figure}
\centering
\includegraphics[width=.4\textwidth]{\ttHtHpath/tH_BSM_schannel_lhc13}
\includegraphics[width=.4\textwidth]{\ttHtHpath/tH_BSM_schannel_lhc14}
\caption{Cross sections for $s$-channel $tH$ and $\bar t H$ production
in the extended Higgs boson mass range.}
\label{fig:tH_scha_BSM_plots}
\end{figure}
