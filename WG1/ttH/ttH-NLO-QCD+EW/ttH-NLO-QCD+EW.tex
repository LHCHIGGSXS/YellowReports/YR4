\def\tthxstabhead{
\midrule
$\sqrt{s}$&
$\mh$ &
$\sigmaNLOqcd$&
$\sigmaNLOqcdew$ &
$K_{\mathrm{QCD}}$ &
$\deltaEW$[\%] &
Scale[\%] &
$\als$[\%] &
PDF[\%] &
PDF+$\als$[\%]
\\   \midrule}


\section{NLO QCD+EW predictions for \texorpdfstring{$t\bar{t}H$}{ttH} production}
\label{sec:ttH-XS}

Predictions for inclusive and differential $\tth$ production at NLO
QCD are available since more than a
decade~\cite{Reina:2001sf,Dawson:2002tg,Dawson:2003zu,Beenakker:2001rj,Beenakker:2002nc},
while EW corrections have been calculated only
recently~\cite{Yu:2014cka,Frixione:2014qaa,Frixione:2015zaa}. Although
their effect on total rates is usually suppressed with respect to NLO
QCD corrections by a factor of order $\alpha/\als$, when hard
scales are probed they can be enhanced by electroweak Sudakov
logarithms~\cite{Ciafaloni:1998xg,Ciafaloni:2000df,Denner:2000jv,Denner:2001gw}.
For what concerns $\tth$ production, in particular for a precise
extraction of the top quark Yukawa coupling $y_{\mathrm{t}}$, EW corrections
should be accounted for because of at least two reasons. First, EW
corrections, unlike QCD corrections, spoil the trivial dependence of
the total cross section on $\sim y_{\mathrm{t}}^2$ , introducing also (small)
terms where the Higgs couples to $W^\pm$ and $Z$ bosons, or to
itself. Second, EW corrections show Sudakov effects: in order to
suppress backgrounds, many $\tth$ searches are performed in a boosted
regime~\cite{Butterworth:2008iy,Plehn:2009rk,Buckley:2013auc}, where
Sudakov logarithms can be important.


This section presents NLO QCD+EW predictions for inclusive $\tth$
production.  All input parameters are chosen according
to~\cite{LHCHXSWG-INT-2015-006}, and the hadronic cross section is obtained
using the PDF4LHC15~\cite{Butterworth:2015oua} and
NNPDF2.3QED~\cite{Ball:2013hta} parton distribution functions as explained in
detail below.  For the top-quark and Higgs boson masses the on-shell
scheme is used, and the top-quark Yukawa
coupling\footnote{In the adopted convention the Feynman rule of
  the $t\bar{t}H$ vertex is $(-iy_{\mathrm{t}})$.} is related to the
top-quark mass and the Fermi constant ($G_\mu$) by
\begin{equation}
y_{\mathrm{t}}=(\sqrt{2}G_{\mu})^{1/2}\mt\,.
\end{equation}
The central value for renormalization and factorization scales is set to
\begin{equation}
            \mu = \mt + \mh/2\,,
\end{equation}
and the scale uncertainty is estimated by independent variations of
renormalization ($\muR$) and
factorization ($\muF$) scales in the range $ \mu/2 \le \muR, \muF \le
2\mu$, with $1/2 \le \muR/ \muF \le 2$.

The NLO QCD+EW predictions presented in the following,
\begin{equation}
\sigmaNLOqcdew=\sigmaNLOqcd+\dsigmaew\,,
\end{equation}
result from the combination of various contributions.  The usual NLO QCD cross section,
\begin{equation}
\sigmaNLOqcd=\sigmaLOqcd+\delta\sigmaNLOqcd,
\end{equation}
comprises LO terms of $\mathcal{O}(\als^2\alpha)$ and NLO terms of
$\mathcal{O}(\als^3\alpha)$, which involve $gg$, $q\bar q$, and $gq$
partonic channels.  The remaining EW corrections, denoted as
$\dsigmaew$, include three types of terms:
\begin{enumerate}
\item LO EW terms of $\mathcal{O}(\alpha^3)$ that result from squared
  EW tree amplitudes in the $q\bar q$ and $\gamma\gamma$ channels;
\item LO mixed terms of $\mathcal{O}(\als\alpha^2)$ that result
  from the interference of EW and QCD tree diagrams in the $b\bar b$
  and $\gamma g$ channels (other $q\bar q$ channels do not contribute
  at this order due to the vanishing interference of the related
  colour structures);
\item NLO EW corrections of $\mathcal{O}(\als^2\alpha^2)$ in the
  $q\bar q$, $gg$ and $\gamma g$ channels.  Subleading NLO terms of
  $\mathcal{O}(\als\alpha^3)$ and $\mathcal{O}(\alpha^4)$ are not
  included as they are expected to be strongly suppressed.
\end{enumerate}
For $\sqrt{s}=7$--$14\,\UTeV$ and $\mh=125\,\UGeV$, the corrections
resulting from LO EW, LO mixed and NLO EW effects are all positive and
amount, respectively, to $0.5\%$, $0.8$--$1.5\%$ and $1.1$--$1.9\%$ of
the NLO QCD cross section.\footnote{Here $\mathcal{O}(\alpha)$ effects
  related to QED evolution of PDF (see below) are not included.}
Photon-induced partonic channels dominate the LO mixed terms, while
their contribution to LO EW and NLO EW terms is almost negligible.

A fully consistent treatment of NLO QCD+EW corrections requires
corresponding precision in the employed PDF.  In particular, parton
distributions should include QED evolution effects and, consequently,
a photon density.  In order to circumvent the absence of QED effects
in the PDF4LHC15 distributions the following approach is adopted:
\begin{enumerate}
\item NLO QCD contributions are computed using the PDF4LHC15 set: more
  precisely, the PDF4LHC15 set with 30+2 members is used for PDF and
  $\als$ uncertainty estimates;
\item all EW correction effects resulting form partonic channels with
  initial-state quarks and/or gluons are computed with the same
  PDF4LHC15 set;
\item for all $\gamma$-induced EW correction effects the NNPDF2.3QED set is used;
\item the missing $\mathcal{O}(\alpha)$ effect due to the QED
  evolution of quark PDF is estimated from the difference between
  NNPDF2.3QED parton densities and their NLO QCD counterpart without
  QED evolution, NNPDF2.3, both with $\als(\mz)=0.118$. The
  relevant $\mathcal{O}(\alpha)$ correction factor is determined as
  follows by means of a LO QCD calculation,
\begin{equation}
\label{eq:tthpdfqed}
\delta\sigma_{\ewpdf}
= \sigmaNLOqcd \left( 1 + \delta_{\ewpdf} \right)\,,\qquad\mbox{where}\qquad
\delta_{\ewpdf} = \frac{\sigma_{\rm LO~QCD}^{\rm NNPDF\,QED}}{\sigma_{\rm LO~QCD}^{\rm NNPDF}} -1 \,.
\end{equation}
For $\sqrt{s}=7$--$14\,\UTeV$ and $\mh=125\,\UGeV$, the effect of QED
PDF evolution ranges from $-0.7\%$ to $-0.9\%$. Being negative it
compensates in part the effect of LO and NLO EW corrections to the
partonic cross sections.
\end{enumerate}

At the same order of $\sigma^{\rm NLO}_{\rm EW}$ also the real
emission of an extra heavy weak gauge boson can in principle
contribute to the cross section for inclusive $\tth$ production.  Such
a contribution from heavy-boson radiation (HBR) is generally not
considered as part of EW corrections, owing to the fact that the
emission of an extra heavy boson can be distinguished from the
corresponding non emission.  However, it can contribute to the cross
section when the decay products of the heavy boson escape from, {\it
  e.g.}, the detector acceptance or the experimental selection cuts.
Furthermore, these contributions might compensate the Sudakov
logarithms which enhance the NLO EW corrections at large scales.  We
will not include HBR contributions in the following results. Their
impact has been computed for the total cross section and differential
observables in~\cite{Frixione:2014qaa, Frixione:2015zaa}, where it has
been found to be small (less than $1\%$) on total rates and, unlike
NLO EW corrections, only marginally enhanced when large energy scales
are probed.


Tables~\ref{tab:sm7tev}--\ref{tab:smescan12509} present NLO QCD+EW
predictions for different collider energies and Higgs boson mass
values.  The relative impact of QCD and EW corrections is illustrated
in the form of a QCD correction factor
\begin{equation}
K_{\mathrm{QCD}}=\frac{\sigmaNLOqcd}{\sigmaLOqcd}\,,
\end{equation}
and a relative EW correction factor
\begin{equation}
\delta_{\mathrm{EW}}=\dsigmaew/\sigmaNLOqcd\,.
\end{equation}
All result in Tables~\ref{tab:sm7tev}--\ref{tab:smescan12509} are based on
\MGfiveamcnlo~\cite{Alwall:2014hca}, similarly as
in~\cite{Frixione:2015zaa}.  A cross check against an independent
calculation based on \Sherpa+\Openloops~\cite{Kallweit:2014xda} has
confirmed the correctness of NLO QCD+EW predictions for
$\sqrt{s}=7,8,13,14~\UTeV$ and $\mh=125~\UGeV$ at the per mille level.
Predictions for the production of a Higgs boson in the mass range
$\mh=120$--$130~\UGeV$ are reported in
Tables~\ref{tab:sm7tev}--\ref{tab:sm14tev} for
$\sqrt{s}= 7,8,13,14~\UTeV$ respectively.  The relative scale and
PDF+$\als$ uncertainties are the same for NLO QCD as for NLO
QCD+EW cross sections, therefore they can be computed for the former
and applied to the latter.
Tables~\ref{tab:smescan125},~\ref{tab:smescan12509} list numbers at
different $\sqrt s$ for $\mh=125.00~\UGeV$ and $\mh=125.09~\UGeV$,
respectively.  The integration uncertainty affecting results is at
0.1\% level for $\sigma^{\rm NLO}_{\rm QCD+EW}$.
 % Results for an extended Higgs boson mass range at $\sqrt s=8,13~\UTeV$ are shown in Tables~\ref{tab:bsm8tev} and \ref{tab:bsm13tev}, respectively.
The
left and right plots in \refF{fig:ttHEWplot13} show the $\tth$
cross section as a function of the Higgs boson mass at $13~\UTeV$, for
the SM and BSM range respectively. The scale, PDF, and $\als$
uncertainties are also shown, together with the QCD and EW correction
factors.

\begin{figure}
    \centering
    \includegraphics[width=0.49\textwidth]{\ttHNLOpath/figures/plot_tth_13tevsm.pdf}
    \includegraphics[width=0.49\textwidth]{\ttHNLOpath/figures/plot_tth_13tevbsm.pdf}
    \caption{\label{fig:ttHEWplot13} The upper panel shows the $\tth$
      total cross section as a function of $\mh$, at $13~\UTeV$,
      including only NLO QCD corrections (blue curve) and both NLO
      QCD+EW corrections (red curve). The intermediate panel
      illustrates the estimated theoretical uncertainties from scale,
      PDF, and $\als$ variation over the same $\mh$ ranges. The
      lower panel shows the size of the electroweak corrections as a
      function of $\mh$.}
\end{figure}
