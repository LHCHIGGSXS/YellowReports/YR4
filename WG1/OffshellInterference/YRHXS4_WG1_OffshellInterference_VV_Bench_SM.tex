\subsection[Off-shell and interference benchmarks: Standard Model]{Off-shell and interference benchmark cross sections and distributions: Standard Model}

\label{sec:offshell_interf_vv_bench_sm}

Gluon-fusion SM benchmark results were computed with \texttt{gg2VV} \cite{Kauer:2012hd} (see also \Brefs{Binoth:2005ua,Binoth:2006mf,Kauer:2013qba,Kauer:2015dma}) and 
\texttt{MG5\_aMC@NLO} \cite{Alwall:2014hca,Hirschi:2015iia} (see also \Bref{Kauer:2015dma}).  The \texttt{gg2VV} and \texttt{MG5\_aMC} 
results were found to be in good agreement.
Benchmark cross sections for $\Pg\Pg\ (\to \PH)\to \PV\PV \to 4$ leptons 
processes in $\Pp\Pp$ collisions at $\sqrt{s}=13\UTeV$ in the SM
are given in \refT{tab:ofs:leptonicxs}.
Results for the Higgs boson signal, the signal including signal-background
interference as well as the interfering background without Higgs contribution 
are displayed for minimal cuts 
$M_{\Pl\PAl} > 10\UGeV, M_{\Pl'\PAl'} > 10\UGeV$.  
The cross sections are calculated at loop-induced leading order.  
The recommended next-to-leading 
order $K$-factor is discussed in \refS{sec:offshell_interf_ggVV_NLO}.
Similarly, in \refT{tab:ofs:semileptxs} benchmark cross sections for $\Pg\Pg\ (\to \PH)\to \PV\PV \to$ semileptonic final states
in $\Pp\Pp$ collisions at $\sqrt{s}=13\UTeV$ in the SM are given.
As above, the signal amplitude is calculated at loop-induced leading order.
But, for the semileptonic decay modes, the $\Ord(g_{\mathrm{s}}^2e^2)$ tree-level as 
well as the important loop-induced $\Ord(g_{\mathrm{s}}^2e^4)$ amplitude contributions 
to the interfering background are taken into 
account \cite{Kauer:2015dma}.
The following minimal cuts are applied: $M_{\Pl\PAl} > 10\UGeV, M_{\PQq\PAQq} > 10\UGeV,
\ptt{j}>25\UGeV$.
Higgs boson invariant mass distributions corresponding to the cross sections given 
in \refTs{tab:ofs:leptonicxs} and \ref{tab:ofs:semileptxs} are displayed in 
\refFs{fig:ofs:ZAZA2l2l_ZAZA4l}, \ref{fig:ofs:ZAZ_2l2v_WW2l2v}
and \ref{fig:ofs:WWZAZ_2l2v_WWlvqq}.

\begin{table}%[Htbp]
\caption{\label{tab:ofs:leptonicxs}
Cross sections (\UfbZ) for $\Pg\Pg\ (\to \PH)\to \PV\PV \to 4$ leptons processes in 
$\Pp\Pp$ collisions at $\sqrt{s}=13\UTeV$ in the SM. 
Results for the Higgs boson signal (S), the signal including signal-background
interference (S+I) as well as the interfering background without 
Higgs contribution ($\Pg\Pg$ bkg.) are given.
Minimal cuts are applied: $M_{\Pl\PAl} > 10\UGeV, M_{\Pl'\PAl'} > 10\UGeV$.
Cross sections are given at loop-induced leading order and for a single
lepton flavour ($\Pl$) or single different-flavour combination ($\Pl$,$\Pl'$).
$\PGg^\ast$ contributions are included in $\PZ\PZ$.
The integration error is displayed in brackets.
} 
\renewcommand{\arraystretch}{1.2}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
\begin{tabular}{cccc}
%\cline{1-4}
%\multicolumn{4}{|c|}{$ \Pg\Pg \to \PH \to \PV\PV \to 4$ leptons} \\ 
%  \multicolumn{4}{|c|}{$\sigma[\UfbZ]$, $\LHC$, $\sqrt{s}=13$ \UTeV} \\ 
%  \multicolumn{4}{|c|}{min.\ cuts} \\
\toprule
final state & S & S+I & $\Pg\Pg$ bkg. \\ 
\midrule
$\Pl\PAl \Pl^{\prime}\PAl^{\prime}$ 	& $0.9284(7)$ & $0.6707(8)$ & $4.264(2)$ \\
$\Pl\PAl \Pl\PAl$	& $0.4739(8)$	& $0.3467(8)$	& $1.723(3)$ \\
$\Pl\PAl \PGn_{\Pl'}\PAGn_{\Pl'}$ & $1.896(2)$ & $1.386(2)$ & $5.730(5)$ \\
$\PAl\PGnl\PAGn_{\Pl'}\Pl'$ 	& $37.95(4)$ 	& $33.60(4)$	& $45.31(4)$ \\
$\PAl\PGnl \PAGnl\Pl$   & 36.01(3)	& $31.19(3)$	& $50.52(4)$	\\
\bottomrule
\end{tabular}
\end{center}
\end{table}

\begin{table}%[Htbp]
\caption{\label{tab:ofs:semileptxs}
Cross sections  (\UfbZ) for $\Pg\Pg\ (\to \PH)\to \PV\PV \to$ semileptonic final states
in $\Pp\Pp$ collisions at $\sqrt{s}=13\UTeV$ in the SM. 
Results for the Higgs boson signal (S), the signal including signal-background
interference (S+I) as well as the interfering background without 
Higgs contribution ($\Pg\Pg$ bkg.) are given.
The signal amplitude is calculated at loop-induced leading order.
For the semileptonic decay modes, the $\Ord(g_{\mathrm{s}}^2e^2)$ tree-level as 
well as the important loop-induced $\Ord(g_{\mathrm{s}}^2e^4)$ amplitude contributions 
to the interfering background are taken into 
account \cite{Kauer:2015dma}.
Minimal cuts are applied: $M_{\Pl\PAl} > 10\UGeV, M_{\PQq\PAQq} > 10\UGeV,
\ptt{j}>25\UGeV$.
Cross sections are given for a single lepton flavour.
Other details as in \refT{tab:ofs:leptonicxs}.
}
\renewcommand{\arraystretch}{1.2}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
\begin{tabular}{cccc}
%\cline{1-4}
%\multicolumn{4}{|c|}{$ gg \to \PH \to VV \to$ semilept.\ final states} \\ 
% \multicolumn{4}{|c|}{$\sigma$ [fb], $\LHC$, $\sqrt{s}=13$ \UTeV} \\ 
%%\cline{4-9}
%\multicolumn{4}{|c|}{min.\ cuts} \\
%%\multicolumn{3}{c|}{interference} & \multicolumn{3}{c|}{ratio} \\ 
\toprule
final state & S & S+I & $\Pg\Pg$ bkg. \\ 
\midrule
$\Pl \PAl  \PQd \PAQd$ 	& $1.711(3)$	 & $0.96(1)$  	& $1.575(6){\cdot}10^3$	\\
$\Pl \PAl  \PQu \PAQu$	& $1.334(3)$	& $0.750(5)$	& $2.30(5){\cdot}10^3$	\\
$\Pl \PAGnl  \PQu \PAQd$ & $38.66(5)$ 	& $30.58(8)$	& $1.111(3){\cdot}10^4$	\\
$\PAl \PGnl  \PAQu \PQd$&  $38.68(5)$	 & $30.59(8)$ 	& $1.112(3){\cdot}10^4$	\\
\bottomrule
\end{tabular}
\end{center}
\end{table}

\begin{figure}%[Htbp]
%\includegraphics[trim=3cm 0cm 0cm 0cm, clip, width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/ZAZA2l2l.eps}\hfil
%\includegraphics[trim=3cm 0cm 0cm 0cm, clip, width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/ZAZA4l.eps}
\includegraphics[trim=3cm 0cm 0cm 0cm, clip, width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/ZAZA2l2l.pdf}\hfil
\includegraphics[trim=3cm 0cm 0cm 0cm, clip, width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/ZAZA4l.pdf}
\caption{\label{fig:ofs:ZAZA2l2l_ZAZA4l}Invariant mass distributions for 
$\Pg \Pg\ (\to \PH) \to \PZ\PZ \to \Pl \PAl  \Pl' \PAl'$
and 
$\Pg \Pg\ (\to \PH)  \to \PZ\PZ \to  \Pl \PAl  \Pl \PAl$.
Other details as in \refT{tab:ofs:leptonicxs}.}
\end{figure}

\begin{figure}%[Htbp]
%\includegraphics[trim=3cm 0cm 0cm 0cm, clip, width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/ZAZ_2l2v.eps}\hfil
%\includegraphics[trim=3cm 0cm 0cm 0cm, clip, width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/WW2l2v.eps}
\includegraphics[trim=3cm 0cm 0cm 0cm, clip, width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/ZAZ_2l2v.pdf}\hfil
\includegraphics[trim=3cm 0cm 0cm 0cm, clip, width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/WW2l2v.pdf}
\caption{\label{fig:ofs:ZAZ_2l2v_WW2l2v}Invariant mass distributions for 
$ \Pg \Pg\ (\to \PH) \to \PZ\PZ \to \Pl\PAl \PGn_{\Pl'}\PAGn_{\Pl'}$
and 
$\Pg \Pg\ (\to \PH) \to \PW\PW \to \PAl\PGnl\PAGn_{\Pl'}\Pl'$.
Other details as in \refT{tab:ofs:leptonicxs}.}
\end{figure}

\begin{figure}%[Htbp]
%\includegraphics[trim=3cm 0cm 0cm 0cm, clip, width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/WWZAZ_2l2v.eps}\hfil
%\includegraphics[trim=3cm 0cm 0cm 0cm, clip, width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/WWlvqq.eps}
\includegraphics[trim=3cm 0cm 0cm 0cm, clip, width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/WWZAZ_2l2v.pdf}\hfil
\includegraphics[trim=3cm 0cm 0cm 0cm, clip, width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/WWlvqq-eps-converted-to.pdf}
\caption{\label{fig:ofs:WWZAZ_2l2v_WWlvqq}Invariant mass distributions for 
$ \Pg \Pg\ (\to \PH)  \to \PW\PW/\PZ\PZ \to  \PAl\PGnl \PAGnl\Pl$
and 
$ \Pg \Pg\ (\to \PH) \to \PW\PW \to \PAl \PGnl  \PAQu \PQd$.
Other details as in \refTs{tab:ofs:leptonicxs} and \ref{tab:ofs:semileptxs}.}
\end{figure}

%---------------------------------------------------------------------------
%---------------------------------------------------------------------------
%---------------------------------------------------------------------------
%---------------------------------------------------------------------------
%---------------------------------------------------------------------------

The following experimental Higgs off-shell search selections 
are recommended for use in $\Pg\Pg\to \PV\PV$ calculations:\\
%
Jets: ATLAS: $\ptt{j} > 25$\UGeV{} for $|\eta_j|<2.4$, $\ptt{j}>30$\UGeV{} for 
$2.4<|\eta_j|<4.5$\\
Jets: CMS: $\ptt{j} > 30$\UGeV{} for $|\eta_j|<4.7$\\
%
$\PH\to \PZ\PZ\to 4\Pl$ channel: ATLAS:\\
$\ptt{\Pl,1}> 20\UGeV$, 
$\ptt{\Pl,2}> 15\UGeV$,
$\ptt{\Pl,3}> 10\UGeV$,
$\ptt{\Pe,4}> 7\UGeV$,
$\ptt{\PGm,4}> 6\UGeV$,
$|\eta_{\Pe}|<2.47$,
$|\eta_{\PGm}|<2.7$,
$M_{4\Pl} > 220\UGeV$\\
$\PH\to \PZ\PZ\to 4\Pl$ channel: CMS:\\
$\ptt{\Pl,1}> 20$\UGeV,
$\ptt{\Pl,2}> 10$\UGeV,
$\ptt{\Pe,3,4}> 7$\UGeV,
$\ptt{\PGm,3,4}> 5$\UGeV,
$|\eta_{\Pe}|<2.5$,
$|\eta_{\PGm}|<2.4$,
$M_{4\Pl} > 220$\UGeV\\
%
$\PH\to \PZ\PZ\to 2\Pl2\PGn$ channel: ATLAS:\\
$\ptt{\Pl}> 20$\UGeV\ (electron, muon),
$|\eta_{\Pe}|<2.47$,
$|\eta_{\PGm}|<2.5$,
$E_{T,miss} > 180\UGeV$,
$\Delta\phi_{\Pl\Pl} < 1.4$,
$\Mtt{,\PZ\PZ} > 380\UGeV$\\
ATLAS transverse mass definition (recommended for $M_{\PV\PV}> 2 M_{\PZ}$):
\begin{equation}
\label{eq:ofs:MTZZ}
\Mtt{,\PZ\PZ}=\sqrt{(\Mtt{,\ell\ell}+\Mtt{,miss})^2-({\bf{p}}_{T,\ell\ell}+{\bf{p}}_{T,miss})^2}\,,\
{\rm where}\ \ \ \Mtt{,X}=\sqrt{\ptt{,X}^2+M_Z^2}
\end{equation}
%
$\PH\to \PZ\PZ\to 2\Pl2\PGn$ channel: CMS:\\
$\ptt{\Pl}> 20$\UGeV\ (electron, muon),
$E_{T,miss} > 80\UGeV$\\
%
$\Mtt{,\PZ\PZ}$ used by CMS: Eq.~(\ref{eq:ofs:MTZZ}) with $M_Z$ replaced by
$M_{\ell\ell}$\\
%
$\PH \to \PW\PW\to 2\Pl2\PGn$ channel: ATLAS:\\
$\ptt{\Pl,1}> 22$\UGeV, 
$\ptt{\Pl,2}> 10$\UGeV, 
$|\eta_{\Pe}|<2.47$, 
$|\eta_{\PGm}|<2.5$, 
$M_{\Pl\Pl} > 10\UGeV$, 
$\ptt{,miss} > 20\UGeV$,
$\Mtt{,\PW\PW}>200\UGeV$\\
ATLAS transverse mass definition (recommended):
\begin{equation}
\label{eq:ofs:MTWW}
\Mtt{,\PW\PW}=\sqrt{(\Mtt{,\ell\ell}+\ptt{,miss})^2-({\bf{p}}_{T,\ell\ell}+{\bf{p}}_{T,miss})^2}\,,\
{\rm where}\ \ \ \Mtt{,\ell\ell}=\sqrt{\ptt{,\ell\ell}^2+M_{\ell\ell}^2}
\end{equation}

%---------------------------------------------------------------------------
%---------------------------------------------------------------------------
%---------------------------------------------------------------------------
%---------------------------------------------------------------------------
%---------------------------------------------------------------------------

Vector-boson-fusion SM benchmark results were computed with 
\texttt{PHANTOM} \cite{Ballestrero:2007xq} (see also \Bref{Ballestrero:2015jca})
and 
\texttt{VBFNLO} \cite{Arnold:2008rz,Baglio:2014uba} (see also \Brefs{Jager:2006zc,Jager:2006cp,Bozzi:2007ur,Jager:2009xx,Feigl:2013naa}).
Good agreement was achieved for all fully-leptonic Higgs boson decay modes.
For VBF, two selection cut sets are applied which have the 
following selection in common:
\begin{itemize}
\item $\ptt{j} > 20\UGeV$, $|\eta_j| < 5.0$, $M_{jj} > 60\UGeV$ for all jets, anti-$k_\mathrm{T}$ jet clustering with $R=0.4$
\item $\ptt{\Pl} > 20\UGeV$, $|\eta_{\Pl}| < 2.5$, $M_{\Pl\PAl}>20\UGeV$ (same flavour only), $E_{\mathrm{T}}^{\mathrm{miss}} > 40\UGeV$
\item tagging jets: $j_1,j_2$, ordered by decreasing $|\eta_j|$
\end{itemize}
Exception: for the \textit{resonance} (RES) region (see \refS{sec:offshell_interf_vv_settings}) 
$M_{\Pl\PAl}>10\UGeV$ is applied instead of $M_{\Pl\PAl}>20\UGeV$.
With this common selection, we define:
\begin{itemize}
\item \textit{loose VBF cuts}: common selection and $M_{j_1j_2} > 130\UGeV$
\item \textit{tight VBF cuts}: common selection and $M_{j_1j_2} > 600\UGeV$, $\Delta y_{j_1j_2} > 3.6$, $y_{j_1} y_{j_2} < 0$ (opposite hemispheres)
\end{itemize}

Benchmark cross sections for $\Pq\Pq'(\to \Pq\Pq'\,\PH) \to \Pq\Pq'\,\PZ(\PGg^\ast)\PZ(\PGg^\ast) \to \Pq\Pq'\,\Pl \PAl  \Pl' \PAl'$ and $\Pq\Pq'(\to \Pq\Pq'\,\PH) \to \Pq\Pq'\,\PW\PW \to \Pq\Pq'\,\PAl\PGnl\PAGn_{\Pl'}\Pl'$ in $\Pp\Pp$ collisions at $\sqrt{s}=13\UTeV$ in the SM with tight and loose VBF cuts are given 
in \refTs{tab:ofs:SM_VBF_ZZ_tight}, \ref{tab:ofs:SM_VBF_WW_tight}, 
\ref{tab:ofs:SM_VBF_ZZ_loose} and \ref{tab:ofs:SM_VBF_WW_loose}.
Leading order and next-to-leading order results for the Higgs boson signal, 
the signal including signal-background interference as well as the interfering background without Higgs contribution are displayed.
Corresponding Higgs boson invariant mass (for $\PZ\PZ$) and transverse mass 
(for $\PW\PW$) distributions are shown in 
\refFs{fig:ofs:VBFNLO_MZZ_loose_tight} and \ref{fig:ofs:VBFNLO_MTWW_loose_tight},
respectively, for loose and tight VBF cuts.

The full set of SM benchmark cross sections and distributions is available
at \url{https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWGOFFSHELL}.

\begin{table}%[Htbp]
\caption{\label{tab:ofs:SM_VBF_ZZ_tight}
Cross sections for $\Pq\Pq'(\to \Pq\Pq'\,\PH) \to \Pq\Pq'\,\PZ(\PGg^\ast)\PZ(\PGg^\ast) \to \Pq\Pq'\,\Pl \PAl  \Pl' \PAl'$ in $\Pp\Pp$ collisions at $\sqrt{s}=13\UTeV$ in the SM. 
Leading order (LO) and next-to-leading order (NLO) results for 
the Higgs boson signal (S), the signal including 
signal-background interference (S+I) as well as the interfering 
background without Higgs contribution (B) are given.
Tight VBF cuts are applied (see main text).
Cross sections are given for a single lepton flavour combination. 
The integration error is displayed in brackets.
}
\renewcommand{\arraystretch}{1.2}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
%\footnotesize
\scriptsize 
\begin{tabular}{c c c c c c}
\toprule
$\sigma[\UfbZ]$ & & $110\UGeV <M_{\PZ\PZ}< 140\UGeV$ & $M_{\PZ\PZ}>140\UGeV$ & $220\UGeV<M_{\PZ\PZ}<300\UGeV$ & $M_{\PZ\PZ}>300\UGeV$ \\ 
\midrule
S & LO & $6.88(2) {\cdot} 10^{-3}$ & $1.2501(9) {\cdot} 10^{-2}$ &  $1.316(3) {\cdot} 10^{-3} $   &  $1.0644(9) {\cdot} 10^{-2}$ \\ 
S+I & LO &  $6.92(4) {\cdot} 10^{-3}$ & $- 1.398(6) {\cdot} 10^{-2}$   & $- 1.85(3) {\cdot} 10^{-3}$  & $- 1.126(5) {\cdot} 10^{-2}$ \\ 
B & LO & $1.0(2) {\cdot} 10^{-4}$ & $6.554(4) {\cdot} 10^{-2} $  & $1.672(2) {\cdot} 10^{-2}$   & $4.126(3) {\cdot} 10^{-2}$ \\ 
\midrule
S & NLO & $5.67(4) {\cdot} 10^{-3}$ & $1.371(3) {\cdot} 10^{-2}$ &  $1.234(8) {\cdot} 10^{-3} $   &  $1.198(3) {\cdot} 10^{-2}$ \\  
S+I & NLO &  $5.2(6) {\cdot} 10^{-3}$ & $- 1.55(2) {\cdot} 10^{-2}$   & $- 1.75(6) {\cdot} 10^{-3}$  & $- 1.288(9) {\cdot} 10^{-2}$ \\  
B & NLO & $5(2) {\cdot} 10^{-5}$ & $6.749(9) {\cdot} 10^{-2} $  & $1.627(5) {\cdot} 10^{-2}$   & $4.400(7) {\cdot} 10^{-2}$ \\ 
\bottomrule
\end{tabular}
\end{center}
\end{table}

\begin{table}%[Htbp]
\caption{\label{tab:ofs:SM_VBF_WW_tight}
Cross sections for $\Pq\Pq'(\to \Pq\Pq'\,\PH) \to \Pq\Pq'\,\PW\PW \to \Pq\Pq'\,\PAl\PGnl\PAGn_{\Pl'}\Pl'$ in $\Pp\Pp$ collisions at $\sqrt{s}=13\UTeV$ in the SM. 
Tight VBF cuts are applied (see main text).
Cross sections are given for a single lepton flavour combination, but taking
into account both charge assignments, e.g.\ $(\Pl,\Pl')=(\Pe,\PGm)$ or $(\PGm,\Pe)$.
Other details as in \refT{tab:ofs:SM_VBF_ZZ_tight}.
}
\renewcommand{\arraystretch}{1.2}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
%\footnotesize
\scriptsize 
\begin{tabular}{c c c c c c}
\toprule
$\sigma[\UfbZ]$ & & $110\UGeV <M_{\PW\PW}< 140\UGeV$ & $M_{\PW\PW}>140\UGeV$ & $220\UGeV<M_{\PW\PW}<300\UGeV$ & $M_{\PW\PW}>300\UGeV$ \\ 
\midrule
 S & LO & $1.7411(9)$ & $2.370(6) {\cdot} 10^{-1}$ &  $3.08(2) {\cdot} 10^{-2} $   &  $1.783(5) {\cdot} 10^{-1}$ \\  
S+I & LO & $1.740(3)$ & $- 3.00(4) {\cdot} 10^{-1}$   & $- 4.9(2) {\cdot} 10^{-2}$  & $- 0.197(3)$ \\  
B & LO & $8(2) {\cdot} 10^{-4}$ & $3.387(2) $  & $0.8642(6)$                 & $1.856(2)$ \\ 
\midrule
 S & NLO & $1.453(4)$ & $2.51(2) {\cdot} 10^{-1}$ &  $2.96(6) {\cdot} 10^{-2} $   &  $1.95(2) {\cdot} 10^{-1}$ \\  
S+I & NLO & $1.45(1)$ & $- 3.0(2) {\cdot} 10^{-1}$   & $- 3(2) {\cdot} 10^{-2}$  & $- 0.234(9)$ \\  
B & NLO & $6.7(7) {\cdot} 10^{-4}$ & $3.381(6) $  & $0.825(4)$                 & $1.933(4)$ \\ 
\bottomrule
\end{tabular}
\end{center}
\end{table}

\begin{table}%[Htbp]
\caption{\label{tab:ofs:SM_VBF_ZZ_loose}
Cross sections for $\Pq\Pq'(\to \Pq\Pq'\,\PH) \to \Pq\Pq'\,\PZ(\PGg^\ast)\PZ(\PGg^\ast) \to \Pq\Pq'\,\Pl \PAl  \Pl' \PAl'$ in $\Pp\Pp$ collisions at $\sqrt{s}=13\UTeV$ in the SM. 
Loose VBF cuts are applied (see main text).
Other details as in \refT{tab:ofs:SM_VBF_ZZ_tight}.
}
\renewcommand{\arraystretch}{1.2}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
%\footnotesize
\scriptsize 
\begin{tabular}{c c c c c c}
\toprule
$\sigma[\UfbZ]$ & & $110\UGeV <M_{\PZ\PZ}< 140\UGeV$ & $M_{\PZ\PZ}>140\UGeV$ & $220\UGeV<M_{\PZ\PZ}<300\UGeV$ & $M_{\PZ\PZ}>300\UGeV$ \\ 
\midrule
S & LO & $1.202(2) {\cdot} 10^{-2}$  & $1.662(2) {\cdot} 10^{-2}$   & $2.153(5) {\cdot} 10^{-3}$  & $1.351(2) {\cdot} 10^{-2}$ \\ 
 S+I & LO & $1.197(7) {\cdot} 10^{-2}$ & $- 1.95(2) {\cdot} 10^{-2}$ &  $- 3.34(5) {\cdot} 10^{-3} $   &  $- 1.441(8) {\cdot} 10^{-2}$ \\  
B & LO &  $2.2(2) {\cdot} 10^{-4}$ & $1.3535(7) {\cdot} 10^{-1} $  & $3.821(3) {\cdot} 10^{-2}$   & $7.909(5) {\cdot} 10^{-2}$ \\ \midrule
S & NLO & $1.035(4) {\cdot} 10^{-2}$  & $1.781(3) {\cdot} 10^{-2}$   & $1.993(9) {\cdot} 10^{-3}$  & $1.495(3) {\cdot} 10^{-2}$ \\ 
 S+I & NLO & $1.02(2) {\cdot} 10^{-2}$ & $- 2.04(2) {\cdot} 10^{-2}$ &  $- 3.1(1) {\cdot} 10^{-3} $   &  $- 1.58(2) {\cdot} 10^{-2}$ \\  
B & NLO &  $2.0(4) {\cdot} 10^{-4}$ & $1.346(2) {\cdot} 10^{-1} $  & $3.651(5) {\cdot} 10^{-2}$   & $8.108(9) {\cdot} 10^{-2}$ \\ \bottomrule
\end{tabular}
\end{center}
\end{table}

\begin{table}%[Htbp]
\caption{\label{tab:ofs:SM_VBF_WW_loose}
Cross sections for $\Pq\Pq'(\to \Pq\Pq'\,\PH) \to \Pq\Pq'\,\PW\PW \to \Pq\Pq'\,\PAl\PGnl\PAGn_{\Pl'}\Pl'$ in $\Pp\Pp$ collisions at $\sqrt{s}=13\UTeV$ in the SM. 
Loose VBF cuts are applied (see main text).
Other details as in \refT{tab:ofs:SM_VBF_WW_tight}.
}
\renewcommand{\arraystretch}{1.2}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
%\footnotesize
\scriptsize 
\begin{tabular}{c c c c c c}
\toprule
$\sigma[\UfbZ]$ & & $110\UGeV <M_{\PW\PW}< 140\UGeV$ & $M_{\PW\PW}>140\UGeV$ & $220\UGeV<M_{\PW\PW}<300\UGeV$ & $M_{\PW\PW}>300\UGeV$ \\ 
\midrule
 S & LO & $3.271(2)$ & $3.325(9) {\cdot} 10^{-1}$ &  $5.10(3) {\cdot} 10^{-2} $   &  $2.301(8) {\cdot} 10^{-1}$ \\  
S+I & LO & $3.278(6)$  & $- 4.79(9) {\cdot} 10^{-1}$   & $- 9.7(3) {\cdot} 10^{-2}$  & $- 2.61(7) {\cdot} 10^{-1}$ \\  
B & LO & $1.8(3) {\cdot} 10^{-3}$ & $7.449(5) $  & $2.004(2)$   & $3.830(3)$ \\ 
\midrule
 S & NLO & $2.836(7)$ & $3.46(3) {\cdot} 10^{-1}$ &  $4.75(7) {\cdot} 10^{-2} $   &  $2.50(3) {\cdot} 10^{-1}$ \\  
S+I & NLO & $2.85(5)$  & $- 4.4(2) {\cdot} 10^{-1}$   & $- 7.6(9) {\cdot} 10^{-2}$  & $- 2.7(2) {\cdot} 10^{-1}$ \\  
B & NLO & $1.8(2) {\cdot} 10^{-3}$ & $7.402(9) $  & $1.928(4)$   & $3.949(7)$ \\ 
\bottomrule
\end{tabular}
\end{center}
\end{table}


\begin{figure}%[Htbp]
%\includegraphics[trim=0cm 0cm 0cm 0cm, clip, width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/VBFNLO_MZZloose.eps}\hfil
%\includegraphics[trim=0cm 0cm 0cm 0cm, clip,width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/VBFNLO_MZZtight.eps}
\includegraphics[trim=0cm 0cm 0cm 0cm, clip, width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/VBFNLO_MZZloose.pdf}\hfil
\includegraphics[trim=0cm 0cm 0cm 0cm, clip,width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/VBFNLO_MZZtight.pdf}
\caption{\label{fig:ofs:VBFNLO_MZZ_loose_tight} Invariant mass distributions for 
$\Pq\Pq'(\to \Pq\Pq'\,\PH) \to \Pq\Pq'\,\PZ(\PGg^\ast)\PZ(\PGg^\ast) \to \Pq\Pq'\,\Pl \PAl  \Pl' \PAl'$ in $\Pp\Pp$ collisions at $\sqrt{s}=13\UTeV$ in the SM. Loose and tight VBF cuts are applied in the left and right graphs, respectively.
Leading order (dashed) and next-to-leading order (solid) results for 
the Higgs boson signal (S), the signal including signal-background 
interference (S+I) as well as the interfering background without 
Higgs contribution (B) are given.
Cross sections are given for a single lepton flavour combination.
}
\end{figure}

\begin{figure}%[Htbp]
%\includegraphics[trim=0cm 0cm 0cm 0cm, clip, width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/VBFNLO_MTWWloose.eps}\hfil
%\includegraphics[trim=0cm 0cm 0cm 0cm, clip,width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/VBFNLO_MTWWtight.eps}
\includegraphics[trim=0cm 0cm 0cm 0cm, clip, width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/VBFNLO_MTWWloose.pdf}\hfil
\includegraphics[trim=0cm 0cm 0cm 0cm, clip,width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/VBFNLO_MTWWtight.pdf}
\caption{\label{fig:ofs:VBFNLO_MTWW_loose_tight}
Transverse mass $\Mtt{,\PW\PW}$ (see Eq.~(\ref{eq:ofs:MTWW})) distributions for 
$\Pq\Pq'(\to \Pq\Pq'\,\PH) \to \Pq\Pq'\,\PW\PW \to \Pq\Pq'\,\PAl\PGnl\PAGn_{\Pl'}\Pl'$ in $\Pp\Pp$ collisions at $\sqrt{s}=13\UTeV$ in the SM. 
Cross sections are given for a single lepton flavour combination, but taking
into account both charge assignments, e.g.\ $(\Pl,\Pl')=(\Pe,\PGm)$ or $(\PGm,\Pe)$.
Other details as in \refF{fig:ofs:VBFNLO_MZZ_loose_tight}.}
\end{figure}

%\clearpage
