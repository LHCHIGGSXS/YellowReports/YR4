\subsection[Input parameters and recommendations for input parameters and PDF]{Input parameters and recommendations for the QCD scale and the 
order of the gluon PDF}
\label{sec:offshell_interf_vv_settings}

The SM input parameters for Higgs physics given in 
\Bref{LHCHXSWG-INT-2015-006} % LHCHXSWG-INT-2015-006 
are adopted with the $G_\mu$ scheme: 
$\MW = 80.35797$\UGeV, $\MZ = 91.15348$\UGeV, $\GW = 2.08430$\UGeV, 
$\GZ = 2.49427$\UGeV, $\Mt = 172.5$\UGeV, $\Mb(\Mb) =
4.18$\UGeV\ and $\GF = 1.1663787\cdot 10^{-5}$\UGeV$^{-2}$. The CKM matrix
is approximated by the identity matrix.
Finite top and bottom quark mass effects 
are included.  Lepton and light quark masses are neglected.
Results are given for $\Pp\Pp$ collisions at $\sqrt{s}=13$\UTeV\ unless
otherwise noted. The PDF set \texttt{PDF4LHC15\_nlo\_100} \cite{Butterworth:2015oua} is used by default. All PDF sets are used with the default $\alpha_s$ 
of the set.
A fixed-width Breit-Wigner propagator $D(p) \sim (p^2 - M^2 + i M \Gamma)^{-1}$
is employed for $W,Z$ and Higgs bosons, where $M$ and $\Gamma$ are determined 
by the complex pole of the amplitude due to unstable particle propagation.%
\footnote{ In agreement with \textsc{HDECAY}, the $W$ and $Z$ masses and widths 
have been changed from physical on-shell masses to the pole values, see 
\Eq{} (7) in \Bref{LHCHXSWG-INT-2015-006}.  The relative deviation is at the 
$3\cdot 10^{-4}$ level.}
The SM Higgs boson mass is set to 125\UGeV.  The SM Higgs boson width parameter 
is calculated using \textsc{HDECAY} v6.50 \cite{Djouadi:1997yw}. For $\MH=125$\UGeV\ one obtains $\GH= 4.097\cdot 10^{-3}$\UGeV.

For off-shell and high-mass $\PH\to \PV\PV$ cross-section and interference
calculations, we recommend and employ the QCD scale $\mu_R=\mu_F=\MVV/2$
unless otherwise noted.  Next, we elucidate the choice of the PDF order
for the $\Pg\Pg\to \PV\PV$ continuum background and the corresponding 
Higgs-continuum interference.
Combining any $n$-order PDF fit with a $m$-order 
parton-level calculation is theoretically consistent 
as long as $n\ge m$.  Deviations are expected to be
of higher order if the same $\alphas(\MZ)$ is used.
But, using a LO gluon PDF with $\alphas(\MZ)$ 
obtained in the LO fit is not recommended: The
gluon PDF is mostly determined by DIS data, especially in the 
SM Higgs region.  
At LO, DIS does not have a gluon channel. It only enters at 
NLO, with a large $K$-factor. A LO fit cannot properly account for 
this $\Ord(50\%)$ contribution, but incorrectly adjusts the gluon
evolution to compensate, which results in an overestimated value of 
$\alphas(\MZ)$ of approximately $0.13$.
We therefore recommend using a NLO PDF set when computing 
the $\Pg\Pg\ (\to \PH)\to \PV\PV$ interference 
and the $\Pg\Pg$ continuum background at LO as well as NLO. 
For consistency, we also use the NLO PDF set for the 
corresponding signal process.\footnote{We note that the LO and NLO VBF results
have also been obtained with the NLO PDF set.}

The variation induced by different PDF and QCD scale 
choices is illustrated in 
Tables\ \ref{tab:ofs:pdfmin}, \ref{tab:ofs:pdfcms}, \ref{tab:ofs:scalemin}
and \ref{tab:ofs:scalecms} 
using the process $\Pg\Pg\ (\to \PH)\to \Pl\PAl\Pl'\PAl'$.
The Higgs boson signal ($S$), $\Pg\Pg$ background ($B$) and
the signal-background interference ($I$) are displayed
at LO for four Higgs boson invariant mass regions:
\begin{itemize}
\item \textit{off-shell} (OFS): $\MVV > 140$\UGeV 
\item \textit{off-shell high-mass (interference)} (HM1): $220 < \MVV < 300$\UGeV 
\item \textit{off-shell high-mass (signal enriched)} (HM2): $\MVV > 300$\UGeV 
\item \textit{resonance} (RES): $110 < \MVV < 140$\UGeV 
\end{itemize}
Motivated by the Higgs boson width constraints of \Brefs{Khachatryan:2014iha,Aad:2015xua}, the off-shell high-mass region is divided into the 
interference-sensitive (HM1) and signal-enriched (HM2) regions.
Two sets of selection cuts are considered:
\begin{itemize}
\item \textit{minimal cuts} (MIN): $\M{\Pl\PAl}>10$\UGeV, $\M{\Pl'\PAl'}>10$\UGeV
\item \textit{CMS} $\PH\to 4\Pl$ \textit{cuts} (CMS): $\ptt{1}>20$\UGeV, $\ptt{2}>10$\UGeV, $\ptt{3,4}>5$\UGeV, $|\eta_{\Pe}|<2.5$, $|\eta_{\PGm}|< 2.4$, $\M{\Pe\aaa{\Pe}}>4$\UGeV, $\M{\PGm\aaa{\PGm}}>4$\UGeV
\end{itemize}
The PDF4LHC15 \cite{Butterworth:2015oua} NLO and NNLO sets ($\alphas(\MZ)=0.118$) and the CT14 \cite{Dulat:2015mca} LO sets with $\alphas(\MZ)=0.130$ and $\alphas(\MZ)=0.118$ (and $1$- and $2$-loop evolution, respectively) are compared in Tables\ \ref{tab:ofs:pdfmin} and \ref{tab:ofs:pdfcms}.  As expected, the deviations
for PDF sets with $\alphas(\MZ)=0.118$ are of order $10\%$ or less while 
the LO set with $\alphas(\MZ)=0.130$ yields results that differ by up to 
$30\%$.  The deviation between the NLO and NNLO sets is at the per cent level.
Furthermore, different choices for the QCD scale $\mu=\mu_R=\mu_F$ are compared
in Tables\ \ref{tab:ofs:scalemin} and \ref{tab:ofs:scalecms}.
As central scale choices, the dynamic scale $\mu_0=\Mtltl/2$ and the fixed 
scales $\MH/2$ and $\MZ$ are considered.  The LO scale variation is 
estimated for $\mu_0$ using the scales $\mu_0/2$ and $2\mu_0$.  
The results illustrate that using a fixed scale appropriate for
resonant signal or background will significantly overestimate the 
signal, background and interference cross sections in the far off-shell 
and high-mass regions.  With the recommended central scale $\Mtltl/2$,
a factor-two scale variation yields a LO scale uncertainty of 
$20\%{-}25\%$ for the off-shell signal and signal plus background interference.
The results of these comparisons were calculated using 
\textsc{gg2VV} \cite{Kauer:2012hd}.

%################## T A B L E tab:ofs:pdfmin (BEGIN) ##########################
\begin{table}%[Htbp]
\caption{PDF dependence of off-shell $\Pg\Pg\ (\to \PH)\to \Pl\PAl\Pl'\PAl'$ 
cross sections at LO in \Ufb\ for one lepton flavour combination. 
MIN cuts are applied.  $R$ is the ratio of NNLO, LO result to NLO 
result.  The bottom rows show the ratio of OFS, HM1, HM2 to RES result
for $S$ and $S+I$. The recommended QCD scale $\mu_R=\mu_F=\Mtltl/2$ is used.
The MC error is given in brackets.
See main text for other details.}
\label{tab:ofs:pdfmin}%
\renewcommand{\arraystretch}{1.2}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
\footnotesize
\begin{tabular}{llccccccc}
%\cline{3-9}
\toprule
\multicolumn{2}{c}{} & \multicolumn{7}{c}{PDF set order} \\
\cmidrule(lr){3-9}
Reg. & Amp. & \textbf{NLO} & NNLO & $R$ & LO($0.118$) & $R$ & LO($0.130$) & $R$ \\
\midrule
    & $S$  & $0.1266(1)$  & $0.1255(1)$ & $0.991(2)$  & $0.1255(1)$  & $0.992(2)$  & $0.1414(2)$  & $1.116(2)$ \\
OFS & $S+I$  & $-0.1313(2)$ & $-0.1298(2)$ & $0.988(2)$   & $-0.1307(2)$  & $0.995(2)$  & $-0.149(1)$ & $1.138(8)$  \\
    & $B$  & $2.988(4)$  & $2.945(5)$ & $0.986(2)$  & $2.960(4)$  & $0.991(2)$  & $3.448(5)$  & $1.154(3)$ \\
\midrule
    & $S$  & $0.01933(4)$ & $0.01906(4)$ & $0.986(3)$ & $0.01899(4)$ & $0.982(3)$ & $0.02210(5)$  & $1.143(4)$ \\
HM1 & $S+I$  & $-0.04550(8)$  & $-0.04475(8)$  & $0.984(3)$  & $-0.04486(7)$  & $0.986(3)$  & $-0.0516(6)$  & $1.13(2)$ \\
    & $B$  & $1.182(3)$ & $1.165(3)$ & $0.985(3)$ & $1.166(3)$ & $0.986(3)$ & $1.354(3)$ & $1.145(4)$ \\
\midrule
    & $S$  & $0.0981(1)$  & $0.0974(1)$  & $0.993(2)$  & $0.0973(1)$  & $0.992(2)$  & $0.1084(2)$  & $1.105(2)$ \\
HM2 & $S+I$  & $-0.0465(1)$ & $-0.04622(9)$ & $0.994(3)$ & $-0.04637(9)$ & $0.997(3)$ & $-0.0522(6)$ & $1.12(2)$ \\
    & $B$  & $0.611(2)$  & $0.605(2)$  & $0.990(4)$  & $0.598(2)$  & $0.980(4)$  & $0.676(2)$  & $1.107(5)$ \\
\midrule
    & $S$  & $0.800(1)$ & $0.780(1)$ & $0.976(2)$ & $0.843(1)$ & $1.054(2)$ & $1.021(2)$ & $1.276(3)$ \\
RES & $S+I$  & $0.803(2)$  & $0.784(2)$  & $0.976(4)$  & $0.845(4)$  & $1.052(6)$  & $1.023(3)$  & $1.274(5)$ \\
    & $B$  & $0.1092(2)$ & $0.1063(2)$ & $0.974(2)$ & $0.1150(2)$ & $1.053(3)$ & $0.1389(2)$ & $1.272(3)$ \\
\toprule
OFS/ & $S$   & $0.1583(3)$  & $0.1609(3)$  &  & $0.1490(3)$  &  & $0.1385(3)$  &  \\
RES  & $S+I$ & $-0.1635(4)$  & $-0.1655(5)$  &  & $-0.1547(7)$ &  & $-0.146(2)$  &  \\
HM1/ & $S$   & $0.02418(6)$  & $0.02443(6)$  &  & $0.02253(6)$  &  & $0.02165(5)$  &  \\
RES  & $S+I$ & $-0.0566(2)$ & $-0.0571(2)$ &  & $-0.0531(3)$  &  & $-0.0504(6)$  &  \\
HM2/ & $S$   & $0.1227(2)$  &  $0.1249(3)$   &  & $0.1155(2)$  &  & $0.1062(2)$  &  \\
RES  & $S+I$ & $-0.0579(2)$  & $-0.0589(2)$  &  & $-0.0549(3)$  &  & $-0.0510(6)$  &  \\
\bottomrule
\end{tabular}
\end{center}
\end{table}
%################## T A B L E tab:ofs:pdfmin (END)   #########################

%################## T A B L E tab:ofs:pdfcms (BEGIN) ##########################
\begin{table}%[Htbp]
\caption{PDF dependence of off-shell 
$\Pg\Pg\ (\to \PH)\to \Pem\Pep\PGmm\PGmp$ 
cross sections at LO in \Ufb. 
CMS cuts are applied.  Other details as in \refT{tab:ofs:pdfmin}.}
\label{tab:ofs:pdfcms}%
\renewcommand{\arraystretch}{0.8}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
\footnotesize
\begin{tabular}{llccccccc}
%\cline{3-9}
\toprule
\multicolumn{2}{c}{} & \multicolumn{7}{c}{PDF set order} \\
\cmidrule(lr){3-9}
Reg. & Amp. & \textbf{NLO} & NNLO & $R$ & LO($0.118$) & $R$ & LO($0.130$) & $R$ \\
\midrule
% ----------------------------------------------------------------------------
% ---------------------- results from Claire O'Brien -------------------------
% ----------------------------------------------------------------------------
    & $S$  & $0.0952(3)$  & $0.09396(8)$  & $0.986(3)$  & $0.09034(7)$  & $0.949(3)$  & $0.10191(8)$  & $1.070(3)$ \\
OFS & $S+I$  & $-0.0893(3)$  & $-0.0883(1)$  & $0.989(3)$  & $-0.08436(9)$  & $0.944(3)$  & $-0.0973(1)$  & $ 1.089(4)$ \\
    & $B$  & $1.869(3)$  & $1.841(3)$  & $0.985(2)$  & $1.736(2)$  & $0.928(2)$  & $2.033(3)$  & $1.088(2)$ \\
\midrule
        & $S$  & $0.01303(9)$  & $0.01278(3)$  & $0.981(7)$  & $0.01200(3)$  & $0.921(7)$  & $0.01402(3)$  & $1.076(8)$ \\
HM1 & $S+I$  & $-0.0298(2)$  & $-0.02942(6)$  & $0.986(6)$  & $-0.02759(5)$  & $0.925(5)$  & $-0.03227(6)$  & $1.082(6)$ \\
    & $B$  & $0.738(2)$  & $0.727(2)$  & $ 0.986(3)$  & $0.679(2)$  & $0.920(3)$  & $0.795(2)$  & $1.079(4)$ \\
\midrule
    & $S$  & $0.0761(3)$  & $0.07531(8)$  & $ 0.990(4)$  & $0.07271(7)$  & $0.956(3)$  & $0.08123(8)$  & $1.067(4)$ \\
HM2 & $S+I$  & $-0.0349(2)$  & $-0.03471(7)$  & $ 0.994(6)$  & $-0.03376(6)$  & $0.967(6)$  & $-0.03757(7)$  & $1.076(7)$ \\
    & $B$  & $0.382(2)$  & $0.377(2)$  & $0.987(5)$  & $0.353(1)$  & $ 0.925(5)$  & $0.403(2)$  & $1.055(5)$ \\

\midrule
    & $S$  & $0.4392(7)$  & $0.4284(7)$  & $0.975(3)$  & $0.4343(7)$  & $0.989(3)$  & $0.5267(8)$  & $1.199(3)$ \\
RES & $S+I$  & $0.439(2)$  & $0.428(2)$  & $0.975(4)
$  & $0.433(2)$  & $0.988(4)$  & $0.527(2)$  & $1.200(5)$ \\
    & $B$  & $0.06294(8)$  & $0.06155(8)$  & $0.978(2)$  & $0.06243(9)$  & $ 0.992(2)$  & $0.0755(1)$  & $1.200(3)$ \\
\toprule
OFS/ & $S$   & $0.2169(7)$  & $0.2193(4)$  &  & $0.2080(4)$  &  & $0.1935(4)$  &  \\
RES  & $S+I$ & $-0.2036(8)$  & $-0.2065(6)$  &  & $-0.1946(6)$  &  & $ -0.1847(6)$  & \\
HM1/ & $S$   & $0.0297(2)$  & $0.02984(8)$  &  & $0.02762(8)$  &  & $0.02662(7)$  &  \\
RES  & $S+I$ & $-0.0680(4)$  & $-0.0688(3)$  &  & $-0.0637(3)$  &  & $-0.0613(2)$  &  \\
HM2/ & $S$   & $0.1733(6)$  & $0.1758(4)$  &  & $0.1674(4)$  &  & $0.1542(3)$  &  \\
RES  & $S+I$ & $-0.0796(5)$  & $-0.0811(3)$  &  & $-0.0779(3)$  &  & $-0.0714(3)$  &  \\
% ----------------------------------------------------------------------------
% ---------------------- results from Hugues Brun ----------------------------
% ----------------------------------------------------------------------------
%****        & $ S     $ & $0.0950(3)$ & $0.0936(3)$ & $0.985(4)$ & $0.0903(3)$ & $0.951(4)$ & $0.1019(3)$ & $1.072(4)$ \\ 
%****    OFS & $ S + I $ & $-0.105(8)$ & $-0.100(8)$ & $1.0(1)$ & $-0.092(7)$ & $0.88(9)$ & $-0.102(8)$ & $1.0(1)$ \\ 
%****        & $ B     $ & $1.883(8)$ & $1.848(8)$ & $0.982(6)$ & $1.741(7)$ & $0.925(6)$ & $2.038(8)$ & $1.082(6)$ \\ 
%****    \hline 
%****        & $ S     $ & $0.0130(5)$ & $0.01280(5)$ & $0.984(6)$ & $0.0120(5)$ & $0.923(5)$ & $0.01410(6)$ & $1.085(6)$ \\ 
%****    HM1 & $ S + I $ & $-0.031(4)$ & $-0.030(5)$ & $1.0(2)$ & $-0.026(4)$ & $0.8(2)$ & $-0.043(5)$ & $1.4(3)$ \\ 
%****        & $ B     $ & $0.743(4)$) & $0.729(4)$ & $0.981(8)$ & $0.673(3)$ & $0.905(7)$ & $0.801(5)$ & $1.077(8)$ \\ 
%****    \hline 
%****        & $ S     $ & $0.0763(3)$ & $0.0753(3)$ & $0.987(6)$ & $0.0728(3)$ & $0.954(6)$ & $0.0814(3)$ & $1.067(6)$ \\ 
%****    HM2 & $ S + I $ & $-0.034(2)$ & $-0.035(2)$ & $1.01(6)$ & $-0.034(2)$ & $0.99(6)$ & $-0.036(2)$ & $1.06(7)$ \\ 
%****        & $ B     $ & $0.379(2)$ & $0.374(2)$ & $0.986(6)$ & $0.352(2)$ & $0.928(6)$ & $0.400(2)$ & $1.055(7)$ \\ 
%****    \hline 
%****        & $ S     $ & $0.461(6)$ & $0.449(6)$ & $0.97(2)$ & $0.454(3)$ & $0.99(2)$ & $0.551(4)$ & $1.20(2)$ \\ 
%****    RES & $ S + I $ & $0.45(3)$ & $0.48(4)$ & $1.1(1)$ & $0.44(2)$ & $0.98(8)$ & $0.54(3)$ & $1.2(1)$ \\ 
%****        & $ B     $ & $0.0623(3)$ & $0.0610(3)$ & $0.979(6)$ & $0.0622(3)$ & $0.998(6)$ & $0.0753(4)$ & $1.209(8)$ \\ 
%****    \hline 
%****    \hline   
%****    OFS/ & $ S     $& $0.206(3)$ & $0.209(3)$ & & $0.199(2)$ & & $0.164(2)$ & \\ 
%****    RES & $ S + I $& $-0.23(3)$ & $-0.21(3)$ & & $-0.21(2)$ & & $-0.17(2)$ & \\ 
%****    HM1/ & $ S     $& $0.0282(3)$ & $0.0285(3)$ & & $0.0264(2)$ & & $0.0218(2)$ & \\ 
%****    RES & $ S + I $& $-0.07(1)$ & $-0.06(1)$ & & $-0.059(8)$ & & $-0.048(9)$ & \\ 
%****    HM2/ & $ S     $& $0.166(2)$ & $0.168(2)$ & & $0.160(2)$ & & $0.132(2)$ & \\ 
%****    RES & $ S + I $& $-0.076(6)$ & $-0.073(7)$ & & $-0.077(5)$ & & $-0.063(5)$ & \\ 
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
\bottomrule
\end{tabular}
\end{center}
\end{table}
%################## T A B L E tab:ofs:pdfcms (END)   #########################

%################## T A B L E tab:ofs:scalemin (BEGIN) ########################

\begin{table}%[Htbp]

%\thispagestyle{empty}
\vspace{-0.9cm}

\caption{QCD scale $\mu=\mu_R=\mu_F$ dependence and symmetric scale uncertainty of 
off-shell $\Pg\Pg\ (\to \PH)\to \Pl\PAl\Pl'\PAl'$ 
cross sections at LO in \Ufb\ for one lepton-flavour combination. 
MIN cuts are applied.  $R$ is the ratio of the result to the cross section with 
the recommended scale choice $\mu=\Mtltl/2$.    
As recommended, the NLO PDF set is used. Other details as in \refT{tab:ofs:pdfmin}.}
\label{tab:ofs:scalemin}%
\renewcommand{\arraystretch}{1}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
\footnotesize
\begin{tabular}{llccccccc}
%\cline{3-9}
\toprule
\multicolumn{2}{c}{} & \multicolumn{3}{c}{Dynamic scale} & \multicolumn{4}{c}{Fixed scales} \\
\cmidrule(lr){3-9}
     &      &                   & $\Delta(\Mtltl)$ & $R$ &  &  &  & \\
Reg. & Amp. & \textbf{$\Mtltl/2$} & $\Delta(\Mtltl/4)$ & $R$ &  $\MH/2$ & $R$ & $\MZ$ & $R$ \\
     &      &                   & symmetr.\  $\Delta$  & $R$ &  &  &  & \\
\midrule
    & $S$  &   & $-0.0258(2)$  & $-0.204(2)$  &   &   &   &  \\
    & $S$  & $0.1266(1)$ &  $0.0349(2)$ &  $0.276(2)$ & $0.2038(2)$ & $1.610(2)$ & $0.1760(2)$& $1.390(2)$\\
    & $S$  &   & $\pm0.0303(2)$  & $\pm0.240(1)$  &   &   &   &  \\
    & $S+I$  &   & $0.0251(2)$  & $0.182(2)$  &   &   &   &  \\
OFS & $S+I$  & $-0.1313(2)$ & $-0.0328(2)$ & $-0.250(2)$& $-0.1831(2)$ & $1.394(2)$ & $-0.1604(2)$ & $1.221(2)$ \\
    & $S+I$  &   & $\pm0.0290(2)$  &$\pm0.221(1)$ &   &   &   &  \\
    & $B$  &   &  $-0.545(5)$ & $-0.182(2)$  &   &   &   &  \\
    & $B$  & $2.988(4)$ & $0.699(7)$ &  $0.234(3)$ & $3.751(4)$ & $1.255(3)$ & $3.327(4)$ & $1.114(2)$ \\
    & $B$  &   &  $\pm0.6225(4)$ & $\pm0.209(2)$    &   &   &   &  \\
\midrule
    & $S$  &   &  $-0.00355(4)$ &$-0.184(3)$  &   &   &   &  \\
    & $S$  & $0.01928(3)$ &$0.00455(6)$ &  $0.236(3)$ & $0.02406(6)$ & $1.248(4)$ & $0.02150(5)$ & $1.115(3)$ \\
    & $S$  &   &  $\pm0.00405(4)$ & $\pm0.210(2)$  &   &   &   &  \\
    & $S+I$  &   & $0.0085(1)$ & $0.187(3)$  &   &   &   &  \\
HM1 & $S+I$  & $-0.04553(8)$ &  $-0.0106(2)$& $-0.233(3)$ & $-0.0561(1)$ & $1.233(3)$ & $-0.05002(9)$ & $1.099(3)$ \\
    & $S+I$  &   & $\pm0.0096(1)$  & $\pm0.2095(2)$  &   &   &   &  \\
    & $B$  &   & $-0.223(4)$  & $-0.188(3)$ &   &   &   &  \\
    & $B$  & $1.186(3)$ &  $0.273(5)$ & $0.230(4)$ & $1.462(3)$ & $1.232(4)$ & $1.302(3)$ & $1.098(4)$ \\
    & $B$  &   & $\pm0.248(2)$  & $\pm0.209(3)$  &   &   &   &  \\
\midrule
    & $S$  &   &$-0.0207(2)$  & $-0.211(2)$  &   &   &   &  \\
    & $S$  & $0.0982(2)$& $0.0284(2)$ & $0.289(2)$ & $0.1693(2)$ & $1.724(3)$ & $0.1451(2)$ & $1.478(3)$ \\
    & $S$  &   & $\pm0.0246(2)$  & $\pm0.250(2)$  &   &   &   &  \\
    & $S+I$  &   & $0.0099(2)$  & $0.212(3)$  &   &   &   &  \\
HM2 & $S+I$  & $-0.04651(8)$ & $-0.0136(2)$ & $-0.293(3)$ & $-0.0818(2)$ & $1.760(5)$ & $-0.0700(2)$ & $1.505(4)$ \\
    & $S+I$  &   & $\pm0.0118(1)$  & $\pm0.253(2)$  &   &   &   &  \\
    & $B$  &   & $-0.123(2)$ & $-0.201(3)$  &   &   &   &  \\
    & $B$  & $0.610(1)$ &  $0.167(3)$ & $0.275(5)$ & $0.929(3)$ & $1.524(5)$ & $0.807(2)$ & $1.323(4)$ \\
    & $B$  &   & $\pm0.145(2)$  & $\pm0.238(3)$  &   &   &   &  \\
\midrule
    & $S$  &   & $-0.115(2)$  & $-0.143(2)$  &   &   &   &  \\
    & $S$  & $0.800(1)$ & $0.131(2)$ & $0.164(2)$ & $0.801(2)$ & $1.001(2)$ & $0.737(1)$ & $0.921(2)$ \\
    & $S$  &   & $\pm0.123(2)$  & $\pm0.154(2)$ &   &   &   &  \\
    & $S+I$  &   & $-0.116(3)$  & $-0.145(2)$  &   &   &   &  \\
RES & $S+I$  & $0.803(2)$ & $0.130(3)$ & $0.162(3)$ & $0.803(2)$ & $1.000(3)$ & $0.739(2)$ & $0.920(3)$ \\
    & $S+I$  &   & $\pm0.123(2)$  & $\pm0.153(2)$ &   &   &   &  \\
    & $B$  &   & $-0.0158(3)$ & $-0.145(3)$  &   &   &   &  \\
    & $B$  & $0.1092(2)$ & $0.0176(3)$ &  $0.162(3)$ & $0.1089(2)$ & $0.998(2)$ & $0.1002(2)$ & $0.917(2)$ \\
    & $B$  &   & $\pm0.0167(2)$  & $\pm0.153(2)$ &   &   &   &  \\
\toprule
OFS/ & $S$   & $0.1583(3)$  &   &  & $0.2545(5)$  &  &$0.2389(4)$ &  \\
RES  & $S+I$ & $-0.1635(4)$  &   &  & $-0.2279(5)$  &  & $-0.2172(5)$  &  \\
HM1/ & $S$   & $0.02411(5)$  &   &  & $0.03005(8)$ &  & $0.02918(8)$ &  \\
RES  & $S+I$ & $-0.0567(2)$  &   &  & $-0.0699(2)$  &  & $-0.0677(2)$  &  \\
HM2/ & $S$   & $0.1228(3)$  &   &  & $0.2114(4)$ &  & $0.1970(4)$ &  \\
RES  & $S+I$ & $-0.0579(2)$  &   &  & $-0.1019(3)$  &  & $-0.0948(3)$  &  \\
\bottomrule
\end{tabular}
\end{center}
\end{table}
%################## T A B L E tab:ofs:scalemin (END)   ########################

%################## T A B L E tab:ofs:scalecms (BEGIN) ########################

\begin{table}%[Htbp]

%\thispagestyle{empty}
\vspace{-0.1cm}

\caption{QCD scale $\mu=\mu_R=\mu_F$ dependence and symmetric scale uncertainty of 
off-shell $\Pg\Pg\ (\to \PH)\to \Pl\PAl\Pl'\PAl'$ 
cross sections at LO in \Ufb\ for one lepton-flavour combination. 
CMS cuts are applied.  Other details as in \refT{tab:ofs:scalemin}.}
\label{tab:ofs:scalecms}%
\renewcommand{\arraystretch}{1.2}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
\footnotesize
\begin{tabular}{llccccccc}
%\cline{3-9}
\toprule
\multicolumn{2}{c}{} & \multicolumn{3}{c}{Dynamic scale} & \multicolumn{4}{c}{Fixed scales} \\
\cmidrule(lr){3-9}
     &      &                   & $\Delta(\Mtltl)$ & $R$ &  &  &  & \\
Reg. & Amp. & \textbf{$\Mtltl/2$} & $\Delta(\Mtltl/4)$ & $R$ &  $\MH/2$ & $R$ & $\MZ$ & $R$ \\
     &      &                   & symmetr.\  $\Delta$  & $R$ &  &  &  & \\
\midrule
    & $S$  &   & $-0.0196(3)$  & $ -0.206(4)$  &   &   &   &  \\
    & $S$  & $0.0952(3)$ & $0.0257(4)$ & $0.270(4)$ & $0.1545(4)$ & $1.622(6)$ & $0.1338(4)$ & $1.405(5)$ \\
    & $S$  &   & $\pm0.0227(3)$  & $\pm0.238(3)$  &   &   &   &  \\
    & $S+I$  &   & $ 0.0164(4)$  & $0.184(4)$  &   &   &   &  \\
OFS & $S+I$  & $-0.0893(3)$ & $-0.0223(4)$ & $-0.250(5)$ & $-0.1282(4)$ & $1.435(6)$ & $-0.1119(3)$ & $1.253(5)$ \\
    & $S+I$  &   & $\pm0.0194(3)$  & $\pm0.217(3)$  &   &   &   &  \\
    & $B$  &   &  $-0.331(4)$ & $-0.177(2)$  &   &   &   &  \\
    & $B$  & $1.869(3)$ & $0.430(4)$ & $ 0.230(2)$ & $2.341(3)$ & $1.252(3)$ & $2.084(3)$ & $1.115(2)$ \\
    & $B$  &   &  $\pm0.381(3)$ & $\pm0.204(2)$  &   &   &   &  \\
\midrule
    & $S$  &   &  $-0.00235(3)$ & $-0.181(2)$  &   &   &   &  \\
    & $S$  & $0.01302(2)$ & $0.00303(3)$ & $0.233(3)$ & $0.0163(2)$ & $1.25(1)$ & $0.0145(1)$ & $1.115(8)$ \\
    & $S$  &   &  $\pm0.00269(2)$ & $\pm0.207(2)$  &   &   &   &  \\
    & $S+I$  &   & $0.00536(6)$  & $0.179(2)$  &   &   &   &  \\
HM1 & $S+I$  & $-0.02986(5)$ & $-0.00682(7)$ & $-0.228(3)$ & $-0.0370(2)$ & $1.241(7)$ & $-0.0326(2)$ & $1.092(6)$ \\
    & $S+I$  &   & $\pm0.00609(5)$  & $\pm0.204(2)$  &   &   &   &  \\
    & $B$  &   & $-0.132(2)$  & $-0.178(2)$ &   &   &   &  \\
    & $B$  & $0.739(1)$ & $0.168(2)$ & $0.227(3)$ & $0.908(2)$ & $1.229(3)$ & $0.811(2)$ & $1.097(3)$ \\
    & $B$  &   & $\pm0.150(1)$  & $\pm0.203(2)$  &   &   &   &  \\
\midrule
    & $S$  &   & $-0.0160(2)$  & $-0.210(2)$  &   &   &   &  \\
    & $S$  & $0.0761(1)$ & $0.0218(2)$ & $0.286(3)$ & $0.1315(4)$ & $1.727(6)$ & $0.1131(4)$ & $1.485(5)$ \\
    & $S$  &   & $\pm0.0189(1)$  & $\pm0.248(2)$  &   &   &   &  \\
    & $S+I$  &   & $0.00740(7)$  & $0.211(2)$  &   &   &   &  \\
HM2 & $S+I$  & $-0.03505(6)$ & $-0.01006(9)$ & $-0.287(3)$ & $-0.0630(3)$ & $1.798(9)$ & $-0.0537(3)$ & $1.533(8)$ \\
    & $S+I$  &   & $\pm0.0088(1)$  & $\pm0.249(2)$  &   &   &   &  \\
    & $B$  &   & $-0.0768(8)$ & $-0.201(2)$  &   &   &   &  \\
    & $B$  & $0.3822(6)$ & $0.1019(9)$ & $0.267(3)$ & $0.582(2)$ & $ 1.522(5)$ & $0.506(2)$ & $1.324(4)$ \\
    & $B$  &   & $\pm0.090(1)$  & $\pm0.234(2) $  &   &   &   &  \\
\midrule
    & $S$  &   & $-0.0603(9)$  & $-0.137(2)$  &   &   &   &  \\
    & $S$  & $ 0.4392(7)$ & $0.066(1)$ & $0.151(3)$ & $0.4389(7)$ & $0.999(3)$ & $0.4044(6)$ & $0.921(2)$ \\
    & $S$  &   & $\pm0.064(2)$  & $\pm0.145(2)$ &   &   &   &  \\
    & $S+I$  &   & $-0.060(2)$  & $-0.136(4)$  &   &   &   &  \\
RES & $S+I$  & $0.439(2)$ & $0.067(2)$ & $ 0.154(5)$ & $0.438(2)$ & $0.999(4)$ & $ 0.406(2)$ & $ 0.925(4)$ \\
    & $S+I$  &   & $\pm0.064(2)$  & $\pm0.145(3)$ &   &   &   &  \\
    & $B$  &   & $-0.0086(2)$  & $-0.136(2)$  &   &   &   &  \\
    & $B$  & $0.06294(8)$ & $0.0097(2)$ & $0.155(2)$ & $0.06302(9)$ & $1.001(2)$ & $0.05816(8)$ & $0.924(2)$ \\
    & $B$  &   & $\pm0.0092(1)$  & $\pm0.146(2)$ &   &   &   &  \\
\toprule
OFS/ & $S$   & $0.2169(7)$  &   &  & $ 0.352(1)$  &  & $0.331(1)$  &  \\
RES  & $S+I$ & $-0.2036(8)$  &   &  & $-0.292(2)$  &  & $-0.276(2)$  &  \\
HM1/ & $S$   & $0.02964(6)$  &   &  & $0.0371(3)$  &  & $0.0359(3)$  &  \\
RES  & $S+I$ & $-0.0681(3)$  &   &  & $-0.0845(5)$  &  & $-0.0804(5)$  &  \\
HM2/ & $S$   & $0.1734(4)$  &   &  & $0.300(1)$  &  & $0.280(1)$  &  \\
RES  & $S+I$ & $-0.0799(3)$  &   &  & $-0.1437(8)$  &  & $-0.1325(7)$  &  \\
\bottomrule
\end{tabular}
\end{center}
\end{table}
%################## T A B L E tab:ofs:scalecms (END)   ########################

%\clearpage
