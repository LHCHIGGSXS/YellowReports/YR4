%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}

The Higgs boson measurements in the resonant region (on-peak) are broadly consistent
with Standard Model expectations.
The observed Higgs boson cross-sections are primarily measured via decays
into two electroweak bosons ($WW$, $ZZ$ and $\gamma\gamma$).
However, the measured on-peak cross-sections are affected by an intrinsic scaling 
ambiguity between the Higgs boson couplings and the total Higgs boson width: 
$\sigma_{i\to H\to f}\sim g_i^2 g_f^2/\Gamma_H$.
Disentangling this ambiguity would make it possible to constrain or even measure the 
total Higgs boson width at the LHC, which would be highly desirable.
%
The total width of the SM Higgs boson is about 4 MeV, and hence much smaller than the 
experimental resolution of the Higgs boson mass measurements in the two high-resolution 
channels $H\to 4\ell$ and $H\to \gamma\gamma$, which is of the order of 1 GeV.
For this reason, a direct measurement of the Higgs boson width is not feasible at the LHC.
%With some assumptions in the Higgs boson couplings, measurements of the Higgs boson through invisible decays can provide indirect limit on the total Higgs boson.

A novel method has recently been proposed to constrain the Higgs boson
width using events away from the on-peak region in the decays into $ZZ$ and $WW$ 
\cite{Kauer:2012hd,Caola:2013yja,Campbell:2013una}.
The off-shell cross-section of $gg\to H^\ast\to VV$ contributes $\mathcal{O}(15\%)$ due to
two threshold effects, near $2 M_{V}$ from the Higgs boson decay and $2 m_{t}$ from the $gg\to H$ production.
The electroweak diboson continuum $gg\to VV$ plays an important role in this off-shell region,
mainly due to the large destructive interference with the $gg\to H^\ast\to VV$ signal.
At leading order, $gg\to VV$ proceeds through a box diagram, which makes higher
order calculations difficult.
In this off-shell region, where $M_{VV}\gg M_H$, the cross-section dependence on the total Higgs boson width is negligible, providing a unique opportunity to measure the absolute Higgs boson couplings.
The off-shell Higgs boson couplings can then be correlated with the on-shell cross-sections
to provide a novel indirect constraint on the total Higgs boson width.
It has been pointed out \cite{Englert:2014aca,Logan:2014ppa} that BSM physics that alters 
the relation between Higgs cross-sections in the on-peak and off-shell regions could invalidate the method as applied in \cite{Caola:2013yja,Campbell:2013una}.  Using future LHC 
data to constrain New Physics affecting the off-shell Higgs boson couplings is therefore
important \cite{Gainer:2014hha,Azatov:2014jga}.

The method has been promptly adopted by the CMS and ATLAS collaborations.
The analyses \cite{Khachatryan:2014iha,Aad:2015xua,Khachatryan:2016ctc} present constraints on the off-shell Higgs boson event
yields normalized to the Standard Model prediction (signal strength) in the
$ZZ$$\rightarrow$4$\ell$, $ZZ$$\rightarrow$2$\ell$2$\nu$ and $WW$$\rightarrow\ell\nu\ell\nu$ channels.
In the ATLAS analysis \cite{Aad:2015xua}, using the CLs method, the observed 95\% confidence level (CL)
upper limit on the off-shell signal strength is in the range 5.1--8.6, with an expected range of 6.7--11.0.
This range is determined by varying the unknown  \footnote{cf.\ \refS{sec:offshell_interf_ggVV_NLO}}
 $gg$$\rightarrow$ $ZZ$ and $gg$$\rightarrow$$ WW$ background K-factor
from higher-order QCD corrections between half and twice the value of the evaluated signal K-factor.
Under the assumption that the Higgs boson couplings are independent of the energy scale of the Higgs boson production,
a combination of the off-shell constraint with the on-shell Higgs peak measurement yields an observed (expected) 95\% CL
upper limit on the Higgs boson total width normalized to the one predicted by the Standard Model, i.e. $\Gamma_{\mathrm{H}}/\Gamma_{\mathrm{SM}}$,
in the range of 4.5--7.5 (6.5--11.2) employing the same variation of the background K-factor.
Assuming that the unknown gg$\rightarrow VV$ background K-factor is equal to the signal K-factor,
this translates into an observed (expected) 95\% CL upper limit on the Higgs boson total width of 22.7 (33.0) MeV.


In the CMS analysis of the $ZZ$ and $WW$ channels combined \cite{Khachatryan:2016ctc}, an observed (expected) upper limit on the off-shell Higgs boson event yield normalized to the Standard Model prediction of 2.4 (6.2) is obtained at the 95\% CL for the gluon fusion process and of 19.3 (34.4) for the VBF process. The observed and expected constraints on the Higgs boson total width are 13 MeV and 26 MeV, respectively, at the 95\% CL. Concerning the $gg\rightarrow$$VV$ background K-factor, the central values and uncertainties are assumed to be equal to those of the signal K-factor, with an additional 10\% uncertainty.

In addition to the off-shell $H^*\to VV$ channels, the $H\to\gamma\gamma$
channel also provides a very clean signature for probing Higgs boson properties, including its mass.
However, there is also a large continuum background $gg\to \gamma\gamma$
to its detection in this channel.
It is important to study how much the coherent interference between the Higgs boson signal
and the background could affect distributions in
diphoton observables, and possibly use it to constrain Higgs boson properties.
An interesting study\cite{Martin:2012xc,Dixon:2013haa} showed that this interference
can lead to a shift in the Higgs boson mass, which has a
strong dependence on the $p_T$ of the diphoton system and the total Higgs boson width.
This provides another way to constrain the Higgs boson width.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
