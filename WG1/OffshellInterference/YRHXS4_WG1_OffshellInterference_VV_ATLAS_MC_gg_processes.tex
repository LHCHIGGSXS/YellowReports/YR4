\subsection{Study of higher-order QCD corrections in the \texorpdfstring{gg$\rightarrow$H$\rightarrow$VV}{gg to H to VV} process}
\label{sec:offshell_interf_vv_atlas_mc_gg}

\subsubsection{Introduction}
\label{sec:offshell_gghvv_shower_intro}

The analysis \cite{Aad:2015xua} employed to extract the off-shell signal strength in the high mass ($m_{\mathrm{4\ell}}>$220 GeV) ZZ$\rightarrow$4$\ell$, ZZ$\rightarrow$2$\ell$2$\nu$ and WW$\rightarrow\ell\nu\ell\nu$ final states, is based on two Monte Carlo simulations for gg-initiated processes,
namely gg2VV \cite{Kauer:2013qba} and MCFM \cite{Campbell:2013una}. The dominant gg-initiated processes used in the analysis \cite{Aad:2015xua} are listed below:
\begin{enumerate}
\item gg$\rightarrow H \rightarrow$ ZZ, the signal (S) comprising both the on-shell peak at $m_{\mathrm{H}}=$125.5 GeV and the off-shell region where the Higgs boson acts as a propagator;
\item gg$\rightarrow$ ZZ, the continuum background (B);
\item gg$\rightarrow (H^{*}) \rightarrow$ ZZ, the signal, continuum background and interference contribution, labelled as SBI in what follows.
\end{enumerate}
However, only Lowest-Order (LO) in QCD Monte Carlo simulations are available, namely gg2VV and MCFM with Pythia8 \cite{Sjostrand:2007gs} showering. For this reason, mass-dependent K-factors to higher order accuracy are needed to achieve a better precision.
\begin{itemize}
\item For the signal process, higher order QCD corrections are computed: LO to Next-to-Next-to-Leading-Order (NNLO) K-factors are calculated as a function of the diboson invariant mass $m_{\mathrm{ZZ}}$.
\item  For the background process, the full K-factor from LO to NNLO accuracy is currently not available.
\end{itemize}

Different approaches exploited in order to take into account the absence of higher order QCD corrections in gg$\rightarrow (H^{*}) \rightarrow$VV final states (it is to note that Next-to-Leading Order, NLO, gg$\rightarrow$ZZ QCD calculation has been recently performed \cite{Caola:2015psa}) and the systematic uncertainties associated to these processes will be detailed in the following sections.

\subsubsection{Parton Shower Scheme Dependence}
\label{sec:offshell_gghvv_shower_psscheme}

Given that no higher order matrix element calculations are available for the $gg$-initiated processes, the only way to simulate QCD radiation is through the parton shower. However, as the generation is done at LO in QCD, there is no clear prescription to evaluate the systematic uncertainties on the QCD scale. According to the maximum jet $p_{\mathrm{T}}$ scale emission characterizing the parton showers, two different configurations \cite{ATL-PHYS-PUB-2016-006} are exploited, the \textit{power shower} (the emission is allowed up to the kinematical limit) and the \textit{wimpy shower} (the shower is started at the value of the factorization or the renormalization scale). Pythia8 is tuned as default with the power shower option. The comparison is carried out involving the following parton shower schemes at generator level:

\begin{itemize}
\item Pythia8 power shower including a matrix element correction on the first jet emission such that information coming from the exact matrix element calculation is exploited for the hardest jet in the shower \cite{Sjostrand:2007gs};
\item Pythia8 power shower  without a matrix element correction;
\item Pythia8 wimpy shower without a matrix element correction;
\item Herwig6.5 \cite{Corcella:2000bw} in combination with Jimmy.
\end{itemize}
The items above are finally compared to high-mass Powheg-Box \cite{Alioli:2008tz} NLO gg$\rightarrow H \rightarrow$ ZZ event sample with a Higgs boson mass generated with $m_{\mathrm{H}}$=380 GeV, chosen around the most sensitive off-shell invariant mass region for the analysis. The normalized $p_{\mathrm{T}}$(ZZ) distributions, detailed in \refF{fig:ps} as reported in Ref. \cite{ATL-PHYS-PUB-2016-006} for the sample above in the text are plotted in the same high ZZ mass range (345$<m_{\mathrm{4\ell}}<$415) GeV in order to ensure a compatible mass of the hard interaction system. As the default samples are generated with the LO gg$\rightarrow (H^{*})\rightarrow$ZZ matrix element with Pythia8 using the power shower parton shower option and this sample shows the largest deviation from Powheg, the full difference of the order of 10\% is taken as a systematic uncertainty in the ATLAS analysis as in \cite{Aad:2015xua}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure*}
\begin{center}
\includegraphics[width=0.6\linewidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/newShowerComparison-eps-converted-to.pdf}
\end{center}
\caption{Distribution of $p_{\mathrm{T}}$(ZZ) comparing the NLO generator Powheg showered with Pythia8, the LO generator gg2VV + Pythia (power or wimpy shower), the LO generator gg2VV showered with Jimmy+Herwig. All samples are restricted to the range (345$<m_{\mathrm{4\ell}}<$415) GeV.}
\label{fig:ps}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsubsection{Higher order QCD corrections to the transverse momentum and the rapidity of the ZZ system}
\label{sec:qcdPS}
Higher order QCD corrections for the gg$\rightarrow$ ZZ processes are studied using the Sherpa+OpenLoops \cite{Gleisberg:2008ta,Cascioli:2011va} generator that contains the LO gg$\rightarrow$ZZ+1-jet matrix element and merges this with the LO gg$\rightarrow$ZZ+ 0-jet matrix element. For the gg$\rightarrow H\rightarrow$ ZZ signal contribution with $m_{\mathrm{H}}$=380 GeV (on-shell signal), the Powheg generator reweighted (as a function of $p_{\mathrm{T}}$) to the HRes2.1 prediction \cite{deFlorian:2012mx} to reach NNLO+NNLL accuracy is also used. Figures \ref{fig:allPLOT}, \ref{fig:allPLOT2} and \ref{fig:allPLOT3} include validation distributions of various comparisons of the variables of interest, namely the transverse momentum, $p_{\mathrm{T}}$(ZZ),  and the rapidity, Y(ZZ), of the ZZ system in both on-shell and off-shell mass regions using Powheg+Pythia8, Sherpa+OpenLoops and gg2VV+Pythia8 generators using kinematic variables computed at truth level. The list of cuts applied in the generation level can be found below ($p_{\mathrm{T}}^{\mathrm{\ell}}$ is the transverse momentum of each lepton in the final state, $|\eta^{\mathrm{\ell}}|$ represents its rapidity \footnote{ATLAS uses a right-handed coordinate system with its origin at the nominal interaction point (IP) in the centre of the detector, and the z-axis along the beam line. The x-axis points from the IP to the centre of the LHC ring, and the y-axis points upwards. Cylindrical coordinates ($r,\phi$) are used in the transverse plane, $\phi$ being the azimuthal angle around the beam line. Observables labelled \textit{transverse} are projected into the x-y plane. The pseudorapidity is defined in terms of the polar angle $\theta$ as $\eta$=-$\ln \tan(\frac{\theta}{2})$.} while $m_{\textrm{Z1}}$ is the Z boson mass closest to the Z peak, being $m_{\mathrm{Z2}}$ the mass of the second lepton pair):

\begin{itemize}
\item $m_{\mathrm{4\ell}}>$100 GeV;
\item $p_{\mathrm{T}}^{\mathrm{\ell}}>$3 GeV;
\item $|\eta^{\mathrm{\ell}}|<$2.8;
\item $m_{\mathrm{Z1,Z2}}>$ 4 GeV.
\end{itemize}

Additional selection criteria are applied on the final state quadruplet (the leptons in the quadruplet are ordered in transverse momentum and denoted with the superscript $\mathrm{\ell}$ in what follows) in the Monte Carlo samples in such a way to mimic the standard selection reported in Ref. \cite{Aad:2015xua}, namely:
\begin{itemize}
\item  $p_{\mathrm{T}}^{\mathrm{\ell 1}}>$20 GeV, $p_{\mathrm{T}}^{\mathrm{\ell 2}}>$15 GeV, $p_{\mathrm{T}}^{\mathrm{\ell 3}}>$10 GeV, $p_{\mathrm{T}}^{\mathrm{\ell 4}}>$5 (6) GeV for muons (electrons);
\item $|\eta^{\mathrm{\ell}}|<$2.5;
\item (50$<m_{\mathrm{Z1}}<$106) GeV;
\item if $m_{\mathrm{4\ell}}<$140 GeV $\rightarrow$ $m_{\mathrm{Z2}}>$12 GeV, if 140$<m_{\mathrm{4\ell}}<$190 GeV $\rightarrow$ $m_{\mathrm{Z2}}>$0.76$\cdot$($m_{\mathrm{4\ell}}$-140)+12 GeV, if $m_{\mathrm{4\ell}}>$190 GeV $\rightarrow$ $m_{\mathrm{Z2}}>$50 GeV.
\end{itemize}

The errors bars in \refFs{fig:allPLOT}, \ref{fig:allPLOT2} and \ref{fig:allPLOT3} indicate the statistical uncertainty related to the finite Monte Carlo statistics only. The systematic uncertainties, when applicable, are drawn as shaded boxes, extracted from scale variations on Sherpa+OpenLoops and HRes2.1 as described in the following Section \ref{sec:sysBDT}. The systematic uncertainties from the HRes2.1 are applicable here as the Powheg generator is directly reweighted to the HRes2.1 prediction. The results and the distributions reported in the following figures refer to Monte Carlo samples generated  at a collision energy $\sqrt{s}$=8 TeV.
\\
\\
As highlighted in \refF{fig:allPLOT} (a) for what concerns the on-shell and \refF{fig:allPLOT} (b) for the off-shell, the lack of higher QCD calculations in gg2VV results in different $p_{\mathrm{T}}$ spectra (order of 20\% in the relevant kinematic region) compared to the higher order Powheg and Sherpa+OpenLoops Monte Carlo. In the high mass region, the off-shell (generated with $m_{\mathrm{H}}$=125.5 GeV) and on-shell (produced with $m_{\mathrm{H}}$=380 GeV) Higgs boson productions with gg2VV match fairly well as shown in \refF{fig:allPLOT} (b).
\\
\refF{fig:allPLOT2} (a) shows that the differences in $p_{\mathrm{T}}$ between Sherpa and gg2VV in the off-shell high mass region are not fully covered by the uncertainties assigned to Sherpa. Since the Sherpa generator has a better treatment of the first hard jet emission, in the $H\rightarrow$ZZ$\rightarrow$4$\ell$ analysis, gg2VV is reweighted to the Sherpa prediction in the ATLAS analysis \cite{Aad:2015xua}. As for the rapidity distribution reported in \refF{fig:allPLOT2} (b), no significant difference between gg2VV and Sherpa is present in the high mass region; hence, the reweighting procedure on Y is not necessary.
\\
Figures \ref{fig:allPLOT2} (c) and (d) stress the fact that the ZZ-transverse momentum and the rapidity of the signal process gg$\rightarrow (H^{*})\rightarrow$ ZZ differ from the gg$\rightarrow$ ZZ background process and the SBI unlike the gg2VV generator as noted in Figures \ref{fig:allPLOT3} (a) and (b). This is caused by the presence of the additional matrix element correction to the first jet emission included in Sherpa that generates a different treatment of signal and background components. This statement has been explicitly validated by removing the 1-jet matrix element computation in Sherpa: full compatibility is found between signal and background once the 1-jet ME treatment is removed in Sherpa.
\\
\\
In the analysis deployed by ATLAS \cite{Aad:2015xua}, the LO gg2VV generator, whose $p_{\mathrm{T}}$ and y distributions are displayed in \refF{fig:allPLOT3}, is reweighted to Sherpa+OpenLoops in the $p_{\mathrm{T}}$ of the VV system to achieve a better description of the $p_{\mathrm{T}}$ spectrum: the impact of the reweighting on the acceptance is calculated to be below 1\% for the signal and at the level of 4-6\% for the background. In the ZZ$\rightarrow$4$\ell$ channel, the reweighting procedure is only used to account for the acceptance effects, as the matrix-element discriminant employed to disentangle signal and background components is insensitive to the $p_{\mathrm{T}}$ of the ZZ system. For the ZZ$\rightarrow$2$\ell$2$\nu$ channel, the reweighting is applied in both the transverse mass shape and acceptance as the $m_{\mathrm{T}}$ holds dependence on the transverse momentum of the ZZ system.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\begin{center}
\subfigure[]{\includegraphics[width=0.47\linewidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/onShellPT_atlas_new-eps-converted-to.pdf}}
\subfigure[]{\includegraphics[width=0.47\linewidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/highMassPT_atlas_new3-eps-converted-to.pdf}}
\caption{Comparison of the on-shell gg$\rightarrow (H^{*})\rightarrow$ ZZ signal process in $p_{\mathrm{T}}$ (a) generated with $m_{\mathrm{H}}$=125.5 GeV in the mass range $m_{\mathrm{ZZ}} \in$ [124,126] GeV for Powheg, Sherpa and gg2VV. Comparison of the gg$\rightarrow (H^{*})\rightarrow$ ZZ off-shell signal process in $p_{\mathrm{T}}$ (b) with $m_{\mathrm{H}}$=125.5 GeV produced with gg2VV and Sherpa and gg$\rightarrow (H^{*})\rightarrow$ ZZ signal process with $m_{\mathrm{H}}$=380 GeV using gg2VV (on-shell) in the region $m_{\mathrm{ZZ}} \in$ [345,415] GeV.}
\label{fig:allPLOT}
\end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\begin{center}
\subfigure[]{\includegraphics[width=0.47\linewidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/highMassBisPt_atlas_new2_modified-eps-converted-to.pdf}}
\subfigure[]{\includegraphics[width=0.47\linewidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/highMassBisY_atlas_modified-eps-converted-to.pdf}}\\
\subfigure[]{\includegraphics[width=0.47\linewidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/SherpaPT_atlas_new2-eps-converted-to.pdf}}
\subfigure[]{\includegraphics[width=0.47\linewidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/SherpaY_atlas_new3-eps-converted-to.pdf}}
\caption{Comparison of the gg$\rightarrow (H^{*})\rightarrow$ ZZ off-shell signal process in $p_{\mathrm{T}}$ (a) and rapidity (b) generated with $m_{\mathrm{H}}$=125.5 GeV produced with gg2VV and Sherpa and gg$\rightarrow (H^{*})\rightarrow$ ZZ signal process with $m_{\mathrm{H}}$=380 GeV using Powheg (on-shell) in the region $m_{\mathrm{ZZ}} \in$ [345,415] GeV. Off-shell comparison in $p_{\mathrm{T}}$ (c) and rapidity (d) of the gg$\rightarrow (H^{*})\rightarrow$ ZZ signal sample generated with $m_{\mathrm{H}}$=125.5 GeV, the gg$\rightarrow$ ZZ background and the SBI contribution using Sherpa in the mass range $m_{\mathrm{ZZ}} \in$ [345,415] GeV.}
\label{fig:allPLOT2}
\end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\begin{center}
\subfigure[]{\includegraphics[width=0.47\linewidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/ggVVPT_atlas_new-eps-converted-to.pdf}}
\subfigure[]{\includegraphics[width=0.47\linewidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs//ggVVY_atlas-eps-converted-to.pdf}}
\caption{Comparison in $p_{\mathrm{T}}$ (a) and rapidity (b) of the three gg2VV contributions (signal generated with $m_{\mathrm{H}}$=125.5 GeV, background and SBI) in the mass region $m_{\mathrm{ZZ}} \in$ [345,415] GeV.}
\label{fig:allPLOT3}
\end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsubsection{Scale variations on the gg-initiated samples}
\label{sec:sysBDT}
In order to evaluate the systematic effects on the uncertainties on $p_{\mathrm{T}}$ and $\eta$ in the ZZ frame, the procedure is applied by varying the renormalization scale ($\mu_{\mathrm{R}}$), the factorization scale ($\mu_{\mathrm{F}}$), the resummation scale ($\mu_{\mathrm{Q}}$) and the resummation scale related to the bottom quark mass ($\mu_{\mathrm{B}}$).
\\
\\
The impact of the PDF uncertainties is also evaluated: the nominal PDF set, CT10 \cite{Gao:2013xoa}, applied on the Powheg signal sample at $m_{\mathrm{H}}$=125.5 GeV are compared with MSTW2008 \cite{Martin:2009iq} and with NNPDF2.3 \cite{Ball:2012cx} in bins of ZZ-transverse momentum and rapidity. Its impact is found to be below 3\%.

The Monte Carlo simulations employed for these studies and the full scheme of scale variations applied to these samples are listed in Table \ref{tab:scaleVAR}. Assuming that the resummation scales ($\mu_{\mathrm{Q}}$ and $\mu_{\mathrm{B}}$) variations are independent of the normalization and factorization scales ($\mu_{\mathrm{R}}$ and $\mu_{\mathrm{F}}$), we fix the vector pair ($\mu_{\mathrm{R}}$, $\mu_{\mathrm{F}}$) while varying $\mu_{\mathrm{Q}}$ or $\mu_{\mathrm{B}}$. Similarly we fix the resummation scales, $\mu_{\mathrm{Q}}$ and $\mu_{\mathrm{B}}$, while varying $\mu_{\mathrm{R}}$ and $\mu_{\mathrm{F}}$. Following the usual prescriptions, the nominal scale of the process is set to $m_{\mathrm{ZZ}}/2$ while the nominal value for the resummation scale related to the bottom mass is set to $m_{\mathrm{b}}$ and the Powheg nominal values for renormalization and factorization scales are set to $m_{\mathrm{ZZ}}$.

%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}
\caption{Scale variations considered in the evaluation of the theoretical uncertainties related to the $p_{\mathrm{T}}(\mathrm{ZZ})$ and Y(ZZ) for the $gg\to H\rightarrow$ZZ and $q\bar{q}\rightarrow$ZZ processes. The scale variations on Sherpa signal detailed in the second row are also applied on the Sherpa gg$\rightarrow$ZZ continuum background as stated in the text. The merging scale for Sherpa has not been modified for this study.}
\label{tab:scaleVAR}
\centering % centring table
\tabcolsep=0.11cm
\begin{tabular}{c |  c |  c | c | c}
  \toprule
Process & MC & Nominal Scales & Scale variations & \# Variations \\ [0.5ex]
\midrule
 {$gg\to H\to ZZ$} & {HRes} & $\mu_{\mathrm{R}}=\mu_{\mathrm{F}}={m_{\mathrm{ZZ}}\over2}$
& $({1\over2}\mu_{\mathrm{R/F}},2\mu_{\mathrm{R/F}})$, ${1\over2}\le\mu_{\mathrm{F}}/\mu_{\mathrm{R}}\ge2$ & 6 \\ [0.8ex]
&  &  $\mu_{\mathrm{Q}}=m_{\mathrm{ZZ}}/2$, $\mu_{\mathrm{B}}=m_{\mathrm{b}}$ & $({1\over2}\mu_{\mathrm{Q}}, 2\mu_{\mathrm{Q}})$, $({1\over4}\mu_{\mathrm{B}}, 4\mu_{\mathrm{B}})$ & 8 \\ [0.8ex]
\midrule
 {$gg\to H\to ZZ$}  &{Sherpa}
& $\mu_{\mathrm{R}}=\mu_{\mathrm{F}}={m_{\mathrm{ZZ}}\over2}$ & $({1\over2}\mu_{\mathrm{R/F}},2\mu_{\mathrm{R/F}})$, ${1\over2}\le\mu_{\mathrm{F}}/\mu_{\mathrm{R}}\ge2$ & 6 \\ [0.8ex]
& &  $\mu_{\mathrm{Q}}=m_{\mathrm{ZZ}}/2$, $\mu_{\mathrm{B}}=m_{\mathrm{b}}$ & $({1\over\sqrt{2}}\mu_{\mathrm{Q}}, \sqrt{2}\mu_{\mathrm{Q}})$ & 2 \\ [0.8ex]
\midrule
$q\bar{q}\to ZZ$ & Powheg & $\mu_{\mathrm{R}}=\mu_{\mathrm{F}}=m_{\mathrm{ZZ}}$ & $({1\over2}\mu_{\mathrm{R/F}},2\mu_{\mathrm{R/F}})$  &  6\\ [0.5ex]
\bottomrule
 \end{tabular}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%

\refF{fig:Hresplot} shows the shape-only variations on $p_{\mathrm{T}}$(ZZ) and Y(ZZ) for a high mass $m_{\mathrm{H}}$=380 GeV gg$\rightarrow H \rightarrow$ ZZ signal process, produced by QCD scale variations evaluated with the HRes2.1 Monte Carlo generator. The scale variations on the rapidity in \refF{fig:Hresplot} (b) can be neglected since they are much smaller than those of the transverse momentum, \refF{fig:Hresplot} (a). \refF{fig:sherpaPL} shows the variation of the signal process (a) and the background processes on $p_{\mathrm{T}}$(ZZ) created with the Sherpa+OpenLoops Monte Carlo sample. The envelope of these independent variations on $p_{\mathrm{T}}$(ZZ) is calculated as the maximal up and down contribution for each $p_{\mathrm{T}}$ bin for the HRes2.1 case as well as for Sherpa signal and background. Since the contribution of the resummation scale is dominant, a first envelope encompassing renormalization and factorization scales summed it in quadrature with the envelope extracted from the resummation scale provides enough accuracy for this study. Note that the Sherpa variations enclose the variations of HRes2.1 because Sherpa does not contain the full NLO calculations, hence its variations are larger than the typical scales of HRes2.1.The systematic uncertainties reported in Ref. \cite{Aad:2015xua} associated with the Sherpa-based reweighting in $p_{\mathrm{T}}$ of the VV system are assessed by varying the relevant scales in Sherpa: the larger in value between the scale variations in Sherpa and 50\% of the difference between Sherpa and gg2VV+Pythia  is assigned as the systematic uncertainty. This conservative approach is chosen to consider potential uncertainties not accounted for by the scale variations. The impact of the PDF uncertainties is found to be negligible.


%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure*}
\subfigure[]
{
\includegraphics[width=0.47\linewidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/HRES_pt-eps-converted-to.pdf}
}
\subfigure[]
{
\includegraphics[width=0.47\linewidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/HRES_y-eps-converted-to.pdf}
}
\caption{Relative change of the $p_{\mathrm{T}}$ and Y spectra due to the QCD scale variations produced with HRes2.1 signal generated at $m_{\mathrm{H}}$=380 GeV: ratio of the up or down variations $p_{\mathrm{T}}$ or rapidity with respect to the nominal distribution. Q labels the resummation scale, B the resummation scale related to the bottom quark mass, R the renormalization scale, F the factorization scale. The numbers coupled with each variation characterize the nominal value (1), the down variation (0) and the up variation (2).}
\label{fig:Hresplot}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure*}
\subfigure[]
{
\includegraphics[width=0.47\linewidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/sherpaS_atlas-eps-converted-to.pdf}
}
\subfigure[]
{
\includegraphics[width=0.47\linewidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/sherpaB_atlas-eps-converted-to.pdf}
}
\caption{Relative uncertainties on the $p_{\mathrm{T}}$ spectrum for the Sherpa+OpenLoops signal (a) and background (b) samples induced by the QCD scale variations: ratio of the up or down variations with respect to the nominal distribution. Q labels the resummation scale, R the renormalization scale, F the factorization scale.}
\label{fig:sherpaPL}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%%%%%%%
%\clearpage
