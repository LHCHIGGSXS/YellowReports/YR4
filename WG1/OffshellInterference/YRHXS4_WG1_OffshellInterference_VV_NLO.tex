%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{\texorpdfstring{$gg\to VV$}{gg to VV} at NLO QCD}
%\section{$\Pg\Pg\to \PV\PV$ at NLO QCD}
%\section{$\Pg\Pg\to \PZ\PZ$ at NLO QCD}
\label{sec:offshell_interf_ggVV_NLO}

\subsection{The status of theoretical predictions}
A good theoretical control of the off-shell region requires the
knowledge of higher order QCD correction for both the signal $pp\to H
\to 4l$ and the SM background $pp\to 4l$ processes.  At high invariant
masses, the signal $gg\to H \to 4l$ and the background $gg\to 4l$
processes individually grow with energy, eventually leading to
unitarity violations. In the SM, a strong destructive interference
between signal and background restores unitarity in the high energy regime,
and its proper modelling is important for reliable predictions in
the off-shell tail. At invariant masses larger than the top threshold
$m_{4l} > 2 \mt$ the effect of virtual top quarks running in the
loops is non negligible and must be taken into account.
%

The state of the art for theoretical predictions of signal, background
and interference is very different. For an exhaustive description of
the signal cross section we refer the reader to the relevant sections
of this report. As far as perturbative QCD is
concerned, the signal is known through NLO with exact quark mass
dependence~\cite{Djouadi:1991tka,Spira:1995rr}. NNLO corrections are
known as an expansion around the $\mt \to \infty$
limit~\cite{Harlander:2003ai,Anastasiou:2002yz,Ravindran:2003um},
matched to the exact high-energy limit~\cite{Marzani:2008az} to avoid
a spurious growth at high energies~\cite{Harlander:2009my,Pak:2009dg}.
%
Very recently, the N$^3$LO corrections became
available~\cite{Anastasiou:2015ema} in the infinite top mass
approximation. They turned out to be moderate, with a best stability
of the perturbative expansion reached for central scale
$\mu=\mh/2$. So far, results are known as an expansion around
threshold, which is expected to reproduce the exact result to better
than a per cent.
%

We now briefly discuss the status of theoretical description of the
background. In the SM, four-lepton production is dominated by quark
fusion processes $q\bar q \to VV \to 4l$.  Recently, NNLO QCD
corrections were computed for both the $ZZ$~\cite{Cascioli:2014yka} and the
$WW$~\cite{Gehrmann:2014fva} processes, leading to a theoretical uncertainty coming
from scale variation of a few per cent. In these prediction, the
formally NNLO gluon fusion channel $gg\to 4l$ enters for the first
time, i.e. effectively as a LO process. At the LHC, it is enhanced
by the large gluon flux and corresponds to roughly 60\%(35\%) of the
total NNLO corrections to the $ZZ$($WW$) process.
%
Despite being sub-dominant for $pp\to4l$ production, the $gg\to 4l$
sub-channel is of great importance for off-shell studies. First of
all, as we already mentioned there is a strong negative interference
between $gg\to 4l$ and $gg\to H\to 4l$. Second, the gluon fusion SM
background is harder to separate from the Higgs boson signal.

Computing NLO corrections to $gg\to 4l$ is highly non trivial as it
involves the knowledge of complicated two-loop amplitudes with both
external and internal massive particles. Parton shower studies based
on merged $gg\to 4l + 0,1~{\rm jet}$ have been performed for
example in~\cite{Cascioli:2013gfa}.  Very recently, NLO QCD corrections
for $gg\to VV\to 4l$ process were computed in the case of massless
quark running in the loop~\cite{Caola:2015psa,Caola:2015rqy}.
%
This approximation is expected to hold very well below threshold,
$m_{4l} < 2 \mt \sim 300~\rm{GeV}$. As in the Higgs case, finite top
quark effects are known as an expansion in
$1/\mt$~\cite{Melnikov:2015laa}.  Going beyond that would require
computing two-loop amplitudes which are currently beyond our
technological reach, so the exact result is not expected in the near
future.

\subsection{Brief description of the NLO computation for \texorpdfstring{$gg\to 4l$}{gg to 4l}}
\subsubsection{Massless quark contribution}
In this section, we briefly report the main details of the $gg\to VV
\to 4l$ NLO QCD
computations~\cite{Caola:2015psa,Caola:2015rqy}. Despite being NLO
calculations, they pose significant technical challenges.
%
First, complicated two-loop amplitude are required, see
\refF{offshell_interf_ggVV_NLO:twoloop} for a representative sample. These amplitudes were
recently computed in~\cite{Caola:2015ila,vonManteuffel:2015msa}. They
include decay of the vector bosons and account for full off-shell
effects. For the results in~\cite{Caola:2015psa,Caola:2015rqy}, the
public C++ implementation of Ref.~\cite{vonManteuffel:2015msa} was
used. To ensure the result is stable, the code code compares numerical
evaluations obtained with different (double, quadruple and, if
required, arbitrary) precision settings until the desired accuracy is
obtained.  For a typical phase space point, the evaluation of all
two-loop amplitudes requires about two seconds.
%

\begin{figure}
\centering
\includegraphics{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_NLO_figs/twoloop.pdf}
\caption{Representative two-loop diagrams for the $gg\to 4l$ process.
Leptonic decays of the vector bosons is assumed.}\label{offshell_interf_ggVV_NLO:twoloop}
\end{figure}

\begin{figure}
\centering
\includegraphics{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_NLO_figs/oneloop.pdf}
\caption{Representative double (left) and single (right) resonant one-loop diagrams
for the $gg\to4l+g$ process.}\label{offshell_interf_ggVV_NLO:oneloop}
\end{figure}

Second, one-loop real emission amplitudes are required, see
\refF{offshell_interf_ggVV_NLO:oneloop}.  Despite being only one-loop amplitudes, they must
be evaluated in degenerate soft/collinear kinematics, so they must be
numerically stable.  For the computations
in~\cite{Caola:2015psa,Caola:2015rqy}, these amplitudes were computed
using a mixture of numerical~\cite{Ellis:2007br} and
analytical~\cite{Badger:2008cm} unitarity. As a cross-check, the
obtained amplitudes were compared against
OpenLoops~\cite{Cascioli:2011va} for several different kinematic
points.  Possible numerical instabilities are cured by increasing the
precision of the computation. The typical evaluation time for a phase
space point, summed over colour and helicities, is about 0.1
seconds. Also in this case, full decay of the vector bosons into
leptons/neutrinos and off-shell effects are understood. Note that the
latter involve single-resonant diagrams, see 
\refF{offshell_interf_ggVV_NLO:oneloop}(right).
Arbitrary cuts on the final state
leptons/neutrinos (and additional jet) are possible.  In the
computations~\cite{Caola:2015psa,Caola:2015rqy}, interference between
$WW$ and $ZZ$ mediated processes for $2l2\nu$ final states are
neglected. They are expected to be irrelevant in the
experimental fiducial regions. Full $ZZ$/$\gamma\gamma$ interference
effects are included.

In~\cite{Caola:2015psa,Caola:2015rqy}, contributions coming from
$qb\to VV q$ mediated by closed fermion loops were not included. This
is because at $\mathcal O(\alpha_s^3)$ there are several other
contributions to the $qg$ channel other than one-loop squared
amplitudes, which in principle are not sub-dominant. Neglecting these
channels is fully justified in the large gluon approximation
of~\cite{Caola:2015psa,Caola:2015rqy}. Residual factorization scale
uncertainties are expected to give an estimate of the impact of neglected
channels.

In the $ZZ$ computation~\cite{Caola:2015psa}, the top quark
contribution is neglected and the bottom quark is considered massless
(see~\cite{Caola:2015psa} for more details).  This approximation is
expected to work at the 1\% level for the total $gg\to ZZ$
cross-section, but it is not reliable in the high invariant mass
regime. To quantify this, in \refF{offshell_interf_ggVV_NLO:mass_fig} we compare at LO the full massive
computation with the approximation~\cite{Caola:2015psa}.
\begin{figure}
\centering
\includegraphics[width=0.45\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_NLO_figs/plot_mass.pdf}
\includegraphics[width=0.45\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_NLO_figs/plot_mass_k.pdf}
\caption{Top quark mass contribution to $gg\to ZZ\to 4l$ at LO. Left: comparison between the exact result (blue) and the
approximation where the top quark contribution is omitted and the bottom quark is considered massless (see~\cite{Caola:2015psa} for details).
Right: ratio between the exact and approximate results for the central scale $\mu=m_{4l}/2$. See text for details.}\label{offshell_interf_ggVV_NLO:mass_fig}
\end{figure}
From the figure it is clear that below the top threshold the approximation~\cite{Caola:2015psa} is essentially
exact, while above the top quark contribution becomes rapidly important. The relative size of the top quark contribution is
quantified in the right panel of \refF{offshell_interf_ggVV_NLO:mass_fig}, where
\begin{equation}
\mathcal{R}^{\rm LO}_{tb}(m_{4l}) \equiv \left.\frac{{\rm d}\sigma^{\rm LO}_{t,b}/{\rm d}m_{4l}}{{\rm d}\sigma^{\rm LO}_{{\rm no}-t}/{\rm d}m_{4l}}
\right|_{\mu_r=\mu_f=m_{4l}/2}.\label{offshell_interf_ggVV_NLO:rtilde}
\end{equation}
For the $WW$ case, in the calculation~\cite{Caola:2015rqy} both the top and
the bottom quark contributions are omitted. At LO, top/bottom
contributions account for $\mathcal O(10\%)$ of the total $gg\to WW$
cross section.

\subsubsection{Finite top quark effects}
The effect of finite top quark mass in $gg\to ZZ$ at NLO was
investigated in~\cite{Melnikov:2015laa}. Similar to what is done in
the Higgs case, the authors performed the computation as an expansion
in the $\mt\to\infty$ limit.  The first two non trivial terms in the
expansion were kept, which allowed for a reliable description of the
top quark contribution up to invariant masses of order $m_{4l}\sim 300~{\rm GeV}$.
In this computation, only the total $gg\to ZZ$
cross-section was considered, although this should be enough to have a
rough estimate of the size of the mass effects. The result on the NLO
corrections, compared to the signal case, are shown in
\refF{offshell_interf_ggVV_NLO:mass}. For these results, the Higgs boson signal is computed in
the $\mt\to\infty$ limit as well. Also, compared to the $K$-factor
defined in~\cite{Melnikov:2015laa}, here we used NLO PDFs and $\alpha_s$
evolution for both the LO and the NLO contributions. The band represent
scale variation uncertainty, obtained from a factor of two variation around
$\mu_0=m_{4l}/2$.
%
\begin{figure}
\centering
\includegraphics[width=0.45\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_NLO_figs/k_factor_mass_zz.pdf}
\includegraphics[width=0.45\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_NLO_figs/k_factor_mass_h.pdf}
\caption{$K-$factors for signal and background, in the heavy top expansion.
Both LO and NLO contributions are computed with NLO PDFs and $\alpha_s$. See text for details.}\label{offshell_interf_ggVV_NLO:mass}
\end{figure}

Close to the $ZZ$ threshold, the background $1/\mt$ expansion is
expected to be accurate within $\mathcal O(20\%)$~\cite{Melnikov:2015laa}.
Signal and $K-$factors are of the same order of magnitude, in
agreement with what expected from soft gluon
approximations~\cite{Bonvini:2013jha}. Below the top threshold, the
precision on the approximation~\cite{Melnikov:2015laa} can be
systematically improved by computing more terms in the $1/\mt$ expansion.
Above the top threshold $m_{4l}\sim 300~{\rm GeV}$, the
expansion~\cite{Melnikov:2015laa} alone is no longer reliable. Since
the full computation is not available, the expansion could be improved
along two directions. In principle, it could be matched against the
exact high energy behaviour. While this does not pose any conceptual
challenge, the computation of the high energy limit is technically
more involved than in the Higgs case and it is presently unknown. A
second option would be to rescale by the exact LO and hence consider
and expansion for the $K-$factor, for which the $1/\mt$ expansion
should be better behaved.

\subsection{Results and recommendation for the \texorpdfstring{$gg\ (\to H)\to ZZ$}{gg (to H) to ZZ} interference \texorpdfstring{$K$}{K}-factor
%\footnote{While this report was finalized, \Brefs{Campbell:2016ivq,Caola:2016trd} appeared.  The results for the NLO corrections to the signal-background interference presented there support the approach advocated in this section.} -> to bottom of this section
}
As explained in the previous section, exact predictions valid up to
high $\sim 1~$TeV invariant masses are only known at NLO for the
$gg\to H \to 4l$ signal and LO for the $gg\to 4l$ background. However,
several indications point towards sizeable higher order corrections,
both for signal and background. In this section we study this issue
and present a possible practical recommendation for the
signal, background and interference $K-$factors.

We start by describing the setup used for the results presented in
this section. LO and NLO results are both obtained with NLO PDFs and
$\alpha_s$. In principle, one could envision using LO PDFs (and
$\alpha_s$) for the LO results, and this would in general lead to
smaller corrections, with reduced shape dependence. However, since
PDFs fits are still dominated by DIS data, the LO gluon distribution
is almost entirely determined by DGLAP evolution. The large LO gluon
flux hence is artificially driven by the large NLO DIS $K-$factor and
it is not reliable. Until LO gluon PDFs are obtained by fitting
hadronic data, using the NLO gluon distribution is preferable, see the
PDFs section of this report for more details.
%
NNLO PDFs could be used as well, since the $gg\to 4l$ process
enters at NNLO in the $q\bar q \to 4l$ computation. However, here we are
mostly interested in interference effects, so for consistency with 
the Higgs case we use NLO PDFs for NLO signal, 
$gg\to 4l$ background and interference.
%

Regarding the scale choice, it is well known that for Higgs boson production
an optimal choice would be $\mu\sim
\mh/2$~\cite{Anastasiou:2002yz}. Theoretically, it is justified both
by all-order analysis of the $Hgg$ form factor and by the fact that
the average $p_\perp$ of the Higgs boson is $\sim \mh/2$.
Empirically, a much better convergence is observed with this scale
choice, as well as a reduced impact of resummation
effects~\cite{Anastasiou:2016cez}. For off-shell studies, this
translates into choosing as a central scale half of the virtuality of
the Higgs boson, i.e. $\mu = m_{4l}/2$. Since most of the above
consideration are only based on the colour flow of the process, they
also apply for the background and interference scale
choice. Incidentally, we note that this was also the preferred choice
for the NNLO $pp\to WW/ZZ$ computations~\cite{Gehrmann:2014fva,Cascioli:2014yka}.
%

In the region $m_{4l} < 2 m_t$, precise results exist for both the
signal and the background.  In more detail, NNLO results for the signal
can be obtained from~\cite{Harlander:2009my,Pak:2009dg}. For the background, NLO
contributions from massless quarks can be obtained
using~\cite{Caola:2015psa}\footnote{A numerical code for background
  predictions should be made public soon.} while
top quark contributions can be obtained from~\cite{Melnikov:2015laa}.
%
In principle, these results could be used to obtain a NLO prediction
for the interference. However, this calculation has not been performed
yet.
Given the similarity of signal and background $K-$factors, until
a better computation is available the interference $K-$ factor can be
obtained as the geometric average of the signal and background $K-$
factors. Scale variation uncertainties should account for missing
higher order in the perturbative expansion. Alternatively, we note
that even with our scale choice the signal still exhibits a non
negligible NNLO $K-$factor, and it is not unreasonable to expect a
similar $K-$factor also for the background~\cite{Bonvini:2013jha}.
One may then apply the signal NNLO $K-$factor to the background as
well, and take the difference between NNLO and NLO as a conservative
estimate of perturbative uncertainties.

In the high invariant mass region $m_{4l}>2 m_t$, it is not possible
at this stage to provide a full NNLO (NLO) theoretical prediction for the
signal (background), since exact heavy quark mass effects at NLO are
unknown. In the following, we investigate signal and background
$K-$factors in this region making different assumptions for missing
top quark contributions.
%
First, we compare in \refF{offshell_interf_ggVV_NLO:kfactor}
results for signal -- with full top and bottom mass dependence through NLO --
 and background neglecting top quark contributions, as described in the
previous sections and in~\cite{Caola:2015psa}. For
reference, we also show the effect of NNLO QCD corrections (computed
with NNLO PDFs and $\alpha_s$, and in the heavy-top approximation).
%
\begin{figure}
\centering
\includegraphics[width=0.45\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_NLO_figs/plot_m4l_13_RR.pdf}
\includegraphics[width=0.45\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_NLO_figs/plot_m4l_13_h.pdf}
\caption{Left: NLO $K$-factor for $gg\to 4l$ background, massless quark contribution.
Right: $K$-factor for $gg\to H \to 4l$ signal. NLO with full mass dependence, NNLO in the HEFT
approximation. See text for details.}\label{offshell_interf_ggVV_NLO:kfactor}
\end{figure}
%
This figure shows that signal and background $K$-factors are
similar throughout the whole invariant mass spectrum considered here.
%

To quantify the effect of the missing top quark contribution in the
background, we study two extreme approaches. First, we assume that the
$K-$factor for massive and massless contributions is identical. Given
their similarity in the low-mass region, we believe this assumptions
to be reasonable. This leads to the $K$-factor shown in
\refF{offshell_interf_ggVV_NLO:kfactor} (see also
Eq.~\ref{offshell_interf_ggVV_NLO:rtilde})
\begin{equation}
K_{gg\to 4l} = \frac{{\rm d}\sigma^{\rm LO}_{t,b}/{\rm d}m_{4l}+\mathcal{R}^{\rm LO}_{t,b}{\rm d}\Delta\sigma^{\rm NLO}_{{\rm no}-t}/{\rm d}m_{4l}}{{\rm d}\sigma^{\rm LO}_{t,b}/{\rm d}m_{4l}} = \frac{{\rm d}\sigma^{\rm LO}_{{\rm no}-t}/{\rm d}m_{4l}+{\rm d}\Delta\sigma^{\rm NLO}_{{\rm no}-t}/{\rm d}m_{4l}}{{\rm d}\sigma^{\rm LO}_{{\rm no}-t}/{\rm d}m_{4l}} = K_{gg\to 4l}^{{\rm no}-t}.\label{offshell_interf_ggVV_NLO:kfactor_k}
\end{equation}
Second, we use full mass dependence in the LO contribution and only
add NLO corrections for massless quarks\footnote{Note that this second
  approach is rather unrealistic, as it assumes no interference
  between LO massive amplitudes and NLO massless ones. We consider it here
  only as a way to estimate possible top quark effects in a conservative way.}


\begin{equation}
\tilde K_{gg\to 4l} = \frac{{\rm d}\sigma^{\rm LO}_{t,b}/{\rm d}m_{4l}+{\rm d}\Delta\sigma^{\rm NLO}_{{\rm no}-t}/{\rm d}m_{4l}}{{\rm d}\sigma^{\rm LO}_{t,b}/{\rm d}m_{4l}}.\label{offshell_interf_ggVV_NLO:kfactor_kt}
\end{equation}
A comparison between $K$ Eq.~\ref{offshell_interf_ggVV_NLO:kfactor_k}
and $\tilde K$ Eq.~\ref{offshell_interf_ggVV_NLO:kfactor_kt} is shown
in \refF{offshell_interf_ggVV_NLO:comparison}. Up to invariant masses
$m_{4l}\sim500~$GeV the two results are in good agreement, while they differ
significantly at higher mass. The spread of these two results is a way
to probe the uncertainty due to unknown mass effects.

\begin{figure}
\centering
\includegraphics[width=0.45\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_NLO_figs/plot_m4l_13_massK.pdf}
\includegraphics[width=0.45\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_NLO_figs/plot_m4l_13_massKT.pdf}
\caption{Comparison of different ways of treating quark mass effects
  at higher orders. Left: assume identical correction to massive and
  massless contributions.  Right: assume zero corrections for massive
  contributions. See text for
  details.}\label{offshell_interf_ggVV_NLO:comparison}
\end{figure}

Summarizing, for background predictions in the high invariant mass
region we suggest to use exact LO multiplied by the massless
$K$-factor Eq.~\ref{offshell_interf_ggVV_NLO:kfactor_k}. The spread
shown in \refF{offshell_interf_ggVV_NLO:comparison} may be used as
a way to estimate the uncertainty of this procedure until a better
computation becomes available. As for low invariant mass region, the
interference $K$-factor is then determined as geometric mean of signal
and background $K$-factors.
%
Alternatively, given the similarity of signal and background
$K-$factors and the size of uncertainties a simpler alternative -- until
more precise theoretical predictions are available -- would be to
assume the same $K$-factor for signal and background, and assign to it
a systematic uncertainty which covers the effects described
above. Note that both these approaches lead to a smooth interference
$K-$factor over the whole $m_{4l}$ spectrum, with an uncertainty
increasing at large invariant masses to reflect the effect of unknown
top quark mass effects.
%
While this report was finalized, \Brefs{Campbell:2016ivq,Caola:2016trd} appeared.  The results for the NLO corrections to the signal-background interference presented there support the approach advocated in this section. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\clearpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
