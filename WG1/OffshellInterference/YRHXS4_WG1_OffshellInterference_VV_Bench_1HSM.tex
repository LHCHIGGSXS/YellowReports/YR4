\subsection{Off-shell and interference benchmarks: 1-Higgs Singlet Model}

\label{sec:offshell_interf_vv_bench_1hsm}

\providecommand{\Ph}{\HepParticle{h}{}{}\Xspace}
\providecommand{\Pho}{\Ph_1}
\providecommand{\Pht}{\Ph_2}
\providecommand{\Mho}{\mathswitch {M_{\Ph 1}}}
\providecommand{\Mht}{\mathswitch {M_{\Ph 2}}}

The simplest extension of the SM Higgs sector is given by the addition
of a singlet field which is neutral under the SM gauge groups.  
We adopt the definition of the 1-Higgs Singlet Model (1HSM), a.k.a.\ EW Singlet Model,
which is given in Section 13.3 of \Bref{Heinemeyer:2013tqa}.
Here, interference benchmark cross sections and distributions in 
the 1HSM are presented.  We employ basis (335) of 
\Bref{Heinemeyer:2013tqa} and specify four benchmark points:
\begin{enumerate}
\item $\Mht = 400$\UGeV, $\sin\theta = 0.2$,
\item $\Mht = 600$\UGeV, $\sin\theta = 0.2$,
\item $\Mht = 600$\UGeV, $\sin\theta = 0.4$,
\item $\Mht = 900$\UGeV, $\sin\theta = 0.2$,
\end{enumerate}
where $\Mho = 125$\UGeV\ and $\mu_1 = \lambda_2 = \lambda_1 = 0$ for all 
points.  The corresponding Higgs boson widths are given in \refT{tab:ofs:widths}.  
They have been calculated using \textsc{FeynRules} \cite{Alloul:2013bka}.
%
\begin{table}%[Htbp]
\caption{\label{tab:ofs:widths}Widths of the physical Higgs bosons $\Pho$ and $\Pht$ in the 1-Higgs-Singlet Extension of the SM with mixing angles $\sin\theta=0.2$ and $\sin\theta=0.4$ as well as $\mu_1= \lambda_1=\lambda_2=0$.}
\renewcommand{\arraystretch}{1.2}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
\begin{tabular}{c|c|c|ccc}
\toprule
\multicolumn{2}{c}{} & $\Pho$ & \multicolumn{3}{c}{$\Pht$}  \\ 
\cmidrule(lr){3-6}
$\sin\theta$ & $M$ [\UGeV] & 125 & 400 & 600 & 900 \\ 
\midrule
$0.2$ & $\Gamma$ [\UGeV] & $4.34901{\cdot} 10^{-3}$ & 1.52206 & 5.95419 & 19.8529 \\ 
$0.4$ &  $\Gamma$ [\UGeV] & $3.80539{\cdot} 10^{-3}$ & & 22.5016 	&  	\\ 
\bottomrule
\end{tabular}
\end{center}
\end{table}


Gluon-fusion 1HSM benchmark results were computed with \textsc{gg2VV} \cite{Kauer:2012hd} 
(see also \Bref{Kauer:2015hia}).  More specifically,
cross sections for $\Pg\Pg\ (\to \{\Pho,\Pht\}) \to \PZ(\PGg^\ast)\PZ(\PGg^\ast) \to 
\Pl\PAl\Pl'\PAl'$ for the 13\UTeV\ LHC are given in \refTs{tab:ofs:sigplusint} and 
\ref{tab:ofs:allcontribs}.  The corresponding distributions are shown in 
\refFs{fig:ofs:sigplusint} and \ref{fig:ofs:allcontribs}, respectively.
Results for the heavy Higgs boson signal and its interference with the 
light Higgs and continuum background and the combined interference are given in 
\refT{tab:ofs:sigplusint} and \refF{fig:ofs:sigplusint}. In \refT{tab:ofs:sigplusint}, 
the ratio $R_i = (S+I_i)/S$ is used to illustrate the 
relative change of the heavy Higgs boson signal due to interference with the 
light Higgs and continuum background amplitude contributions.
Heavy-Higgs-light-Higgs interference effects and the coherent sum of all interfering 
contributions is shown in \refT{tab:ofs:allcontribs} and \refF{fig:ofs:allcontribs}.

Vector-boson-fusion 1HSM benchmark results were computed with 
\textsc{PHANTOM} \cite{Ballestrero:2007xq} (see also \Brefs{Maina:2015ela,Ballestrero:2015jca})
and 
\textsc{VBFNLO} \cite{Arnold:2008rz,Baglio:2014uba} (see also \Brefs{Jager:2006zc,Jager:2006cp,Bozzi:2007ur,Jager:2009xx,Feigl:2013naa}).
Good agreement was achieved for all fully-leptonic Higgs boson decay modes.
Cross sections for $\Pq\Pq'(\to \Pq\Pq'\{\Pho, \Pht\}) \to \Pq\Pq'\,\PZ(\PGg^\ast)\PZ(\PGg^\ast) \to \Pq\Pq'\,\Pl \PAl  \Pl' \PAl'$ 
and 
$\Pq\Pq'(\to \Pq\Pq'\{\Pho, \Pht\}) \to \Pq\Pq'\,\PW\PW \to \Pq\Pq'\,\Pl \PAGnl  \PAl'\PGn_{\Pl'}$ 
for the 13\UTeV\ LHC are given in 
\refTs{tab:ofs:singlet_vbf_ZZ_tight} and \ref{tab:ofs:singlet_vbf_ZZ_loose}
and \refTs{tab:ofs:singlet_vbf_WW_tight} and \ref{tab:ofs:singlet_vbf_WW_loose} for tight and loose VBF cuts (see \refS{sec:offshell_interf_vv_bench_sm}), respectively.  More specifically, the sum of the light and heavy Higgs contributions including light-heavy interference, the interfering background without Higgs contributions and the sum of the Higgs boson signal and its interference with 
the background are given.
VBF Higgs boson invariant mass distributions in the 1HSM are shown in \refF{fig:ofs:singlet_vbf}.

The full set of 1HSM benchmark cross sections and distributions is available
at \url{https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWGOFFSHELL}.

\begin{table}%[Htbp]
\caption{\label{tab:ofs:sigplusint}
Cross sections (\UfbZ) for $\Pg\Pg\ (\to \{\Pho,\Pht\}) \to \PZ(\PGg^\ast)\PZ(\PGg^\ast) \to \Pl\PAl\Pl'\PAl'$ in $\Pp\Pp$ collisions at $\sqrt{s}=13\UTeV$\ at loop-induced leading order 
in the 1-Higgs-Singlet Extension of the SM (1HSM) with $\Mho = 125 \UGeV$, $\Mht = 400, 600, 900\UGeV$ and mixing angle $\sin\theta=0.2$ or $0.4$ as indicated. 
Results for the heavy Higgs ($\Pht$) signal ($S$) and its interference with the 
light Higgs ($I_{\Ph 1}$) and the continuum background ($I_{bkg}$) and the full 
interference ($I_{full}$) are given.
The ratio $R_i = (S+I_i)/S$ illustrates the relative change of 
the heavy Higgs boson signal due to interference with 
the light Higgs and continuum background amplitude contributions.
Cross sections are given for a single lepton flavour combination.  
Minimal cuts are applied: $M_{\Pl\PAl} > 4\UGeV, M_{\Pl'\PAl'} > 4\UGeV, 
p_{\mathrm{T}\PZ} > 1\UGeV$.
The integration error is displayed in brackets.
} 
\renewcommand{\arraystretch}{1.3}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
\footnotesize
\begin{tabular}{ccccccccc}
%\cline{1-3}
%\multicolumn{3}{|c|}{$\Pg\Pg \to \Pht \to \PZ\PZ \to \Pl \PAl  \Pl' \PAl' $} & \multicolumn{3}{|c}{} \\ 
%  \multicolumn{3}{|c|}{$\sigma[\UfbZ]$, $\Pp\Pp$, $\sqrt{s}=13$ \UTeV} & \multicolumn{3}{|c}{} \\ 
%\cline{4-9}
\toprule
\multicolumn{3}{c}{} & \multicolumn{3}{c|}{interference} & \multicolumn{3}{c}{ratio} \\ 
\cmidrule(lr){4-9}
 $\sin\theta$ & $ M_{\Ph2}$  [\UGeV]  & $S (\Pht)$& $I_{\Ph1}$  & $I_{bkg}$ & $I_{full}$  &$R_{\Ph1}$ & $R_{bkg}$ & $R_{full}$ \\ 
 \midrule
0.2 & 400	& 0.07412(6)		& 0.00682(6)		& -0.00171(2)		& 0.00511(6)		& 1.092(2)		& 0.977(1)		& 1.069(2)		\\ 
0.2 & 600	& 0.01710(2)		& -0.00369(3) 		& 0.00384(3) 		& 0.00015(4) 		& 0.784(2)		& 1.225(2)		& 1.009(3)		\\ 
0.2 & 900	& 0.002219(2)		& -0.003369(9)		& 0.003058(8) 		& -0.00031(2)		& -0.518(4) 	& 2.378(4)		& 0.860(6)			\\ 
0.4 & 600	& 0.07065(6)		& -0.01191(6)		& 0.01465(6)		& -0.00274(9)		& 0.831(2)		& 1.207(2)		& 1.039(2)		\\ 
\bottomrule
\end{tabular}
\end{center}
\end{table}

\begin{figure}%[Htbp]
\includegraphics[trim=3cm 0cm 0cm 0cm, clip, width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/ZZ600theta02plot1b}\hfil
\includegraphics[trim=3cm 0cm 0cm 0cm, clip,width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/ZZ600theta04plot1b}
\caption{\label{fig:ofs:sigplusint}Invariant mass distributions for $ \Pg \Pg\ (\to \{\Pho, \Pht\}) \to \PZ(\PGg^\ast)\PZ(\PGg^\ast) \to \Pl \PAl  \Pl' \PAl'$, other details as in \refT{tab:ofs:sigplusint}. }
\end{figure}

\begin{table}%[Htbp]
\caption{\label{tab:ofs:allcontribs}
Cross sections (\UfbZ) for $\Pg \Pg\ (\to \{\Pho,\Pht\}) \to \PZ(\PGg^\ast)\PZ(\PGg^\ast) \to \Pl \PAl  \Pl' \PAl'$ in $\Pp\Pp$ collisions at $\sqrt{s}=13$ \UTeV\ in the 1HSM with $\Mho = 125$ \UGeV, $\Mht= 400, 600, 900$ \UGeV\ and mixing angle $\sin\theta=0.2$ or $0.4$ as indicated.
Results for the heavy Higgs ($\Pht$) signal ($S$), light Higgs background ($\Pho$) and continuum background (gg bkg.) are given.  Where more than one contribution is 
included, all interferences are taken into account.
Other details as in \refT{tab:ofs:sigplusint}.
} 
\renewcommand{\arraystretch}{1.2}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
\begin{tabular}{ccccccc}
%\cline{1-3}
%\multicolumn{3}{|c|}{$ \Pg\Pg \to \Pht \to \PZ\PZ \to \Pl \PAl  \Pl' \PAl' $} & \multicolumn{4}{|c}{} \\ 
%  \multicolumn{3}{|c|}{$\sigma[\UfbZ]$, $\Pp\Pp$, $\sqrt{s}=13$ \UTeV} & \multicolumn{4}{|c}{} \\ 
%%\cline{4-9}
%  \multicolumn{3}{|c|}{min.\ cuts} & \multicolumn{3}{|c}{}\\
%%\multicolumn{3}{c|}{interference} & \multicolumn{3}{c|}{ratio} \\ 
\toprule
 $\sin\theta$ & $ M_{\Ph2}$  [\UGeV]  & $S (\Pht)$ & $ \Pho$  & gg bkg. & $S$ + $\Pho$ + $I_{\Ph1}$  & all  \\ 
 \midrule
0.2 & 400	& 0.07412(6)		& 0.854(2)		& 21.18(7)	 	& 0.934(2)		& 21.86(7)		\\
0.2 & 600	& 0.01710(2)		& 0.854(2)		& 21.18(7)		& 0.867(2) 	& 21.80(7)		\\
0.2 & 900	& 0.002219(2)		& 0.854(2)		& 21.18(7)		& 0.852(2)		& 21.79(7)		\\
0.4 & 600	& 0.07065(6)		& 0.734(2)		& 21.18(7)		& 0.793(2)		& 21.77(7)		\\
\bottomrule
\end{tabular}
\end{center}
\end{table}

\begin{figure}%[Htbp]
\includegraphics[trim=3cm 0cm 0cm 0cm, clip, width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/ZZ600theta02plot2}\hfil
\includegraphics[trim=3cm 0cm 0cm 0cm, clip,width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/ZZ600theta04plot2}
\caption{\label{fig:ofs:allcontribs} Invariant mass distributions for $ \Pg \Pg\ (\to \{\Pho, \Pht\}) \to \PZ(\PGg^\ast)\PZ(\PGg^\ast) \to \Pl \PAl  \Pl' \PAl'$, other details as in \refT{tab:ofs:allcontribs}.}
\end{figure}

\begin{table}%[Htbp]
\caption{\label{tab:ofs:singlet_vbf_ZZ_tight}
Cross sections for $\Pq\Pq'(\to \Pq\Pq'\{\Pho, \Pht\}) \to \Pq\Pq'\,\PZ(\PGg^\ast)\PZ(\PGg^\ast) \to \Pq\Pq'\,\Pl \PAl  \Pl' \PAl'$ in $\Pp\Pp$ collisions at $\sqrt{s}=13\UTeV$ in the 1-Higgs-Singlet Extension of the SM (1HSM). Tight VBF cuts (see \refS{sec:offshell_interf_vv_bench_sm}) are applied.  Results are given for the first, second, third and fourth 1HSM benchmark points with $\Mho = 125$\UGeV,\ $\mu_1 = \lambda_2 = \lambda_1 = 0$ and $(\Mht[\UGeVZ],\sin\theta)=(400,0.2),(600,0.2),(600,0.4),(900,0.2)$, respectively.  The sum of the light and heavy Higgs contributions including light-heavy interference (S), the interfering background without Higgs contributions (B) and the sum of the Higgs boson signal and its interference with 
the background (S+I) are given.
Cross sections are given at leading order and for a single lepton flavour combination.  
The integration error is displayed in brackets.
} 
\renewcommand{\arraystretch}{1.2}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
\begin{tabular}{c c c c c}
\toprule
$\sigma[\UfbZ]$ & 1HSM point & $M_{\PZ\PZ}>140\UGeV$ &   $220\UGeV<M_{\PZ\PZ}< 300\UGeV$ & $M_{\PZ\PZ}> 300\UGeV$ \\ 
\midrule
 S & 1 & $1.686(2){\cdot}10^{-2}$    &  $1.185(4){\cdot}10^{-3} $   &  $1.514(2){\cdot}10^{-2}$ \\  
S+I & 1 & $- 9.69(3){\cdot}10^{-3}$   & $- 1.85(2){\cdot}10^{-3}$    & $- 6.90(2){\cdot}10^{-3}$ \\  
B & 1  & $6.725(2){\cdot}10^{-2} $   & $1.750(1){\cdot}10^{-2}$     & $4.148 (2){\cdot}10^{-2}$ \\ 
\midrule
 S & 2 & $1.436(1){\cdot}10^{-2}$    &  $1.232(4){\cdot}10^{-3} $   &  $1.259(1){\cdot}10^{-2}$ \\  
S+I & 2 & $- 1.180(3){\cdot}10^{-2}$  & $- 1.88(2){\cdot}10^{-3}$    & $- 9.00(2){\cdot}10^{-3}$ \\  
B & 2 & $6.725(2){\cdot}10^{-2} $   & $1.750(1){\cdot}10^{-2}$     & $4.148 (2){\cdot}10^{-2}$ \\ 
\midrule
 S & 3 & $2.025(2){\cdot}10^{-2}$    &  $8.90(4){\cdot}10^{-4} $    &  $1.895(2){\cdot}10^{-2}$ \\  
S+I & 3 & $- 4.34(3){\cdot}10^{-3}$  & $- 1.74(2){\cdot}10^{-3}$    & $- 1.72(3){\cdot}10^{-3}$ \\  
B & 3 & $6.725(2){\cdot}10^{-2} $   & $1.750(1){\cdot}10^{-2}$     & $4.148 (2){\cdot}10^{-2}$ \\ 
\midrule
 S & 4 & $1.263(1){\cdot}10^{-2}$    &  $1.238(4){\cdot}10^{-3} $   &  $1.085(1){\cdot}10^{-2}$ \\  
S+I & 4 & $- 1.309(3){\cdot}10^{-2}$  & $- 1.86(2){\cdot}10^{-3}$    & $- 1.029(2){\cdot}10^{-2}$ \\  
B & 4 & $6.725(2){\cdot}10^{-2} $   & $1.750(1){\cdot}10^{-2}$     & $4.148 (2){\cdot}10^{-2}$ \\ 
\bottomrule
\end{tabular}
\end{center}
\end{table}

\begin{table}%[Htbp]
\caption{\label{tab:ofs:singlet_vbf_WW_tight}
Cross sections for $\Pq\Pq'(\to \Pq\Pq'\{\Pho, \Pht\}) \to \Pq\Pq'\,\PW\PW \to \Pq\Pq'\,\Pl \PAGnl  \PAl'\PGn_{\Pl'}$ in $\Pp\Pp$ collisions at $\sqrt{s}=13\UTeV$ in the 1-Higgs-Singlet Extension of the SM. Tight VBF cuts are applied.  
Cross sections are given for a single lepton flavour combination, but taking
into account both charge assignments, e.g.\ $(\Pl,\Pl')=(\Pe,\PGm)$ or $(\PGm,\Pe)$.
Other details as in \refT{tab:ofs:singlet_vbf_ZZ_tight}.
} 
\renewcommand{\arraystretch}{1.2}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
\begin{tabular}{c c c c c}
\toprule
$\sigma[\UfbZ]$ & 1HSM point & $M_{\PW\PW}>140\UGeV$ &   $220\UGeV<M_{\PW\PW}< 300\UGeV$ & $M_{\PW\PW}> 300\UGeV$ \\ 
\midrule
 S & 1 & $3.283(3){\cdot}10^{-1}$  &  $2.68(1){\cdot}10^{-2} $    &  $2.758(3){\cdot}10^{-1}$ \\  
S+I & 1 & $- 1.98(2){\cdot}10^{-1}$ & $- 4.9(1){\cdot}10^{-2}$     & $- 9.8(1){\cdot}10^{-2}  $ \\  
B & 1 & $3.382(2) $           & $8.63(1){\cdot}10^{-1}$      & $1.854(1)$ \\ 
\midrule
 S & 2 & $2.727(3){\cdot}10^{-1}$  &  $2.80(1){\cdot}10^{-2} $    &  $2.189(2){\cdot}10^{-1}$ \\  
S+I & 2 & $- 2.48(2){\cdot}10^{-1}$ & $- 4.9(1){\cdot}10^{-2}$     & $- 1.48(1){\cdot}10^{-1}  $ \\  
B & 2 & $3.382(2) $            & $0.863(1)$                & $1.854(1)$ \\ 
\midrule
 S & 3 & $3.937(4){\cdot}10^{-1}$  &  $2.01(1){\cdot}10^{-2} $    &  $3.541(4){\cdot}10^{-1}$ \\  
S+I & 3 & $- 8.4(2){\cdot}10^{-2}$ & $- 4.6(1){\cdot}10^{-2}$      & $ 9(1){\cdot}10^{-3}  $ \\  
B & 3 & $3.382(2) $           &  $0.863(1)$               & $1.854(1)$ \\ 
\midrule
 S & 4 & $2.377(2){\cdot}10^{-1}$  &  $2.81(1){\cdot}10^{-2} $    &  $1.836(2){\cdot}10^{-1}$ \\  
S+I & 4 & $- 2.75(1){\cdot}10^{-1}$ & $- 4.88(1){\cdot}10^{-2}$    & $- 1.74(1){\cdot}10^{-1}  $ \\  
B & 4 & $3.382(2) $            & $0.863(1)$               & $1.854(1)$ \\ 
\bottomrule
\end{tabular}
\end{center}
\end{table}

\begin{table}%[Htbp]
\caption{\label{tab:ofs:singlet_vbf_ZZ_loose}
Cross sections for $\Pq\Pq'(\to \Pq\Pq'\{\Pho, \Pht\}) \to \Pq\Pq'\,\PZ(\PGg^\ast)\PZ(\PGg^\ast) \to \Pq\Pq'\,\Pl \PAl  \Pl' \PAl'$ in $\Pp\Pp$ collisions at $\sqrt{s}=13\UTeV$ in the 1-Higgs-Singlet Extension of the SM. Loose VBF cuts are applied.  Other details as in \refT{tab:ofs:singlet_vbf_ZZ_tight}.
} 
\renewcommand{\arraystretch}{1.2}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
\begin{tabular}{c c c c c}
\toprule
$\sigma[\UfbZ]$ & 1HSM point & $M_{\PZ\PZ}>140\UGeV$ &   $220\UGeV<M_{\PZ\PZ}< 300\UGeV$ & $M_{\PZ\PZ}> 300\UGeV$ \\ 
\midrule
S & 1 & $2.272(2){\cdot}10^{-2}$   &  $1.94(1){\cdot}10^{-3}$       & $1.983(2){\cdot}10^{-2}$ \\ 
 S+I & 1 & $- 1.34(1){\cdot}10^{-2}$  &  $- 3.33(4){\cdot}10^{-3} $    &  $- 8.17(5) {\cdot}10^{-3}$ \\  
B & 1 & $1.3950(5){\cdot}10^{-1} $ & $4.005(3){\cdot}10^{-2}$       & $7.964(4){\cdot}10^{-2}$ \\ 
\midrule
S & 2 & $1.889(2){\cdot}10^{-2}$   &  $2.00(1){\cdot}10^{-3}$       & $1.592(2){\cdot}10^{-2}$  \\ 
 S+I & 2 & $- 1.68(1){\cdot}10^{-2}$  &  $- 3.40(4){\cdot}10^{-3} $    &  $- 1.154(5) {\cdot}10^{-2}$ \\  
B & 2 & $1.3950(5){\cdot}10^{-1} $ & $4.005(3){\cdot}10^{-2}$       & $7.964(4){\cdot}10^{-2}$ \\ 
\midrule
S & 3 & $2.590(3){\cdot}10^{-2}$   &  $1.45(1){\cdot}10^{-3}$       & $2.372(2){\cdot}10^{-2}$ \\ 
 S+I & 3 & $- 6.9(1){\cdot}10^{-3}$  &  $- 3.12(4){\cdot}10^{-3} $    &  $- 2.1(1) {\cdot}10^{-3}$ \\  
B & 3 & $1.3950(5){\cdot}10^{-1} $ & $4.005(3){\cdot}10^{-2}$       & $7.964(4){\cdot}10^{-2}$ \\ 
\midrule
S & 4 & $1.658(2){\cdot}10^{-2}$   &  $2.02(1){\cdot}10^{-3}$       & $1.359(2){\cdot}10^{-2}$ \\ 
 S+I & 4 & $- 1.85(1){\cdot}10^{-2}$  &  $- 3.36(4){\cdot}10^{-3} $    &  $- 1.329(5) {\cdot}10^{-2}$ \\ 
B & 4 & $1.3950(5){\cdot}10^{-1} $ & $4.005(3){\cdot}10^{-2}$       & $7.964(4){\cdot}10^{-2}$ \\ 
\bottomrule
\end{tabular}
\end{center}
\end{table}

\begin{table}%[Htbp]
\caption{\label{tab:ofs:singlet_vbf_WW_loose}
Cross sections for $\Pq\Pq'(\to \Pq\Pq'\{\Pho, \Pht\}) \to \Pq\Pq'\,\PW\PW \to \Pq\Pq'\,\Pl \PAGnl  \PAl'\PGn_{\Pl'}$ in $\Pp\Pp$ collisions at $\sqrt{s}=13\UTeV$ in the 1-Higgs-Singlet Extension of the SM. Loose VBF cuts are applied.  Other details as in \refT{tab:ofs:singlet_vbf_WW_tight}.
} 
\renewcommand{\arraystretch}{1.2}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
\begin{tabular}{c c c c c}
\toprule
$\sigma[\UfbZ]$ & 1HSM point & $M_{\PW\PW}>140\UGeV$ &   $220\UGeV<M_{\PW\PW}< 300\UGeV$ & $M_{\PW\PW}> 300\UGeV$ \\ 
\midrule
 S & 1 & $4.600(5){\cdot}10^{-1}$      &  $4.46(1){\cdot}10^{-2} $   &  $3.692(4){\cdot}10^{-1}$ \\  
S+I & 1 & $- 3.25(4){\cdot}10^{-1}$     & $- 9.3(2){\cdot}10^{-2}$    & $- 1.23(3){\cdot}10^{-1}$ \\  
B & 1 & $7.424(3) $               & $2.001(1)$               & $3.815(2)$ \\ 
\midrule
 S & 2 & $3.733(3){\cdot}10^{-1}$      &  $4.59(1){\cdot}10^{-2} $   &  $2.805(3){\cdot}10^{-1}$ \\  
S+I & 2 & $- 4.05(4){\cdot}10^{-1}$     & $- 9.2(2){\cdot}10^{-2}$    & $- 2.00(3){\cdot}10^{-1}$ \\  
B & 2 & $7.424(3) $               & $2.001(1)$               & $3.815(2)$ \\ 
\midrule
 S & 3 & $5.17(1){\cdot}10^{-1}$      &  $3.33(1){\cdot}10^{-2} $    &  $4.482(5){\cdot}10^{-1}$ \\  
S+I & 3 & $- 1.88(4){\cdot}10^{-1}$     & $- 8.5(2){\cdot}10^{-2}$    & $+ 1(3){\cdot}10^{-3}$ \\  
B & 3 & $7.424(3) $               & $2.001(1)$               & $3.815(2)$ \\ 
\midrule
 S & 4 & $3.274(3){\cdot}10^{-1}$      &  $4.65(1){\cdot}10^{-2} $   &  $2.339(3){\cdot}10^{-1}$ \\  
S+I & 4 & $- 4.43(4){\cdot}10^{-1}$     & $- 9.6(2){\cdot}10^{-2}$    & $- 2.38(3){\cdot}10^{-1}$ \\  
B & 4 & $7.424(3) $               & $2.001(1)$               & $3.815(2)$ \\ 
\bottomrule
\end{tabular}
\end{center}
\end{table}

\begin{figure}%[Htbp]
\includegraphics[clip, width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/loose_eemumu8_singl2_off.pdf}\hfil
 \includegraphics[clip,width=.49\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_VV_figs/tight_eemumu8_singl2_off.pdf}
\caption{\label{fig:ofs:singlet_vbf} Invariant mass distributions for 
$\Pq\Pq'(\to \Pq\Pq'\{\Pho, \Pht\}) \to \Pq\Pq'\,\PZ(\PGg^\ast)\PZ(\PGg^\ast) \to \Pq\Pq'\,\Pl \PAl  \Pl' \PAl'$ in $\Pp\Pp$ collisions at $\sqrt{s}=13\UTeV$. Loose and tight VBF cuts are applied in the left and right graphs, respectively.  Results for the second 1HSM benchmark point ($\Mho = 125 \UGeV$, $\Mht = 600\UGeV$, $\sin\theta=0.2$) are shown: the sum of the light and heavy Higgs contributions including light-heavy interference (Signal), the interfering background without Higgs contributions (Bkg), the sum of Signal and Bkg including interference (Tot), and the 
negative of the sum of Signal and its interference with Bkg (-(S+I)).
Other details as in \refT{tab:ofs:singlet_vbf_ZZ_tight}.}
\end{figure}

%\clearpage
