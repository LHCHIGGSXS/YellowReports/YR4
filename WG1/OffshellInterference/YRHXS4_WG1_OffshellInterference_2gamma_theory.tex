\subsection{Theory overview}
\label{sec:HggIntTheory}

The Higgs boson is dominantly produced by gluon fusion through a top quark loop.
Its decay to two photons, $H\to\gamma\gamma$, provides a very clean signature for probing Higgs boson properties, including its mass.
However, there is also a large continuum background to its detection in this channel.
It is important to study how much the coherent interference between the Higgs boson signal and the background could affect distributions in
diphoton observables, and possibly use it to constrain Higgs boson properties.

The interference of the resonant process $ij \to X+H ( \to \gamma \gamma )$ with the continuum QCD background $ij \to X+\gamma\gamma $
induced by quark loops can be expressed at the level of the partonic cross section as:

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{eqnarray}
\delta\hat{\sigma}_{ij\to X+ H\to \gamma\gamma} &=& 
-2 (\sh-m_H^2) \,\, { \text{Re} \left( \ca_{ij\to X+H} \ca_{H\to\gamma\gamma} 
                          \ca_{\rm cont}^* \right) 
        \over (\sh - m_H^2)^2 + m_H^2 \Gamma_H^2 }
\nonumber\\
&& 
-2 m_H \Gamma_H \,\, { \text{Im} \left( \ca_{ij\to X+H} \ca_{H\to\gamma\gamma} 
                          \ca_{\rm cont}^* \right)
        \over (\sh - m_H^2)^2 + m_H^2 \Gamma_H^2 } \,,
\label{intpartonic}
\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
where $m_H$ and $\Gamma_H$ are the Higgs boson mass and decay width, and  $\sh$ is the partonic invariant mass.
The interference is written in two parts, proportional to the real and imaginary parts of the Higgs Breit-Wigner propagator respectively,
to which will be referred to as the real and imaginary part of the interference from now on.

The real part interference is odd in $\sh$ around the Higgs boson mass peak, and thus its effect on the total $\gamma\gamma$ rate is subdominant as pointed out in ref.~\cite{Dixon:2003yb,Dicus:1987fk}.
The imaginary part of the interference, depending on the phase difference between the signal and background amplitudes, could significantly affect the total cross section.
However, for the gluon-gluon partonic subprocess, it was found that the loop-induced background continuum amplitude has a quark mass suppression in its imaginary part for the relevant helicity combinations,
making it dominantly real, therefore bearing the same phase as the Higgs boson production and decay amplitudes \cite{Dicus:1987fk}.
As a result, the contribution of the interference to the total cross section in the gluon fusion channel is highly suppressed at leading order (LO).
The main contribution of the interference to the total rate comes from the two-loop imaginary part of the continuum amplitude $gg \to \gamma\gamma$, and only amounts to around $3\%$ of the total signal rate~\cite{Dixon:2003yb}.

Later, in ref. \cite{Martin:2012xc} it was shown that even though the real part of the interference hardly contributes to the total cross section,
it has a quantifiable effect on the position of the diphoton invariant mass peak, producing a shift of $ {\cal O}(100\,\text{MeV})$ towards a lower mass region,
once the smearing effect of the detector was taken into account.
In ref. \cite{deFlorian:2013psa}, the $qg$ and $q\bar{q}$ channels of this process were studied, completing the full ${\cal O}(\alpha_{\mathrm{S}}^2)$
computation of the interference effects between the Higgs diphoton signal and the continuum background at the LHC.
Note that the extra $qg$ and $q\bar{q}$ channels involve one QCD emission in the final states, but the corresponding background amplitudes start at tree level,
and therefore the relevant interference is of the same order as the LO $gg$ channel in which the background amplitude is induced by a quark loop.
The extra LO $qg$ interference is depicted by the top right diagram in \refF{FeynDiag}, and the $q\bar{q}$ channel is related by cross symmetry.
It was found that the contribution from the $q\bar{q}$ channel is numerically negligible due to the quark PDF suppression.

%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\includegraphics[width=0.8\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_2gamma_figs/InterfFig-eps-converted-to.pdf}
\caption{\label{FeynDiag}
  Representative diagrams for interference between the
Higgs boson resonance and the continuum in the diphoton channel.
The dashed vertical lines separate the resonant amplitudes
from the continuum ones.}
\end{figure}
%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


More recently, the dominant next-to-leading order (NLO) QCD corrections to the interference were calculated in ref. \cite{Dixon:2013haa}, where the dependence of the mass shift on the acceptance cuts was also studied. The left panel of \refF{Mdistr} shows the Gaussian-smeared diphoton invariant mass distribution for the pure signal at both LO and NLO in QCD. Standard acceptance cuts were applied to the photon transverse momenta, $p_{T,\gamma}^{\textrm{hard/soft}}>40/30$~GeV, and rapidities, $|\eta_{\gamma}|<2.5$. In addition, events were discarded when a jet with $p_{T,j}>3$~GeV was within $\Delta R_{\gamma j}<0.4$ of a photon.
The scale uncertainty bands were obtained by varying $m_H/2<\mu_F,\mu_R<2m_H$ independently. For NLO, an additional $qg$ process was included, where the background is induced by a quark loop as shown in the bottom right diagram of \refF{FeynDiag}; this is required as part of NLO $gg$ channel to cancel the quark to gluon splitting in PDF evolution and reduces dependence on the factorization scale $\mu_F$. As a result, the scale uncertainty bands come mostly from varying the renormalization scale $\mu_R$. 

The right panel of \refF{Mdistr} shows the corresponding Gaussian-smeared interference contributions. Each band is labelled according to \refF{FeynDiag}. The destructive interference from the imaginary part shows up at two-loop order in the gluon channel in the zero mass limit of light quarks~\cite{Dixon:2003yb}. It produces the offset of the NLO $gg$ curve from zero at $M_{\gamma\gamma} = 125$~GeV.


%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
  \begin{center}
	\begin{tabular}{c c}
	\includegraphics[width=0.45\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_2gamma_figs/Signal-eps-converted-to.pdf} &
    \includegraphics[width=0.45\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_2gamma_figs/Interf-eps-converted-to.pdf}
\end{tabular}	   
    \end{center}
\vspace{-0.5cm}
  \caption{\label{Mdistr}
   Diphoton invariant mass $M_{\gamma\gamma}$ distribution for pure signal 
   (left panel) and interference term (right panel) after Gaussian smearing.}
\end{figure}
%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Figure \ref{dMvspTh} shows the study of the mass shift dependence on a lower cut on the Higgs boson transverse momentum $p_T > p_{T,H}$. This strong dependence could potentially be observed experimentally, completely within the 
$\gamma\gamma$ channel, without having to compare against a mass measurement using the only other high-precision channel, $ZZ^*$\footnote{The mass shift for $ZZ^*$ is much smaller than for $\gamma\gamma$, as can be inferred from Figure~17 of ref. \cite{Kauer:2012hd},
because $H \to ZZ^*$ is a tree-level decay, while the continuum background $gg \to ZZ^*$ arises at one loop, the same order
as $gg\to \gamma\gamma$.}.
Using only $\gamma\gamma$ events might lead to reduced experimental systematics associated with the absolute photon energy scale.
The $p_{T,H}$ dependence of the mass shift was first studied in ref.~\cite{Martin:2013ula}. The dotted red band includes, in addition,
the continuum process $qg\to\gamma\gamma q$ at one loop via a light quark loop, a part of the full ${\cal O}(\alpha_s^3)$ correction as explained above.
This new contribution partially cancels against the tree-level $qg$ channel, leading to a larger negative Higgs boson mass shift. The scale variation of the mass shift at finite $p_{T,H}$ is very small, because it is essentially a LO analysis; the scale variation largely cancels in the ratio between interference and signal that enters the mass shift.

Due to large logarithms, the small $p_{T,H}$ portion of \refF{dMvspTh} is less reliable than the large $p_{T,H}$ portion. In using the $p_{T,H}$ dependence of the mass shift to constrain the Higgs boson width, the theoretical accuracy will benefit from using a wide first bin in $p_T$. One could take the difference between apparent Higgs boson masses for $\gamma\gamma$ events in two bins, those 
having $p_T$ above and below, say, 40~GeV.


%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
  \begin{center}
    \includegraphics[width=0.5\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_2gamma_figs/dMvspTh-eps-converted-to.pdf}
  \end{center}
  \vspace{-0.5cm}
  \caption{\label{dMvspTh}
   Apparent mass shift for the SM Higgs boson versus the lower cut
   on the Higgs boson transverse momentum, $p_T > p_{T,H}$.}
\end{figure}
%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


The Higgs boson width in the SM is $\Gamma_{H,\text{SM}} = 4.07$ MeV, far too narrow to observe directly at the LHC. In global analyses of various Higgs boson decay channels \cite{Dobrescu:2012td,Djouadi:2013qya,CMS:yva}, it is impossible to decouple the Higgs boson width from the couplings in experimental measurements without a further assumption, because the Higgs boson signal strength is always given by the product of squared couplings for Higgs boson production and for decay, divided by the Higgs boson total width $\Gamma_H$. Typically, the further assumption is that the Higgs boson coupling to electroweak vector bosons does not exceed the SM value. However, as was also pointed out in ref.~\cite{Dixon:2013haa}, the apparent mass shift could be used to bound the value of the Higgs boson width. This is because the interference effect has different dependence on the Higgs boson width, allowing $\Gamma_H$ to be constrained independently of assumptions about couplings or new decay modes in a lineshape model. Such a measurement would complement more direct measurements of the Higgs boson width at future colliders such as the ILC \cite{Richard:2007ru,Peskin:2012we} or a muon collider \cite{Han:2012rb,Conway:2013lca}, but could be accomplished much earlier.

Using $\mu_{\gamma\gamma}$ to denote the ratio of the experimental signal strength in $gg\to H\to \gamma\gamma$ to the SM prediction
($\sigma/\sigma^{\rm SM}$), the following equation can be set up,
%
\begin{equation}
\frac{c_{g\gamma}^2 S}{m_H\Gamma_H}+c_{g\gamma} I 
= \left( \frac{S}{m_H\Gamma_{H, \text{SM}}} + I \right)
 \mu_{\gamma\gamma} \,,
\label{constyield}
\end{equation}
%
where $c_{g\gamma} = c_g c_\gamma$  is the rescaling factor to be solved to preserve the signal yield when the Higgs boson width is varied. Once the relation between the $c_{g\gamma}$ and the Higgs boson width $\Gamma_H$ is obtained, it can be used to determine the size of the apparent mass shift as a function of $\Gamma_H$. Neglecting the interference contribution $I$ to the total rate, and assuming $\mu_{\gamma\gamma}=1$, the mass shift was found to be proportional to the square root of the Higgs boson width, $\delta m_H \propto \sqrt{\Gamma_H/\Gamma_{H,\text{SM}}}$, given that the width is much less than the detector resolution. \refF{dMvsGam} plots the mass shift with $\mu_{\gamma\gamma}=1$ and a smearing Gaussian width of $1.7$ GeV. It is indeed proportional to $\sqrt{\Gamma_H}$ up to small corrections. If new physics somehow reverses the sign of the Higgs diphoton amplitude, the interference $I$ would be constructive and the mass shift would become positive.


%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
  \begin{center}
    \includegraphics[width=0.5\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_2gamma_figs/dMvsGam-eps-converted-to.pdf}
  \end{center}
  \vspace{-0.5cm}
  \caption{\label{dMvsGam}
   Higgs boson mass shift as a function of the Higgs boson width. The coupling 
   $c_{g\gamma}$ has been adjusted to maintain a constant signal strength,
   in this case $\mu_{\gamma\gamma}=1$.}
\end{figure}
%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%------------------------------------------------------------------------------------------------------------------------------------%

In ref. \cite{Coradeschi:2015tna} it was proposed to use another $\gamma\gamma$ sample to determine the Higgs boson resonance peak, in which the two photons were produced in association with two jets. Although this process is relatively rare, so is the background, making it possible to obtain reasonable statistical uncertainties on the position of the mass peak in this channel despite the lower number of events. The production of a Higgs boson in association with two jets is characteristic of the Vector Boson Fusion (VBF) production mechanism. While, in general terms, VBF is subdominant with respect to GF, it has a very different kinematical signature and can be selected through an appropriate choice of the experimental cuts. From a theoretical point of view, the VBF production mechanism has the additional advantage that perturbative corrections are much smaller than for GF (see e.g.~ref.~\cite{Bolzoni:2010xr}). The effect of the signal-background interference for both the GF and VBF production mechanisms were studied, and the relevant diagrams are given in \refF{VBFDiag}. There are two kinds of backgrounds amplitudes, each of QCD and EW origin. It turns out that the interferences between GF signal and EW background or VBF signal and QCD background are highly suppressed by QCD colour factors, and therefore only the remaining combinations are shown in the first two diagrams of \refF{VBFDiag}. In addition, the interference with loop-induced QCD background, as given in the third diagram of \refF{VBFDiag}, was also considered, since it is enhanced by large gluonic luminosity at the LHC.


%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
  \begin{center}
    \includegraphics[width=0.5\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_2gamma_figs/VBFIntFig-eps-converted-to.pdf}
  \end{center}
  \vspace{-0.5cm}
  \caption{\label{VBFDiag}
   Examples of the Feynman diagrams computed for
the calculation. The vertical dotted line separates signal from
background. Above, the VBF signal and EW background con\-tributions; in the middle the GF signal with tree level QCD
mediated background; below, gluon-initiated signal, with the
corresponding loop-induced LO background.}
\end{figure}
%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In \refF{fig:eta} the values of the apparent mass shift $\delta m_H$ obtained for different cuts on the difference in pseudorapidities between the jets $|\Delta\eta_{jj}|$ are shown. The contributions from VBF and GF are presented separately, as well as the total shift. At the bottom of the plot, the total integrated signal is shown, also separated into VBF and GF contributions for the same cuts. For this plot no cut in $p_{T,H}$ was applied, and only events with the invariant mass of the dijet system $M_{jj}>400$~GeV were considered. When no cut in $|\Delta\eta_{jj}|$ is applied, the shift in the Higgs boson invariant mass peak position produced by these two main production mechanisms is of the same magnitude, but of opposite sign; hence one observes a partial cancellation between them, with a net shift of around $-6\text{ MeV}$. As the value of $|\Delta\eta_{jj}|_{\text{min}}$ is increased, VBF becomes the dominant contribution, and GF becomes negligible, leading to a shift of around 20~MeV toward lower masses.

%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\begin{center}
\hskip-0.4cm \includegraphics[width=0.48\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_2gamma_figs/eta_plot-eps-converted-to.pdf}
\caption{Top: Plot of mass shift $\delta m_H$ for different values of $|\Delta \eta_{jj}|_{\text{min}}$. The dashed blue line represents the contribution from the VBF mechanism alone, the dotted red line shows GF only, and the solid black line displays the total shift of the Higgs boson invariant mass peak. Bottom: Total integrated signal cross section, also separated into VBF and GF contributions for the same cuts. No cut on $p_{T,H}^{\text{min}}$ was applied, and an additional cut was set of $M_{jj}>400$GeV.}
\label{fig:eta}
\end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Next, the dependence of the mass shift on $p_{T,H}^{\text{min}}$ was studied. In \refF{fig:ptH_theory} the mass shift and the signal cross section for a range of $p_{T,H}^{\text{min}}$ between 0~GeV and 160~GeV is presented. The curves are labelled in the same way as in \refF{fig:eta}. Once again, both production mechanisms contribute to the shift in invariant mass with opposite signs. For this plot, additional cuts in $M_{jj}>400$~GeV and $|\Delta\eta_{jj}|>2.8$ were applied, enhancing in this way the VBF contributions. However, at higher $p_{T,H}^{\text{min}}$, GF becomes as important as VBF.

%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\begin{center}
\hskip-0.4cm \includegraphics[width=0.48\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_2gamma_figs/ptH_plot-eps-converted-to.pdf}
\caption{Top: Plot of mass shift $\delta m_H$ for different values of $p_{T,H}^{\text{min}}$ for VBF, GF and total contributions.
The curves are labelled as in \refF{fig:eta}. Bottom:  Total integrated signal, also separated into VBF and GF contributions for the same cuts. The following additional cuts were applied: $M_{jj}>400$~GeV and $|\Delta\eta_{jj}|>2.8$.}
\label{fig:ptH_theory}
\end{center}
\end{figure} 
%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

As has already been mentioned, the shift in the Higgs boson invariant mass peak in $pp\to H(\to\gamma\gamma)+2\text{ jets}\,+\,X$ is considerably smaller than in the inclusive channel $pp\to H(\to\gamma\gamma)\,+\,X$. For appropriate cuts it can be almost zero. This makes it useful as a reference mass for experimental measurement of the mass difference,

\begin{equation}
\Delta m_H^{\gamma\gamma} \equiv \delta m_H^{\gamma\gamma,\,\text{incl}} - \delta m_H^{\gamma\gamma,\,\text{VBF}} \,,
\label{BigDeltaDef}
\end{equation}

where $\delta m_H^{\gamma\gamma,\,\text{incl}}$ is the mass shift in the inclusive channel, as computed at NLO in ref. \cite{Dixon:2013haa},
and $\delta m_H^{\gamma\gamma,\,\text{VBF}}$ is the quantity computed in ref. \cite{Coradeschi:2015tna}.
In computing $\delta m_H^{\gamma\gamma,\,\text{VBF}}$ for use in \eqn{BigDeltaDef} the basic photon and jet $p_T$ and $\eta$ cuts were imposed, and also $M_{jj} > 400$~GeV,
but no additional cuts on $p_{T,H}$ or $\Delta\eta_{jj}$ were applied. This choice of cuts results in a small reference mass shift and a relatively large rate with which to measure it.

The lineshape model of ref.~\cite{Dixon:2013haa}, as introduced earlier for the $gg \to \gamma\gamma$ inclusive process, was used in ref. \cite{Coradeschi:2015tna} to
compute the mass shift for the VBF process.
It is in a way relatively independent of the new physics that may increase $\Gamma_H$ from the SM value.
The couplings of the Higgs boson to other SM particles must be modified if the Higgs boson width is varied,
in order to be consistent with the Higgs boson signal strength measurements already made by the LHC,
and prevent the total cross section from suffering large variations.
Here, the deviation from SM coupling is described by a rescaling factor $c_{V\gamma} = c_V c_\gamma$,
similar to $c_{g\gamma}$ in the $\gamma\gamma$ inclusive case, which is adjusted for different values of $\Gamma_H$ to maintain the Higgs boson signal strength near the SM value.

%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\begin{center}
\hskip-0.4cm \includegraphics[width=0.5\textwidth]{WG1/OffshellInterference/YRHXS4_WG1_OffshellInterference_2gamma_figs/widths-eps-converted-to.pdf}
\caption{Plot of measurable mass shift $\Delta m_H^{\gamma\gamma}$ defined in \eqn{BigDeltaDef}, as a function of $\Gamma_H / \Gamma_{H,\text{SM}}$.}
\label{fig:widths}
\end{center}
\end{figure} 
%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
Figure~\ref{fig:widths} shows how the observable $\Delta m_H^{\gamma\gamma}$ depends on the value of the Higgs boson width.
The dependence is proportional to $\sqrt{\Gamma_H/\Gamma_{H,\text{SM}}}$ to a very good accuracy,
as dictated by the linearity of the produced shift in $c_{g\gamma}$ or $c_{V\gamma}$ (in the range shown).
It is dominated by the mass shift for the inclusive sample \cite{Dixon:2013haa}. 
As was stated before, the main theoretical assumption was that the couplings of the Higgs rescale by real factors,
and the same rescaling for the Higgs boson coupling to gluons as for its coupling to vector boson pairs was assumed;
this assumption could easily be relaxed, to the degree allowed by current measurements of the relative yields in different channels.
The strong dependence the shift shows on the Higgs boson width might allow LHC experiments to measure or bound the width.
