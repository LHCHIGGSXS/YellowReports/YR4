%\chapter{Standard Model Parameters \footnote{A.~Denner, S.~Dittmaier, M.~Grazzini, R.~Harlander, R.~Thorne, M.~Spira, M.~Steinhauser}}
%\label{chapter:input}

We summarize the Standard Model input parameters 
for Higgs cross section calculations. The same parameters can be used
for other SM and BSM processes at the LHC.

\section{Lepton masses}

The lepton masses from the PDG\,\cite{Agashe:2014kda} are
%
\begin{align}
& m_e=0.510998928\pm 0.000000011 ~{\rm MeV}\,,\\
& m_\mu=105.6583715\pm 0.0000035 ~{\rm MeV}\,,\\
& m_\tau=1776.82 \pm 0.16 ~{\rm MeV}\,.
\end{align}

\section{Electroweak parameters}

The gauge boson masses and widths from the PDG\,\cite{Agashe:2014kda}
are
%
\begin{align}
m_W&=80.385\pm 0.015~{\rm GeV}\,,& \Gamma_W&=2.085\pm 0.042~{\rm GeV}\,,\\
m_Z&=91.1876\pm 0.0021~{\rm GeV}\,,&\Gamma_Z&=2.4952\pm 0.0023~{\rm GeV}\,.
\end{align}
The Fermi constant is
\begin{equation}
G_F=1.166 378 7(6)\cdot 10^{-5}~{\rm GeV}^{-2}\,.
\end{equation}
These values correspond to the physical on-shell masses. If required,
the complex pole masses can be obtained from the well-known relations
\begin{equation}
\begin{split}
m_V^\text{pole} - i\Gamma_V^\text{pole} 
= \frac{m_V-i\Gamma_V}{\sqrt{1+\Gamma^2_V/m^2_V}}\,,\qquad V\in\{W,Z\}\,.
\label{eq:ospole}
\end{split}
\end{equation}
As for the gauge boson widths, instead of $\Gamma_V^\text{pole}$ from (\ref{eq:ospole}), values consistent with the perturbative order can also be used if more appropriate.

\section{QCD parameters and parton densities}

The most important QCD parameters are the strong coupling $\alpha_s$ and
the quark masses. The default renormalization scheme for these
parameters should be the $\msbar$ scheme. In this scheme, $\alpha_s$
and the quark masses depend on a mass scale $\mu$.

$\alpha_s(\mu)$ and $m_q(\mu)$ are typically determined through their
proper renormalization group equations,
\begin{equation}
\begin{split}
\mu^2\frac{d}{d\mu^2}\alpha_s(\mu) = \alpha_s(\mu)\beta(\alpha_s)\,,\qquad
\mu^2\frac{d}{d\mu^2}m_q(\mu) = m_q(\mu)\gamma_m(\alpha_s)\,,
\end{split}
\end{equation}
combined with their numerical value at a reference mass scale, usually
$\alpha_s(m_Z)$ and $m_q(m_q)$. The perturbative expansions of the
coefficients $\beta(\alpha_s)$ and $\gamma_m(\alpha_s)$ are currently
known through
order~$\alpha_s^4$\,\cite{vanRitbergen:1997va,Czakon:2004bu}
and $\alpha_s^5$, respectively\,\cite{Baikov:2014qja}.

Also the parton density functions depend on a mass scale $\muF$,
\begin{equation}
\begin{split}
\muF^2\frac{d}{d\muF^2}\phi_i(x,\muF) =
P_{ij}(\alpha_s)\otimes\phi_j(x,\muF)\,,\qquad
\end{split}
\end{equation}
where $P_{ij}$ are the splitting functions, currently known through
order $\alpha_s^3$\,\cite{Moch:2004pa,Vogt:2004mw}, and $\otimes$
denotes the usual convolution.  Note that in principle also $\muR$
enters the PDFs implicitly through $\alpha_s$. However, the available
PDF sets assume $\muF=\muR$.

If the typical mass scale $\mu_0$ of a process is not equal to the
reference mass scale, the input quantities $\alpha_s(\mu_0)$ and
$m_q(\mu_0)$ of a perturbative calculation have to be evaluated from
their reference values by RG evolution. While 4-loop evolution may
result in the most precise currently available results for the input
parameters, consistency of the calculation often requires one to use
lower order RG evolution.

\subsection{Strong coupling constant}

The strong coupling $\alpha_s$ enters a typical theory prediction for an
LHC observable in many different ways: explicitly as expansion
parameter in the partonic calculation, or implicitly through its impact
on other input quantities. These sources may be strongly correlated, so
that inconsistencies in the input value $\alpha_s(\mu_0)$ have to be
avoided.  Specifically, the value of $\alpha_s$ used in the partonic
process should coincide with the one corresponding to the parton density
functions. This means that $\alpha_s(m_Z)$ as well as the RG running of
$\alpha_s$ for the evaluation of $\alpha_s(\mu_0)$ have to be adjusted
to the parton density functions that are used.

Concerning the default value for $\alpha_s(m_Z)$ and the estimation of
the uncertainties resulting from $\alpha_s(m_Z)$ and the PDFs, one
should follow the 2015 PDF4LHC recommendation. This amounts to choosing the central value of $\alpha_s(m_Z)$ and the ensuing uncertainty as
\begin{equation}
\alpha_s(m_Z)=0.118\pm 0.0015\, .
\end{equation}

\subsection{Quark masses}

Quark masses (in particular charm and bottom) also enter the PDFs,
albeit in a much weaker way as $\alpha_s$. This releases one from a
similar constraint as it was imposed for $\alpha_s$.

\noindent{\bf Top-quark mass.} The top quark is different from all other
known quarks in the sense that it decays before it hadronizes. To a
first approximation (i.e., neglecting soft QCD effects), the invariant
mass of its decay products may be identified with the on-shell top quark
mass. In fact, the agreement with the determination via the top quark
pair production cross section justifies this with hindsight.

In order to be consistent with existing ATLAS and CMS analyses, we
recommend to use
\begin{equation}
\begin{split}
m_t^\text{OS}=172.5\pm 1\,\text{GeV}
\end{split}
\end{equation}
as reference value for the on-shell top quark mass, corresponding to an
$\msbar$ value of
\begin{equation}
\begin{split}
m_t^{\msbar}(m_t) = 162.7\pm 1\,\text{GeV}\,,
\end{split}
\end{equation}
where we used the four-loop conversion of Ref.\,\cite{Marquard:2015qpa}.
Note that the recommended uncertainty of $\pm 1$\,GeV covers the current
world average of $m_t^\text{OS}\Big|_\text{world ave}=173.2$\,GeV
(notice also Ref.\,\cite{Khachatryan:2015hba}).

The calculation of radiative corrections may require to use the complex
pole mass for the top quark. Due to the lack of experimental data for
the top width, one should use the theoretical value for the top
quark width in the conversion formula (the analogue of
Eq.\,\ref{eq:ospole}), given by\,\cite{Jezabek:1993wk,Czarnecki:1998qc,Chetyrkin:1999ju,Blokland:2004ye,Blokland:2005vq,Gao:2012ja,Brucherseifer:2013iv}
\begin{equation}
\begin{split}
\Gamma_t = 0.89\cdot \Gamma_t^{(0)}\,,
\end{split}
\end{equation}
where
\begin{equation}
\begin{split}
\Gamma_t^{(0)} &= \frac{G_Fm_t^3}{8\sqrt{2}\pi}\left[1 -
  3\left(\frac{m_W^2}{m_t^2}\right)^2 
  + 2\left(\frac{m_W^2}{m_t^2}\right)^3\right]\,.
\end{split}
\end{equation}


%% In Higgs physics, quark masses enter in two different ways. On the one
%% hand, they occur as kinematical masses, associated with propagators,
%% external lines, or phase space factors in the calculation of scattering
%% and decay processes. On the other hand, in particular the masses of the
%% three heaviest quark flavours enter as couplings: foremost in the Yukawa
%% couplings of the quarks themselves, but also in other couplings related
%% to those by symmetries (think of squark-Higgs boson couplings, for
%% example). It is obvious that these Yukawa couplings are essential for
%% Higgs physics, and that it is important to treat them to the best of our
%% knowledge.

\noindent{\bf Bottom-quark mass.} The situation is much different for
the bottom quark mass, because its mass can only be determined
indirectly. We recommend to use the current PDG value for the $\msbar$
bottom mass \cite{Agashe:2014kda}
\begin{equation}
m_b(m_b)=4.18\pm 0.03~{\rm GeV}
\label{eq:mbmb}
\end{equation}
as reference input value.\footnote{The dependence of this value on
  $\alpha_s$ (see Ref.\,\cite{Chetyrkin:2009fv}) can usually be neglected.}
Evolution to the characteristic scale $\mu_0$ of the process should be
done through the highest available perturbative order. The inconsistency
with the PDF value for the bottom quark mass introduced by this
procedure is expected to be small, in particular if no initial-state
$b$-quarks are involved. The uncertainty can be estimated by comparing
the results when using PDFs built on different values of the $b$ quark
mass.

If the use of the on-shell bottom-quark mass cannot be avoided, the
conversion of the $\msbar$ value should be done at the 3-loop level, and
the difference to the 4-loop result should be used as the uncertainty. Using
$\alpha_s(m_Z)=0.118$ and $m_b(m_b)=4.18$\,GeV, one
obtains\,\cite{Marquard:2015qpa}
\begin{equation}
\begin{split}
\label{eq:mb}
m_b^\text{OS}\bigg|_{4.18} = (4.18 + 0.40 + 0.20 + 0.14 + 0.13)\,\text{GeV}\,,
\end{split}
\end{equation}
where the numbers correspond to consecutive loop orders. Our
recommendation for the bottom quark pole mass is therefore obtained by
considering the sum of the first four terms in Eq.~(\ref{eq:mb}),
and assigning the last term as an uncertainty
\begin{equation}
\begin{split}
m_b^\text{OS} = 4.92\pm 0.13\,\text{GeV}\,.
\end{split}
\end{equation}

\noindent{\bf Charm-quark mass.} The charm quark mass is at the edge of
the validity range of perturbation theory. Therefore, using $m_c(m_c)$
as reference input value in order to derive the charm quark mass at a
different scale, or its perturbative on-shell mass, would force one to
apply perturbation theory at these rather low energies. We therefore
recommend to use \cite{Chetyrkin:2009fv}
\begin{equation}
m_c(3~{\rm GeV})=0.986\pm 0.026~{\rm GeV}
\end{equation}
as overall input value. To be conservative, the error quoted in
Ref.\,\cite{Chetyrkin:2009fv} has been multiplied by a factor of two here.

Concerning RG evolution and consistency with the charm quark mass in the
PDFs, the situation is analogous to the case of the bottom quark (see
above).

If the use of the on-shell charm-quark mass cannot be avoided, its mass
should be evaluated from the on-shell bottom mass (calculated as
described above) through the relation\,\cite{Bauer:2004ve}
\begin{equation}
\begin{split}
m_c^\text{OS} = m_b^\text{OS}-3.41\,\text{GeV} = 1.51\pm 0.13\,\text{GeV}\,.
\end{split}
\end{equation}

\section{Higgs boson mass}

The current combination of the measurements of the Higgs boson mass $m_H$ from ATLAS and CMS
is \cite{Aad:2015zhl}
\begin{equation}
\begin{split}
m_H=125.09\pm 0.21\text{(stat.)}\pm 0.11\text{(syst.) GeV}\, .
\end{split}
\end{equation}
As a reference value for $m_H$ in theoretical calculations we recommend to
use the rounded value
\begin{equation}
\begin{split}
m_H=125~\text{GeV}\, .
\end{split}
\end{equation}

