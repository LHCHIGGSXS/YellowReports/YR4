\subsection{Definition of the fiducial phase space}
\label{sec:exp:fid}

The acceptance and selection efficiency for the particular Higgs boson decay channel can vary significantly between different Higgs boson production mechanisms and different exotic models of Higgs boson properties. In processes with large jet activity such as the $\ttH$ production or those with the kinematics of the decay products very different from the SM prediction (such as in case of the exotic Higgs-like spin-one models), the acceptance of signal events within a certain part of the phase space can significantly differ from the acceptance for the SM Higgs boson decays. In order to minimize the dependence of the measurement on the specific theoretical model assumption, the fiducial phase space for the Higgs boson cross section measurements should be defined to match as closely as possible the experimental acceptance in terms of the kinematics of the decay products and topological reconstruction-level event selection.
%(includes the definition of selection observables and selection requirements, as well as the definition of the algorithm for the topological event selection.)

The fiducial phase space is typically defined using the stable particles or more complex objects built out of them (leptons, photons, jets, missing transverse momentum, etc.) at the hard scattering level, before their interaction with the detector material. 
In order to minimize the model dependence, fiducial-level particles and objects are typically defined to be as close as possible to the particles and objects used at the reconstruction level. 
In case of the leptons, it is typical that fiducial-level leptons are defined as the leptons ``dressed'' with the photons from the final state radiation (the photons that are within certain distance $\Delta R$ from the lepton), as at the reconstruction level those photons are typically recovered by the experimental methods.
%These are usually denoted as fiducial-level objects. In order to minimize the uncertainties related to the modelling of the QED finite state radiation (FSR) - it is recommended that fiducial-level leptons are defined as the leptons ``dressed'' with the FSR photons (that are within certain distance from the lepton), or in cases when the effect is negligible - as leptons before any FSR occurs. 
In case of differential measurements as a function of jet-related observables, it is recommended that jets are reconstructed from the individual stable particles, excluding neutrinos, using the anti-$k_t$ clustering algorithm with a distance parameter identical to the one used at the reconstruction level.

\subsubsection{Isolation requirement in the definition of the fiducial volume\SectionAuthor{S.~Menary, A.~Pilkington}}

The inclusion of isolation of photons and leptons can be important in the fiducial phase space definition whenever object isolation is used at the reconstruction level, as it can reduce the differences in signal selection efficiency between different models. It has been verified in simulation that this difference can be significant if the lepton isolation requirement is included at the reconstruction but not at the fiducial level~\cite{Aad:2014tca,Khachatryan:2015yvw}. This can be especially pronounced in case of large associated jet activity such as in the $\ttH$ production mode. Exclusion of neutrinos from the computation of the isolation sum typically brings the definition of the fiducial phase space closer to the reconstruction level selection, and can additionally improve the model independence of the signal selection efficiency. It is recommended that these effects are studied in each particular analysis separately.

The experimental analyses measuring fiducial cross sections in the $H \to \gamma\gamma$ channel typically require two isolated photons with a $p_T$ above a certain threshold. In order to minimize the extrapolation when correcting experimental yields to particle level fiducial cross sections, it is useful to impose a similar criterion which duplicates a similar requirement using stable particles. Imposing such drastically reduces the underlying model dependence: this can be readily understood if one compares for example Higgs boson production with gluon fusion versus in association with a top quark pair: $H \to \gamma\gamma$ photons from the latter fail more often the isolation criterion due to the large hadronic activity and thus have a lower reconstruction efficiency. Figure~\ref{fig:exp:isolation} shows the correction factors mapping reconstructed yields into fiducial cross sections with and without imposing a similar particle level isolation cut. A particle level isolation criterion can be imposed by summing around a fixed cone the energies of all stable particles and events are similarly rejected when the isolation energy is larger than a certain threshold. The exact cut can be tuned such that the model dependence becomes minimal, i.e. that the efficiency difference between rejecting a reconstructed event and a true event is very similar: In Figure~\ref{fig:exp:isolation} the correlation between true and reco isolation is shown, and an illustrative reconstruction cut is mapped to a given true value using a profile of both observables. The effect of imposing this criterion is illustrated as well, resulting in near matching correction factors for all Higgs boson production processes. 

\begin{figure}
\includegraphics[width=0.55\textwidth]{./WGx/FXS/figs/correction_factor}
\includegraphics[width=0.45\textwidth]{./WGx/FXS/figs/correction_factor2}
\caption{
(left) Illustration for correction factor which maps reconstructed yields to fiducial cross sections without (red) and with (green) imposing a particle level isolation criterion are shown. Imposing particle level isolation significantly reduces the differences between different Higgs boson production modes which minimizes the model dependence. (right) The procedure to map a reconstructed isolation criterion to a particle level isolation criterion using profiles is illustrated.
}
\label{fig:exp:isolation}
\end{figure}

\subsubsection{Signal contributions from outside of the fiducial phase space}

At the reconstruction level, additional signal contribution from events that do not originate from the fiducial phase space can arise due to detector resolution effects that cause differences between the quantities used for the fiducial phase space definition (such as the lepton or photon isolation, jet transverse momentum, missing transverse momentum etc.) and the analogous quantities used for the event selection. This contribution should be treated as background and subtracted before the unfolding procedure is applied. Hereafter we refer to this contribution as the ``nonfiducial signal'' contribution. It has been shown in simulation that the shape of these events is typically very similar to the shape of the fiducial signal.
In order to minimize the model dependence of the measurement - it should be studied how to optimize fiducial phase space definition to minimize the effect that arises from nonfiducial signal' contribution, and how to experimentally treat this contribution in the measurement. Studies in simulation have shown that this component can vary from just few per cent e.g. for the $\ggH$ production mode to several per cent for the $\ttH$ production mode~\cite{Aad:2014tca,Khachatryan:2015yvw}. The variation of this fraction between different signal models can be included in the model dependence estimation.
%The size of this component for different signal models is shown in [Table].

The nonfiducial signal contribution deserves special attention when the observables used to define the signal region have poor experimental resolution (such as missing transverse energy, transverse momentum of jets, etc.). In those cases effects of migration of the signal events can be large, and it might be worth studying if the measurement can benefit (in terms of the overall model dependence) from relaxing the requirements on such observables at the fiducial level with respect to the reconstruction level. These effects have been discussed in the light of the fiducial measurements of the Higgs boson transverse momentum in the $H \to WW$ decay channel~\cite{Khachatryan:2016vnn}.

The fraction of signal events within the fiducial phase space $\mathcal{A}_{\rm fid}$, the reconstruction efficiency $\epsilon$ for signal events within the fiducial phase space for individual SM production modes and exotic signal models, as well as the fraction of signal events outside of the fiducial phase space $f_{\rm nonfid}$ are listed in Table~\ref{tab:Accept_Eff_fOut}. Values are given for characteristic signal models assuming $m_{\rm H} = 125.0 \UGeV$, $\sqrt{s}=8\,\UTeV$, and the overall picture is similar in case of the pp collision at $\sqrt{s}=13\,\UTeV$.

\begin{table}[!h!tb]
\begin{center}
\small
\caption{
The fraction of signal events within the fiducial phase space (acceptance $\mathcal{A}_{\rm fid}$), reconstruction efficiency ($\epsilon$) for signal events from within the fiducial phase space, and ratio of reconstructed events which are from outside the fiducial phase space to reconstructed events which are from within the fiducial phase space ($f_{\rm nonfid}$). 
Values are given for characteristic signal models assuming $m_{\rm H} = 125.0\,\UGeV$, $\sqrt{s}=8\,\UTeV$, and the uncertainties include only the statistical uncertainties due to the finite number of events in MC simulation.
%In case of the first seven signal models, decays of the Higgs-like boson to four leptons proceed according to SM via the ${\rm H} \to {\rm Z}{\rm Z^{*}} \to 4\ell$ process.
%Definition of signal excludes events where at least one reconstructed lepton originates from associated vector bosons or jets.
\label{tab:Accept_Eff_fOut}
}
\begin{tabular}{l|cccc} 
%\hline %---------------------------------------------------------
%\hline %---------------------------------------------------------
\toprule
\textbf{Signal process} & $\mathcal{A}_{\rm fid}$ & $\epsilon$ & $f_{\rm nonfid}$  & $(1+f_{\rm nonfid})\epsilon$ \\ 
%\hline %---------------------------------------------------------
%\hline %---------------------------------------------------------
\midrule
\multicolumn{5}{c}{Individual Higgs boson production modes} \\
%\hline %---------------------------------------------------------
\midrule
$\ggH$ ({\sc Powheg+JHUGen})   & 0.422 $\pm$ 0.001 & 0.647 $\pm$ 0.002 & 0.053 $\pm$ 0.001  & 0.681 $\pm$ 0.002 \\ 
VBF ({\sc Powheg})                      & 0.476 $\pm$ 0.003 & 0.652 $\pm$ 0.005 & 0.040 $\pm$ 0.002  & 0.678 $\pm$ 0.005 \\ 
WH ({\sc Pythia})                          & 0.342 $\pm$ 0.002 & 0.627 $\pm$ 0.003 & 0.072 $\pm$ 0.002  & 0.672 $\pm$ 0.003 \\ 
ZH ({\sc Pythia})                          & 0.348 $\pm$ 0.003 & 0.634 $\pm$ 0.004 & 0.072 $\pm$ 0.003  & 0.679 $\pm$ 0.005 \\ 
$\ttH$ ({\sc Pythia})                      & 0.250 $\pm$ 0.003 & 0.601 $\pm$ 0.008 & 0.139 $\pm$ 0.008  & 0.685 $\pm$ 0.010 \\ 
%\hline %---------------------------------------------------------
\midrule
\multicolumn{5}{c}{Some characteristic models of a Higgs-like boson with exotic decays and properties} \\
%\hline %---------------------------------------------------------
\midrule
${\rm q\bar{q}} \to {\rm H}(J^{CP}=1^{-})$ ({\sc JHUGen})         & 0.238 $\pm$ 0.001 & 0.609 $\pm$ 0.002 & 0.054 $\pm$ 0.001  & 0.642 $\pm$ 0.002 \\ 
${\rm q\bar{q}} \to {\rm H}(J^{CP}=1^{+})$ ({\sc JHUGen})        & 0.283 $\pm$ 0.001 & 0.619 $\pm$ 0.002 & 0.051 $\pm$ 0.001  & 0.651 $\pm$ 0.002 \\ 
${\rm gg} \to {\rm H} \to {\rm Z}\gamma^{*}$ ({\sc JHUGen})         & 0.156 $\pm$ 0.001 & 0.622 $\pm$ 0.002 & 0.073 $\pm$ 0.001  & 0.667 $\pm$ 0.002 \\ 
${\rm gg} \to {\rm H} \to \gamma^{*}\gamma^{*}$ ({\sc JHUGen}) & 0.188 $\pm$ 0.001 & 0.629 $\pm$ 0.002 & 0.066 $\pm$ 0.001  & 0.671 $\pm$ 0.002 \\ 
%\hline %---------------------------------------------------------
%\hline %---------------------------------------------------------
\bottomrule
\end{tabular}
\normalsize
\end{center}
\end{table}

\subsubsection{Signal definition}

The requirement on the invariant masses of the Higgs boson decay products is also important as the off-shell production cross section in the dominant gluon fusion production mode can be sizeable and can amount up to a few per cent of the total cross section~\cite{Kauer:2012hd}.

