%\documentclass{article}
%\usepackage{graphicx}
%\usepackage{amsmath}
\def\ltap{\raisebox{-.6ex}{\rlap{$\,\sim\,$}} \raisebox{.4ex}{$\,<\,$}}
\def\gtap{\raisebox{-.6ex}{\rlap{$\,\sim\,$}} \raisebox{.4ex}{$\,>\,$}}
%\pagestyle{plain}
% ---------------------------------------------------
\subsection{Template fiducial regions for benchmark }
% ---------------------------------------------------
\label{sec:fiducialvolumes}
In this section we report the definitions of the template fiducial
volumes for Higgs boson production.
%
These templates do not serve as a reference for the fiducial
definitions that will be used in Run 2 analyses, but rather as a
plausible set of cuts that will be used in the benchmarking
process. Kinematic cuts are listed according to the Higgs boson decay
mode, for the three final states $\PH \to \PZ\PZ^*\to 4\Pl$,
$\PH \to \PGg \PGg$, and $\PH \to \PW\PW^*\to 2\Pl 2 \PGn$.
%
As mentioned in the introduction to this chapter, the definition of
the fiducial regions should have as little impact as possible on the
phase space available for BSM searches in Higgs signatures, and should
be defined mainly in order to minimize the model dependence.
%As an
%example, searches based on total rates typically require to collect as
%many events as possible from the bulk of the SM-like signal phase
%space. In contrast, searches in kinematic distributions are often
%enhanced by large momentum flow through the relevant Higgs
%interactions, leading to large effects in the tails of distributions.
Therefore, fiducial regions will be largely defined by the detector
coverage and by trigger performances, rather than by theoretical
prejudice.

\subsubsection{Fiducial volume for \texorpdfstring{$\PH \to \PZ\PZ^*\to 4\Pl$}{H to ZZ* to 4l}}
Muons (electrons) are required to have a transverse momentum larger
than $5$\,\UGeV ( $7$\,\UGeV) and rapidity $|\eta|\leq 2.5$.
%
The leading lepton pair is defined as the
same-flavoured-opposite-signed (SFOS) lepton pair with the smallest
$|m_{\PZ} - m_{\Pl\Pl}|$. The leading-lepton-pair invariant mass is denoted by
$m_{12}$. The subleading lepton pair is the remaining pair of SFOS
leptons with smallest $|m_{\PZ} - m_{\Pl\Pl}|$. Its invariant mass is denoted
by $m_{34}$.
%
Jets are reconstructed using the anti-$k_t$
algorithm~\cite{Cacciari:2008gp} with a radius parameter of 0.4. All
jets with a transverse momentum larger than $30$\,\UGeV and rapidity
$|\eta|\leq 4.4$ are used in the selection criteria. Neutrinos are not
considered in the jet definition.
%

\noindent The fiducial volume for the $\PH \to \PZ\PZ^*\to 4\Pl$ channel is reported
in Table~\ref{tab:fiducialZZ}
\begin{table}[htp]
\caption{Template fiducial cuts for the $\PH \to \PZ\PZ^*\to 4\Pl$ channel.}
\label{tab:fiducialZZ}
\centering
\footnotesize
\begin{tabular}{l}
  \toprule
  Template fiducial region for $\PH \to \PZ\PZ^*\to 4\Pl$\\
  \midrule
  Leading lepton:  $p_{t}>20$\,\UGeV\\\\
  1$^{st}$ subleading lepton: $p_{t}>10$\,\UGeV\\\\
  2$^{nd}$ subleading lepton: $p_{t}>7\,(5)$\,\UGeV for electrons
  (muons)\\\\
  3$^{rd}$ subleading lepton: $p_{t}>7\,(5)$\,\UGeV for electrons
  (muons)\\\\
  All leptons are required to be isolated:\\
  ratio of the sum of $p_t$'s of all charged particles within $\Delta R =  [ \left( \Delta \phi \right)^2 + \left( \Delta \eta \right)^2 ]^{1/2} <
  0.4$\\
  from the lepton to the lepton's $p_t$ must be smaller than $0.4$  \\\\
  Mass requirements: $40\,{\rm GeV} \leq m_{12} \leq 120\,\UGeV$;
  $12\,\UGeV \leq m_{34} \leq 120\,\UGeV$\\\\
  Lepton separation: $\Delta R(i,j) > 0.1$ for all leptons $i$, $j$\\\\
  $J/\Psi$ invariant mass veto: $m_{ij} > 4$\,\UGeV for all SFOS leptons
  $i$, $j$\\\\
  Invariant mass cut: $120\,\UGeV \leq m_{4\Pl} \leq 130\,\UGeV$\\
  \\ \bottomrule
\end{tabular}
\end{table}


\subsubsection{Fiducial volume for \texorpdfstring{$\PH \to \PGg\PGg$}{H to gamma gamma}}
Photons are requested to have a transverse momentum larger than
$25$\,\UGeV and rapidity $|\eta|\leq 2.5$.
%
The photon pair with the largest transverse momentum is denoted as the
leading photon pair. Its invariant mass is denoted by
$m_{\PGg\PGg}$.
%
Jets are reconstructed using the anti-$k_t$
algorithm~\cite{Cacciari:2008gp} with a radius parameter of 0.4. All
jets with a transverse momentum larger than $30$\,\UGeV and rapidity
$|\eta|\leq 4.4$ are used in the selection criteria. Neutrinos are not
considered in the jet definition.


\noindent The fiducial volume for the $\PH \to \PGg\PGg$ channel is reported
in Table~\ref{tab:fiducialyy}
\begin{table}[htp]
\caption{Template fiducial cuts for the $\PH \to \PGg\PGg$ channel.}
\label{tab:fiducialyy}
\centering
\footnotesize
\begin{tabular}{l}
  \toprule
  Fiducial region for $\PH \to \PGg\PGg$\\
  \midrule
  Leading photon:  $p_{t}/m_{\PGg\PGg}>0.35$\\\\
  Subleading photon:  $p_{t}/m_{\PGg\PGg}>0.25$\\\\
  All photons are required to be isolated:\\
  ratio of the sum of $E_t$'s of all charged particles within $\Delta R =  [ \left( \Delta \phi \right)^2 + \left( \Delta \eta \right)^2 ]^{1/2} <
  0.2$\\
  from the photon to the photon's $E_t$ must be smaller than $0.2$  \\ \\
  Invariant mass cut: $105\,\UGeV \leq m_{\PGg\PGg} \leq 160\,\UGeV$\\
  \\ 
  \bottomrule
\end{tabular}
\end{table}


\subsubsection{Fiducial volume for \texorpdfstring{$\PH\to W^+W^-\to 2\Pl 2\PGn$}{H to WW to 2l2nu}}
The leading (subleading) lepton is required to have a transverse
momentum larger than $20$\,\UGeV ($10$\,\UGeV), and rapidity
$|\eta|\leq 2.5$.
%
Jets are reconstructed using the anti-$k_t$
algorithm~\cite{Cacciari:2008gp} with $R=0.4$. All jets with a
transverse momentum larger than $30$\,\UGeV and rapidity
$|\eta|\leq 4.4$ are used in the selection criteria. Neutrinos are not
considered in the jet definition.
%

\noindent The fiducial volume for the $\PH\to W^+W^-\to 2\Pl 2\PGn$
channel is reported in Table~\ref{tab:fiducialWW}
\begin{table}[htp]
\caption{Template fiducial cuts for the $\PH\to W^+W^-\to 2\Pl 2\PGn$ channel.}
\label{tab:fiducialWW}
\centering
\footnotesize
\begin{tabular}{l}
  \toprule
  Fiducial region for $\PH\to W^+W^-\to 2\Pl 2\PGn$\\
  \midrule
  Lepton invariant mass:     $m(ll) > 12$\,\UGeV \\\\
  Lepton transverse momentum:     $p_{t, \Pl\Pl} > 30$\,\UGeV \\\\
  All leptons are required to be isolated:\\
  ratio of the sum of $p_t$'s of all charged particles within $\Delta R =  [ \left( \Delta \phi \right)^2 + \left( \Delta \eta \right)^2 ]^{1/2} <
  0.4$\\
  from the lepton to the lepton's $p_t$ must be smaller than $0.4$  \\\\
  Lepton transverse mass:     $m_{T}(ll,\PGn\PGn) > 50$\,\UGeV \\\\
  where  $m_{T}(ll,\PGn\PGn) = \sqrt{(E_{t,\Pl\Pl} + p_{t,\PGn\PGn})^2 - |\vec{p}_{t,\Pl\Pl} +
  \vec{p}_{t,\PGn\PGn}|^2}$, and $E_{t,\Pl\Pl}=\sqrt{p_{t,\Pl\Pl}^2+m_{\Pl\Pl}^2}$ \\\\
  Missing transverse energy:     $E^{\rm miss}_{T} > 15$\,\UGeV \\
  \\
  \bottomrule
\end{tabular}
\end{table}
