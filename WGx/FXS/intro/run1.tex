In the first running period of the LHC, several measurements of fiducial cross sections have been carried out by the ATLAS and CMS experiments. These measurements mark the transition from the discovery of the Higgs boson, and first measurement characterizing its quantum numbers, to less statistical powerful but more model independent statements about its properties. Such measurements were first carried out in the channels with high mass resolution, $\PH \to \PGg\PGg$~\cite{Aad:2014lwa,Khachatryan:2015rxa} and $\PH \to \PZ\PZ^*\to 4\Pl$~\cite{Aad:2014tca,Khachatryan:2015yvw}, 
%first by ATLAS and then by CMS. [PM: had feeling that it is too much stress on first/then]
More recently, the first measurements of the differential fiducial cross sections in $H \to WW$ decay channel also appeared~\cite{Khachatryan:2016vnn}.
%~\cite{Khachatryan:2016vnn, ATLAS:HWW}.
% FB: should be out next week, if final reader signs it off. 
First combinations of total and differential information from $\PH \to \PGg\PGg$ and $\PH \to \PZ\PZ^*\to 4\Pl$ also were carried out, extrapolating into the inclusive phase space and correcting for the difference in branching fraction, cf. Ref.~\cite{Aad:2015lha}. A variety of results were reported, ranging from inclusive and exclusive jet multiplicities to differential information characterizing the Higgs boson, its decay products, or objects produced in association with it: the Higgs boson $p_T$ distribution and absolute rapidity has been measured, the number of jets as well as the $p_T$ and rapidity distributions of the leading and sub-leading jets. Angular observables from jets and jets plus the Higgs boson decay products have been reported. Figure~\ref{fig:run1:results} shows a summary of the fiducial regions measured by ATLAS in the $\PH \to \PGg\PGg$  channel and the measured Higgs boson $p_T$ spectrum reported by CMS using $\PH \to \PZ\PZ^*\to 4\Pl$ events. The precision of the probed fiducial regions and differential observables are all statistically limited at this point.

\begin{figure}
\includegraphics[width=0.54\textwidth]{./WGx/FXS/figs/fig03}
\includegraphics[width=0.38\textwidth]{./WGx/FXS/figs/cmspT}
\caption{
(left) Various fiducial regions measured by Ref.~\cite{Aad:2014lwa} using $\PH \to \PGg\PGg$ decays: the data points show measured cross sections of total, inclusive jet multiplicities as well as VBF and $\PV\PH$ enhanced regions. The coloured bands show several theory predictions for the different fiducial regions for gluon fusion and other SM Higgs boson production. (right) The measured differential Higgs boson $\pT$ spectrum is shown from Ref.~\cite{Khachatryan:2015yvw} using $\PH \to \PZ\PZ^*\to 4\Pl$ events. The data points show the measured cross section and the different shaded bands theory predictions from gluon fusion and other SM Higgs boson production. 
}
\label{fig:run1:results}
\end{figure}

The reported cross sections are unfolded to the particle level, defined by particles that have lifetimes such that $c \tau > 10$ mm. The reported fiducial volumes differ for each final state and between experiments. The defining criteria of the fiducial volumes were chosen to be very similar to the criteria applied at detector level to ensure minimal model dependence in the final measurements. In both experiments, leptons are identified using an isolation criterion by summing over energetic clusters in a cone around the charged track trajectory. This can be mimicked at particle level by requiring a similar isolation in a cone around the particle-level lepton. For photons a similar requirement can be imposed to closely match the experimental selection. Other requirements that enter the fiducial acceptance is the overall detector acceptance, trigger threshold energies, and analysis selection cuts. Once defined, selection related efficiencies can be reverted to convert fitted yields into fiducial cross sections. Migrations inside the fiducial volumes from finite resolution are reverted as well, what allows direct comparison with theory predictions. Two different methods to accomplish this are used right now: the ATLAS $\PH \to \PGg\PGg$ and $\PH \to \PZ\PZ^*\to 4\Pl$ results chose to use correction factors, while the CMS $\PH \to \PZ\PZ^*\to 4\Pl$ and the CMS $\PH \to \PGg\PGg$ results directly inverted the migration matrix. 
%[PM: I think was not necessary. We will have discussion later.] Either approach is fine, as long as potential biases were evaluated and are found to be small with respect to the overall experimental uncertainties. 
%[PM: I propose to rephrase it so that references are given for those results available/not-available at HEPDATA, to make it less harsh on CMS.] 
%All results are already (ATLAS) or will be (CMS) published on HEPDATA~\cite{hepdata} along with example RIVET routines~\cite{rivet} to apply the corresponding particle level fiducial selection of each analysis. 
Several results~\cite{Aad:2014lwa,Aad:2014tca} are already published on HEPDATA~\cite{hepdata} along with example RIVET routines~\cite{rivet} to apply the corresponding particle level fiducial selection of each analysis, while the others are expected to follow~\cite{Khachatryan:2015rxa, Khachatryan:2015yvw, Khachatryan:2016vnn}. 

The ATLAS experiment reported in a follow up publication to Ref.~\cite{Aad:2014lwa} also the statistical correlations between five measured differential distributions in Ref.~\cite{Aad:2015tna}. This allows the simultaneous analysis of several differential distributions and the result of a proof-of-concept analysis constraining BSM physics is shown in Figure~\ref{fig:run1:results2} along with the determined statistical correlations between exclusive jet bins and the Higgs boson $\pT$. Finally, ATLAS reported fiducial cross sections of $\Pp\Pp \to \PZ\PZ^{(*)}\to 4\Pl$ in which the Higgs boson contribution from $\PH \to \PZ\PZ^*\to 4\Pl$ was included as part of the signal definition~\cite{Aad:2015rka}.

\begin{figure}
\includegraphics[width=0.56\textwidth]{./WGx/FXS/figs/corr}
\includegraphics[width=0.42\textwidth]{./WGx/FXS/figs/wilson}
\caption{
(left) Statistical correlations between exclusive jet bins and Higgs boson $p_T$. (right) Proof-of-concept analysis using five differential distributions: allowed 95\% and 68\% CL for two Wilson coefficients which add additional point-like interactions for Higgs boson production via gluon fusion ($c_g$) and Higgs boson decay into two photons ($c_\gamma$) are shown.
}
\label{fig:run1:results2}
\end{figure}

Fiducial cross sections results using early Run~2 data were reported by both ATLAS and CMS for the $\PH \to \PGg\PGg$ and $\PH \to \PZ\PZ^*\to 4\Pl$ channels. \refF{fig:run1:results3} shows preliminary results of the total fiducial cross section for centre-of-mass energies at $7$, $8$ and $13$ \UTeV\ from Refs.~\cite{ATLAS-CONF-2015-060,CMS:2016rqf}.

\begin{figure}
\includegraphics[width=0.53\textwidth]{./WGx/FXS/figs/13TevATLAS}
\includegraphics[width=0.47\textwidth]{./WGx/FXS/figs/13TevCMS}
\caption{
(left) Fiducial cross sections from $\PH \to \PGg\PGg$ for $\sqrt{s} = 7$, $8$ and $13$\UTeV. The fiducial volumes were extrapolated so that there is an identical definition between all centre-of-mass energies. The hatched theory band shows the prediction from the SM. (right) Fiducial cross sections from $\PH \to \PZ\PZ^*\to 4\Pl$ for $\sqrt{s} = 7$, $8$ and $13$\UTeV also with matching fiducial volume definitions. 
}
\label{fig:run1:results3}
\end{figure}

