\subsection{Fiducial cross sections for Higgs boson production in association with \texorpdfstring{$n_{\rm jet} \geq 1$}{n_jet >= 1} jets}
\label{sec:hjet}
%\SectionAuthor{F.~Caola, K.~Melnikov, M.~Schulze}}


In this section we present results for fiducial cross section and
differential observables obtained from the NNLO QCD computation of
Higgs boson production in association with one hard jet at the LHC
\cite{Caola:2015wna}.  First, we briefly describe the setup of the
computation. We work in a Higgs Effective Theory where the massive top
quark is integrated out. This approximation is parametric in
$\frac{q^2}{m_t}$ where $q$ is a typical scale of the process, and it
is known to work well up to $q \sim m_t$ \cite{Harlander:2012hf}.  In
principle, the exact top quark mass dependence is known at LO
\cite{Ellis:1987xu,Baur:1989cm} and could be included in our
predictions.  However, for simplicity we refrain from including them
in the results presented here. In our computation we include all
partonic channels at NLO. For the NNLO corrections we include the $gg$
and $qg$ channels which are the only ones relevant for phenomenology
within the $p_T$ range considered here.  The effect of the missing
channels is expected to be much smaller than the residual scale
uncertainty and probably comparable to finite top quark mass
corrections.  We treat the Higgs boson on-shell and include all
relevant decays to $\gamma\gamma$ and four lepton final states.  We
neglect interference effects in the case of identical fermions and
$W-Z$ interference for $2\ell \, 2\nu$ final states. All these effects
are expected to be small in the fiducial region considered in this
section, with the exception of off-shell production in the $WW$
channel.  Indeed, the fiducial region studied in this section does not
include any cut on the $WW$ transverse mass, $m_T$. As it is well
known~\cite{Kauer:2012hd}, this leads to a sizeable off-shell and
signal/background effects on the Higgs cross-section\footnote{Note
  that in the the $ZZ$ fiducial region off-shell effects are
  negligible thanks to the $m_{4l}$.}. One should carefully take this
into account when performing any study in the fiducial region defined
in this section. As we said, our numbers are for on-shell Higgs and do
not include $pp\to 2l 2\nu$ signal/background interference, so by
construction they don't account for such (potentially large)
effects. As a consequence, they lead to reliable predictions only in a
region where off-shell effects are negligible.
%
Finally, we use $\mu=\mu_\mathrm{fact}=\mu_\mathrm{ren}=m_H$ as the
central scale and vary by a factor of two upwards and downwards.  We
believe that this choice gives a more conservative estimate of
residual theoretical uncertainty while not changing much (at NNLO) the
central value compared to the more traditional choice $\mu=m_H/2$.
Also, we note that the stability w.r.t. scale variation of the NNLO
results presented in this section makes the use of dynamical scales
unnecessary at this order.


Our results show that NNLO corrections lead to a stabilization of
fiducial cross sections and shapes of differential distributions.  The
unphysical scale dependence of the NNLO result is reduced by a factor
of more than two with respect to NLO\footnote{Our results do not
  include PDFs uncertainties which are estimated to be at the level of
  5\%~\cite{Boughezal:2015dra}.}.  In general, we find that applying
fiducial cuts does not spoil the convergence of the perturbative
expansion.  Acceptances are found to be stable when moving from NLO to
NNLO, in agreement to our results in Ref.~\cite{Caola:2015wna}. To
illustrate this, and to explore the validity range of pure fixed order
computations, in \refF{fig:hjetcml}, we present the cumulative
leading jet $p_\perp$ distribution as a function of the lower cut on
$p_\perp$, for the $\gamma\gamma$ channel in the fiducial region.  In
the lower pane we plot the NLO and NNLO $K$-factors and observe very
stable corrections to $p_\mathrm{cut}$ as low as 30~GeV\footnote{In
  this section, both NLO and the NNLO results are computed with
  NNLO PDFs and $\alpha_s$.}.  There is no indication of a breakdown of
perturbation theory for these values of transverse momentum.  Hence,
it appears, that also in the fiducial region the NNLO result already
captures the dominant logarithmic enhancements and additional
resummation effects are small~\cite{Banfi:2015pju} for $p_\perp \ge
30$~GeV.  This is in contrast to NLO predictions where missing higher
logarithmic terms still lead to sizeable corrections. A similar
behaviour is observed also in the $WW$ and $ZZ$ channels.


\begin{figure*}[ht]
\centering
\includegraphics[width=0.45\textwidth]{./WGx/FXS/figs/plot_cpTJ1_aa.pdf}
\caption{ \label{fig:hjetcml}
Cumulative jet $p_\perp$
Distribution in the fiducial volume at NLO (yellow) and NNLO (blue) for $H\to \gamma\gamma$. Both NLO and NNLO
curves obtained with NNLO PDFs and $\alpha_s$. Solid line: value for $\mu_r=\mu_f=m_H$. Filled band: scale uncertainty.
In the lower pane the ratio of NNLO to the NLO central $\mu_r=\mu_f$ value is shown. See text for details.
}
\end{figure*}

Next, we show in \refT{tab:hjetfid} the NLO and NNLO cross-sections in the fiducial region for
the $\gamma\gamma$, $WW$ and $ZZ$ channel. Note that since all leptons are massless in our 
computation, there is no distinction between $2e2\nu$ and $2\mu 2\nu$ predictions for the fiducial
region considered here. Our result show that the $K-$factor is similar for all the three channels, and
similar to the inclusive $K-$factor.

\begin{table*}[ht]
\caption{The NLO and NNLO cross-sections in the fiducial region for the $\gamma\gamma$, $WW$ and $ZZ$ channel.}
\label{tab:hjetfid}
\centering
\renewcommand{\arraystretch}{1.5}
\begin{tabular}{c|c|c|ccc}
\toprule
     & $\gamma \gamma$ & $WW\to e\mu \nu\nu$ & \multicolumn{3}{c}{$ZZ\to 4l$} \\
%\cmidrule{2-6}
     &                 &                     & $4\mu$ & $2e2\mu$ & $4e$ \\
\midrule
% NLO (fb)  & $    19.1^{+     3.7}_{-     3.3}$ & $14.9^{+     3.0}_{-     2.5}$     & $0.336^{+   0.065}_{-   0.057}$ & $0.611^{+   0.120}_{-   0.103}$ & $   0.282^{+   0.055}_{-   0.048}$         \\
% NNLO (fb) & $    22.6^{+     4.4}_{-     3.9}$ & $17.6^{+     3.5}_{-     3.0}$     & $0.396^{+   0.077}_{-   0.067}$ & $   0.721^{+   0.141}_{-   0.121}$       & $   0.333^{+   0.065}_{-   0.056}$         \\
NLO (fb)  & $    19.1^{+     3.8}_{-     3.2}$ & $14.9^{+     3.0}_{-     2.5}$     & $0.336^{+   0.065}_{-   0.057}$ & $0.611^{+   0.120}_{-   0.103}$ & $   0.282^{+   0.055}_{-   0.048}$         \\
NNLO (fb) & $    22.7^{+     1.4}_{-     1.9}$ & $17.9^{+     1.0}_{-     1.6}$     & $   0.388^{+   0.010}_{-   0.030}$ & $   0.707^{+   0.020}_{-   0.055}$       & $   0.327^{+   0.008}_{-   0.026}$         \\
\bottomrule
\end{tabular}
\end{table*}

Finally, we present result for selected differential distributions in
the fiducial region, for the $\gamma\gamma$
(Figs.~\ref{fig:hjetaa01},\ref{fig:hjetaa02},\ref{fig:hjetaa03},\ref{fig:hjetaa04}),
$WW$ (Figs.~\ref{fig:hjetww01},\ref{fig:hjetww02},\ref{fig:hjetww03}) and $ZZ$
(Figs.~\ref{fig:hjetzz01},\ref{fig:hjetzz02}).
Note that for the $ZZ$ channel we only show results for the $4\mu$ sub-channel and
for lepton observables.
Results for other sub-channels are very similar to these ones, and results for
jet observables are very similar to the $WW$ case already shown.

\begin{figure*}[ht]
\centering
\includegraphics[width=0.45\textwidth]{./WGx/FXS/figs/plot_yH_aa.pdf}
\hfill
\includegraphics[width=0.45\textwidth]{./WGx/FXS/figs/plot_pTH_aa.pdf}
%
\caption{ \label{fig:hjetaa01} Distribution in the fiducial volume at
  NLO (yellow) and NNLO (blue) for $H\to \gamma\gamma$. Left:
  di-photon rapidity. Right: di-photon $p_\perp$. Both NLO and NNLO
  curves obtained with NNLO PDFs and $\alpha_s$. Solid line: value for
  $\mu_r=\mu_f=m_H$. Filled band: scale uncertainty. See text for
  details.   }
\end{figure*}

\begin{figure*}[ht]
\centering
\includegraphics[width=0.45\textwidth]{./WGx/FXS/figs/plot_pTJ1_aa.pdf}
\hfill
\includegraphics[width=0.45\textwidth]{./WGx/FXS/figs/plot_yJ1_aa.pdf}
%
\caption{ \label{fig:hjetaa02} Distribution in the fiducial volume at
  NLO (yellow) and NNLO (blue) for $H\to \gamma\gamma$. Left: leading
  jet $p_\perp$.  Right: leading jet rapidity. Both NLO and NNLO
  curves obtained with NNLO PDFs and $\alpha_s$. Solid line: value for
  $\mu_r=\mu_f=m_H$. Filled band: scale uncertainty. See text for
  details.   }
\end{figure*}

\begin{figure*}[ht]
\centering
\includegraphics[width=0.45\textwidth]{./WGx/FXS/figs/plot_HT_aa.pdf}
\hfill
\includegraphics[width=0.45\textwidth]{./WGx/FXS/figs/plot_dyJH_aa.pdf}
%
\caption{ \label{fig:hjetaa03} Distribution in the fiducial volume at
  NLO (yellow) and NNLO (blue) for $H\to \gamma\gamma$. Left:
  $H_\perp$. Right: rapidity difference between the di-photon system
  and the leading jet. Both NLO and NNLO curves obtained with NNLO
  PDFs and $\alpha_s$. Solid line: value for $\mu_r=\mu_f=m_H$. Filled
  band: scale uncertainty. See text for details.}
\end{figure*}

\begin{figure*}[ht]
\centering
\includegraphics[width=0.45\textwidth]{./WGx/FXS/figs/plot_costs_aa.pdf}
%
\caption{ \label{fig:hjetaa04} $\cos\theta^*$ distribution in the
  fiducial volume at NLO (yellow) and NNLO (blue) for $H\to
  \gamma\gamma$. Both NLO and NNLO curves obtained with NNLO PDFs and
  $\alpha_s$. Solid line: value for $\mu_r=\mu_f=m_H$. Filled band:
  scale uncertainty. See text for details.}
\end{figure*}


\begin{figure*}[ht]
\centering
\includegraphics[width=0.45\textwidth]{./WGx/FXS/figs/plot_pTH_ww.pdf}
\hfill
\includegraphics[width=0.45\textwidth]{./WGx/FXS/figs/plot_yll_ww.pdf}
%
\caption{ \label{fig:hjetww01} Distribution in the fiducial volume at
  NLO (yellow) and NNLO (blue) for $H\to WW \to e\mu \nu\nu$. Left:
  Higgs boson $p_\perp$.  Right: di-lepton rapidity. Both NLO and NNLO
  curves obtained with NNLO PDFs and $\alpha_s$. Solid line: value for
  $\mu_r=\mu_f=m_H$. Filled band: scale uncertainty. See text for
  details.   }
\end{figure*}

\begin{figure*}[ht]
\centering
\includegraphics[width=0.45\textwidth]{./WGx/FXS/figs/plot_pTJ1_ww.pdf}
\hfill
\includegraphics[width=0.45\textwidth]{./WGx/FXS/figs/plot_dphi_ww.pdf}
%
\caption{ \label{fig:hjetww02}
Distribution in the fiducial volume at NLO (yellow) and NNLO (blue) for $H\to WW \to e\mu \nu\nu$.
Left: leading jet $p_\perp$. Right: di-lepton azimuthal separation. Both NLO and NNLO
curves obtained with NNLO PDFs and $\alpha_s$. Solid line: value for $\mu_r=\mu_f=m_H$. Filled band: scale uncertainty. See text for details.
}
\end{figure*}

\begin{figure*}[ht]
\centering
\includegraphics[width=0.45\textwidth]{./WGx/FXS/figs/plot_ETmiss_ww.pdf}
\hfill
\includegraphics[width=0.45\textwidth]{./WGx/FXS/figs/plot_mT_ww.pdf}
%
\caption{ \label{fig:hjetww03} Distribution in the fiducial volume at
  NLO (yellow) and NNLO (blue) for $H\to WW \to e\mu \nu\nu$. Left:
  $E_{\perp,\mathrm{miss}}$. Right: $WW$ transverse mass
  $m_\perp$. Both NLO and NNLO curves obtained with NNLO PDFs and
  $\alpha_s$. Solid line: value for $\mu_r=\mu_f=m_H$. Filled band:
  scale uncertainty. See text for details.}
\end{figure*}


\begin{figure*}[ht]
\centering
\includegraphics[width=0.45\textwidth]{./WGx/FXS/figs/plot_pTH_zz.pdf}
\hfill
\includegraphics[width=0.45\textwidth]{./WGx/FXS/figs/plot_yH_zz.pdf}
%
\caption{ \label{fig:hjetzz01} Distribution in the fiducial volume at
  NLO (yellow) and NNLO (blue) for $H\to ZZ \to 4\mu$. Left: Higgs boson $p_\perp$.  
  Right: Higgs boson rapidity. Both NLO and NNLO curves obtained
  with NNLO PDFs and $\alpha_s$. Solid line: value for
  $\mu_r=\mu_f=m_H$. Filled band: scale uncertainty. See text for
  details.   }
\end{figure*}

\begin{figure*}[ht]
\centering
\includegraphics[width=0.45\textwidth]{./WGx/FXS/figs/plot_m34_zz.pdf}
\hfill
\includegraphics[width=0.45\textwidth]{./WGx/FXS/figs/plot_costs_zz.pdf}
%
\caption{ \label{fig:hjetzz02} Distribution in the fiducial volume at
  NLO (yellow) and NNLO (blue) for $H\to ZZ \to 4\mu$. Left: sub-leading
  di-lepton invariant mass. Right: $\cos\theta^*$. Both NLO and
  NNLO curves obtained with NNLO PDFs and $\alpha_s$. Solid line:
  value for $\mu_r=\mu_f=m_H$. Filled band: scale uncertainty. See
  text for details.   }
\end{figure*}




%\bibliographystyle{atlasnote}
%\bibliography{hjetfidu}

%\end{document}
