

Sometimes in the experimental analyses the fiducial cross sections are
extrapolated onto the full phase space by simulating the relevant
geometric acceptances by means of various Monte Carlo generators in
order to quote a total cross section for the underlying process
(cf. Ref.~\cite{Aad:2015lha} and Section~\ref{sec:exp:xsec_comb}).
%
In view of the high precision expected in Run 2 fiducial measurements,
it is of primary relevance to provide the experiments with accurate
theory predictions for two reasons.
%
On the one hand, it is necessary to perform an comparison to data at
the fiducial level, before extrapolating to the inclusive phase space,
and to quote the relative measurement publicly. 
%
On the other hand, it is important to study how the tools used in the
comparison and in the extrapolation behave in the presence of the
fiducial cuts, in order to avoid the propagation of unwanted
generator-dependent effects into the quoted total cross sections.
%
In order to have a robust control over the extrapolation procedure, it
is in fact necessary to assess precisely the performance of the event
generators as well as the theoretical uncertainties associated with
such simulations, both at the perturbative and non-perturbative level.
%


%
The ultimate and future goal of this section of the Task Force is to
perform a comprehensive comparison and validation of the predictions
obtained with the event generators currently used in experimental
analyses to the best available results.
%

The development of several computational techniques in the past few
years considerably improved the known perturbative accuracy of many of
the relevant signal and background processes. In the rest of this
section we provide templates for the fiducial volumes relative to the
three final states $\gamma\gamma$, $\PW\PW^{*}\to 2\Pl 2\PGn$, $\PZ\PZ^{*}\to
4\Pl$, followed by the state-of-the-art predictions for the signal and,
whenever possible, background processes.
%
As far as the signal is concerned, since the template fiducial volumes
that will be given below are dominated by the gluon fusion production
mode, we limit ourselves to considering the latter in this section. A
comprehensive review of the available predictions and public tools can
be found in Chapter~\ref{chap:ggF} of this volume.
%
The contribution of additional production modes, notably VBF and VH
associate production, should be also taken into account when
considering the fiducial template volumes presented here. A review of
the available results for both QCD and EW effects can be found in the
Chapter~\ref{chap:VBF+VH} of the present volume. However, considering
the moderate size of the latter production channels in comparison to
the gluon-fusion mode, it is necessary to define different sets of
fiducial cuts which enhance their contribution.
%
This study is addressed in Chapter~\ref{chap:VBF+VH} of this report,
and it will be considered in the future by this Task Force.
%
Analogously, the precise study of subdominant contributions to a given
fiducial volume may require the definition of multiple specific
fiducial sub-categories, with the goal of enhancing different
kinematic regimes and increasing the experimental
sensitivity. Examples of these regions are the tails of the
differential distributions, or the off-shell production regime.


In the rest of this section, we classify the signal predictions into
two categories, according to their jet multiplicity: totally inclusive
in the number of QCD jets, and with at least one jet. For such
processes NNLO QCD predictions for the fiducial cross sections and
distributions will be reported in
Sections~\ref{sec:hres-YR4},~\ref{sec:hjet} below.
%
Since some analyses use the theory calculations to perform the
 background subtraction, the same type of validation is necessary for
 the relevant background processes. As far as the irreducible
 background reactions are concerned, while in $\gamma\gamma$
 production a data-driven fit is used to estimate the background, in
 the $\PZ\PZ^*$ and $\PW\PW^*$ final states a precise theoretical calculation
 is necessary. Section~\ref{sec:ZZ-YR4} reports NNLO QCD predictions for
 $\PZ\PZ^*\to 4\Pl$ production, while the $\PW\PW^*$ case is left for future
 work.

A similar comprehensive benchmarking in the presence of exclusive cuts
on the QCD activity accompanying Higgs boson production is being conducted
within the context of the Les Houches 2015 Workshop~\cite{Badger:2016bpw}. We
conclude this section with a summary of the detailed comparisons which
have been carried out. The future validation of tools in the presence
of realistic fiducial cuts should benefit from the interaction and
coordination of activities between the two working groups.


