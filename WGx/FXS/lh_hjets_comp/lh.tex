\subsection[QCD activity associated with Higgs  production in gluon fusion]{Comparison of the description of QCD activity associated with Higgs  production in gluon fusion} 
\label{sec:lh}
%\SectionAuthor{S.\ Badger, J.\ Huston, M.\ Sch\"onherr}

In this section we present a brief summary of the extensive comparison of 
the description of QCD activity in association with Higgs boson production in 
gluon fusion, undertaken within the context of the Les Houches 2015 
workshop~\cite{Badger:2016bpw}. It aims at comparing the description of the QCD 
activity accompanying the production of a Higgs boson in gluon fusion. 
To this end contributions obtained from from a multitude of authors 
applying different approximations and calculational schemes were subjected 
to a comprehensive list of inclusive and successively exclusive observables. 
To facilitate the comparison a common setup was adopted. 
Beside working in the pure Higgs effective theory (HEFT) in the 
$m_t\to\infty$ limit, scales reducing to $\tfrac{1}{2}\,\MH$ in the 
zero jet limit were adopted along with the common PDF sets 
\texttt{MMHT2014nlo68clas0118}/\texttt{MMHT2014nnlo68cl} \cite{Harland-Lang:2014zoa}, 
as appropriate, with $\alpha_s(\MZ)=0.118$ were used where possible. 
It is important to stress that for many tools this does not necessarily 
constitute their respective best setup, but was adopted for the sake of 
the comparison.
%

\noindent
The contributions comprise the analytical resummations of 
\begin{itemize}
  \item \textsc{HqT} \cite{Bozzi:2005wk,deFlorian:2011xf} for the Higgs boson 
transverse momentum, 
  \item STWZ \cite{Stewart:2013faa} for the jet veto cross sections, the leading jet 
        transverse momentum, the inclusive cross section and the exclusive 
        zero jet cross section, 
  \item \textsc{ResBos}~2 \cite{Wang:2012xs,Sun:2016kkh} for inclusive 
        zero and one jet observables, 
\end{itemize}
the fixed-order computations of 
\begin{itemize}
  \item \textsc{Sherpa Nnlo} \cite{Gleisberg:2008ta,Hoche:2014dla} calculation 
        of $pp\to h+X$, 
  \item BFGLP \cite{Boughezal:2015aha,Boughezal:2015dra} NNLO  calculation 
        of $pp\to h+j+X$, 
  \item \textsc{GoSam+Sherpa} \cite{Cullen:2011ac,Cullen:2014yla,
          Gleisberg:2008ta,Gleisberg:2008fv} NLO calculation of 
        $pp\to h+1,2,3j+X$ \cite{Cullen:2013saa,Greiner:2015jha}, 
        here also a \textsc{MiNlo} \cite{Hamilton:2012np} and 
        \textsc{LoopSim} \cite{Rubin:2010xp} (labelled nNLO) calculation 
        are available
\end{itemize}
the \textsc{NnloPs} matched computations of 
\begin{itemize}
  \item \texttt{Powheg} \textsc{NnloPS} \cite{Hamilton:2013fea}, showered 
        with \textsc{Pythia~8.253} \cite{Sjostrand:2014zea},
  \item \textsc{Sherpa NnloPs} \cite{Gleisberg:2008ta,Hoche:2014dla},
\end{itemize}
the \textsc{Nlo} multijet merged computations of 
\begin{itemize}
  \item \texttt{MG5\_aMC@NLO} in the FxFx scheme \cite{Alwall:2014hca,Frederix:2012ps}, 
  showered with \textsc{Pythia 8.210} \cite{Sjostrand:2014zea}
  \item \textsc{Sherpa} in the \textsc{MePS@Nlo} scheme \cite{Gleisberg:2008ta,
          Hoeche:2012yf,Hoeche:2014lxa,Hoeche:2011fd,Hoche:2012wh} using 
          one-loop matrix elements from \textsc{GoSam} \cite{Cullen:2011ac,
          Cullen:2014yla,Cullen:2013saa,Greiner:2015jha},
  \item \textsc{Herwig 7.1} in the unitarized merging scheme \cite{Bellm:2015jjp,
          Platzer:2012bs,Lonnblad:2012ix} using its dipole shower 
          \cite{Platzer:2009jq} and matrix elements from 
          \texttt{MG5\_aMC@NLO} \cite{Alwall:2014hca} and 
          \textsc{OpenLoops} \cite{Cascioli:2011va},
\end{itemize}
and the BFKL resummation of 
\begin{itemize}
  \item \textsc{Hej} \cite{Andersen:2009nu,Andersen:2011hs,Andersen:2008ue} 
        describing $pp\to h+2j+X$,
\end{itemize}
and therefore cover a large space of calculations available. Uncertainties 
are determined varying the appropriate scales, cf.\ \cite{Badger:2016bpw} for details. 

\begin{figure}
  \begin{minipage}{0.32\textwidth}
    \includegraphics[width=\textwidth]{./WGx/FXS/figs/NJet_incl_30}
  \end{minipage}
  \begin{minipage}{0.32\textwidth}
    \includegraphics[width=\textwidth]{./WGx/FXS/figs/jet1_pT_incl}
  \end{minipage}
  \begin{minipage}{0.32\textwidth}
    \lineskip-1.35pt
    \includegraphics[width=\textwidth]{./WGx/FXS/figs/xs_jet_veto_j0}\\
    \includegraphics[width=\textwidth]{./WGx/FXS/figs/ratiopanelplaceholder}\\
    \includegraphics[width=\textwidth]{./WGx/FXS/figs/ratiopanelplaceholder}
  \end{minipage}
  \caption{
    Example comparisons of the description of QCD activity accompanying 
    Higgs boson production in gluon fusion, taken from~\cite{Badger:2016bpw}. 
    The inclusive jet multiplicity (top left), the leading jet 
    transverse momentum (right) and the jet vetoed inclusive cross 
    section (bottom left) are shown. The ratio panels compare to the 
    appropriate reference as indicated. See text for details.
    \label{fig:lh_hjets_comp:njet_ptj1_jvxs}
  }
\end{figure}

To facilitate comparisons with as diverse calculations as possible we 
consider inclusive Higgs boson production with no restriction on its decay 
products. Jets are identified using the anti-$k_{\mathrm{T}}$ algorithm 
\cite{Cacciari:2008gp} with $R=0.4$ and are required to have $\pT>30\,\mathrm{GeV}$ 
and $|\eta|<4.4$. An implementation in \textsc{Rivet} \cite{Buckley:2010ar} exists. 
\refF{fig:lh_hjets_comp:njet_ptj1_jvxs} displays exemplary results 
of this comparison for three observables of interest out of the 79 observables 
considered: the inclusive jet multiplicity, the leading jet transverse 
momentum, and the jet vetoed inclusive cross section. All plots show 
a main plot accompanied by multiple ratio plots, grouping the individual 
contributions to ease the comparison. Noteworthy in the observables 
exhibited here are the agreement 
in the inclusive $n_j\ge 1$ cross section between all tools considered 
which possess at least NLO accuracy in this observable. While for $n_j\ge 2$ 
all predictions including parton showering agree well, for $n_j\ge 3$ the 
spread is larger. However, it has to be kept in mind that the \textsc{NnloPs} 
matched calculations revert leading order accuracy for $n_j\ge 2$ and to 
pure parton shower accuracy for $n_j\ge 3$ while 
\texttt{MG5\_aMC@NLO} and \textsc{Herwig 7.1} are NLO 
and LO accurate there, respectively. Only, the \textsc{GoSam+Sherpa} and the 
\textsc{Sherpa MePs@Nlo} prediction possess NLO accuracy in both cases. 
\textsc{Hej}, being LO accurate for $n_j\ge 2,3$, predicts slightly 
different inclusive jet multiplicities. 
The leading jet transverse momentum spectrum shows a consensus between 
(almost) all parton shower matched and/or multijet merged calculations 
exhibiting NLO accuracy for this observable: \texttt{Powheg} \textsc{NnloPs}, 
\textsc{Sherpa Nnlops}, \texttt{MG5\_aMC@NLO}, 
\textsc{Sherpa MePs@Nlo} and \textsc{Herwig 7.1}. The parton level calculations 
deviate mostly due to the individual scales set. However, the pure fixed 
order NLO and NNLO calculation, employing the same scale, agree very well, 
indicating a $K$-factor very close to unity. Finally, the 
degree of congruence in the jet vetoed inclusive cross section between the 
\textsc{NnloPs} matched calculations and the STWZ dedicated resummation is 
remarkable. Here, the NLO multijet merged tools primarily suffer from their 
NLO normalization in the $\pT^{\mathrm{veto}}\to\infty$ limit.
Generally, a remarkable level of agreement is found between the individual 
calculations throughout most observables.

