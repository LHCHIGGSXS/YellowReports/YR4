New physics beyond the SM could affect Higgs physics in total rates
(including differences in efficiencies of selection cuts) and
differential distributions.  While one can attempt to isolate effects
in either production or decay processes, a full BSM scenario typically
affects both simultaneously.  (Differential) fiducial cross sections
are an appropriate complementary tool for scrutinizing the Lagrangian
structure of the Higgs boson interactions, for instance through tests for
new tensorial couplings, non-standard production modes, and effective
form factors. In addition to the measurement of specific fiducial
regions, the combined analysis of all available fiducial measurements
in a global fit seems a promising approach.

Below we address the effects that can be expected in a variety of BSM
scenarios and we discuss interesting distributions and fiducial
regions that can be used to target them. Unless otherwise specified,
we will not consider any generic fiducial cuts on the results shown
below. The eventual feasibility to measure such regions in a model
independent way needs though to be scrutinized by experimentalists,
since in some cases the poor resolution for some of the observable
used to define the fiducial volumes could lead to non-negligible
migration effects, cf.~\refS{sec:exp:fid}.
%
We also note that a parallel effort is required from the theory
community, in obtaining predictions with adequate precision, and a
robust determination of the associated uncertainties, and their
correlations, within the theoretical framework used in the comparison
to the measurements, be it specific BSM models or effective Lagrangian
descriptions. The status of the presently existing tools is discussed 
in the various other sections of this report. 
%in \textcolour{blue}{[refer to other chapters of YR4: e.g. \refS{sec:VBF-XS} (VBF) and \refS{sec:VH-XS} (VH) in WG1]}


\subsection{Higgs boson production in gluon fusion}

Higgs boson production in gluon fusion has one specific feature which allows
us to test to what degree the observed Higgs is described by the
SM: its production amplitude at one loop is mediated by 
virtual top quarks, which means that any new, strongly interacting particle can
lead to order-one corrections to the effective Higgs-gluon
coupling. The relevant Lagrangian describing this channel is 
%
\begin{align}
\mathcal{L}
= \mathcal{L} \Bigg|_{\kappa_j =0} + 
\left[  \kappa_{\PQt} \; g_{\Pg\Pg\PH} + \kappa_{\Pg}\frac{\alpha_s}{12 \pi}  \right] \; 
\frac{\PH}{v} \; 
G_{\mu\nu}G^{\mu\nu}
- \kappa_{\PQt} \; 
\frac{\Mt}{v} \PH \left( \PAQt_R \PQt_L + \mathrm{h.c.} \right)
\; . 
\label{eq:fxs-bsm-lagrangian}
\end{align}
%
In combination with a second measurement of the top Yukawa
coupling, for example in $\ttbar\PH$ production, already a total rate
measurement in this largest Higgs boson production channel can constrain
particles like light top partners.

While the Lagrangian of \eqn{eq:fxs-bsm-lagrangian} only features shifts
in SM-like Higgs boson couplings, the interplay between the renormalizable
top Yukawa coupling and the dimension-6 Higgs-gluon coupling can
affect kinematic distributions.  For example, the reach for new
particles contributing to the effective Higgs-gluon coupling can be
enhanced by adding off-shell Higgs boson production to the set of
measurements~\cite{Campbell:2011cu,Kauer:2012hd,Caola:2013yja}. Strictly speaking, off-shell Higgs
production with a subsequent decay $\PH \to 4\Pl$ is best described by
a shape analysis of the $\Mfourl$ distribution.  Based on the
Lagrangian of \eqn{eq:fxs-bsm-lagrangian} we can write the complete
gluon-induced amplitude $\Pg\Pg\to \PZ\PZ$ as
%
\begin{alignat}{5}
\mathcal{M}_{\PZ\PZ} & =
\kappa_{\PQt} \mathcal{M}_{\PQt} + \kappa_{\Pg}\mathcal{M}_{\Pg} + \mathcal{M}_c  \; ,
\label{eq:fxs-bsm-amplitude_offshell}
\end{alignat}
%
where the last term arises from the Higgs-independent continuum
diagram. Numerically, the interference between the Higgs and continuum diagrams 
is one of the key features in the measurement of off-shell Higgs
effects at the LHC.  
While this phase space region is not included in the template fiducial regions in \refS{sec:fiducialvolumes}, 
considering it is extremely beneficial~\cite{Aad:2015rka}.%
\footnote{Off-shell Higgs boson production and Higgs interference are discussed in detail in \refC{chap:offshell_interf} of this report.}
%\footnote{Indeed a measurement of four-lepton production from the decays of resonant $\PZ$ and Higgs bosons and the non-resonant $\PZ\PZ$ continuum was already done by ATLAS at $\sqrt{s}=8\UTeV$ in~\cite{Aad:2015rka}.}
The longitudinal components to the different
contributions feature different dependences on $\Mfourl$, namely
%
\begin{alignat}{5}
\mathcal{M}_{\Pg}^{++00}
&\approx
-\frac{\Mfourl^2}{2\MZ^2} \qquad \qquad  
&&\mathrm{with} \;\; \Mt \gg \Mfourl \gg \MH,\MZ \notag \\
\mathcal{M}_{\PQt}^{++00}
&\approx
+\frac{\Mt^2}{2\MZ^2}
\log^2 \frac{\Mfourl^2}{\Mt^2}\qquad \qquad 
&&\mathrm{with} \;\; \Mfourl \gg \Mt \gtrsim \MH,\MZ \notag \\
\mathcal{M}_c^{++00}
&\approx
-\frac{\Mt^2}
{2\MZ^2}\log^2 \frac{\Mfourl^2}{\Mt^2} 
&&\mathrm{with} \;\; \Mfourl \gg \Mt \gtrsim \MZ \; .
\label{eq:fxs-bsm-m4l_tgc}
\end{alignat}
%
In the proper limit a logarithmic dependence on $\Mfourl/\Mt$ develops
far above the Higgs boson mass shell.  The ultraviolet logarithm cancels
between the correct Higgs amplitude and the continuum, ensuring the
proper ultraviolet behaviour of the full amplitude. The sensitivity due
to this logarithmic dependence on $\Mfourl/\Mt$, as shown in the left
panel of \refF{fig:fxs-bsm-m4l}, should allow us to extract the top
mass dependence of the observed signal from the $\Mfourl$
distribution. This means Higgs boson production is a good example of a
process where it is not clear where we can expect to best find BSM
effects: on the one hand, a precise measurement of the total rates of
inclusive Higgs boson production and $\PQt\PAQt\PH$ production can be
expected to constrain the Lagrangian if
\eqn{eq:fxs-bsm-lagrangian}. On the other hand, off-shell Higgs
production, or better the $\Mfourl$ distribution might well benefit
from its known dependence on the ratio $\Mfourl/\Mt$ in the Standard
Model, in that case relying on the tails of a momentum-related
kinematic distribution.

%%-------------------------------------------------------
%\begin{figure}\centering
% \includegraphics[width=0.44\textwidth]{./FXS/figs/m4l_gamma} \quad
% \raisebox{-4mm}{\includegraphics[width=0.47\textwidth]{./FXS/figs/pT_H_NLO_subsamples}}
% \caption{(left) Invariant $\Mfourl$ distributions for the process
%   $\Pg\Pg\rightarrow \PZ\PZ$ at $13\UTeV$, the result for
%   $\qqbar \rightarrow \PZ\PZ$ is shown for comparison.  (right)
%   $\pTH$ production including more than one jet recoiling against the
%   Higgs. Figure from
%   Ref.~\cite{Buschmann:2014sia}. \textcolour{red}{\em [improve
%     caption, explain what's NLO]} }
%\label{fig:fxs-bsm-m4l} 
%\end{figure}
%%-------------------------------------------------------

%-------------------------------------------------------
\begin{figure}\centering
 \includegraphics[width=0.44\textwidth]{./WGx/FXS/figs/m4l_gamma} \quad
 \raisebox{-4mm}{\includegraphics[width=0.47\textwidth]{./WGx/FXS/figs/pT_H_NLO_subsamples}}
\caption{(left) Invariant $\Mfourl$ distributions for the process
  $\qqbar (\Pg\Pg)\rightarrow \PZ\PZ$ at $13\UTeV$, obtained with MCFM~\cite{Campbell:2016jau} (right) $\pTH$
  including jets recoiling against the on-shell Higgs. We merge 0-jet and 1-jet production to NLO with
  the full top mass dependence, 2-jet production to LO with the full top mass dependence, and parton
  shower effects. Figure from Ref.~\cite{Buschmann:2014sia}. }
\label{fig:fxs-bsm-m4l}
\end{figure}
%-------------------------------------------------------


\subsection{Boosted Higgs boson production in gluon fusion}

An alternative way to search for BSM effects in gluon-fusion Higgs boson production 
is to require a large boost of the Higgs to generate a
large momentum flow through its production vertex. The leading
partonic signal process is $\Pg\Pg \to \PH\Pg$, where the $2 \to 2$
kinematics defines the momentum flow through the Higgs vertex for
example in terms of $\pTH$ or, equivalently, $\pTj$. When a second jet
is considered, the $\pTH$ spectrum provides the information about this
production mode. Following the Lagrangian given in
\eqn{eq:fxs-bsm-lagrangian} we can again compute the leading
${\cal O}(\alpha_s)$ dependence on the ratio
$\pTH/\Mt$~\cite{Buschmann:2014twa,Buschmann:2014sia},
%
\begin{alignat}{5}
|\mathcal{M}_{\PH j(j)}|^2 
\propto 
\Mt^4 \; \log^4 \frac{(\pTH)^2}{\Mt^2} \; .
\label{eq:fxs-bsm-pt_log}
\end{alignat}
%
The effect of the different partonic sub-processes is shown in the
right panel of \refF{fig:fxs-bsm-m4l}. In this observable, sizeable top mass effects already appear for $\pTH > 250\UGeV$. In the tail of the distributions, where for $\pTH > 500\UGeV$ the SM expectations have dropped below a per mille of all events, the corrections from the new dimension-6 Higgs-gluon operator dominate the distribution. The sensitivity of off-shell Higgs boson production and boosted Higgs boson production in terms of the modified Lagrangian of \eqn{eq:fxs-bsm-lagrangian} can be compared. Because both methods rely on a small number of events in the tail of the kinematic distribution, large luminosities will be needed to detect sizeable deviations from the SM.
%The H+jets channel (i.e. Higgs boson production in association with a high-$p_T$ jet) provides information on BSM, notably any new physics affecting the gluon fusion production.  As with VH, the inherent boost of the Higgs in this channel enhances the sensitivity to new physics. 

Going beyond the description in terms of dimension-6 operators, benchmarks for searches in this channel are coloured partners to the top, which participate in gluon fusion but whose contribution cancels at leading order. In this case, the $\PH+j$ channel provides the best handle on BSM. Examples of this situation-- cancellation of effects at LO Higgs boson production-- are fermionic top-partners in Composite Higgs models~\cite{Banfi:2013yoa,Azatov:2013xha} and stops in the so-called {\it funnel region}~\cite{Espinosa:2012in,Grojean:2013nya}. 

In the right plot of \refF{fig:fxs-bsm-VH_Hj} we show the $\pTj$
distribution in the SM case (blue), and the effect of introducing
top-partners of different masses ($M_T$) and mixing angles
($\sin \theta$). This distribution has been obtained using a modified
version of MCFM~\cite{Campbell:2016jau} including the effect of top partners, see
Ref.~\cite{Banfi:2013yoa} for details. Note that the lowest order
effect in gluon fusion of these top-partners is exactly cancelled by
the top, due to a low-energy
theorem~\cite{Ellis:1975ap,Shifman:1979eb,Kniehl:1995tn}, hence
information on these new particles is delegated to the $\PH$+jets
channel, or parametrically small finite-mass effects.


\subsection{VH associated production}

In this channel, the Higgs recoils against a vector boson, and thus it
has an inherent boost. This boost enhances the momentum-dependent
effects of New Physics with respect to the SM production, where the
$\PV\PV\PH$ interaction has no momentum dependence. Because the signal
process has a simple $2 \to 2$ kinematic structure, the description of
these effects in terms of $\pTH$ or $\pTV$, or $\MVH$ is, from a
theory perspective, essentially equivalent. The relevant question is
for which observable the experimental boundary conditions allow for
the best coverage of phase space.

Generally speaking, the higher the momentum of the Higgs and vector
boson one has access to, the higher the sensitivity to BSM
effects~\cite{Ellis:2012xd,Ellis:2013ywa}. In the left plot of
\refF{fig:fxs-bsm-VH_Hj} we show this dependence in the context of an
EFT approach to BSM performed at NLO QCD accuracy in {\sc POWHEG} and
showered through {\sc PYTHIA}v8~\cite{Mimasu:2015nqa}.  The selection
criteria applied to produce this plot are jets using $k_T$ algorithm
of $\Delta R=0.4$, $\pT > 25\UGeV$ and $|\eta_j| < 2.5$, 2 $\PQb$-jets
with $\pT > 25\UGeV$ and $|\eta_{\PQb}| < 2.5$, and 1 lepton
($\Pl=\Pe$ or $\PGm$) with $\pT > 25\UGeV$, and $|\eta_{\Pl}| <
2.5$.
Needless to say, in this channel there is a strong correlation between
the distributions in $\pTH$ (or $\pTV$) and the invariant mass of the
system, $\MVH$ for resolved final states or $m_{T}$ in the channels
with missing energy, see Figure~10 in~\cite{Ellis:2014dva} and Figure~3
in~\cite{Biekotter:2016ecg}.

\begin{figure}%\centering
   \hspace*{-5mm}
   \includegraphics[width=.54\textwidth]{./WGx/FXS/figs/PTH_WH.pdf}\hspace*{-4mm}%
   \includegraphics[width=.5\textwidth]{./WGx/FXS/figs/ptspectra14_bis.pdf}	
   \caption{ (left) Comparison of differential distribution of
       the Higgs $\pT$ in the SM and the two EFT benchmarks of $\bar
       c_W=0.004$ and $\bar c_W=-\bar c_{HW}=-0.004$ using {\sc
         Powheg} + {\sc Pythia8} in the process $\Pp\Pp\to \PWp \PH \to
       \Plp \PGn \bbbar$. The lower panel show the percentage
       deviation of the EFT benchmarks from the SM prediction,
       $\delta_{\mathrm{BSM}}$. Figure taken from
       Ref.~\cite{Mimasu:2015nqa}. (right) Jet $\pT$ differential
       distribution in the channel $\PH+j$, with the SM case in blue,
       and the effect of introducing top-partners of different masses
       ($M_T$) and mixing angles ($\sin \theta$) shown in other
       colours. Figure from Ref.~\cite{Banfi:2013yoa}. }
 \label{fig:fxs-bsm-VH_Hj}
 \end{figure}

In Run~1, the best limit on BSM phenomena using this channel  was obtained by looking at the last reported bin, the overflow bin, which typically contains a low number of events. Despite this, one can use the estimate of SM background to set limits on new physics~\cite{Ellis:2014dva,Corbett:2015ksa}. This procedure is analogous to searches for anomalous trilinear gauge couplings in $\PW\PW$ by looking at the overflow bin in the leading lepton $p_T$ distribution performed at LEP and now at the LHC~\cite{Chatrchyan:2013yaa,Chatrchyan:2013oev,Aad:2016wpd}. The combination of both sets of measurements would in fact be useful to enhance the sensitivity to anomalous trilinear couplings. 

The sensitivity to BSM obtained via the last bin raises questions on the validity of the EFT approach at high-momentum transfer, as in this region one {\it could} be able to resolve new physics effects. This question is discussed in Sections~\ref{s.eftval}~and~\ref{s.eftmodels}.
%"This question is discussed in Sections II.2.2 and II.2.5"
% \textcolour{blue}{[refer to relevant chapters (Chapter 14 Section 4 and Chapter 20?)]} of this report.  
Tools incorporating BSM (incl.\ EFT) effects at higher order in precision are discussed in Section~\ref{s.efttools}.
% are also discussed in other chapters of this report. %\textcolour{blue}{[refer here to tools sections]}.

\subsection{Vector boson fusion}

In several ways, vector boson fusion is the most prolific of the usual
Higgs boson production channels when it comes to measuring the properties of
the Higgs boson. The first reason is that its $2 \to 3$ kinematics
allows us to test a sizeable number of observables, including pure
tagging jet correlations; second, we can test modifications of the
gauge sector as well as modifications of the scalar or Higgs sector as
long as they affect the central $\PV\PV\PH$ coupling; third, it allows
us to separate the very specific Lorentz structure of the $\PV\PV\PH$
coupling in the SM from many modified structures induced by BSM
physics; and finally, as an electroweak process with further
suppressed QCD corrections we expect theoretical uncertainties to be
under better control than in the gluon-fusion process.

A prime example for a general test of the SM nature of the Higgs boson
is the direct test of the Lorentz structure of the $\PV\PV\PH$
coupling which can be performed via the measurement of the azimuthal
angle between the tagging jets~\cite{Plehn:2001nj}. While the SM
predicts a rather flat distributions, the typical CP-even and CP-odd
structures could be identified with essentially zero events at 90
degrees or for back-to-back configurations, respectively. Similarly,
the transverse momentum spectra of the tagging jets in the SM show a
strong peak below $\pTj \sim \MW/2$. Generally, the longitudinal or
transverse polarization of the gauge bosons affects this
spectrum~\cite{Brehmer:2014pka}, as originally computed in the
effective $\PW$ approximation~\cite{Dawson:1984gx}. Finally, the
rapidity difference of the two tagging jets is particularly large for
SM Higgs boson production, and the tagging jets should become significantly
more central if we change the $\PV\PV\PH$ coupling in any
way~\cite{Hagiwara:2009wt,Englert:2012xt,Djouadi:2013yb}.  The
important aspect of all these measurements (illustrated in
\refF{fig:fxs-bsm-jj}) is that they do not require a reconstructed
Higgs 4-momentum, provided that global cuts ensure that we are working
with a Higgs-rich event sample.  This implies that it is secondary how
we modify the $VVH$ coupling, \textsl{i.e.}  through a modification of
the Higgs quantum numbers or through higher-dimensional operators for
a SM-like Higgs.  On the other hand, adding information from the Higgs boson decay 
will of course enhance the power of these measurements, for
example re-formulating all questions originally asked in the framework
of $\PW\PW \to \PW\PW$ scattering at high
energies~\cite{Han:2009em,Kilian:2014zja,Brehmer:2014pka}.

%-------------------------------------------------------
\begin{figure}
 \includegraphics[width=0.24\textwidth]{./WGx/FXS/figs/spin0ew_dphijj}
 \hfill
 \includegraphics[width=0.24\textwidth]{./WGx/FXS/figs/wbf_pt1}
 \hfill
 \includegraphics[width=0.24\textwidth]{./WGx/FXS/figs/spin0ew_detajj}
 \hfill
 \includegraphics[width=0.24\textwidth]{./WGx/FXS/figs/spin0ew_detajx}
 \caption{Normalized correlations between the two tagging jets in VBF
   production of a SM-like Higgs and a CP-even or CP-odd scalar
   coupled through a higher-dimensional operator. We show the
   difference in the azimuthal angle $\Delta \phi_{jj}$, the tagging
   jet $p_T$, the rapidity difference between the tagging jets $\Delta
   \eta_{jj}$, and the rapidity difference between the tagging jet and
   the Higgs-like resonance $X$. Figure from
   Ref.~\cite{Englert:2012xt}.}
\label{fig:fxs-bsm-jj}
\end{figure}
%-------------------------------------------------------

In the spirit of the discussion of gluon fusion Higgs boson production and
VH associated production we focus on experimental tests enhanced by
the momentum flow through the Higgs boson production vertex.  We can link
the transverse momenta of the two tagging jets or the Higgs to the
virtuality of the weak bosons. Obviously, all of them are strongly
correlated, so it becomes a theoretical as well as experimental
question which of these observables to include in an
analysis~\cite{Biekotter:2016ecg}. The only key requirement is that we
do not bias these distributions for example by cutting on the tagging
jet correlations discussed before. In \refF{fig:fxs-bsm-VBF_monoH} we
present the $p_T$ of the Higgs, as a function of CP-violating
operators in an EFT approach to BSM. 
Basic cuts applied to these events are $m_{jj} > 400\UGeV$, $\Delta \eta_{jj}>$ 2.8 and $|\eta_j|< 4.5$.

\subsection{Invisible Higgs boson decays}

Once it is possible to experimentally target a specific Higgs
production mechanism in terms of fiducial cross sections, we can focus
on specific Higgs boson decay modes in this production mechanism. Arguably
the hardest Higgs boson decay mode to search for the LHC are invisible Higgs boson decays. 
The SM predicts a very small invisible Higgs boson branching ratio
through $\PH \to \PZ \PZ^* \to 4\PGn$, but for example in Higgs portal
models~\cite{Djouadi:2011aa} or in supersymmetry~\cite{Butter:2015fqa}
this decay can be an observable effect of weakly interacting dark
matter.  To date, searches for invisible Higgs boson decays in weak boson
fusion, \textit{i.e.} two tagging jets combined with missing
transverse momentum~\cite{Eboli:2000ze}, appear to be the most
promising strategy.  The key feature of this signature are two tagging
jets with exactly the same kinematics as in other VBF Higgs boson production
channels. This means that fiducial volumes are related to other VBF
studies by replacing the central Higgs boson decay products by missing
transverse momentum.

Current projections for different LHC luminosities are shown in
\refT{tab:fxs-bsm-reach}. The main background is $\PZ$+jets
production, with an invisible $\PZ$ decay. Two production mechanisms
contribute to the background, one at the order $\alpha_s^2 \alpha$ and
one at the order $\alpha_s \alpha^2$. The QCD-like channel can be
strongly reduced by a central jet veto, while weak boson fusion
$\PZ$-production is essentially irreducible, with some kinematic
differences for example in the azimuthal correlation of the tagging
jets reflecting the Lorentz structures of the $\PZ$ and $\PH$ production
vertices. For a success of this channel it is crucial to understand
the central jet activity, so the right columns of
\refT{tab:fxs-bsm-reach} should be considered a challenge to the
experimental performance of jet and particle-flow-like algorithms.  In
general, weak boson fusion signatures might also be extracted just
based on one tagging jet~\cite{Mellado:2004tj}, a channel which has
not (yet) been studied for invisible Higgs boson decays.

%---------------------------------------
\begin{table}
%\providecommand{\arraystretch}{1.}
\caption{Exclusion reach in $\textrm{BR}_{\textrm{inv}} = \Gamma_{\textrm{inv}}/\Gamma_H$ at 95\% CLs
  to an invisible Higgs boson width at various luminosities and different
  combinations of cuts and multivariate analyses. Here, $\Gamma_H$ is defined to be 
  the width of the Higgs boson in the SM without the additional invisible
  component due to new physics. Table from Ref.~\cite{Bernaciak:2014pna}.}
\label{tab:fxs-bsm-reach}
\centering
%\begin{small}
%\begin{tabular}{1.0\textwidth}{@{\extracolsep{\fill} }r|cccc|cc} 
\begin{tabular}{@{\extracolsep{\fill}}r|cccc|cc} 
\toprule
& \multicolumn{4}{c|}{$\pTj > 20\UGeV$} & \multicolumn{2}{c}{$\pTj > 10\UGeV$} \\ 
$\mathcal{L} [\text{fb}^{-1}]$   & VBF cuts & + jet veto  &  + $\Delta\phi_{jj}$ & BDT 2-jets & BDT 2-jets  & + BDT 3-jets \\ 
\midrule
10           & 1.02                & 0.49                       & 0.47                  & 0.28  & 0.18  & 0.16 \\
100          & 0.49                & 0.20                       & 0.18                  & 0.10  & 0.07  & 0.061 \\
3000         & 0.25                & 0.094                      & 0.069                 & 0.035 & 0.025 & 0.021 \\
 \bottomrule
\end{tabular}
%\end{small}
\end{table}
%---------------------------------------

\subsection{Mono-Higgs signatures}

An alternative way to probe the connection between the Higgs and Dark
Matter are channels where the Higgs recoils against missing energy,
\textit{i.e.} mono-Higgs signatures. Again, fiducial measurements are
closely linked to a SM signatures, $\Pp\Pp \to \PGn\PAGn\PH$ 
arising in the VH topology. Unlike for invisible Higgs boson decays, the
kinematical structure of BSM mono-Higgs events will not resemble that
in VH production. Instead, we expect significant deviations for
example in the distributions of the reconstructed Higgs. Nevertheless,
we would be able to utilize fiducial volumes defined for associated VH
production with only a transverse reconstruction of the gauge boson.

Studies at Run~1 on the mono-Higgs signature have been done in the context of the Higgs portal~\cite{Aad:2015yga}, and other extensions of the SM are now being considered. Particularly interesting distributions  are the transverse mass of the system or the $\pT$ distribution of the Higgs. In \refF{fig:fxs-bsm-VBF_monoH}, we show the Higgs $\pT$ distribution in events where the Higgs is produced in association with a pair of Dark Matter particles of mass 500 GeV at LHC13. The labels correspond to different assumptions of the coupling of Dark Matter to the Higgs sector with the blacks line the benchmark of standard Higgs portal coupling $\lambda_{hs} h^2 S^2$. Note that to produce this distribution, no cuts at generation level have been applied. 

\begin{figure}
   \includegraphics[width=.48\textwidth]{./WGx/FXS/figs/pTH_VBF.pdf} \quad %
   \raisebox{3mm}{\includegraphics[width=.47\textwidth]{./WGx/FXS/figs/DM_Diff_MonoH_500.pdf}}	
   \vspace*{1mm}
     \caption{Distributions of the Higgs $\pT$ in two channels. (left) Vector boson fusion, with the SM case represented by the solid distribution, and the SM plus additional modifications due to different values for the possible CP-violating operator $\tilde c_{HW}$~\cite{Alloul:2013naa} are given by the coloured lines. (right) The production of the Higgs boson in association with a pair of Dark Matter scalar particles of mass 500 GeV~\cite{Brivio:2015kia} at LHC13. The black line corresponds to the standard portal coupling $h^2 S^2$, and the other lines represent different contributions due to a non-linear nature of electroweak symmetry breaking. 
     }
 \label{fig:fxs-bsm-VBF_monoH}
 \end{figure}
