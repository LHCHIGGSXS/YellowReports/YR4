%\documentclass{article}
%\usepackage{graphicx}
%\usepackage{amsmath}
\def\ltap{\raisebox{-.6ex}{\rlap{$\,\sim\,$}} \raisebox{.4ex}{$\,<\,$}}
\def\gtap{\raisebox{-.6ex}{\rlap{$\,\sim\,$}} \raisebox{.4ex}{$\,>\,$}}
%\pagestyle{plain}
%\begin{document}

\subsection{Fiducial cross sections for Higgs boson production in   association with \texorpdfstring{$n_{\rm jet} \geq 1$}{n_jet >= 0} jets}
%\SectionAuthor{D.~de Florian, G.~Ferrera, M.~Grazzini, H.~Sargsyan, D.~Tommasini}
\label{sec:hres-YR4}

In this section we present results at $13$\,TeV for the fiducial cross
sections and some kinematic distributions for Higgs boson production,
inclusive in the number of QCD jets.
%
The calculation is performed with the {\ttfamily
  HRes}\,\cite{deFlorian:2012mx,Grazzini:2013mca} program, which
computes the cross section for SM Higgs boson ($\PH$) production by
gluon-gluon fusion. {\ttfamily HRes}\footnote{The program can be
  downloaded from Ref.\,\cite{hres}, together with some accompanying
  notes.} combines the fixed-order calculation of the cross section up
to NNLO with the resummation of the logarithmically-enhanced
contributions at small transverse momentum ($q_T$) up to NNLL accuracy
in QCD. The method that is used to perform the resummation is
presented in Ref.~\cite{Catani:2000vq}.


The produced Higgs boson subsequently decays into the three final
states $\PH\rightarrow \PGg\PGg$,
$\PH\rightarrow \PW\PW^* \rightarrow 2\Pl 2\PGn$ or
$\PH \rightarrow Z Z^* \rightarrow 4\Pl$.  In the case of
$\PH\rightarrow \PW\PW^*$ and $\PH\rightarrow \PZ\PZ^*$ decays, finite-width
effects of the vector bosons and the appropriate interference
contributions are also included.
%
The results below are generally obtained in the large-$m_t$
approximation, and the effects of top- and bottom-quarks in the
production are included following ref.~\cite{Grazzini:2013mca}.
%
The calculation retains the full kinematics of the Higgs boson and of
its decay products, allowing the user to apply arbitrary cuts on these
final-state kinematical variables, and to plot the corresponding
distributions in the form of bin histograms. Given that the
Higgs boson transverse momentum resummation is fully inclusive in the
QCD initial-state radiation, we consider the sets of fiducial volumes
presented in the previous subsection, ignoring the cuts on QCD
jets. This in particular affects the isolation requirements on photons
and charged leptons.

%%%%%%%%%%%%%%%%%

The parton distribution functions are chosen according to the
PDF4LHC15 recommendation~\cite{Butterworth:2015oua}, with densities
and $\alpha_S$ evaluated at each corresponding perturbative order.  As
for the electroweak parameters, we follow the LHCHXSWG
recommendations~\cite{Heinemeyer:2013tqa}, in particular we set the Higgs boson
mass at $\MH = 125$~\UGeV .  The renormalization and factorization
scales are fixed to the value $\mu_R=\mu_F=\MH$ while the resummation
scale is fixed at the value $Q=\MH/2$.

We start by considering the diphoton decay channel
$\PH\rightarrow\PGg\PGg$.  The cuts on final state photons are
reported in \refT{tab:fiducialyy}, and QCD jets are not considered
in the isolation criterion.
% The cuts that define the {\em fiducial} region are as follows.  The
% invariant mass $m_{\PGg\PGg}$ of the photon pair is required to
% be in the range 105~\UGeV $< m_{\PGg\PGg} <$ 160~\UGeV and the
% photons must be in the central rapidity region, with pseudorapidity
% $|\eta_\PGg|<2.5$, and with transverse momentum
% $p_{T,\PGg} > 25$~\UGeV.  For each event, we classify the photon
% transverse momenta according to their minimum and maximum value,
% $p_{T \mathrm{min}}$ and $p_{T \mathrm{max}}$, and we require that
% $p_{{T \mathrm{max}},\PGg}/m_{\PGg \PGg} > 0.35$ and
% $p_{{T \mathrm{min}},\PGg}/m_{\PGg \PGg} > 0.25$.
The corresponding fiducial cross sections are shown in
\refT{tab}, which reports the resummed results at NLL+NLO and
NNLL+NNLO level, and we compare them with the NNLO fixed order
predictions obtained with the {\ttfamily HNNLO}
code.\,\cite{Catani:2007vq}.

\begin{table}
  \caption{
    Fiducial cross sections for $p p\to \PH+ X \to \PGg\PGg+ X$, $p p\to \PH+ X \to W^+W^-  + X\to 2\Pl2\PGn + X$ and
    $p p\to \PH+ X \to \PZ\PZ  + X\to 4\Pl + X$ at the LHC ($\sqrt{s}=13$~TeV): fixed-order
    results at NNLO and corresponding resummed results at NLL+NLO and
    NNLL+NNLO. The result in the $W^+W^-$ channel refers to a single
    lepton family.
    The uncertainties are obtained by varying the scales $\mu_R=\mu_F$
    by a factor of two around the central value. For the resummed
    calculations, in addition, the resummation scale $Q$ is varied by
    a factor of two in either direction by keeping $\mu_R=\mu_F=2Q$.}
\label{tab}
\begin{center}
\begin{tabular}{lcccc}
\toprule
Cross section [fb]        &  NLL+NLO          &  NNLL+NNLO       &   NNLO             \\
\midrule
%Total [fb]
$\PH\to \PGg\PGg$    &  41.63$^{+9\%}_{-8\%}$& 54.2$^{+9\%}_{-8\%}$  &    54.6$^{+9\%}_{-8\%}$  \\
$\PH\to W^+W^- \to 2\Pl2\PGn$   & 34.99$^{+9\%}_{-8\%}$ & 45.4$^{+9\%}_{-8\%}$  &  46.0 $^{+9\%}_{-8\%}$  \\
$\PH\to \PZ\PZ \to e^+e^-\PGm^+\PGm^-$  & 0.792$^{+9\%}_{-8\%}$ &  1.042$^{+9\%}_{-8\%}$   &  1.042$^{+9\%}_{-8\%}$ \\
$\PH\to \PZ\PZ \to e^+e^-e^+e^-$    &   0.441$^{+9\%}_{-8\%}$  &  0.581$^{+9\%}_{-8\%}$   &    0.583$^{+9\%}_{-8\%}$  \\
$\PH\to \PZ\PZ \to \PGm^+\PGm^-\PGm^+\PGm^-$    &   0.521$^{+9\%}_{-8\%}$  &  0.685$^{+9\%}_{-8\%}$   &    0.687$^{+9\%}_{-8\%}$  \\
\bottomrule
\end{tabular}
\end{center}
\end{table}

We recall that at large values of $q_T$ the resummed calculation
implements a perturbative unitarity constraint\,\cite{Catani:2000vq},
that guarantees to exactly reproduce the NNLO value of the total cross
section for the Higgs boson production after integration over $q_T$.
However, kinematic cuts affects in a different way fixed order and
resummed predictions, leading to small differences in the fiducial
cross sections, as shown in \refT{tab}.
%
%Therefore, comparing the resummed and fixed order predictions reported
%in Tab.\,\ref{tab}, we see that there are small differences between
%the NNLL+NNLO and NNLO accepted cross section, due to the fact that
%the integration is performed over a wide kinematical range {\bf This
%  is very poor - add something}.
%

Figures~\ref{fig:pTgg} and~\ref{fig:pTglead} show some relevant
differential distributions. The left plot of \refF{fig:pTgg}
shows the diphoton transverse-momentum $p_T^{\PGg\PGg}$ distribution,
and the right plot shows the $p_T^{t,\PGg\PGg}$ distribution defined
as the magnitude of the transverse momentum of the diphoton system
perpendicular to the diphoton thrust axis $\hat t$.  The transverse
momentum distributions for the leading and subleading photons are
shown in \refF{fig:pTglead}. By inspecting these plots we see
that the resummed calculations are essential to restore the
predictivity of perturbation theory in the small-transverse-momentum
region. Moreover, NNLL resummation gives a sizeable effect, with
respect to the NNLO calculations, in a wide intermediate region up to
the chosen resummation scale $q_T \ltap \MH/2$. Finally in the
large-$q_T$ region ($q_T\sim \MH$), where the resummation does not
improve the accuracy of the fixed-order expansion, the NNLL+NNLO
predictions obtained with {\ttfamily HRes} show perfect agreement with
the NNLO ones.
%


\begin{figure} \centering
\includegraphics[width=0.485\textwidth]{./WGx/FXS/figs/ptgg.pdf}
\includegraphics[width=0.485\textwidth]{./WGx/FXS/figs/pttgg.pdf}
\caption{\label{fig:pTgg}{ Higgs boson production and diphoton decay
at the LHC.  Transverse-momentum distribution $p_T^{\PGg\PGg}$ (left)
and $p_T^{t,\PGg\PGg}$ distribution (right) obtained by resummed
NLL+NLO and NNLL+NNLO and fixed-order NNLO calculations.  Selection
cuts on the final-state photons are described in the text.  }}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.485\textwidth]{./WGx/FXS/figs/ptleadgg.pdf}
\includegraphics[width=0.485\textwidth]{./WGx/FXS/figs/ptsubleadgg.pdf}
\caption{\label{fig:pTglead}{ Higgs boson production and diphoton
    decay at the LHC.  Transverse-momentum distributions for the
    leading $p_{ T,{\rm lead}}$ (left) and subleading
    $p_{ T,{\rm sublead}}$ (right) photon obtained by resummed NLL+NLO
    and NNLL+NNLO and fixed-order NNLO calculations.  Selection cuts
    on the final-state photons are described in the text.  }}
\end{figure}

Next we consider the decay channels
$\PH \rightarrow Z Z \rightarrow 4\Pl$ and
$\PH \rightarrow W^+W^- \rightarrow 2\Pl 2\PGn$ with the cuts on the
final state leptons as in
tabs.~\ref{tab:fiducialZZ},~\ref{tab:fiducialWW}.
%,
The corresponding fiducial cross sections at NLL+NLO, NNLL+NNLO and
NNLO are reported in \refT{tab}.  In the left plot of
\refF{fig:pTVV} we show the transverse-momentum spectrum of the
$e^+e^-\mu^+\mu^-$ system relative to the
$\PH \rightarrow Z Z \rightarrow 4\Pl$ channel.
%
Regarding the $\PH \rightarrow W^+W^- \rightarrow 2\Pl 2\PGn$ final
state, the right plot of \refF{fig:pTVV} shows the transverse
momentum of the $2\Pl2\PGn$ (right plot) system, while the leading-
and subleading-lepton transverse momentum distributions are displayed
in \refF{fig:pTllead}.


\begin{figure}
\centering
\includegraphics[width=0.485\textwidth]{./WGx/FXS/figs/pt4l.pdf}
\includegraphics[width=0.485\textwidth]{./WGx/FXS/figs/pt2l2nu.pdf}
\caption{\label{fig:pTVV}{Transverse-momentum distributions
    $p_T^{4\Pl}$  for the
$\PH\to \PZ\PZ \to e^+e^-\mu^+\mu^-$ signal (left) and $p_T^{2\Pl2\PGn}$ for
the  $\PH\to W^+W^- \to 2\Pl2\PGn$ signal (right) at the LHC.
Prediction for NNLL+NNLO, NLL+NLO and NNLO  calculations.
Selection cuts on the final-state leptons are described in the text.}}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.485\textwidth]{./WGx/FXS/figs/ptleadWW.pdf}
\includegraphics[width=0.485\textwidth]{./WGx/FXS/figs/ptsubleadWW.pdf}
\caption{\label{fig:pTllead}{Transverse-momentum distributions for the
    leading $p_{ T,{\rm lead}}$ (left) and subleading
    $p_{ T,{\rm sublead}}$ (right) charged lepton obtained by resummed
    NLL+NLO and NNLL+NNLO and fixed-order NNLO calculations in the
    $\PH\to W^+W^- \to 2\Pl2\PGn$ channel.  Selection cuts on the
    final-state photons are described in the text.  }}
\end{figure}

From the above results we observe that resummation effects on top of
the exclusive NNLO prediction tend to be very sizeable in specific
kinematic configurations. It is therefore important to validate the
Monte-Carlo event generators used in experimental analyses for a
complete set of kinematic distributions in the presence of realistic
fiducial cuts. This validation should be carried out over the whole
spectrum - both in the multijet region and in the regime where the
radiation is mainly unresolved - by comparing with the
state-of-the-art perturbative predictions.
