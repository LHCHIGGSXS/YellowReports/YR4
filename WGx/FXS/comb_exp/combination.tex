
\subsection{Combination of inclusive cross sections for Higgs boson production}
%\SectionAtuhor{C.~Meyer, M.~Queitsch-Maitland (eds.)}
\label{sec:exp:xsec_comb}

\subsubsection{Introduction}
\label{sec:xsec_comb:intro}

This section discusses the combination of measured Higgs boson cross sections across multiple decay channels.
The combination is only possible in the inclusive phase space, where the effects of the Higgs boson decay products have been removed.
This introduces some model dependence: namely, the correction for the acceptance of the fiducial selection, and the assumption of Standard Model decay branching fractions for each decay channel.
Although measurements of cross sections within a fiducial phase space offer a more model-independent way of probing the properties of the Higgs boson, measurements at the LHC are currently statistically limited~\cite{Aad:2014lwa,Aad:2014tca,Khachatryan:2015yvw,Khachatryan:2015rxa,Khachatryan:2016vnn}.
It is therefore beneficial to combine the data across several channels, and maximize its potential.
This has been performed in Run-1 by the ATLAS Collaboration in the diphoton and four-lepton decay channels~\cite{Aad:2015lha}.

Both total and differential cross sections can be combined, provided that there is a consistent definition of the observable of interest between the different decay channels.
If only the shape of the differential distribution is of interest, the uncertainties from the combination can be reduced further as the assumptions of the SM decay branching fractions for each decay channel can be neglected.

\subsubsection{Combination method}
\subsubsubsection{Extrapolation to the total phase space}
\label{sec:xsec_comb:extrap}

The inclusive phase space must be carefully defined such that all observables of interest are independent of the Higgs boson decay products.
It is preferable to retain the philosophy of the fiducial cross-section measurements and keep the theoretical predictions and measurement as disentangled as possible, to maximize the longevity of the data.
The data should therefore ideally be corrected to the particle level in the inclusive phase space.
In order to compare theoretical predictions at the parton level to measurements, non-perturbative corrections accounting for the impact of underlying event, multi-parton interactions and hadronization can be provided separately.
The central values and uncertainties should be evaluated using a range of generators with different tunes for showering, hadronization and underlying event, such as in Refs.~\cite{ATLAS:2011krm,ATLAS:2011gmi,ATLAS:2011zja}.

\subsubsubsection{Jets in the inclusive phase space}

When measuring differential cross sections, it is important that all observables of interest are independent of the decay products of the Higgs boson, in particular when considering jets.
Generally the inputs to jet finding at the particle level are all final state particles with lifetimes $c\tau > 10$\ mm, preferably excluding neutrinos, electrons, and muons that do not originate from hadronic decays (as suggested in Ref.~\cite{ATL-PHYS-PUB-2015-013}).
Using this method, some of the resulting particle-level jets will contain decay products of the Higgs boson.
It is then possible for jets to veto -- or be vetoed by -- nearby decay products, due to overlap removal between physics objects.
This creates an inconsistency in the definition of a jet between different decay channels, such as diphoton (two decay products in the final state) and four-lepton (four decay products in the final state).

The effect of the Higgs boson decay can be effectively removed by reconstructing jets at the particle level and explicitly excluding the decay products of the Higgs boson.
This is possible in generators that retain the history of the Higgs boson decay on the event record.
The effects of final-state radiation from the Higgs boson decay products can also be removed.
For example, photons can be excluded from jet finding if they lie inside a cone of radius $\Delta\mathrm{R} < 0.1$ of an electron or muon, where neither the photon nor lepton originate from a hadron decay.

Optimally, a similar definition can be used when reconstructing detector-level jets.
This can be done using the so-called particle-flow method, which reconstructs individual leptons and photons before using them as inputs to the jet finding algorithm.

\subsubsubsection{Binning of differential observables}

In order to carry out a statistical combination of differential observables, matching bin boundaries are helpful. This reduces the combination in the inclusive phase space to a statistical problem of statistically combining coarse cross sections which hold information about the sum of more fine cross section sums. In case no matching bin boundaries are present, a combination requires additional theory input to approximate such matching bin boundaries. For the Run 1 results, a range of criteria have been used to justify the binning of a given observable: binning choices aiming to have equal (expected) statistical significance in each bin or identical expected purities for example, offer a first rough guideline but no unique choice. In analyses which rely on extracting signal yields by subtracting non-resonant production, a certain given binning choice cannot introduce a bias. Thus future measurements should be encouraged to use matching bin boundaries wherever possible and practical to facilitate the possibility of a later inclusive combination. 

\subsubsection{Treatment of uncertainties}
\label{sec:xsec_comb:uncert}

Care must be taken to appropriately correlate shared uncertainty sources (both experimental and theoretical) between the different decay channels.
In particular, the uncertainties on the corrections for acceptance and branching fractions need to be assessed appropriately.

The branching fraction uncertainties for Run-1 are described in the LHC HXSWG YR3~\cite{Heinemeyer:2013tqa}.
For example, in Ref.~\cite{Aad:2015lha} five nuisance parameters were used to describe the branching fraction uncertainties; three fully correlated and two uncorrelated between the diphoton and four-lepton decay channels.

Since the acceptance corrections are largely driven by the Higgs boson rapidity distribution, which is shaped by the phase space of the PDF, the choice of PDF set is one of the largest contributions to the acceptance factor uncertainty.
In Run-1 this uncertainty was assessed by following the PDF4LHC recommendations~\cite{Botje:2011sn}.
Uncertainties associated with missing higher-order corrections are evaluated by varying the renormalization and factorization scales, also following the PDF4LHC recommendations.

The acceptance may also be sensitive to the choice of the assumed mass of the Higgs boson, if for example a mass window around the peak is chosen~\cite{Aad:2014tca}.
In this case the mass of the Higgs boson in the Monte Carlo samples used to calculate the fiducial acceptance should be varied within the current uncertainties.
In order to cover a range of different event topologies and modest deviations from the SM couplings, the composition of the SM signal should also be varied within current experimental constraints.
Finally, uncertainties in the MC simulation of underlying event, multi-parton interactions and hadronization should be included.
This can be done using different MC tunes, preferably the ``systematic variation'' eigentunes which give more accurate variations of the perturbative effects~\cite{ATLAS:2011krm}.

\subsubsubsection{Statistical procedure}

Provided that the uncertainties are normal distributed, a weighted average between the two measurements may be performed.
However, because channels are often statistically limited the use of Poisson statistics is frequently necessary.
In this case a combined likelihood fit that accounts for common theoretical and experimental uncertainties can be used.

\subsubsection{Summary}
\label{sec:xsec_comb:summary}

Combining different Higgs boson decay channels to reduce the large statistical uncertainty will continue to be important until a larger data sample is available.
For example, the combination of the diphoton and four-lepton final states reduced the total uncertainty on the ATLAS 8 TeV inclusive measurement by on average $25{-}30\%$.
While special care must be taken to unify the object definitions and to account for appropriate correlations between decay channels, the recommendations given here should provide a useful starting point for future combined measurements.

