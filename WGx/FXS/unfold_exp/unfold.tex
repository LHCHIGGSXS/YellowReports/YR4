
\subsection{Unfolding of experimental data}
\label{sec:exp:unfold}
%\SectionAuthor{F.~Bernlochner (Ed.)}

Dealing with experimental resolutions will become a major aspect of differential fiducial measurements in Run 2: the large increase in integrated luminosity will mark the transition of the total uncertainties  being statistically dominated to becoming systematically limited. This will offer new challenges to reverting experimental resolutions. A summary about the caveats and various approaches of unfolding can be found for instance in Ref.~\cite{Cowan:2002in}, which this overview follows. 

\subsubsection{Introduction}

In measurements of differential cross sections, one often faces the problem of non-negligible migrations due to the finite resolution of the experimental apparatus. The reversion of such resolution migrations is typically called 'unfolding' or 'deconvolution' and compromises an essential ingredient that allows the easy comparison of theory predictions with measured fiducial cross sections. Mathematically the problem can be formulated in finding an inversion to the function
\begin{align}\label{eq:exp:unfold:master}
 f_{\rm meas} (x) = \int \, R(x | y| f_{\rm true}(y) \, \text{d} y \, ,
\end{align}
where $ f_{\rm meas} (x) $ is the PDF of the measured values $x$, and   $ f_{\rm true} (y) $ the PDF of true (but unknown) values $y$, smeared out by a detector response $R(x|y)$. In practice measurements are carried out in bins of observables, reducing Eq.~\ref{eq:exp:unfold:master}, to a matrix multiplication
\begin{align}\label{eq:exp:unfold:master2}
 x_i = \sum_{j=1}^N R_{ij} y_j \, .
\end{align}
and the response matrix $R_{ij}$ can be interpreted as a conditional probability
\begin{align}\label{eq:exp:unfold:matrix}
 R_{ij} = \mathcal{P}( {\rm reconstructed \, in \, bin \,} i \, | \, {\rm true \, value \, in \, bin \,} j)
\end{align}
with the sum 
\begin{align}
\sum_{i=1}^N R_{ij} = \mathcal{P}( {\rm observed \, anywhere} \, | \, { \rm true \, value \, in \, bin \,} j) = \epsilon_j \, ,
\end{align}
resulting in the the reconstruction efficiency. The task of unfolding is now to revert Eq.~\ref{eq:exp:unfold:master2} to convert measured values to true values. There exist several approaches for this, each with different strengths and caveats. 

\subsubsection{Inverting the response matrix and correction factors}

The most straightforward approach of unfolding involves the inversion of the matrix Eq.~\ref{eq:exp:unfold:matrix} and construct $R_{ij}^{-1}$.
This is generally often possible, but has some drawbacks: if the response matrix has large off-diagonal elements, e.g. if the chosen bin size is too small compared to the measurement resolution or one tries to measure an observable with an intrinsic poor resolution, such as jet multiplicities, the resulting expression for the true value
\begin{align}
y = R_{ij}^{-1} \, x
\end{align}
can have extremely large variances and strong negative correlations between neighbouring bins. If the measured values $x$ themselves are affected by large statistical fluctuations, these get amplified as one tries to revert migrations in a given bin using the estimated bin content of neighbouring bins with large variances themselves. In scenarios with small measured variances the resulting variances also can get amplified, if a high degree of fine structure is present, cf. Ref.~\cite{Cowan:2002in}. Technically the inversion of $R_{ij}$ can be implemented using least square estimators and in case the inversion is not possible, a pseudo-inverse may be constructed. The advantage of this inversion approach is that the resulting values for $y$, albeit in generally affected by large variances are in fact unbiased and the variance itself has the smallest possible value for any unbiased estimator. Thus any other method that aims to reduce the variance will necessarily introduce a bias. Thus the strategy that is followed is to accept a small bias in exchange for a large reduction in variance, i.e. trading statistical for systematic errors. 

\subsubsection{Correction factor method}

A relative simple method, often used in low statistics situation, is based on multiplicative correction factors derived from Monte Carlo simulations. The estimator for $y$ in a given bin $i$ is constructed as
\begin{align}
 y_i = C_i x_i
\end{align}
where the correction factor $C_i$ is
\begin{align}
 C_i = \frac{y_i^{\rm MC}}{x_i^{\rm MC}} \, ,
\end{align}
where $y_i^{\rm MC}$ and $x_i^{\rm MC}$ are the expected true and reco yields from the simulation. This inversion has a much smaller variance than the inversion of the migration matrix, but has a potential bias of the size
\begin{align}\label{eq:exp:bias}
 b_i = \left(  \frac{y_i^{\rm MC}}{x_i^{\rm MC}} - \frac{y_i^{\rm true}}{x_i^{\rm true}} \right) x_i^{\rm obs}
\end{align}
which has to be carefully estimated. In Eq.~\ref{eq:exp:bias} the quantities $y_i^{\rm true}$ and $x_i^{\rm true}$ are the true underlying mean population of the bin. The bias is zero in case the model is correct, which is not something that can be inferred prior a measurement. Typically the size of the bias is estimated with respect to some baseline scenario (e.g. the SM) and a maximal deviation one expects (e.g. a certain amount of new physics). One is satisfied when $b_i$ is small with respect to the variance and the size of the estimated bias is added to the systematic error of the measurement. This maximal deviation can also be composed by an entire of ensemble of scenarios. 

\subsubsection{Regularized unfolding}

Regularized unfolding tries to find some middle ground between these two approaches: inverting the migration matrix assumes that all the relevant information on how to revert migrations comes from the neighbouring bins of a measured distributions. The correction factor method excludes all the information from neighbouring bins to revert migrations. In regularized unfolding the information from both is used, and the weighting of either piece of information is typically controlled by one or many regularization parameters. There exist a range of different methods following different philosophies, often used are for example Refs.~\cite{Hocker:1995kb,D'Agostini:1994zf}. It is important to note, that regularized unfolding also has to carefully control and estimate the size of a potential bias. Here, as for the correction factor method, the found bias using a baseline scenario and a scenario for the largest to be expected deviation is added to the systematic error of the unfolded spectrum.  In addition special attention has to be payed to not 'over-regularize' the unfolded distributions, that is to impose a too strong dampening of statistical fluctuations. This is usually achieved by careful tuning of the regularization parameter(s).

A priori any method for unfolding is fine to use, as long as the bias is properly estimated. Even the combination of information gained via different unfolding methods is not a priori a problem, as long as the bias is negligible with respect to the statistical precision and is included into the error budget properly. With the expected large data sets of Run 2 of the LHC, it is expected that experiments will shift away from simpler methods and follow suit what is standard practice in SM measurements.  

