We acknowledge discussions in Les Houches 2015 and WG2 and contributions and feedback from Aaron Armbruster, Josh Bendavid, Fawzi Boudjema, Andr\'e David, Marco Delmastro, Dag Gillberg, Admir Greljo, Thibault Guillemin, Chris Hays, Gino Isidori, Sabine Kraml, Kirtimaan Mohan, James Lacey, Carlo Pandini, Elisabetta Pianori, Tilman Plehn, Michael Rauch, Chris White, and many others.

\section{Overview}
\label{PO_STCS:STCS_Overview}

After the successful Higgs boson coupling measurements during the LHC Run1, which had
as their main results measured signal strength and multiplicative coupling modifiers,
it is important to discuss in which way the experiments should present and perform
Higgs boson coupling measurements in the future.
Simplified template cross sections were developed to provide a natural way to
evolve the signal strength measurements used during Run1. Compared to the Run1
measurements, the simplified template cross section framework allows one to
reduce in a systematic fashion the theory dependences that must be directly folded into
the measurements. This includes both the dependence on the theoretical uncertainties in the SM
predictions as well as the dependence on the underlying physics model (i.e.\ the SM or BSM models).
In addition, they provide more finely-grained measurements (and hence more information for theoretical
interpretations), while at the same time allowing and benefitting from the global combination of
the measurements in all decay channels.

The primary goals of the simplified template cross section framework are to maximize the sensitivity
of the measurements while at the same time to minimize their theory dependence. This means in
particular
\begin{itemize}
\item combination of all decay channels
\item measurement of cross sections instead of signal strengths, in mutually exclusive regions of
  phase space
\item cross sections are measured for specific production modes (with the SM production serving as kinematic template)
\item measurements are performed in abstracted/simplified fiducial volumes
\item allow the use of advanced analysis techniques such as event categorization, multivariate techniques, etc.
\end{itemize}

The measured exclusive regions of phase space, called ``bins'' for simplicity, are specific to
the different production modes. Their definitions are motivated by
\begin{itemize}
\item minimizing the dependence on theoretical uncertainties that are directly folded into the measurements
\item maximizing experimental sensitivity
\item isolation of possible BSM effects
\item minimizing the number of bins without loss of experimental sensitivity
\end{itemize}
These will of course be competing requirements in some cases and some compromise has to be achieved.
The implementation of these basic design principles is discussed in more detail below.

\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]{WGx/STXS/simplifiedXS}
\end{center}
\caption{Schematic overview of the simplified template cross section framework.}
\label{fig:STCS:sketch}
\end{figure}

A schematic overview of the simplified template cross section framework is shown in \refF{fig:STCS:sketch}.
The experimental analyses shown on the left are very similar to the Run1 coupling measurements. For each decay channel, the
events are categorized in the analyses, and there are several motivations
for the precise form of the categorization. Typically, a subset of the experimental event categories
is designed to enrich events of a given Higgs boson production mode, usually making use of specific
event topologies. This is what eventually allows the splitting of the production modes in the global fit. Another subset
of event categories is defined to increase the sensitivity of the analysis by splitting events
according to their expected signal-to-background ratio and/or invariant-mass resolution. In other cases,
the categories are motivated by the analysis itself, e.g. as a consequence of the backgrounds being estimated
specifically for certain classes of events. While these are some of the primary motivations, in the future the details of the event
categorization can also be optimized in order to give good sensitivity to the
simplified template cross sections to be measured.

The centre of \refF{fig:STCS:sketch} shows a sketch of the simplified template cross sections, which are determined from
the experimental categories by a global fit that combines all decay channels and which represent the main results of
the experimental measurements. They are cross sections per production mode, split into mutually exclusive kinematic bins for each of
the main production modes.
In addition, the different Higgs boson decays are treated by fitting the partial decay widths.
Note that as usual, without additional assumptions on the total width, only ratios of partial widths
and ratios of simplified template cross sections are experimentally accessible.

The measured simplified template cross sections together with the partial decay widths then serve as input for
subsequent interpretations, as illustrated on the right of \refF{fig:STCS:sketch}.
Such interpretations could for example be the determination of signal strength modifiers or
coupling scale factors $\kappa$ (providing compatibility with earlier results), EFT coefficients, tests
of specific BSM models, and so forth. For this purpose, the experimental results should quote the full covariance among the different bins.
By aiming to minimize the theory dependence that is folded into the first step of determining the simplified
template cross sections from the event categories, this theory dependence is shifted into the second interpretation step, making the measurements more long-term useful. For example, the treatment of theoretical uncertainties can be decoupled from the measurements and can be dealt with at the interpretation stage. In this way, propagating improvements in theoretical predictions and their uncertainties into the measurements itself, which is a very time-consuming procedure and unlikely to be feasible for older datasets, becomes much
less important. Propagating future theoretical advances into the interpretation, on the other hand, is generally much easier.

To increase the sensitivity to BSM effects, the simplified template cross sections can be
interpreted together with e.g. POs in Higgs boson decays. To make this possible, the experimental
and theoretical correlations between the simplified template cross sections and the decay POs would need
to be evaluated and taken into account in the interpretation. This point will not be expanded on further
in this section, but would be interesting to investigate in the future.

While the simplified template cross section bins have some similarity to a differential cross section measurement,
they aim to combine the advantages of the signal strength measurements and fiducial
and differential measurements. In particular, they are complementary to full-fledged fiducial and
differential measurements and are neither designed nor meant to replace these. Fully fiducial differential
measurements are of course essential but can only be carried out in a subset of decay channels in
the foreseeable future. They are explicitly optimized for maximal theory independence. In practice, this
means that in the measurements acceptance corrections are minimized, typically,
simple selection cuts are used, and the measurements are unfolded to a fiducial volume that is as close as possible to the fiducial
volume measured for a particular Higgs boson decay channel. In contrast,
simplified template cross sections are optimized for sensitivity while reducing the dominant theory
dependence in the measurement. In practice, this means that simplified fiducial volumes are used and larger acceptance corrections are allowed in order to maximally
benefit from the use of event categories and multivariate techniques. They are also
inclusive in the Higgs boson decay to allow for the combination of the different decay channels. The
fiducial and differential measurements are designed to be agnostic to the production modes as much
as possible. On the other hand, the separation into the production modes is an essential aspect of
the simplified template cross sections to reduce their model dependence.



\section{Guiding principles in the definition of simplified template cross section bins}
\label{PO_STCS:STCS_GuidingPrinciple}

As outlined above, several considerations have been taken into account in the definition
of the simplified template cross section bins.

One important design goal is to reduce the dependence of the measurements on theoretical uncertainties
in SM predictions. This has several aspects. First, this requires
avoiding that the measurements have to extrapolate from a certain region in phase space to the full
(or a larger region of) phase space whenever this extrapolation carries nontrivial or sizeable theoretical
uncertainties. A example is the case where an event category selects an exclusive
region of phase space, such as an exclusive jet bin. In this case, the associated theoretical uncertainties
can be largely avoided in the measurement by defining a corresponding truth jet bin.
The definition of the bins is preferably in terms of quantities that are directly measured by the experiments
to reduce the needed extrapolation.

There will of course always be residual theoretical uncertainties due to the experimental acceptances for each
truth bin. Reducing the theory dependence thus also requires to avoid cases with large variation in the
experimental acceptance within one truth bin, as this would introduce a direct dependence on the
underlying theoretical distribution in the simulation. If this becomes an issue, the bin can be further split into two
or more smaller bins, which reduces this dependence in the measurement and moves it to the interpretation step.

To maximize the experimental sensitivity, the analyses should continue to use event categories primarily optimized for sensitivity, while
the definition of the truth bins should take into consideration the experimental requirements. However, in cases where multivariate analyses are used in the analyses, it has to be carefully checked and balanced against the requirement to not introduce theory dependence, e.g., by selecting specific regions of phase space.

Another design goal is to isolate regions of phase space, typically at large kinematic scales, where BSM effects could be potentially large and visible above the SM background. Explicitly separating these also reduces the dependence of the measurements on the assumed SM kinematic distribution.

In addition, the experimental sensitivity is maximized by allowing the combination of all decay channels, which requires the framework
to be used by all analyses. To facilitate the experimental implementation, the bins should be mutually exclusive to avoid introducing statistical correlations between different bins. In addition, the number of bins should be kept
minimal to avoid technical complications in the individual analyses as well as the global fit, e.g. in the evaluation of the full covariance matrix. For example, each bin should typically have some sensitivity from at least one event category in order to avoid the need to statistically combine many poorly constrained or unconstrained measurements. On the other hand, in BSM sensitive bins experimental limits are already very useful for the theoretical interpretation.


\subsection{Splitting of production modes}
\label{PO_STCS:STCS_GuidingPrincipleSplitting}

The definition of the production modes has some notable differences compared to Run1 to deal
with the fact that the naive distinction between the $q\bar{q}\to VH$ and VBF processes, and
similarly between $gg\to VH$ and gluon-fusion production, becomes ambiguous at higher order
when the $V$ decays hadronically. For this reason, the $VH$ production mode is explicitly
defined as Higgs boson production in association with a leptonically decaying $V$ boson.
The $q\bar{q}\to VH$ process with a hadronically decaying $V$ boson is considered to be part of
what is called ``VBF production'', which is defined as electroweak $qqH$ production. Similarly,
the $gg\to ZH$ process with hadronically decaying $Z$ boson is included in what is called
``gluon-fusion production''.

In principle, also the separation of $ZH$ production with a leptonic $Z$ into $q\bar{q}$ or $gg$ initial states
becomes ambiguous at higher order. For present practical purposes, on the experimental side the split
can be defined according to the separate MC samples for $q\bar{q}\to ZH$ and $gg\to ZH$ used in the analyses.


\subsection{Staging}
\label{PO_STCS:STCS_GuidingPrincipleStaging}

In practice, it will be impossible to define a set of bins that satisfies all of the above requirements for every analysis.
Some analyses will only be able to constrain a subset of all bins or only constrain the sum of a set of bins.
In addition, the number of bins that will be possible to measure increases with increasing amount of
available data. For this reason, several stages with an increasing number of bins are defined.
The evolution from one stage to the next can take place independently for each production mode.

\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]{WGx/STXS/simplifiedXS_stage0}
\end{center}
\caption{Stage 0 bins.}
\label{fig:STCS:stage0}
\end{figure}

\subsubsection{Stage 0} Stage 0 is summarized in \refF{fig:STCS:stage0} and corresponds most closely to the
measurement of the production mode $\mu$ in Run1. At this stage, each main production mode has a single
inclusive bin,
with associated Higgs boson production separated into $q\bar q\to WH$, $q\bar q\to ZH$ and $gg\to ZH$ channels.

As discussed in Section~\ref{PO_STCS:STCS_GuidingPrincipleSplitting}, VBF production is
defined as electroweak $qqH$ production. For better compatibility with Run1 measurements,
the VBF production is split into a Run1-like VBF and
Run1-like $V(\to jj)H$ bin, where the splitting is defined by the conventional Feynman diagrams included in the
simulations. In practice, most decay channels will only provide a measurement for
the Run1-like VBF bin.


\subsubsection{Stage 1} Stage 1 defines a binning that is targeted to be used by all analyses on an intermediate
time scale. In principle, all analyses should aim to eventually implement the full stage 1 binning.
If necessary, intermediate stages to reach the full stage 1 binning can be implemented by a given analysis by
merging bins that cannot be split. In this case, the analysis should ensure that the merged bins have similar acceptances,
such that the individual bins can still be determined in an unbiased way in the global combination of all channels.
In the diagrams presented below, the possibilities for merging bins are indicated by ``(+)''.

\subsubsection{Stage 2} Defining the stage 2 binning in full detail is very difficult before having gained experience with the
practical implementation of the framework with the stage 1 binning. Therefore, instead of giving a detailed
proposal for the stage 2 binning, we only give indications of interesting further separation of bins that should be considered
for the stage 2 binning.



\section{Definition of leptons and jets}
\label{PO_STCS:STCS_ObjectsAndJets}

The measured event categories in all decay channels are unfolded by the global fit to the
simplified template cross sections bins. For this purpose, and for the
comparison between the measured bins and theoretical predictions from either analytic
calculations or MC simulations, the truth final state particles need to be defined unambiguously.
The definition of the final state particles, leptons, jets, and in particular also the Higgs boson 
are explicitly kept simpler and more idealized than in the fiducial cross section measurements.
Treating the Higgs boson as a final state particle is what allows the combination of the
different decay channels.

For the moment, the definitions are adapted to the current scope of the measurements. Once a
finer binning is introduced for $t\bar{t}H$ or processes such as VBF with the emission of a hard
photon are added, some of the definitions below might have to be adapted or refined.

\subsection{Higgs boson}
\label{PO_STCS:STCS_ObjectsAndJets_Higgs}

The simplified template cross sections are defined for the production of an on-shell Higgs boson,
and the unfolding should be done accordingly. A global cut on the Higgs boson rapidity at $|Y_H| < 2.5$
is included in all bins. As the current measurements have no sensitivity beyond this rapidity range, this
part of phase space would only be extrapolated by the MC simulation. On the other
hand, it is in principle possible to use forward electrons (up to $|\eta|$ of 4.9) in
$H\to ZZ^*\to 4\ell$ and extend the accessible rapidity range. For this purpose, an
additional otherwise
inclusive bin for $|Y_H| > 2.5$ can be included.


\subsection{Leptons}
\label{PO_STCS:STCS_ObjectsAndJets_leptons}

Electrons and muons from decays of signal vector bosons, e.g. from $VH$ production, are defined
as dressed, i.e. FSR photons should be added back to the
electron or muon. $\tau$ leptons are defined from the sum of their decay products (for any $\tau$ decay
mode). There should be
no restriction on the transverse momentum or the rapidity of the leptons.
That is, for a leptonically decaying vector boson
the full decay phase space is included.


\subsection{Jets}
\label{PO_STCS:STCS_ObjectsAndJets_Jets}

Truth jets are defined as anti-$k_t$ jets with a jet radius of $R = 0.4$, and are built from all
stable particles (exceptions are given below), including neutrinos, photons and leptons
from hadron decays or produced in the shower.
Stable particles here have the usual definition, having a lifetime greater than 10~ps, i.e. those
particles that are passed to GEANT in the experimental simulation chain. All decay products from
the Higgs boson decay are removed as they are accounted for by the truth Higgs boson.
Similarly, leptons (as defined above) and neutrinos from decays of the signal $V$ bosons are
removed as they are
treated separately, while decay products from hadronically decaying signal $V$ bosons are included in
the inputs to the truth jet building.

By default, truth jets are defined without restriction on their rapidity. A possible rapidity cut can
be included
in the bin definition. A common $p_\mathrm{T}$ threshold for jets should be used for all truth jets.
A lower threshold would in principle have the advantage to split the events more evenly between the
different jet bins.
Experimentally, a higher threshold at 30~GeV is favored due to pile up and is therefore used for the
jet definition to limit the amount of phase-space extrapolation in the measurements.



\section{Bin definitions for the different production modes}
\label{PO_STCS:STCS_bins}

In the following, the bin definitions for the different production modes in each stage are given.
The bins are easily visualized through cut flow diagrams. In the diagrams, the bins on each branch
are defined to be mutually exclusive and sum up to the preceding parent bin. For simplicity, sometimes
not all cuts are explicitly written out in the diagrams, in which case the complete set of cuts are specified in the text.
In case of ambiguities, a more specific bin is excluded from a more generic bin. As already mentioned, for the stage 1 binning
the allowed possibilities for merging bins at intermediate stages are indicated by a ``(+)'' between two bins.


\subsection{Bins for \texorpdfstring{$gg \to H$}{gg to H} production}
\label{PO_STCS:STCS_ggH}

\subsubsection{Stage 0} Inclusive gluon fusion cross section within $|Y_H|<2.5$. Should the
measurements start to have acceptance beyond 2.5, an additional bin for $|Y_H| > 2.5$ can be
included.


\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]{WGx/STXS/simplifiedXS_ggF}
\end{center}
\caption{Stage 1 binning for gluon fusion production.}
\label{fig:STCS:ggH}
\end{figure}

\subsubsection{Stage 1}

Stage 1 refines the binning for $|Y_H|<2.5$.
The stage 1 binning is depicted in \refF{fig:STCS:ggH} and summarized as follows:
\begin{itemize}
\item Split into jet bins: $N_j=0$, $N_j=1$, $N_j\geq2$, $N_j\geq2$ with VBF topology cuts
  (defined with the same cuts as the corresponding bin in VBF production). For the
  $N_j\geq2$ with VBF topology cuts, $p_\mathrm{T}^H <$ 200~GeV is required, which gives priority
  to the $p_\mathrm{T}^H >$ 200~GeV bin for $N_j\geq2$. Otherwise, the $N_j \geq 2$ with VBF topology cuts
  is excluded from the $N_j \geq 2$ bins. The jet bins are
  motivated by the use of jet bins in the experimental analyses. Introducing them also for the
  simplified template cross sections avoids folding the associated theoretical uncertainties into
  the measurement. The separation of the $N_j\geq 2$ with VBF topology cuts is motivated by the wish to
  separately measure the gluon fusion contamination in the VBF selection. If the fit has no
  sensitivity to determine the gluon fusion and the VBF contributions with this topology, the sum
 of the two contributions can be quoted as result.
\item The $N_j\geq2$ with VBF topology bin is split further into an exclusive $2$-jet-like and inclusive $3$-jet-like
  bin. The split is implemented by a cut on $p_\mathrm{T}^{Hjj} = |\vec p_\mathrm{T}^H + \vec p_\mathrm{T}^{j1} + \vec p_\mathrm{T}^{j2}|$ at 25~GeV.
  See the corresponding discussion for VBF for more details.
  This split is explicitly included here since it induces
  nontrivial theory uncertainties in the gluon-fusion contribution.
\item The $N_j=1$ and $N_j\geq2$ bins are further split into $p_\mathrm{T}^H$ bins.
  \begin{itemize}
  \item 0~GeV $< p_\mathrm{T}^H<$ 60~GeV: The boson channels have most sensitivity in the low
    $p_\mathrm{T}^H$ region. The upper cut is chosen as low as possible to give a more even split
    of events but at the same time high enough that no resummation effects are expected. The cut
    should also be sufficiently high that the jet $p_{\rm T}$ cut introduces a negligible bias.
  \item 60~GeV $< p_\mathrm{T}^H<$ 120~GeV: This is the resulting intermediate bin between the low and high
    $p_\mathrm{T}^H$ regions. The lower cut here is high enough that this bin can be safely treated
    as a hard $H+j$ system in the theoretical description.
  \item 120~GeV $< p_\mathrm{T}^H<$ 200~GeV: The boosted selection in $H\to\tau\tau$ contributes to
    the high $p_\mathrm{T}^H$ region. Defining a separate bin avoids large extrapolations for the
    $H\to\tau\tau$ contribution. For $N_j = 2$, this bin likely provides a substantial part of the gluon-fusion
    contribution in the hadronic $VH$ selection.
  \item $p_\mathrm{T}^H>$ 200~GeV: Beyond the top-quark mass, the top-quark loop gets resolved and top-quark mass effects become relevant.
    Splitting off the high-$p_\mathrm{T}^H$ region ensures the usability of the heavy-top expansion
    for the lower-$p_\mathrm{T}^H$ bins. At the same time, the high $p_\mathrm{T}^H$ bin in principle
    offers the possibility to distinguish a pointlike $ggH$ vertex induced by heavier BSM particles in the loop from
    the resolved top-quark loop.
\end{itemize}
\end{itemize}

At intermediate stages, all lower three $p_\mathrm{T}^H$ bins, or any two adjacent bins, can be merged. Alternatively
or in addition the $N_j=1$ and $N_j\geq2$ bins can be merged by individual analyses as needed, and potentially
also when the combination is performed at an intermediate stage.

\subsubsection{Stage 2}

In stage 2, the high $p_\mathrm{T}^H$ bin should be split further, in particular if evidence for new heavy particles arises.
In addition, the low $p_\mathrm{T}^H$ region can be split further to reduce any theory dependence there. If desired by
the analyses, another possible option is to further split the $N_j \geq 2$ bin into $N_j=2$ and $N_j \geq 3$.


\subsection{Bins for VBF production}
\label{PO_STCS:STCS_VBF}

At higher order, VBF production and $VH$ production with hadronically decaying $V$ become ambiguous.
Hence, what we refer to as VBF in this section, is defined as as electroweak $qq'H$ production,
which includes both VBF and $VH$ with hadronic $V$ decays.

\subsubsection{Stage 0} Inclusive vector boson fusion cross section within $|Y_H|<2.5$. Should the
measurements start to have acceptance beyond 2.5, an additional bin for $|Y_H| > 2.5$ can be
included.

\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]{WGx/STXS/simplifiedXS_VBF}
\end{center}
\caption{Stage 1 binning for vector boson fusion production.}
\label{fig:STCS:VBF}
\end{figure}

\subsubsection{Stage 1}

Stage 1 refines the binning for $|Y_H|<2.5$.
The stage 1 binning is depicted in \refF{fig:STCS:VBF} and summarized as follows:
\begin{itemize}
\item VBF events are split by $p_\mathrm{T}^{j1}$, the transverse momentum of the highest-$p_\mathrm{T}$
jet. The lower $p_\mathrm{T}^{j1}$ region is expected to be dominated by SM-like events, while the
high-$p_\mathrm{T}^{j1}$ region is sensitive to potential BSM contributions, including events with
typical VBF topology as well as boosted $V(\to jj)H$ events where the $V$ is reconstructed as one jet.
The suggested cut is
at 200~GeV, to keep the fraction of SM events in the BSM bin small. Note that events with $N_j = 0$,
corresponding to $p_\mathrm{T}^{j1}<$ 30~GeV, is included in the $p_\mathrm{T}^{j1}<$ 200~GeV bin.
\item The $p_\mathrm{T}^{j1}<$ 200~GeV bin is split further:
  \begin{itemize}
  \item Typical VBF topology: The adopted VBF topology cuts are $m_{jj} >$ 400~GeV,
    $\Delta\eta_{jj} > 2.8$ (and without any additional rapidity cuts on the signal jets).
    This should provide a good intermediate compromise among the various VBF selection cuts
    employed by different channels.
  \begin{itemize}
  \item The bin with typical VBF topology is split into an exclusive $2$-jet-like and inclusive $3$-jet-like
  bin using a cut on $p_\mathrm{T}^{Hjj}$ at 25~GeV, where the cut value is a compromise between providing
  a good separation of gluon fusion and VBF and the selections used in the measurements.
  $p_\mathrm{T}^{Hjj}$ as quantity to define this split is chosen as a compromise between
  the different kinematic
  variables used by different channels to enrich VBF production. (In particular the kinematic
  variables $\Delta\phi_{H-jj}$
  and $p_\mathrm{T}^{j3}$ are both correlated with $p_\mathrm{T}^{Hjj}$).
\end{itemize}
\item Typical $V(\to jj)H$ topology: events with at least two jets and 60~GeV $< m_{jj} <$ 120~GeV.
\item  Rest: all remaining events, including events with zero or one jet.
  The ``rest'' bin can be sensitive to certain BSM
  contributions that do not follow the typical SM VBF signature with two forward jets.

\end{itemize}
\end{itemize}

\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]{WGx/STXS/simplifiedXS_VBF_stage2}
\end{center}
\caption{Possible stage 2 binning for vector boson fusion production.}
\label{fig:STCS:VBF2}
\end{figure}

\subsubsection{Stage 2}

More splits are introduced at stage 2 as illustrated in \refF{fig:STCS:VBF2}. While the details require more discussion and
cannot be finalized at the present, this could include

\begin{itemize}
\item The high-$p_\mathrm{T}^{j1}$ bin can be split further by separating out very high-$p_\mathrm{T}^{j1}$
  events for example with additional cuts at 400~GeV and 600~GeV.
\item The ``rest'' bin can be split further, e.g., by explicitly separating out a looser VBF selection,
  and/or by separating out events with zero or one jets.
\item The $N_j\simeq 2$ VBF topology bin can be split further to gain sensitivity to CP odd
  contributions, e.g. by splitting it into subbins of $\Delta\phi_{jj}$ or alternatively by measuring a
  continuous parameter.
\end{itemize}



\subsection{Bins for \texorpdfstring{$VH$}{VH} production}
\label{PO_STCS:STCS_VH}

In this section, $VH$ is defined as Higgs boson production in association with a leptonically
decaying $V$ boson.

Note that $q\bar{q}\to VH$ production with a hadronically decaying $V$ boson is considered part of
VBF production. Similarly, $gg\to VH$ production with hadronically decaying $V$ boson is
considered part of gluon fusion production.

\subsubsection{Stage 0} Inclusive associated production with vector bosons cross section within
 $|Y_H|<2.5$. Should the
measurements start to have acceptance beyond 2.5, an additional bin for $|Y_H| > 2.5$ can be
included.

\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]{WGx/STXS/simplifiedXS_VH}
\end{center}
\caption{Stage 1 binning for associated production with vector bosons.}
\label{fig:STCS:VH}
\end{figure}

\subsubsection{Stage 1}

Stage 1 refines the binning for $|Y_H|<2.5$.
The stage 1 binning is depicted in \refF{fig:STCS:VH} and summarized as follows:
\begin{itemize}
\item $VH$ production is first split into the production via a $q\bar{q}$ or $gg$ initial state.
This split becomes ambiguous at higher order. For practical
purposes, on the experimental side the split can be defined according to the MC samples used in
the analyses, which are split by $q\bar{q}$ and $gg$.
\begin{itemize}
\item The production via $q\bar{q}\to VH$ is split according to the vector boson: $W\to\ell\nu$ and
  $Z\to\ell\ell + \nu\bar{\nu}$.
\item $W\to\ell\nu$ and $Z\to\ell\ell + \nu\bar{\nu}$ are split further into bins of $p_\mathrm{T}^V$,
  aligned with the quantity used in the $H\to b\bar{b}$ analysis, which is one of the main
  contributors to the $VH$ bins.
  \begin{itemize}
  \item  $p_\mathrm{T}^V<$ 150~GeV receives contributions from the bosonic decay channels and from
    $H\to b\bar{b}$ with $W\to\ell\nu$ and $Z\to\ell\ell$, which do not rely on $E_\mathrm{T}^\mathrm{miss}$
    triggers.
  \item 150~GeV $<p_\mathrm{T}^V<$ 250~GeV receives contributions from $H\to b\bar{b}$ with
    $Z\to\nu\bar{\nu}$ due to the high threshold of the $E_\mathrm{T}^\mathrm{miss}$ trigger, as
    well as from $H\to b\bar{b}$ with $W\to\ell\nu$ and $Z\to\ell\ell$.
    \begin{itemize}
    \item This bin is split further into a $N_j=0$ and a $N_j \geq 1$ bin, reflecting the different
      experimental sensitivity and to avoid the corresponding theory dependence.
    \end{itemize}
  \item $p_\mathrm{T}^V>$ 250~GeV is sensitive to BSM contributions.
  \end{itemize}
\item The production via $gg \to ZH$ is split in analogy to production from the $q\bar{q}$
  initial state, apart from the $p_\mathrm{T}^V>$ 250~GeV bin, which is not split out.
\end{itemize}
\end{itemize}

\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]{WGx/STXS/simplifiedXS_VH_stage2}
\end{center}
\caption{Possible Stage 2 binning for associated production with vector bosons.}
\label{fig:STCS:VH2}
\end{figure}


\subsubsection{Stage 2}

More splits are introduced at stage 2 as illustrated in \refF{fig:STCS:VBF2}. While the details need more discussion, this could include

\begin{itemize}
\item Split of the $Z\to\ell\ell + \nu\bar{\nu}$ into $Z\to\ell\ell$ and $Z\to\nu\bar{\nu}$.
\item Split of the $p_\mathrm{T}^V<$ 150~GeV into a $N_j=0$ and a $N_j \geq 1$ bin, except maybe for
  the $Z\to\ell\ell$ channel, which will suffer from the low $Z\to\ell\ell$ branching ratio.
\item Split of the $p_\mathrm{T}^V>$ 250~GeV bin into $p_\mathrm{T}^V<$ 400~GeV and $p_\mathrm{T}^V>$ 400~GeV,
  to increase the sensitivity to BSM contributions with very high $p_\mathrm{T}^V$, potentially apart
  from the $Z\to\ell\ell$.
\item Potentially analogous splits for $gg\to ZH$ production.
\end{itemize}


\subsection{Treatment of \texorpdfstring{$t\bar tH$}{ttH} production}
\label{PO_STCS:STCS_ttH}

\subsubsection{Stage 0} Inclusive $t\bar t H$ production with $|Y_H|<2.5$. Should the
measurements start to have acceptance beyond 2.5, an additional bin for $|Y_H| > 2.5$ can be
included.

\subsubsection{Stage 1}

Currently no additional splits beyond stage 0 are foreseen. One option might be to
separate different top decay channels for $|Y_H|<2.5$.

\subsubsection{Stage 2}

In the long term it could be useful to split into bins with 0 and $\geq 1$ additional jets or
one or more bins tailored for BSM sensitivity.

\subsection{Treatment of \texorpdfstring{$b\bar b H$}{bbH} and \texorpdfstring{$tH$}{tH} production}
\label{PO_STCS:STCS_bbH}

In the foreseeable future, there will only be one inclusive bin for $b\bar{b}H$ production and
only one inclusive bin for $tH$ production for $|Y_H|<2.5$. Should the
measurements start to have acceptance beyond 2.5, an additional bin for $|Y_H| > 2.5$ can be
included.


\section{Practical considerations}
\label{PO_STCS:practicalities}

To facilitate the combination of the results from ATLAS and CMS, the same bin definitions
need to be used by the two collaborations.
As for the Run1 Higgs boson coupling measurements, a combination of results from ATLAS and CMS will
also require that the two collaborations estimate systematic and residual theoretical uncertainties
in a compatible way.
This might be facilitated for example by the use of the same Monte Carlo generators in the measurements.

After first experience with the measurement and
interpretation has been collected, the stage 1 bin definitions should be reviewed. This
should in particular include the definition of the VBF topology cuts as well as the $p_\mathrm{T}^{Hjj}$ split.
In cases where the bin definitions are clearly inadequate, they should be improved for future
measurements. The stage 2 bins will be defined in detail taking into account the
experience gained during the measurements based on the stage 1 definitions.

A reference implementation of the bin definitions in Rivet has been
developed~\cite{STXSRivet}.
This will facilitate a consistent treatment in both experiments as well as
in theoretical studies.


\section{Summary}
\label{PO_STCS:summary}

Simplified template cross sections provide a way to evolve the signal strength measurements that were
performed during LHC Run1, by reducing the theoretical uncertainties that are directly folded into
the measurements and by providing more finely-grained measurements, while at the same time
allowing and benefitting from the combination of measurements in many decay channels. Several stages
are proposed: stage 0 essentially corresponds to the production mode measurements of Run1 and stage 1
defines a first complete setup, with indications for potential bin merging when a given channel
cannot yet afford the full stage 1 granularity. A complete proposal for the stage 2 binning will need to be based on
experience of using the simplified template cross section framework in real life, but some indications
of what could be interesting are already given here.
