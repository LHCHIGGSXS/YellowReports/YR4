\providecommand{\mhpm}{m_{\PSHpm}}

\section{Introduction}
Charged Higgs bosons $\PSHpm$ appear in many extensions of the Standard Model, in particular when adding
additional doublets or triplets to its scalar sector. Here, the focus is 
on charged Higgs bosons in 2-Higgs-doublet models (2HDM) including the special case of the Higgs sector 
of the minimal supersymmetric extension of the standard model (MSSM). The dominant production mode
for a charged Higgs boson depends on its mass. In particular, for masses below the top quark mass, the charged Higgs boson
is dominantly produced in top-quark decays. Therefore, the production cross section corresponds to the top pair production times 
the branching ratio $\Pt \to \PH^+ \Pb$. For values of the mass close to the top quark mass ($160-180\UGeV$), both contributions with resonant and 
non-resonant top quarks are equally important, and the full $ \PWm \PH^+ \Pb \bar\Pb$ has to be simulated. Finally, heavy charged Higgs bosons
are dominantly produced in association with a top quark. Most of the parameter space for a light charged Higgs boson has already been excluded
at the LHC Run 1~\cite{Aad:2014kga, Khachatryan:2015qxa}. For what concerns the intermediate mass region $\mhpm \sim m_{\Pt}$, no search has been performed to date due to the 
lack of accurate predictions for the signal. In fact, NLO predictions for the total cross section have been made available only recently~\cite{Degrande:2016hyf}. Further theoretical developments in this direction are encouraged. In this chapter we will focus on the heavy mass
range, up to masses of $2\UTeV$, which is being probed at the Run 2.\\
In the following, we present updated NLO predictions for heavy charged Higgs boson production in a type-II 
2HDM. These cross sections can also be translated into predictions 
for a type-I, type-III or type-IV 2HDM according to the recipe in Ref.~\cite{Flechl:2014wfa}. We continue by showing differential cross sections for this production 
process with an emphasis on the comparisons of the 4-flavour scheme (4FS) 
and 5-flavour scheme (5FS) predictions. We conclude the chapter providing some recommendations for the signal simulation in experimental searches.

\section{Inclusive production cross sections}
The dominant charged Higgs boson production mode for $\mhpm > m_t$ in a 2HDM is via the process
\begin{equation*} 
\Pp\Pp\, \rightarrow\, \Pt\PSHpm + X.
\end{equation*}
The cross section for associated $\Pt\PSHpm$ production can be
computed in the 4FS or the 5FS.  In the 4FS there are no $\Pb$ quarks in the initial
state, hence the lowest-order QCD production processes are
gluon-gluon fusion and quark-antiquark annihilation, $\Pg\Pg
\rightarrow \Pt\Pb\PSHpm$ and $\Pq\bar{\Pq} \rightarrow
\Pt\Pb\PSHpm$, respectively. Potentially large logarithms of the
ratio between the hard scale of the process and the mass of the bottom
quark, which arise from the splitting of incoming gluons into nearly
collinear $\Pb\bar{\Pb}$ pairs, can be summed to all orders in
perturbation theory by introducing bottom parton densities.  This
defines the five-flavour scheme (5FS). The use of bottom quark distribution
functions is based on the approximation that the outgoing $\Pb$ quark
is at small transverse momentum and massless, and the virtual $\Pb$
quark has a vanishing virtuality ($m \approx 0$). In this scheme, the LO process for the
inclusive top-quark-associated production cross section is gluon-bottom fusion,
$\Pg\Pb \rightarrow \Pt\PSHpm$. The NLO cross section in the 5FS
scheme includes $\mathcal{O}(\alphas)$ corrections to $\Pg\Pb
\rightarrow \Pt\PSHpm$, including the tree-level processes $\Pg\Pg
\rightarrow \Pt\Pb\PSHpm$ and $\Pq\bar{\Pq} \rightarrow
\Pt\Pb\PSHpm$.  To all orders in perturbation theory the two schemes
are identical, but the way of ordering the perturbative expansion is
different, and the results do not match exactly at finite order. 

Here, we present cross-section predictions for this process by following the methodology described in more detail 
in Refs.~\cite{Flechl:2014wfa,Heinemeyer:2013tqa}. The main differences are the usage of the most recent combination of PDF sets provided by
PDF4LHC15, the centre-of-mass energy of $\sqrt{s}=13 \UTeV$, and the parameters which are set according to the conventions adopted in this report. 
In addition,  we have significantly extended the mass range up to $\mhpm=2$ TeV and we explicitly calculate
the $\tan \beta$ dependence, by computing separately the contributions to the cross section proportional to $y_b^2$
and $y_t^2$ and rescaling each of them by the corresponding overall $\tan \beta$ factor. 
In the previous analysis~\cite{Flechl:2014wfa} the central value of the cross section was computed for all points 
in the $\tan \beta$ scan, the approximation was made that the size of the relative theoretical uncertainty is 
independent of the value of $\tan \beta$. The present analysis goes beyond this approximation by taking into account the theoretical uncertainty
associated to the running of the bottom Yukawa coupling up to the renormalization
scale: the uncertainties for all considered values of $\tan \beta$ are computed explicitly.

For a type-II 2HDM, the $\Pt\bar{\Pb}\PSHm$ coupling is given by $\sqrt{2}\, \left( y_{\Pt} \,P_R\cot\beta + y_{\Pb} \,P_L\tan\beta \right)$. We separately 
evaluate the $y_{\Pt}^2$ and $y_{\Pb}^2$ terms. 
The size of the interference term, $y_{\Pt} y_{\Pb}$, 
is proportional to $m_{\Pb}$ and will be neglected in the following. In the 5FS, this contribution is exactly zero, 
while in the 4FS, it has been shown~\cite{Degrande:2015vpa} that neglecting this term leads to an overestimate of the cross section 
by at most 5\% for $\mhpm=200$ GeV and less than 1\% for $\mhpm>600$ GeV. 
This estimate refers to $\tan \beta=8$; for all other values of $\tan \beta$, the size of this contribution is further suppressed by $\tan^2 \beta$ ($1/\tan^2 \beta$) for 
large (small) $\tan \beta$ values. In 
all cases, the impact of this term remains much smaller than the size of the theoretical uncertainties.
For what concerns supersymmetric corrections, effects due to virtual supersymmetric particles in the loop have to be taken into account. Such corrections
are finite and can be simply added to the total cross section. Among
these corrections the dominant ones are those that modify the relation between the $\Pb$ quark mass and its Yukawa coupling. This class of corrections are 
enhanced at large $\tan \beta$ and can be summed up to all orders through a modification of the $\Pb$ quark Yukawa 
coupling~\cite{Carena:1993bs, Hempfling:1993kv, Pierce:1996zz, Guasch:2003cv, Dittmaier:2009np, Noth:2008tw, Noth:2010jy, Mihaila:2010mp}. The remaining
SUSY-QCD effects are negligible at large $\tan \beta$ but can be of order $10\%$ at small $\tan \beta$. \\
We present results for the 4FS and 5FS schemes,
including the theoretical uncertainty, and combine the two schemes
according to the Santander matching~\cite{Harlander:2011aa}. Fully-matched computations have been presented for bottom-fusion 
initiated Higgs boson production in this
report~\cite{Forte:2015hba,Bonvini:2015pxa}, but are not available for charged Higgs boson production.
Throughout this report we present results
for the $\Pt\PH^-$ final state. The charge-conjugated final state can be included by simply multiplying the results by a factor two.

To estimate the theoretical uncertainty due to missing higher-order
contributions, we vary the renormalization scale $\mu_R$, the 
factorization scale $\mu_F$ and the scale $\mu_{\Pb}$ (which determines the 
running bottom quark mass in the Yukawa coupling and is set to $\mu_R$) by
a factor two about their central values. 
In addition to the scale uncertainties, we have computed
the PDF and $\alphas$ uncertainties following the
PDF4LHC15 recommendation. We stress that the PDF uncertainty computed with the PDF4LHC15 set
also accounts for the parametric uncertainty associated to the value of $\Mb$ used in PDF fits. 
PDF uncertainties are given at 68\% confidence level (CL). 
%The $\alphas$ uncertainty
%corresponds to a variation of $\pm 0.0012$ about the central
%value~\cite{Botje:2011sn}. As the uncertainty for the bottom mass we
%take $\Mb$ = 4.75 $\pm$ 0.25 GeV, which is a conservative choice
%compared to the uncertainty given in~\cite{Beringer:1900zz}. 

The results for heavy charged Higgs boson production within the 4FS 
are based on the calculation presented in
Ref.\,\cite{Dittmaier:2009np} and implemented in 
{\tt MG5\_aMC@NLO}~\cite{Alwall:2014hca, Degrande:2015vpa}, interfaced to the
LHAPDF library~\cite{Bourilkov:2006cj,Buckley:2014ana}. 
The renormalization and factorization scales are
set to $\mu$ = $(\mhpm+\Mt+\Mb)/3$. The resulting 4FS cross section and 
uncertainties are shown in \refF{fig:hplus-4fs}.

\begin{figure}[ht]
  \begin{center}
    \setlength{\unitlength}{\textwidth}
    \includegraphics[width=0.48\textwidth]{./WG3/ChargedHiggs/figures/xsec_2d_4fs.pdf}~~~~~~~
    \includegraphics[width=0.48\textwidth]{./WG3/ChargedHiggs/figures/tot_rel_2d_4fs.pdf}
    \caption{Cross section (left) and average relative uncertainty (right) for $\Pt\PSHpm + X$ production in the 4FS, as a function of $\mhpm$ and $\tan \beta$.}
    \label{fig:hplus-4fs}
  \end{center}
\end{figure}

\begin{figure}%[ht]
  \begin{center}
    \setlength{\unitlength}{\textwidth}
    \includegraphics[width=0.48\textwidth]{./WG3/ChargedHiggs/figures/xsec_2d_5fs.pdf}~~~~~~~
    \includegraphics[width=0.48\textwidth]{./WG3/ChargedHiggs/figures/tot_rel_2d_5fs.pdf}
    \caption{Cross section (left) and average relative uncertainty (right) for $\Pt\PSHpm + X$ production in the 5FS, as a function of $\mhpm$ and $\tan \beta$.}
    \label{fig:hplus-5fs}
  \end{center}
\end{figure}

For the calculation in the 5FS, the program
Prospino~\cite{Plehn:2002vy} has been employed, interfaced to the
LHAPDF library~\cite{Bourilkov:2006cj}.  The renormalization scale is
set to $\mu_{\rm R}$ = $(\mhpm+\Mt)/2$, while the factorization
scale $\mu_{\rm F}=\tilde{\mu}$ is chosen according to the method
proposed in~\cite{Maltoni:2012pa}. The effective factorization scale
entering the initial state logarithms is proportional to the hard
scale, but modified by a phase space factor which tends to reduce the
size of the logarithms for processes at hadron colliders. A table with $\tilde \mu$ values for
the various charged Higgs boson masses is provided on the Twiki 
page \url{https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWGMSSMCharged}.
The  resulting 5FS cross section and uncertainties are shown in \refF{fig:hplus-5fs}.
Before presenting matched predictions, we would like to make some comments on the numbers, see \refT{tab:hp_xsec_summary}. %Table~\ref{tab:hp_xsec_summary}. 
The central values of the four- and five-flavour predictions are compatible within uncertainties, as observed also in 
Ref.~\cite{Flechl:2014wfa}, although the agreement is worse. This is mostly due to the different way of computing the bottom Yukawa coupling
compared to previous predictions. As far
as uncertainties are concerned, the 4FS numbers are affected by a total uncertainty which is about 50\% larger than the
one of the 5FS. Furthermore, in the 5FS the 
largest contribution to the total uncertainty comes from PDFs, in particular
for heavy Higgs bosons $\mhpm > 1 TeV$, while scale uncertainties remain around or below 10\% also
at higher values of the masses.  
In the 4FS, the situation is reversed, as scale uncertainties are dominant and reach up to 20\%
for $\mhpm \sim 1 TeV$. PDF uncertainties on the other hand are smaller, about half the
PDF uncertainty of the 5FS calculation. The smaller scale uncertainty of the 5FS calculation 
is also observed in the case of $\bbh$: in particular, in Ref.~\cite{Forte:2015hba} 
it has been suggested that this fact may be associated with
theoretical uncertainties coming from mass terms included only in the 4FS powers of $\Mb$.
Overall the total theoretical uncertainty is smaller in the 5FS calculation by about 30\%.\\

%
\begin{table}
%\renewcommand*{\arraystretch}{1.2}
\setlength{\tabcolsep}{4pt}
\caption{
Comparison of $pp \to tH^+ + X$ cross sections (in units of pb) and percentage uncertainties for the 4FS, the 5FS, and when matching both.
}
\label{tab:hp_xsec_summary}
\small
\begin{center}
    \begin{tabular}{rr|llll|llll|ll}
      \toprule                                                                                                                 
      $\mhpm$   & $\tan\beta$     & \multicolumn{4}{c|} {4FS}        & \multicolumn{4}{c|} {5FS}       & \multicolumn{2}{c} {matched} \\
%     $\mhpm$ [GeV] & $\tan\beta$ & $\sigma$ [pb] & $\Delta \sigma^\mathrm{scale} / \sigma$ & $\Delta \sigma^\mathrm{pdf} / \sigma$ & $\Delta \sigma^\mathrm{tot} / \sigma$     
%                                 & $\sigma$ [pb] & $\Delta \sigma^\mathrm{scale} / \sigma$ & $\Delta \sigma^\mathrm{pdf} / \sigma$ & $\Delta \sigma^\mathrm{tot} / \sigma$
%                                 & $\sigma$ [pb] & $\Delta \sigma^\mathrm{tot} / \sigma$\\
%     $\mhpm$ [GeV] & $\tan\beta$ & $\sigma$ [pb] & $\Delta \sigma^\mathrm{scale}$ & $\Delta \sigma^\mathrm{pdf}$ & $\Delta \sigma^\mathrm{tot}$     
      $[$GeV$]$ &           & $\sigma$      & $\Delta \sigma^\mathrm{scale}$ & $\Delta \sigma^\mathrm{pdf}$ & $\Delta \sigma^\mathrm{tot}$     
                            & $\sigma$      & $\Delta \sigma^\mathrm{scale}$ & $\Delta \sigma^\mathrm{pdf}$ & $\Delta \sigma^\mathrm{tot}$
                            & $\sigma$      & $\Delta \sigma^\mathrm{tot}$\\
\midrule
      % \midrule
      200 &  1 &     2.90     & 13.1 & 3.1 & 16.6 &   3.63     & 4.1 & 6.6 & 11.8 &     3.36     & 12.5 \\ 
       200 &  8 &     0.0961   & 15.7 & 3.2 & 18.9 &   0.1194   & 6.4 & 5.9 & 13.8 &     0.1109   & 14.4 \\ 
       200 & 30 &     0.718    & 18.0 & 3.2 & 21.2 &   0.886    & 8.5 & 5.6 & 15.7 &     0.825    & 16.2 \\ 
       600 &  1 &     0.143    & 13.3 & 4.9 & 18.9 &   0.186    & 2.7 & 8.1 & 12.9 &     0.175    & 12.6 \\ 
       600 &  8 &     0.00461  & 16.7 & 5.0 & 21.9 &   0.00602  & 5.1 & 7.8 & 15.1 &     0.00566  & 14.8 \\ 
       600 & 30 &     0.0336   & 19.6 & 5.1 & 24.7 &   0.0440   & 7.3 & 7.7 & 17.0 &     0.0413   & 16.9 \\ 
      1000 &  1 &     0.0162   & 14.2 & 6.8 & 21.0 &   0.0217   & 2.3 & 10.6 & 14.7 &     0.0204   & 14.2 \\ 
      1000 &  8 &     0.000516 & 17.4 & 7.0 & 24.4 &   0.000697 & 5.2 & 10.0 & 16.6 &     0.000655 & 16.8 \\ 
      1000 & 30 &     0.00371  & 20.8 & 7.0 & 27.8 &   0.00506  & 7.9 & 9.7 & 18.8 &     0.00475  & 19.4 \\ 
%       200 &  1 &     2.90     & 0.38     & 0.09     & 0.48     &     3.63     & 0.15     & 0.24     & 0.43     &     3.36     & 0.42     \\ 
%       200 &  8 &     0.0961   & 0.0151   & 0.0031   & 0.0182   &     0.1194   & 0.0076   & 0.0071   & 0.0165   &     0.1109   & 0.0160   \\ 
%       200 & 30 &     0.718    & 0.129    & 0.023    & 0.152    &     0.886    & 0.075    & 0.050    & 0.139    &     0.825    & 0.134    \\ 
%       600 &  1 &     0.143    & 0.019    & 0.007    & 0.027    &     0.186    & 0.005    & 0.015    & 0.024    &     0.175    & 0.022    \\ 
%       600 &  8 &     0.00461  & 0.00077  & 0.00023  & 0.00101  &     0.00602  & 0.00031  & 0.00047  & 0.00091  &     0.00566  & 0.00084  \\ 
%       600 & 30 &     0.0336   & 0.0066   & 0.0017   & 0.0083   &     0.0440   & 0.0032   & 0.0034   & 0.0075   &     0.0413   & 0.0070   \\ 
%      1000 &  1 &     0.0162   & 0.0023   & 0.0011   & 0.0034   &     0.0217   & 0.0005   & 0.0023   & 0.0032   &     0.0204   & 0.0029   \\ 
%      1000 &  8 &     0.000516 & 0.000090 & 0.000036 & 0.000126 &     0.000697 & 0.000036 & 0.000070 & 0.000116 &     0.000655 & 0.000110 \\ 
%      1000 & 30 &     0.00371  & 0.00077  & 0.00026  & 0.00103  &     0.00506  & 0.00040  & 0.00049  & 0.00095  &     0.00475  & 0.00092  \\ 
    \bottomrule                                                                                                              
    \end{tabular}
\end{center}
\end{table}



To provide a final prediction for heavy charged Higgs boson production we
combine the NLO 4FS and 5FS cross sections according to Santander
matching\,\cite{Harlander:2011aa}. We note that the 4FS and 5FS calculations
provide the unique description of the cross section in the asymptotic
limits $M_\phi/\Mb \to 1$ and $M_\phi/\Mb \to \infty$, respectively (here
and in the following $M_\phi$ denotes a generic Higgs boson mass). The
4FS and 5FS are thus combined in such a way that they are given
variable weight, depending on the value of the Higgs boson mass. The
difference between the two approaches is formally logarithmic.
Therefore, the dependence of their relative importance on the
Higgs boson mass should be controlled by a logarithmic term, i.e.\
\begin{equation}
  \sigma^{\rm matched} = \frac{\sigma^{\rm 4FS} + w\,\sigma^{\rm 5FS}}{1 +
    w} \quad \mbox{with} \quad w= \ln \frac{\mathrm{M}_\phi}{\Mb} - 2\,.
\end{equation}
The theoretical uncertainties are combined according to 
\begin{equation}
  \Delta\sigma^{\rm matched}_\pm = \frac{\Delta\sigma^{\rm 4FS}_\pm + w\Delta\sigma^{\rm 5FS}_\pm}{1 +
    w}
\end{equation}
where $\Delta\sigma^{\rm 4FS}_\pm$ and $\Delta\sigma^{\rm 5FS}_\pm$
are the upper/lower uncertainty limits of the 4FS and the 5FS,
respectively.
\begin{figure}[t]
  \begin{center}
    \setlength{\unitlength}{\textwidth}
    \includegraphics[width=0.47\textwidth]{./WG3/ChargedHiggs/figures/xsec_tb_1.pdf}~~~~~~~
    \includegraphics[width=0.47\textwidth]{./WG3/ChargedHiggs/figures/xsec_mhp_200.pdf}
    \includegraphics[width=0.47\textwidth]{./WG3/ChargedHiggs/figures/xsec_tb_8.pdf}~~~~~~~
    \includegraphics[width=0.47\textwidth]{./WG3/ChargedHiggs/figures/xsec_mhp_600.pdf}
    \includegraphics[width=0.47\textwidth]{./WG3/ChargedHiggs/figures/xsec_tb_30.pdf}~~~~~~~
    \includegraphics[width=0.47\textwidth]{./WG3/ChargedHiggs/figures/xsec_mhp_2000.pdf}
    \caption{Cross section for $\Pt\PSHpm + X$ production, after matching the 4FS and 5FS results. The result is given for three different 
values of $\tan \beta$ (left) and of $\mhpm$ (right).}
    \label{fig:hplus-matched}
  \end{center}
\end{figure}

The resulting matched cross section is shown in \refF{fig:hplus-matched}.
We observe that the NLO 4FS and 5FS predictions are in fair mutual
agreement, with differences of the central values of roughly $20\%$. 
The dynamical choice
for $\mu_{\rm F}$ in the 5FS used here improves the matching of the
predictions in the two schemes. The overall theoretical uncertainty of
the matched NLO prediction is about 10\%.
%\emph{This still needs to be checked. PDF uncertainties are suspiciously low, and total 
%uncertainties only about half of what we saw before}. 
Cross sections and uncertainties for a two-dimensional grid, 
$\mhpm=200 \UGeV - 2000 \UGeV$ and $\tan \beta=0.1-60$, can be retrieved 
online\footnote{\url{https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWGMSSMCharged}}. 

In contrast to the type-II 2HDM, for type-I the bottom Yukawa coupling is not enhanced by $\tan\beta$,
so that $g_{\Pt\bar{\Pb}\PSHm}|_{\rm type-I} = \sqrt{2}\, \Mt/ v \,P_R\cot\beta + {\cal O}(\Mb/\Mt)$. Up to corrections suppressed by ${\cal O}(\Mb/\Mt)$, the cross section for heavy charged Higgs boson production in the
type-I 2HDM, $\sigma |_{\rm type-I} \propto g^2_{\Pt\bar{\Pb}\PSHm}|_{\rm type-I} \propto 2 (\Mt/v)^2 \cot^2\beta + {\cal O}(\Mb/\Mt)$, can thus be obtained
from the type-II cross section, $\sigma |_{\rm type-II, \tan\beta = 1} \propto g^2_{\Pt\bar{\Pb}\PSHm}|_{\rm type-II, \tan\beta = 1} \propto 2 (\Mt/v)^2 + {\cal O}(\Mb/\Mt)$, evaluated at $\tan\beta = 1$ and rescaled by $\cot^2\beta$. 
This relation is correct to all orders in QCD, but \textit{not} to all orders in the electroweak corrections. Given the overall theoretical uncertainty of the cross section prediction it is, 
however, an excellent approximation and sufficient for all practical purposes. Note that the charged Higgs boson cross section predictions for the type-I and type-II 2HDMs also hold for the so-called lepton-specific 
and flipped 2HDMs, respectively, see e.g. Ref.~\cite{Branco:2011iw}.



\section{Differential production cross sections}

\begin{figure}[t]
  \begin{center}
    \setlength{\unitlength}{\textwidth}
    \includegraphics[width=0.50\textwidth]{./WG3/ChargedHiggs/figures/4vs5F_LOvsNLO_pttop_200.pdf}~~~~~~~
    \includegraphics[width=0.50\textwidth]{./WG3/ChargedHiggs/figures/4vs5F_LOvsNLO_pth_200.pdf}
    \vspace{-0.5cm}
    \caption{LO and NLO predictions matched with {\tt Pythia8} in the 4FS and 5FS, separately for the the $y_{\Pb}^2$ and 
$y_{\Pt}^2$ terms, for the transverse momentum of the top
quark (left) and of the charged Higgs boson (right). Rescaling factors are introduced in the main
frame for better visibility. The first four smaller frames at the bottom show the ratio over the NLO prediction in
the 5FS for the $y_b^2$ and $y_t^2$ terms, and the scale and PDF uncertainty bands for the NLO curves.
The bottom frame shows the differential $K$ factor (NLO/LO) for the four predictions. A charged Higgs boson mass of $\mhpm = 200$ GeV is assumed.}
    \label{fig:hdiff-200-ptht}
  \end{center}
\end{figure}

We now present differential distributions for the production of a heavy charged Higgs boson
in association with a top quark in a type-II 2HDM. We present results in the 4FS and
5FS up to NLO accuracy and including matching to parton shower Monte Carlos. Fully differential results in the 5FS have 
been available for some years~\cite{Weydert:2009vr, Klasen:2012wq}, while 4FS results have been presented only recently~\cite{Degrande:2015vpa}. In this
chapter, we follow
the methodology presented in Ref.~\cite{Degrande:2015vpa}, where fully-differential results in the 4FS were presented for
the first time using
{\tt MG5\_aMC@NLO}~\cite{Alwall:2014hca} together with {\tt Herwig++}~\cite{Bahr:2008pv} 
or {\tt Pythia8}~\cite{Sjostrand:2007gs}. 
In particular, we use a reduced shower scale (generated in the range $0.025 \sqrt s_0 < \mu^2_{sh} < \times 0.25 \sqrt s_0$, $s_0$ being
the born-level partonic centre of mass energy) with respect to 
the default one in {\tt MG5\_aMC@NLO}\ which
improves the matching between NLO+PS and NLO predictions at large transverse momentum. 
In the results shown here, we adapt 
relevant input parameters to match 
the recommendations followed throughout this report. In particular, differences in the setup with respect to 
Ref.~\cite{Degrande:2015vpa} include the running of the bottom Yukawa up to the renormalization scale using 
four loops  for the central predictions and two loops for the renormalization scale variations and the usage of the PDF4LHC15 parton 
distributions~\cite{Butterworth:2015oua}. We employ 4FS and 5FS PDFs consistently with the flavour scheme of the computation. In both 
cases (and also for LO predictions) PDFs are evolved at NLO. As in Ref.~\cite{Degrande:2015vpa}, we assume that the top quark decays leptonically 
while the charged Higgs remains stable. Therefore, $b$ jets in the final state will typically come from the top quark and from the matrix element.\\

\begin{figure}[t]
  \begin{center}
    \setlength{\unitlength}{\textwidth}
    \includegraphics[width=0.50\textwidth]{./WG3/ChargedHiggs/figures/4vs5F_LOvsNLO_ptbj1_200.pdf}~~~~~~~
    \includegraphics[width=0.50\textwidth]{./WG3/ChargedHiggs/figures/4vs5F_LOvsNLO_ptbj2_200.pdf}
    \vspace{-0.5cm}
%    \caption{Like \refF{fig:hdiff-200-ptht}, for the transverse momentum of the hardest (left) and     second hardest $\Pb$ jet (right).}
    \caption{LO and NLO predictions matched with {\tt Pythia8} in the 4FS and 5FS, separately for the the $y_{\Pb}^2$ and 
$y_{\Pt}^2$ terms, for the transverse momentum of the hardest (left) and 
    second hardest $\Pb$ jet (right). Rescaling factors are introduced in the main
frame for better visibility. The first four smaller frames at the bottom show the ratio over the NLO prediction in
the 5FS for the $y_b^2$ and $y_t^2$ terms, and the scale and PDF uncertainty bands for the NLO curves.
The bottom frame shows the differential $K$ factor (NLO/LO) for the four predictions. A charged Higgs boson mass of $\mhpm = 200$ GeV is assumed.}
    \label{fig:hdiff-200-ptbj}
  \end{center}
\end{figure}

\begin{figure}[t]
  \begin{center}
    \setlength{\unitlength}{\textwidth}
    \includegraphics[width=0.50\textwidth]{./WG3/ChargedHiggs/figures/4vs5F_LOvsNLO_ptbh1_200.pdf}~~~~~~~
    \includegraphics[width=0.50\textwidth]{./WG3/ChargedHiggs/figures/4vs5F_LOvsNLO_ptbh2_200.pdf}
%    \caption{Like \refF{fig:hdiff-200-ptht}, for the transverse momentum of the hardest (left) and second hardest $\PB$ hadron (right).}
    \vspace{-0.5cm}
    \caption{LO and NLO predictions matched with {\tt Pythia8} in the 4FS and 5FS, separately for the the $y_{\Pb}^2$ and 
$y_{\Pt}^2$ terms, for the transverse momentum of the hardest (left) and 
    second hardest $\Pb$ hadron (right). Rescaling factors are introduced in the main
frame for better visibility. The first four smaller frames at the bottom show the ratio over the NLO prediction in
the 5FS for the $y_b^2$ and $y_t^2$ terms, and the scale and PDF uncertainty bands for the NLO curves.
The bottom frame shows the differential $K$ factor (NLO/LO) for the four predictions. A charged Higgs boson mass of $\mhpm = 200$ GeV is assumed.}
     \label{fig:hdiff-200-ptbh}
  \end{center}
\end{figure}

In Figs.~\ref{fig:hdiff-200-ptht}-\ref{fig:hdiff-200-njet}, 
we present a comparison between the two schemes at LO and NLO matched with {\tt Pythia8}, for several differential observables. 
All figures refer to the case of a $\mhpm = 200\UGeV$ Higgs boson and $\tan\beta=8$. For the sake of generality, 
the  $y_{\Pt}^2$ and $y_{\Pb}^2$ contributions to the cross section are
shown separately, omitting the negligible interference term. Predictions for different values of $\tan \beta$ can be obtained
by a trivial rescaling of the shown histograms. All figures have the same layout, namely: a main frame with the absolute predictions
in the two schemes, at LO and NLO, and five smaller frames below. In the first four of these frames, the ratio of histograms in the main frame over the 5FS NLO prediction is shown,
together with scale (first and third frames, respectively for the $y_{\Pb}^2$ and $y_{\Pt}^2$ contributions) and PDF uncertainties 
(second and fourth frames). The last frame shows the differential $K$-factors (NLO/LO).

Before looking at the various observables, we outline some general features: the first one is that, as expected, the inclusion of NLO corrections brings 
predictions in the two schemes much closer than at LO. The second is about the size of uncertainties at NLO, which follows the same pattern as
the inclusive cross section described in the previous section: for observables which are described
with the same accuracy in the two schemes (e.g. top and Higgs boson $p_T$, $\Pb$-jet rates for zero and one jet), scale uncertainties in the 4FS are usually larger
than in the 5FS ($\pm10-12\%$ vs$\pm4-6\%$). PDF uncertainties display instead an opposite behaviour (at least for this value of the charged Higgs boson mass): they 
are larger in the 5FS, with a similar size as scale uncertainties, and smaller in the 4FS, where they are negligible with respect to scale variations.
Finally, due to the additional running of the bottom Yukawa, the $y_{\Pb}^2$ contribution
has a broader scale uncertainty band than the $y_{\Pt}^2$ one. 

We now turn to compare the two schemes for a number of differential observables: in \refF{fig:hdiff-200-ptht} we observe that for the transverse momentum of the top quark (reconstructed using Monte Carlo truth information)
and the Higgs boson the difference between the two schemes can be compensated by a simple overall 
rescaling of the total rates at NLO (similar to the one observed in the previous section) 
while LO predictions in the two schemes have quite different
shapes (in particular for the top quark). The same level of agreement is expected to be found also for observables related to the 
decay products of the top quark (and of the charged Higgs boson). 
Indeed, the $p_T$ spectrum of the hardest $\Pb$ jet (left plot in \refF{fig:hdiff-200-ptbj})
displays a flat ratio between the 4FS and 5FS at NLO up to $\approx 120$ GeV. While below 120 GeV the hardest $\Pb$ jet essentially 
coincides with the $\Pb$ jet from the top quark, above 120 GeV secondary $\Pg\to \Pb\bar{\Pb}$
splitting from hard gluons becomes relevant. This fact is reflected
in the growth of the 5FS scale uncertainty band and of the $k$ factor. Larger differences between the two schemes 
appear for the second-hardest $\Pb$ jet, see right plot in \refF{fig:hdiff-200-ptbj}. 
This distribution is expected to be poorly described in the 5FS. 
In particular, its kinematics in the 5FS at LO is determined
by the shower, while at NLO it is driven by a tree-level matrix element (therefore being
formally only LO accurate). As expected, the 5FS develops larger $k$ factors.
The 4FS calculation thus describes these observables significantly better, both because of its more robust 
perturbative behaviour and because of the proper modelling of the final-state $\Pb$ jets.\\
The effect of the different treatment of the bottom quark in the two schemes is even more
visible for the transverse momentum of the hardest and second hardest $\PB$ hadron 
(left and right plot in \refF{fig:hdiff-200-ptbh}). At medium
and large $\pT$ of the hardest $\PB$ hadron similar effects as for the hardest $\Pb$ jet are observed. 
At low momentum, the 4FS prediction is suppressed with respect to the 5FS . 
This is most likely due to mass effects as these kinematical regions correspond to one
$\Pb$ quark being collinear to the beam. In the 5FS these configurations are enhanced because of
the collinear singularities, while in the 4FS such singularities are screened by the $\Pb$ quark mass.
Therefore, even after the parton shower, the 5FS is reminiscent of the collinear enhancement. In the case of
the second-hardest $\PB$ such effects are further enhanced.

\begin{figure}[t]
  \begin{center}
    \setlength{\unitlength}{\textwidth}
    \includegraphics[width=0.50\textwidth]{./WG3/ChargedHiggs/figures/4vs5F_LOvsNLO_nalljet_200.pdf}~~~~~~~
    \includegraphics[width=0.50\textwidth]{./WG3/ChargedHiggs/figures/4vs5F_LOvsNLO_nbjet_200.pdf}
%    \caption{Like \refF{fig:hdiff-200-ptht}, for jet (left) and $\Pb$ jet (right) multiplicity.}
    \vspace{-0.5cm}
    \caption{LO and NLO predictions matched with {\tt Pythia8} in the 4FS and 5FS, separately for the the $y_{\Pb}^2$ and 
$y_{\Pt}^2$ terms, for for jet (left) and $\Pb$ jet (right) multiplicity. Rescaling factors are introduced in the main
frame for better visibility. The first four smaller frames at the bottom show the ratio over the NLO prediction in
the 5FS for the $y_b^2$ and $y_t^2$ terms, and the scale and PDF uncertainty bands for the NLO curves.
The bottom frame shows the differential $K$ factor (NLO/LO) for the four predictions. A charged Higgs boson mass of $\mhpm = 200$ GeV is assumed.}
    \label{fig:hdiff-200-njet}
  \end{center}
\end{figure}
Finally, looking at jet (\refF{fig:hdiff-200-njet} left) 
and $\Pb$-jet multiplicities (\refF{fig:hdiff-200-njet} right), we observe again
that the effect of NLO corrections is very different in the two schemes: while in the 5FS NLO corrections
make the jet spectrum moderately harder, in the 4FS they tend to make it softer, with greater enhancements in the low multiplicity bins. Despite this fact,
at NLO the 4FS still shows a slightly harder spectrum than the 5FS.
Irrespective of whether $\Pb$-tagged jets are required or not, the overall effect of NLO QCD corrections is to bring the two schemes in much 
better agreement in the zero- and one-jet bin. The two-jet bin is only described at LO accuracy by the 5FS NLO prediction, therefore for this
bin the 4FS prediction is expected to be more reliable. Higher multiplicities are described with rather poor accuracy (LO or even LL) by both schemes.

All in all, the global behaviour of predictions closely follows what has been observed in 
Ref.~\cite{Degrande:2015vpa}. The interested reader can find more details 
in that paper, in particular for what concerns the comparison of matched and fixed-order computations and the usage of different parton showers.

\section{Recommendations for signal simulation}
We conclude this chapter by providing some recommendations for the simulation of heavy charged Higgs boson production at the LHC Run 2.
The use of 4FS fully-differential predictions is recommended for any realistic fully-differential signal simulation for experimental
searches. This recommendation is backed by two sets of evidences: first, for a large number of observables, 
the 4FS prediction provides a better description of the
final state kinematics; second, it reduces the systematic error related to the usage of a given parton
shower. Moreover, when matching the NLO calculation to the shower, we recommend to use
a lower shower scale by reducing the current default value from {\tt MG5\_aMC@NLO}
by a factor four. This corresponds to a shower scale generated in the range $0.025\sqrt s_0 < \mu^2_{sh} < \times 0.25 \sqrt s_0$, $s_0$ being
the born-level partonic centre of mass energy. 
This choice provides a better matching
to the fixed-order computation at large transverse momenta, slightly reduces the parton shower
dependence and also improves the agreement of four- and five-flavour scheme computations.\\
For what concerns the normalization of the signal total cross-section, the Santander-matched prediction provided in this section should 
be employed as central value.

As far as the estimate of theoretical uncertainties is concerned, the most obvious choice would be to use the scale and PDF uncertainties
coming from the 4FS fully-differential computation. 
For observables inclusive in the $\Pb$ kinematics,
this choice results into a slightly larger theoretical uncertainty than the one associated with the matched prediction (suggested for the normalization). On 
the other hand, for more exclusive observables, the 4FS uncertainty is smaller and more reliable than the 5FS one.\\



