
In Run 2 of the LHC, the programmatic search for exotic Higgs boson decays will
 increasingly become an important topic of study.  To help guide this experimental 
program, in this section we provide a set of recommendations for searches for
the production of exotic particles in the decays of the Higgs boson.

\paragraph*{\textbf{A signature-based search program for exotic decays:}}
We
recommend that the search program for exotic Higgs boson decays take a
signature-based approach, targeting individual signatures rather than
specific BSM models.
A model that gives rise to exotic decays of the Higgs boson
will typically lead to many different possible final states.  For
instance, a model containing a light pseudo-scalar with
Yukawa-weighted decays will yield, in addition to the dominant $h\to
aa\to 4b$ decay, the much rarer but cleaner $h\to aa\to bb\mu\mu$
decay \cite{Curtin:2014pda}.  Combining the results of searches for
both exotic modes can substantially boost overall sensitivity to the
overall exotic branching fraction $h\to aa$ \cite{Curtin:2013fra}.  On
the other hand, a single given final state, such as $h\to 4b$, may be
predicted by a wide range of different theories; for a detailed
discussion of this point, see Ref.~\cite{Curtin:2013fra}.

\paragraph*{\textbf{Presentation of results:}}
We recommend that searches for specific signatures quote their results
in terms of $\sigma\times\br$.  This model-independent prescription
allows for more flexible interpretation of results in a broad range of
theories, and facilitates the ultimate combination of results obtained
in different final states to achieve the best sensitivity to a
specific model.

\paragraph*{\textbf{Production cross-sections:}}  
The SM Higgs boson width is dominated by the small $b$-quark Yukawa coupling
and is accidentally small, which means that even small couplings of
the Higgs to BSM physics can generate exotic branching fractions of
order the current upper bound, ${\rm{BR}}(h\to\mathrm{BSM})\sim 30\%
$.  However, Higgs boson production cross-sections are controlled by
the much larger electroweak and top couplings, and are thus
substantially less affected by small couplings of the Higgs to new
physics.  As a simple example, consider the case of a real scalar $s$
which mixes with the SM Higgs boson through a small Higgs portal
interaction, $\mathcal {L}_{int} = -\epsilon/2\, |H|^2s^2$, after both
$H$ and $s$ acquire vacuum expectation values, $v$ and $\langle
s\rangle$, respectively.  The Higgs boson production cross-section then
receives corrections at the order $\mathcal{O}(\theta^2)$, where
$\theta = \epsilon \langle s\rangle v/(m_h^2-m_s^2)$ is the $s$-$h$
mixing angle.  However, the values of $\theta$ that induce $\br(\hsm
\to ss) \sim 20\%$ are $\mathcal{O}(10^{-2})$ \footnote{In this
model, a given exotic branching fraction does not uniquely determine the
mixing angle; these numbers are obtained in the generic regime
$\langle s\rangle\sim m_s$.}.
Thus, {\em corrections to Higgs boson production cross-sections are
subleading} in theories yielding exotic Higgs boson decays consistent with
Run 1 data.  As these corrections are model-dependent, and as many
different models can yield the same Higgs boson decay final states, to
maximize the flexibility and utility of experimental searches for
exotic decays we recommend that searches for exotic Higgs boson decays
assume SM Higgs boson production cross-sections.  In the event that an
exotic decay mode is discovered, it will then be of high interest to
consider how possible effects of specific related new physics models
would affect Higgs boson properties and production cross-sections.  However,
to institute and support a broad program of searches for
direct production of exotic particles in Higgs boson decays, we recommend
that SM Higgs boson production be used as the baseline for generating signal
events and performing searches.


\paragraph*{\textbf{Signal event generation:} }
Apart from the special cases of decays with detector-stable or
highly-displaced objects, exotic Higgs boson decays result in at least three
objects in the final state. 
Since the Higgs itself is not heavy with respect to the LHC's
centre-of-mass (CM) energy, one generic feature of exotic Higgs boson decays
is thus that the particles in the final state tend to be
soft. 
The spectrum of the Higgs boson decay products is a major factor in
determining signal acceptance.  Consequently, in carrying out searches
for exotic Higgs boson decays, it is important to model Higgs boson production and
in particular the Higgs boson $p_T$ spectrum with some degree of care.  On
the other hand, the ease and flexibility with which BSM models can be
implemented in {\tt MadGraph} is also of practical importance to
support a broad search program capable of covering the vast number of
possible BSM decays.

Our baseline recommendation for signal event generation in searches
for BSM particles produced in exotic Higgs boson decays is to use {\tt
MadGraph5}~\cite{Alwall:2011uj}, followed by showering and
hadronization in {\tt Pythia}~\cite{Sjostrand:2007gs}.  This
recommendation is supported by the studies in the following section,
where we compare kinematic distributions for the Higgs and its decay
products in the prototypical decay $h\to aa\to 4b$ as predicted by
{\tt MadGraph}$+${\tt Pythia} to those predicted by {\tt
POWHEG}~\cite{Nason:2004rx,Frixione:2007vw,Alioli:2010xd}$+${\tt
Pythia}, demonstrating good agreement overall.  For gluon fusion
production in {\tt MadGraph}, Higgs boson production should be matched to at
least one jet; see Section~\ref{sec:4b} for further relevant settings.
For VBF and VH production, Higgs boson production is well-modeled in
{\tt MadGraph} without matching. In all cases the inclusive Higgs boson production
cross-section should be normalized to the predictions from~\ref{subsec:ADDFGHLM}.
% WG1\textcolour{red}{insert section ref here}.  
For searches that
dominantly rely on gluon fusion production, the Higgs boson $p_T$ spectrum
should be reweighted according to the (N)NLO predictions following the
recommendations of~\ref{subsec:ADDFGHLM}.
% WG1 \textcolour{red}{insert section ref here}.


\paragraph*{\textbf{Mass ranges:} }
While the primary focus of searches for exotic Higgs boson decays is and
should remain the decays of the discovered 125 GeV SM-like Higgs boson, 
analyses should keep in mind the possibility of extending the
search to cover other possible masses for the parent particle,
particularly but not exclusively lower masses.  This extends the
sensitivity of searches to include cases where the originating 125 GeV
Higgs boson decay includes small amounts of missing energy
\cite{Curtin:2013fra} as well as the potential direct production of
BSM Higgs bosons.
