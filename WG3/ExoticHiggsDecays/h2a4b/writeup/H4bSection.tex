%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Introduction}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


In this section we study the decay topology where the Higgs boson decays to
two exotic particles of the same mass, each of which then undergoes a
prompt two-body decay to visible SM particles $Y,\,Y'$: $h\to XX\to 2Y2Y'$.
This decay topology is naturally realized in many well-motivated BSM
frameworks.  In particular, extensions of the Higgs sector by an
additional, possibly complex, singlet scalar can naturally have Higgs boson 
decays to (pseudo-)scalars as one of their leading signatures.
These (pseudo-)scalars decay mainly to fermions, with preference for
heavy flavour. Signals of this class of models, SM+S and 2HDM+S, are
described in detail in \cite{Curtin:2013fra}. The NMSSM is one of the
best-studied examples of this type of extended Higgs sector. It has a
large portion of parameter space where an approximate $R$-symmetry
yields a SM-like Higgs boson with appreciable branching ratio into a
pair of light pseudo-scalars $a$~\cite{Dobrescu:2000yn,Ellwanger:2003jt,Dermisek:2005ar,Morrissey:2008gm,Belyaev:2010ka}. Other motivations for
singlet-extended Higgs sectors include models of first-order
electroweak phase transitions \cite{Profumo:2007wc,Blinov:2015sna} and thermal dark matter
\cite{Silveira:1985rk, Pospelov:2007mp, Ipek:2014gua, Martin:2014sxa}. In composite
Higgs models, a symmetry-protected light pseudo-scalar in the spectrum
may be fermiophobic, with dominant decays to gluon or photon pairs,
thus yielding the exotic decay modes $h\to aa \to 4g, 2\gamma 2g,
4\gamma$~\cite{Dobrescu:2000jt, Chang:2006bw, Bellazzini:2009xt,
  Chen:2010wk, Falkowski:2010hi}.  Another
well-studied extension of the SM is a Higgsed dark $U(1)$ that
kinetically mixes with SM hypercharge
\cite{Holdom:1985ag,Galison:1983pa,Dienes:1996zr}, in which case the
Higgs boson decay $h\to Z_D Z_D\to 2f 2f'$ yields final states weighted by
gauge couplings, rather than Yukawa couplings, giving relatively
leptophilic signatures
\cite{Gopalakrishna:2008dv,Martin:2011pd,Chang:2013lfa,Davoudiasl:2013aya,Curtin:2014cca}.
All these exotic decays have very similar parton-level kinematics
\footnote{Angular correlations present in the decay $h\to Z_D Z_D\to
  4f$ will modify the final state fermion distributions relative to the
  decay $h\to ss (aa)\to 4f$, but the (pseudo-)scalar decays are still
  a good guide to the overall kinematics.}.

As discussed in Section~\ref{sec:recommendations}, exotic Higgs boson
decays are characterized by low $p_T$ objects in the final state.
Thus, object acceptance becomes one of the main limiting factors in
recording and reconstructing many exotic Higgs boson decays.  Consequently,
a good understanding of the parent Higgs boson and resulting decay
product kinematics is necessary to assess realistic triggering
opportunities and analysis strategies.

Here we study the (parton-level) kinematics of the final state particles in the
prototypical exotic decay $h\to aa\to 4b$ in depth, considering gluon
fusion, VBF, and WH associated production modes.  In addition, we
compare the predictions of LO and NLO generators, showing results for
Higgs boson production using both {\tt MadGraph}~\cite{Alwall:2011uj} and
{\tt POWHEG}~\cite{Nason:2004rx,Frixione:2007vw,Alioli:2010xd}. 






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Signal model and event generation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We consider augmenting the SM with a singlet pseudo-scalar $a$, which
obtains interactions with SM fermions through mixing with the
pseudo-scalar state $A^0$ in a 2HDM.  After electroweak symmetry
breaking, the relevant interaction terms are
%
\beq
\mathcal{L}^{\rm BSM}\supset i y^a_b a \bar b \gamma_5 b + \frac 1 2
\lambda_{aH} h a^2 + \frac 1 2 m_a^2 a^2,
\eeq
%
where $m_a<m_h/2$ is the mass of the pseudo-scalar, the effective
Yukawa coupling $y^a_b$ controls the singlet's decay into $b\bar b$
pairs, and the trilinear coupling $\lambda_{aH}$ determines the
partial width for the Higgs boson decay into pairs of pseudo-scalars.  A
discussion of how these parameters depend on the couplings in the full
2HDM+S Lagrangian can be found in \cite{Curtin:2013fra}.  The
trilinear coupling and the pseudo-scalar mass can be independently
adjusted, making the pseudo-scalar mass and the Higgs boson branching fraction $\br
(\hsm\to aa)$ independent parameters.  We assume that $y^a_b$ is large
enough to yield prompt decays; displaced decays are discussed in
Section~\ref{sec:displaced}.

The exotic Higgs boson decay mode $\hsm \to a a (ss) \to b \bar b b
\bar b $ will be the leading  signature of a broad class of SM+S, 
2HDM+S theories.  Here we explore the
kinematics of the final state $b$-partons in this decay for a range of
pseudo-scalar masses $m_a = 20, 30, 40, 50$ and $60$~GeV. 
%The Higgs boson mass is taken to be $m_\hsm = 125$~GeV.  
The kinematics for a scalar
field $s$ decaying to bottom quark pairs are identical.  We consider
three Higgs boson production channels: gluon fusion (ggF); $W$-boson
associated (WH) production; and weak vector-boson-fusion (VBF)
production.  In each production channel, we compare the differential
predictions from events generated at LO ({\tt Madgraph5}$+$ {\tt
  Pythia}) to those generated at NLO ({\tt Powheg}$+${\tt Pythia}).
We find that predictions for the $b$-parton kinematics from {\tt
  Madgraph} and {\tt Powheg} event generators agree very well %excellently
overall, justifying the use of LO signal event generation for BSM
signal models.

We use the {\tt CTEQ6L1}~\cite{Stump:2003yu} PDF set for {\tt
  MadGraph 5}~\cite{Alwall:2014hca} signal samples, with the
factorization and renormalization scales set to {\tt MadGraph}
default.  The signal model is implemented using a modification of {\tt
  MadGraph}'s {\tt heft} model to include an additional
pseudo-scalar.  For ggF production we match events to one jet using
the MLM matching scheme \cite{Alwall:2007fs} with matching parameters
${\tt xqcut} = 15$ GeV and ${\tt QCUT} = 20$ GeV; {\tt
  Pythia6}~\cite{Sjostrand:2006za} is used for showering.  In all
three production modes, the final state is generated inclusively
except for the forward tagging jets in VBF, where cuts of $|\eta|< 5$
and $p_T>20$ GeV are applied.


In {\tt Powheg-Box}
v2~\cite{Frixione:2007nw,Nason:2004rx,Frixione:2007vw,Alioli:2010xd},
signal samples for all three production modes
%of associated Higgs boson production with a $W$ or $Z$
%boson, $pp \rightarrow (W/Z) H$ 
are generated using the CT10 PDF set~\cite{Lai:2010vv}.  The events
are interfaced with {\tt Pythia} 8.186 which is used to decay the
Higgs boson into a pair of pseudo-scalars $a$, which are themselves
decayed to a pair of $b$-quarks with $\br(a \rightarrow b \bar{b})$ =
1.  Event generation is fully inclusive for all three production
modes.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Results}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[thb]
\centering
\subfigure{
\centering
\includegraphics[width=0.45\textwidth]{./WG3/ExoticHiggsDecays/h2a4b/Figs/hpt_generators_a60_ggF.pdf} }
\subfigure{
\centering
\includegraphics[width=0.45\textwidth]{./WG3/ExoticHiggsDecays/h2a4b/Figs/b1pt_generators_a60_ggF.pdf} }
\subfigure{
\centering
\includegraphics[width=0.45\textwidth]{./WG3/ExoticHiggsDecays/h2a4b/Figs/hpt_generators_a60_VBF.pdf} }
\subfigure{
\centering
\includegraphics[width=0.45\textwidth]{./WG3/ExoticHiggsDecays/h2a4b/Figs/b1pt_generators_a60_VBF.pdf} }
\subfigure{
\centering
\includegraphics[width=0.45\textwidth]{./WG3/ExoticHiggsDecays/h2a4b/Figs/hpt_generators_a60_VH.pdf} }
\subfigure{
\centering
\includegraphics[width=0.45\textwidth]{./WG3/ExoticHiggsDecays/h2a4b/Figs/b1pt_generators_a60_VH.pdf} }
\caption[]{Left panels: the $p_T$ distribution of the Higgs boson at
  LHC 13 TeV from different production modes: ggF (top), VBF (centre)
  and WH (bottom) using {\tt MadGraph 5} (blue) and {\tt Powheg}
  (red).  Right panels: same, but for the $p_T$ distribution of the
  leading $b$-parton, for the pseudo-scalar mass $m_a=60$
  GeV.}\label{fig:hpt}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%

We begin with the Higgs boson $p_T$ distribution for ggF, WH and VBF
Higgs boson production at the 13 TeV LHC, shown in the left panels of
\refF{fig:hpt}. For VBF production, we show the Higgs boson $p_T$
distribution in events containing two jets with $p_T>20$ GeV and
$|\eta|<5$.  The Higgs boson $p_T$ spectrum is a key quantity that affects
all subsequent decay product distributions. Overall we find good
agreement between the predictions of the two event generators, {\tt
  MadGraph} and {\tt Powheg}. For VBF and WH, the $p_T$ spectra from
the two generators are in excellent agreement. The greatest
differences are seen in ggF, where {\tt Powheg} predicts a slightly
harder spectrum than does {\tt MadGraph}; however, even here the $p_T$
distributions are quite similar, with good agreement in the tail and
in the peak.  As discussed in Section~\ref{sec:recommendations}, we
recommend that searches that rely on ggF for the bulk of their
sensitivity reweight the Higgs boson $p_T$ distribution according to the
recommendations of WG1.
% \textcolour{red}{place appropriate sectionreference here.}. 
In the right panels of \refF{fig:hpt}, we
compare predictions for the $p_T$ spectrum of the leading $b$-parton
in the same set of events, for the case $m_a=60$ GeV. Good agreement
is shown for VBF and WH production modes, while in ggF the spectrum
predicted by {\tt MadGraph} is slightly softer than the spectrum
predicted by {\tt Powheg}.



We further show various parton-level differential distributions, as
generated in {\tt Powheg}, for masses of the pseudo-scalar $a$ ranging
between 20 to 60 GeV in steps of 10 GeV. In these plots no cuts are
applied to the final state particles; in particular, no cuts on VBF
jets have been imposed.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[thb]
\centering
\subfigure{
\centering
\includegraphics[width=0.45\textwidth]{./WG3/ExoticHiggsDecays/h2a4b/Figs/h_aa_dR_ggF.png} }
\subfigure{
\centering
\includegraphics[width=0.45\textwidth]{./WG3/ExoticHiggsDecays/h2a4b/Figs/h_bbar_dR_ggF.png} }

\subfigure{
\centering
\includegraphics[width=0.45\textwidth]{./WG3/ExoticHiggsDecays/h2a4b/Figs/h_aa_dR_WH.png} }
\subfigure{
\centering
\includegraphics[width=0.45\textwidth]{./WG3/ExoticHiggsDecays/h2a4b/Figs/h_bbar_dR_WH.png} }

\subfigure{
\centering
\includegraphics[width=0.45\textwidth]{./WG3/ExoticHiggsDecays/h2a4b/Figs/h_aa_dR_VBF.png} }
\subfigure{
\centering
\includegraphics[width=0.45\textwidth]{./WG3/ExoticHiggsDecays/h2a4b/Figs/h_bbar_dR_VBF.png} }
\caption[]{The angular separation $\Delta R$ between the two
  pseudo-scalars from Higgs boson decay (left panels), and between the $b\bar b$ pairs from a 
  pseudo-scalar $a$
  decay (right panels) as computed by {\tt Powheg} at LHC 13 TeV, normalized to unity. The upper,
  middle and lower panels correspond to Higgs boson production modes of ggF,
  WH and VBF, respectively.  }\label{fig:ssdR}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In \refF{fig:ssdR} we show the angular separation $\Delta R$
between the two pseudo-scalars $a$ and between the $b\bar b$ pair
originating from the same pseudo-scalar $a$ decay for various Higgs boson production 
modes.  From the left panels of this figure, we can see
that heavy $a$'s are less separated than lighter $a$'s, as they are less
boosted in the Higgs rest frame. Similarly, from the right panels we
can see that the $b\bar b$ pairs from heavier $a$'s are more
back-to-back.  The higher average Higgs boson $p_T$s in the VBF and WH
production channels result in more collimated decay products.  We have
checked that the {\tt MadGraph} samples agree well with the {\tt
  Powheg} samples in modelling these distributions.
%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[thb]
\centering
\subfigure{
\centering
\includegraphics[width=0.42\textwidth]{./WG3/ExoticHiggsDecays/h2a4b/Figs/h_b1parton_pt_ggF.pdf} }
\subfigure{
\centering
\includegraphics[width=0.42\textwidth]{./WG3/ExoticHiggsDecays/h2a4b/Figs/h_b1parton_pt_VBF.pdf} }
\subfigure{
\centering
\includegraphics[width=0.42\textwidth]{./WG3/ExoticHiggsDecays/h2a4b/Figs/h_b1parton_pt_WH.pdf} }
\caption[]{The $p_T$ spectrum of the leading $b$-parton, as computed by {\tt Powheg} at LHC 13 TeV, normalized to unity, for ggF (left, upper panel), VBF (right upper panel), and WH (lower panel).}
\label{fig:ptb1}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%
\refF{fig:ptb1} shows the $p_T$ spectrum of the leading $b$-parton
for various Higgs boson production modes. %\SG{and for various pseudoscalar masses $m_a=20,\cdots, 60$ GeV}.
The broader distributions for lighter $a$ masses reflect the bigger
boost of those $a$'s in the Higgs rest frame.  Again, we have checked
that the {\tt MadGraph} samples agree well with the {\tt Powheg}
samples in modelling these distributions in VBF and WH production
modes, while in ggF there are minor differences comparable to those in
\refF{fig:hpt}.
%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[thb]
\centering
\includegraphics[width=0.98\textwidth]{./WG3/ExoticHiggsDecays/h2a4b/Figs/ptcutefficiency_relabeled.png}
%\subfigure{
%\centering
%\includegraphics[width=0.31\textwidth]{./WG3/ExoticHiggsDecays/h2a4b/Figs/ptcutefficiency_ggF.pdf} }
%\subfigure{
%\centering
%\includegraphics[width=0.31\textwidth]{./WG3/ExoticHiggsDecays/h2a4b/Figs/ptcutefficiency_VBF.pdf} }
%\subfigure{
%\centering
%\includegraphics[width=0.31\textwidth]{./WG3/ExoticHiggsDecays/h2a4b/Figs/ptcutefficiency_VH.pdf} }
\caption[]{Fraction of events having $N_b = 2, 3, 4$ partons above a given $p_T$ threshhold and $|\eta|<2.5$, as computed by {\tt Powheg} at LHC 13 TeV, for ggF (left), VBF (centre), and WH (right).  No other cuts are applied.
Results are shown for both $m_a=20$ GeV and $m_a=60$ GeV. }
\label{fig:ptcuts}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%

In \refF{fig:ptcuts} we show the fraction of events which have
$N_b = 2, 3, 4$ partons above a given $p_T$ for models with heavy
($m_a = 60$ GeV) and light ($m_a = 20$ GeV) pseudoscalars.  Here the
$b$-quarks are required to have $|\eta|<2.5$, but no further cuts are
applied.  These plots quantify the overall softness of the final state
particles.  In all three production modes, the efficiency drops
quickly with the $p_T$ threshold.  For the light (heavy) pseudoscalar,
the efficiency reaches the level of 30\% around a threshold of $\sim
10$ (17) GeV for $N_b=4$, $\sim 20 (25) $ GeV for $N_b=3$, and $35
(35)$ GeV for $N_b=2$. Again, ggF gives rise to an overall softer $b$
spectrum.  The broader $p_T$ distribution produced by the lighter
pseudo-scalar results in a more rapid falloff of efficiency with
increasing $p_T$.%, at the same time making the tails at high $p_T$ threshholds 
%less dramatic, especially for the case $N_b = 2$.
