\subsection{Introduction}

In this section, we discuss exotic Higgs boson decays to two photons
together with missing transverse energy (\MET).  This decay is an
example of an interesting class of {\it semi-invisible} decays, where
visible objects in the final state are accompanied by one or more
detector-stable particles.  Decays featuring multiple electroweak
objects together with \MET\ generally have good prospects at the
LHC~\cite{Curtin:2013fra}, and present an obvious target for Run~2.
However, this class of signatures poses some questions for analysis
design.  First, the multiple possible topologies that can contribute
to any specific final state raise the question of how to design an
analysis strategy capable of providing good coverage to more than one
signal model.  Second, such decays offer several possible trigger
strategies. The presence of electroweak objects in the final state can
potentially make it possible to trigger on the large population of
Higgs bosons produced through gluon fusion, significantly enhancing
the statistical reach.  On the other hand, the relative softness of
these electroweak objects can mean that weak vector boson-associated
or vector boson fusion production modes offer better sensitivity given
realistic trigger thresholds.  Which trigger strategy offers the best
sensitivity to a given decay mode is generally not immediately
obvious, and will depend in detail on the mass spectrum of the BSM
particles produced in the Higgs boson decay.

Here, to illustrate these general points and to begin to answer these
questions, we consider two simplified models yielding Higgs boson decays to
%
a final state consisting of two photons and \MET.
%
In the first, non-resonant, case, the photons arise from opposite
sides of an initial two-body decay: $h\to XX, X\to\gamma Y$, where $Y$
is a stable neutral particle. Such a decay can occur for instance
within gauge-mediated supersymmetry breaking (GMSB) SUSY models, in
which the $X$ corresponds to a neutralino next-to-lightest
supersymmetric particle (NLSP) with mass less than half the 
Higgs boson mass, and the $Y$ corresponds to a gravitino LSP~\cite{Djouadi:1997gw,
  Mason:2009qh, Petersson:2012dp}.
%
In the second, resonant, case, the photons are produced through an
intermediate resonance: $h\to S_1 S_2$, with $S_1 \to\gamma\gamma$ on
one side of the decay, while $S_2$ escapes detection, appearing as
\MET\hspace{0.03cm} in the detector. This signal can arise in
e.g. hidden valley scenarios \cite{Strassler:2006ri}.  The resonant
signal benefits from a peak in the diphoton invariant mass spectrum.
The Feynman diagrams for the non-resonant and resonant decays can be
seen in Figure~\ref{fig:FEYN_SIG}.

Previous searches for the $\gamma\gamma+\MET$ final state in the low
energy regime include searches for the non-resonant Higgs boson decay in the
supersymmetric scenario described above. 
The current bounds on this decay mode come from  searches using both gluon fusion and ZH production for CMS \cite{Khachatryan:2015vta}, and using vector boson fusion for ATLAS  \cite{ATLAS:2015bra}. Both the CMS and ATLAS analyses directly search for the decay $\hsm \to \gamma + \met$, which can be sensitive to the decay $\hsm \to 2\gamma+\met$ when one of the photons is not reconstructed. The CMS search sets a 95\% CL limit on branching ratios larger than 8\%-10\% on this decay, with the neutralino mass ranging between 1 and 60 GeV, and assuming SM Higgs boson production and depending on the assumed topology of the decay; ATLAS sets a  95\% CL limit of 20\%-30\%  under the same assumptions.

In this study, we devise a search strategy for the $\gamma\gamma+\MET$
final state, motivated by the exotic decays of the Higgs described
above. We estimate the sensitivity of this search for $100$ fb$^{-1}$
of $\sqrt{s}=14$ TeV $pp$ data from the LHC.


%%
\begin{figure}[thbp]
\centering
\includegraphics[scale=0.2]{./WG3/ExoticHiggsDecays/ggmet/figs/feyn/feyn_nonres.pdf}
\hspace{1cm}
\includegraphics[scale=0.2]{./WG3/ExoticHiggsDecays/ggmet/figs/feyn/feyn_res.pdf}
\caption{Feynman diagrams for (left) the non-resonant and (right) the resonant signal scenarios.}
\label{fig:FEYN_SIG}
\end{figure}

\subsection{Methodology}
\subsubsection{Simulation Samples \label{sub:simulation}}

Signal and background Monte Carlo (MC) samples were generated with
{\tt MadGraph 5}~\cite{Alwall:2011uj} and hadronized with {\tt Pythia
  8}~\cite{Sjostrand:2007gs}, with the detector simulation provided by
{\tt DELPHES 3}~\cite{deFavereau:2013fsa}.
The samples were produced at $\sqrt{s} = 14$ TeV.  The object
reconstruction and identification are performed with {\tt DELPHES},
according to the information provided in the detector configuration
card. For photon reconstruction and identification, we assume an
efficiency of $95\%$ in the electromagnetic calorimeter barrel
($|\eta| < 1.5$) and $85\%$ in the endcap ($1.5 < |\eta| < 2.5$). We
also impose an isolation cut on the photons by requiring all tracks, neutral hadrons and photons reconstructed by {\tt DELPHES}
 within a cone of $\Delta R < 0.3$ of the photon candidate to
have an energy ratio less than 0.1 with respect to the photon
candidate. For muons, we assume an efficiency of $95\%$ for the whole
detector acceptance ($|\eta| < 2.5$). An isolation cut similar to the
photons is also applied. Jets are reconstructed with the anti-$k_T$
algorithm with jet radius $R = 0.4$.


\medskip
\noindent {\bf Signal Monte Carlo.}
\noindent The signal for the non-resonant case was based on the
supersymmetric cascade decay of the Higgs boson into two neutralinos,
which subsequently decay into two gravitinos and two photons (Figure
\ref{fig:FEYN_SIG}, left). This class of models has been implemented
in {\tt FeynRules} \cite{Christensen:2013aua} and generated using {\tt
  MadGraph}. We assume a gravitino mass close to zero, which is
consistent with gauge mediated low-scale SUSY breaking models with
$\sqrt{f} \approx$ TeV \cite{Petersson:2012dp}. We simulate neutralino
masses in the range $10\,\mathrm{GeV}\leq m_\chi\leq 60$ GeV in steps
of $5$ GeV, with 100,000 events per mass point.

For the resonant case, we assume the Higgs boson decays into two
scalar particles, $S_1$ and $S_2$ (Figure \ref{fig:FEYN_SIG},
right). One of the scalars then decays into two photons, while the
other escapes detection. For this study, we assume the masses of these
two particles are the same; this choice was made for simplicity, but
for detailed studies, more combinations should be investigated. We
generate samples with $M_{1} = M_{2}$ in the range $10\,\mathrm{GeV}
\leq M_1\leq 60$ GeV, in steps of $5$ GeV, with 100,000 events per
mass point.

We investigate the production of the Higgs boson through both gluon
fusion (ggF) and associated production with a $Z$ boson (ZH), with
the $Z$ boson decaying to two muons. The inclusion of the di-electron
decay of the $Z$ can also be considered for future studies.
%
A branching ratio of $\br(\hsm\to\gamma\gamma+\MET) = 10\%$ is assumed
for the signal. This value of the branching ratio was chosen to be
within the current bounds on the Higgs boson width, yet close to the 8
TeV limits from the search for Higgs boson decays to the monophoton final
state ($h\to\gamma+\MET$)~\cite{Khachatryan:2015vta}.


\medskip
\noindent {\bf Background Monte Carlo.}
\noindent Although this analysis is not guaranteed to be entirely free
from QCD multi-jet backgrounds, it has been shown in similar analyses
primarily targeting $\hsm \to \gamma+\MET$ (such as
\cite{Khachatryan:2015vta}) that it is possible to reduce QCD
backgrounds to a sub-dominant contribution. As we require two photons
for most aspects of this analysis, we expect that multi-jet
backgrounds will typically be less important than in
Ref.~\cite{Khachatryan:2015vta}.  For this reason, no pure QCD sample
was produced for this study. As such, the remaining backgrounds for
this analysis arise predominantly from single boson ($\gamma$/$Z$/$W$)
plus jets and diboson processes.

Backgrounds were modeled using the Snowmass LHE simulation
samples~\cite{Anderson:2013kxz}. These consist of single boson samples
($\gamma/Z/W$) with at least one jet and inclusive diboson
($\gamma\gamma/Z\gamma/W\gamma/$ $WW/ZZ/WZ$) samples. The samples
include both hadronic and leptonic decays of the $W$ and $Z$
bosons. The cross sections used for normalizing the single boson
samples were estimated with {\tt MCFM}~\cite{Campbell:2010ff},
assuming an efficiency of $15\%$ for the one jet requirement (as
obtained with {\tt MadGraph}). For the diboson samples, the cross
sections used were estimated from Ref.~\cite{Campbell:2011bn}. The
cross sections and number of events in the samples are shown in Table
\ref{tab:sel_eff}.


\subsubsection{Event Selection}

\medskip
\noindent {\bf Trigger Projections.}

For the ZH channel, the trigger strategy is expected to be
straightforward and can be based on the decay of the $Z$ to two muons.
On the other hand, triggering is one of the main challenges for the
ggF channel, since the final state consists of two soft
photons plus (a relatively small amount of) missing energy. The
standard triggers used for $h\rightarrow\gamma\gamma$ analyses in CMS
typically have a diphoton invariant mass cut which makes it
incompatible with the low energy spectrum of this
analysis. However, we have
identified three possible trigger strategies for this channel, based
on unprescaled triggers used by the CMS experiment in Run~2:
%
\begin{itemize}
\item Asymmetric Diphoton Trigger: This trigger requires two photons
  with different $E_{T}$ and trigger-level identification
  requirements, plus a diphoton invariant mass cut. This type of
  trigger usually has a non-negligible turn-on curve in the leading
  and subleading photon $E_{T}$.
\item Symmetric Diphoton Trigger: This trigger requires two photons
  with the same $E_{T}$ requirement, without any extra requirements.
\item $\gamma+\MET$ Trigger: This trigger requires only one barrel
  photon passing identification requirements and a $E_{T}$ requirement
  that is usually higher than the previous two triggers. In addition,
  there is a calorimetric \MET\, requirement. We expect non-negligible
  turn-on curves with respect to both photon and \MET\, for this
  trigger.
\end{itemize}
%
The three triggers described here represent different selection
strategies that were investigated and will be described below.


\medskip
\noindent {\bf Offline Selection.}
\noindent In the ggF analysis, events are triggered based on the
properties of the photons, and the selection cuts must reflect the
chosen trigger strategy, while maintaining a good signal efficiency.
%
The ZH-produced signal events are tagged through the decay of the
$Z$ boson to muons, minimizing the largest backgrounds.  The photon
selection is chosen to maximize the signal acceptance in the ZH
case, with $E_{T}$ thresholds as low as possible.  The final event
selection requirements for the ggF and ZH channels are
summarized in Table~\ref{tab:sel_all}. In this table, we use the
following definitions for transverse mass:
%\begin{equation}
\begin{align}
M_{T}(\gamma\gamma,\MET) &= \sqrt{2E_{T}(\gamma\gamma)\MET(1-\cos(\Delta\phi(\gamma\gamma,\MET))}, \\
M_{T}(\gamma\gamma+\MET, \mu\mu) &= \sqrt{2E_{T}(\gamma\gamma+\MET)p_{T}(\mu\mu)(1-\cos(\Delta\phi(\gamma\gamma+\MET,\mu\mu))}.
\end{align}

%\afterpage{\clearpage}

\begin{table}%[]
\caption{Analysis selection for the ggF channel (for each trigger scenario) and the ZH channel.}
\label{tab:sel_all}
\centering
\begin{tabular}{r | l | l | l| l}
\toprule
 & \multicolumn{3}{c|}{ggF} & \multicolumn{1}{c}{ZH} \\ 
\midrule
Variable & Asymmetric $\gamma\gamma$ & Symmetric $\gamma\gamma$ & $\gamma+\MET$ & \\ \hline
Number of photons       & $> 1$         & $> 1$         & $> 1$         & $> 1$\\
$p_{T}(\gamma_{1})$     & $ > 45$ GeV   & $ > 40$ GeV   & $ > 55$ GeV   & $ > 20$ GeV\\
$|\eta(\gamma_{1})|$    & $ < 2.5$      & $ < 2.5$      & $ < 1.4$      & $< 2.5$ \\
$p_{T}(\gamma_{2})$     & $ > 30$ GeV   & $ > 40$ GeV   & $ > 20$ GeV   & $ > 20$ GeV\\
$|\eta(\gamma_{2})|$    & $ < 2.5$      & $ < 2.5$      & $ < 2.5$      & $ < 2.5$ \\
$M(\gamma\gamma)$       & $\in [15, 100]$ GeV & $< 100$ GeV & $< 100$ GeV & $< 100$ GeV \\
$\MET$                  & $> 90$ GeV    & $> 90$ GeV    & $> 90$ GeV    & $> 60$ GeV \\
$M_{T}(\gamma\gamma,\MET)$                    & $< 140$ GeV   & $< 140$ GeV   & $< 140$ GeV   & $< 140$ GeV \\
$\Delta\phi(\gamma\gamma,\MET) $& $< 1.5$ & $< 1.5$ & $< 1.5$ & $< 1.5$ \\
Number of leptons       & $< 1$         & $< 1$         & $< 1$         & 2 muons \\
\midrule
%Number of muons & - & - & - & $> 1$ \\
$p_{T}(\mu_{1,2})$ & -  & - & - & $ > 20$ GeV \\
$|\eta(\mu_{1,2})|$ & - & - & - & $ < 2.5$ \\
$M(\mu\mu)$ & - & - & - & $\in [75,115]$ GeV\\
$M_{T}(\gamma\gamma+\MET, \mu\mu)$ & - & - & - & $> 400$ GeV\\
\bottomrule
\end{tabular}
\end{table}

To exploit the topology of the resonant signature, we apply an
additional requirement of a $\pm10$ GeV mass window, in the diphoton
invariant mass distribution ($M(\gamma\gamma)$), around the signal
mass ($M_1$). The efficiencies for each individual process and the
different searches, after the full selection (without the
$M(\gamma\gamma)$ mass window requirement), are shown in Table
\ref{tab:sel_eff}.

For the ZH case, we also explore the strategy performed by CMS in
their Run~1 result \cite{Khachatryan:2015vta}, in which one or more
photons are required in the event, instead of two or more. In this
case, we gain back the efficiency that is lost due to the inefficiency
in reconstructing the subleading photon, which can have very low
$E_T$. The selection is similar to what is described in Table
\ref{tab:sel_all}, but without the $M(\gamma\gamma)$ cut or the mass
window requirement for the non-resonant topology. The other variables
that use the diphoton information are instead reconstructed using only
the leading photon in the event. Plots of some of the discriminating
variables are available at 
\url{https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWGExoticDecayYR4ExtraMaterials}.
%\noindent 
%{\tt https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWGExoticDecayYR4ExtraMaterials}.

\begin{table}%[]
\caption{Cross-sections, numbers of events generated per process, and selection efficiencies for background processes and signal points, for ggF and ZH production mechanisms.  Signal cross-sections are quoted for a 10\% branching ratio.}
\label{tab:sel_eff}
\centering
\resizebox{\textwidth}{!}{
\begin{tabular}{c | c | c | c | c | c| c}
\toprule
 \multirow{ 2}{*}{Process} &  \multirow{ 2}{*}{$\sigma$ (pb)} & \multirow{ 2}{*}{$N_\text{Generated}$} & \multicolumn{3}{c|}{ggF} & \multirow{ 2}{*}{ZH} \\ 
 & & & Asymmetric $\gamma\gamma$ & Symmetric $\gamma\gamma$ & $\gamma+\MET$ & \\ 
 \midrule
\multicolumn{7}{c}{Backgrounds} \\
\midrule
$\gamma$ + Jets    & $1.0\times10^{5}$  &    5425448    &    $1.9\times10^{-6}$  &   $4.7\times10^{-7}$  &   $8.9\times10^{-7}$  &   $\approx 0$   \\
$Z$ + Jets         & $0.94\times10^{4}$   &    1888446    &  $5.6\times10^{-4}$    & $1.5\times10^{-4}$    & $5.0\times10^{-5}$    & $\approx 0$     \\
$W$ + Jets         & $2.96\times10^{4}$  &    5263872    &   $6.2\times10^{-4}$   &  $1.9\times10^{-4}$   &  $2.7\times10^{-5}$   &  $\approx 0$    \\
$\gamma\gamma$     & $10.8\times10^{1}$  &    4268781    &   $3.1\times10^{-5}$   &  $1.0\times10^{-5}$   &  $1.1\times10^{-5}$   &  $\approx 0$             \\
$Z\gamma$          & $6.30\times10^{2}$  &    3406151    &   $4.3\times10^{-4}$   &  $1.4\times10^{-4}$   &  $5.7\times10^{-5}$   &  $\approx 0$    \\
$W\gamma$          & $1.03\times10^{3}$   &    5258034    &  $1.4\times10^{-4}$    & $4.6\times10^{-5}$    & $5.4\times10^{-5}$    & $\approx 0$     \\
$WW$               & $1.24\times10^{2}$  &    8059829    &   $2.6\times10^{-1}$   &  $8.4\times10^{-2}$   &  $9.8\times10^{-5}$   &  $8.2\times10^{-8}$    \\
$ZZ$               & $1.8\times10^{1}$ &    1101611    &     $1.4\times10^{-2}$ &    $4.7\times10^{-3}$ &    $6.7\times10^{-4}$ &    $7.3\times10^{-6}$ \\
$WZ$               & $5.1\times10^{1}$ &    3319770    &     $3.6\times10^{-1}$ &    $1.2\times10^{-1}$ &    $2.5\times10^{-4}$ &    $2.9\times10^{-6}$  \\
\midrule
\multicolumn{7}{c}{Signals} \\
\midrule
Res., M = 10 GeV     &  $10.8\times10^{1}$   &    4268781    &  $2.5\times10^{-4}$   &  $2.2\times10^{-4}$   &  $1.7\times10^{-4}$   &  $5.7\times10^{-4}$       \\
Res., M = 40 GeV     &  $6.30\times10^{2}$   &    3406151    &  $8.7\times10^{-3}$   &  $5.7\times10^{-3}$   &  $5.0\times10^{-3}$   &  $6.9\times10^{-3}$      \\
Res., M = 60 GeV     &  $1.03\times10^{3}$    &    5258034    & $1.6\times10^{-2}$    & $1.1\times10^{-2}$    & $1.1\times10^{-2}$    & $9.2\times10^{-3}$         \\
Non-Res., M = 10 GeV &  $1.24\times10^{2}$   &    8059829    &  $1.5\times10^{-3}$   &  $9.5\times10^{-4}$   &  $1.1\times10^{-3}$   &  $1.1\times10^{-3}$       \\
Non-Res., M = 40 GeV &  $1.8\times10^{1}$  &    1101611    &    $8.0\times10^{-3}$ &    $5.5\times10^{-3}$ &    $5.2\times10^{-3}$ &    $6.3\times10^{-3}$     \\
Non-Res., M = 60 GeV &  $5.1\times10^{1}$  &    3319770    &    $1.1\times10^{-2}$ &    $7.6\times10^{-3}$ &    $6.9\times10^{-3}$ &    $8.1\times10^{-3}$     \\
\bottomrule
\end{tabular}
}
\end{table}

\afterpage{\clearpage}


\subsubsection{Background Estimation for Misidentified Photons}

Background processes with misidentified (or ``fake'') photons, such as
jets and electrons erroneously reconstructed as photons, that pass the final
selection generally have very low efficiency at the LHC. Nonetheless,
such backgrounds may be non-negligible since the production
cross-sections can be large. Such mis-identification rates are
typically measured with data-driven methods at the LHC.
Although this study was limited by MC statistics in measuring fake
photon backgrounds, a method was developed to mitigate this problem,
which we describe below.

The object reconstruction and selection is done at {\tt DELPHES}
level, where, given the photon identification requirements described
in Section \ref{sub:simulation}, we obtain an associated fake
rate. These fake rates are accounted for in the overall efficiencies
in Table \ref{tab:sel_eff}. In order to bypass the efficiency loss due
to the small fake rates, we select jets and electrons to be
redesignated as fake photon candidates. For the background processes
with one prompt photon ($\gamma$+jets, $W\gamma$ and $Z\gamma$), we
select one fake photon candidate. For the processes with no prompt
photons ($W/Z$+jets, $WW$, $WZ$ and $ZZ$), we select two fake photon
candidates. No fake photon selection is done for the
$\gamma\gamma$+jets sample.

With the assumption of a flat fake rate for both jets and electrons,
the fake photon candidates are randomly selected from the jets and
electrons that passed the photon acceptance requirements. One extra
assumption is that the electron-to-photon fake rate is set to be one
order of magnitude larger than the jets-to-photon fake
rate. Therefore, electrons are set to have a probability of being
selected to be redesignated as a photon that is ten times higher than
for jets.

After the choice of fake photon candidates, we calculate weights for
the individual samples based on the $E_{T}$ spectrum of the selected
photons (prompt and fake) to match the spectrum found with the photon
candidates reconstructed directly from {\tt DELPHES}. This
reweighting is done on the sum of $E_{T}$ of the two leading photons
for samples with at least one prompt photon, and on the $E_{T}$ of the
leading photon for samples with no prompt photon. An independent
reweighting is also done in $\eta$. Both reweightings reflect the
different reconstruction efficiencies and energy resolutions of
objects that are not reconstructed as photons (i.e., electrons and
jets). After applying the weights, we observe good agreement between
the kinematic distributions of interest arising from photons
reconstructed by {\tt DELPHES}, and in particular from events with two photons
reconstructed by {\tt DELPHES} in samples containing only one prompt
photon at truth level, and from our fake photon candidates.

\subsection{Results}

We present the expected sensitivity of this search in terms of the
necessary $h\rightarrow\gamma\gamma+\MET$ branching ratio to reach a
$5\sigma$ sensitivity for an assumed integrated luminosity of $100$
fb$^{-1}$ at $\sqrt{s} = 14$ TeV, with the sensitivity defined as:
%
\begin{equation}
\mathcal{S} = \frac{N_\text{Signal}}{\sqrt{N_\text{Background}}}.
\end{equation}

In Figure \ref{fig:triggers}, we show the sensitivity plot for the
different trigger scenarios of the ggF case.  This plot shows
that, after the full selection, the performance of the different
trigger strategies is comparable. Although it is safe to assume that a
diphoton trigger with a low $M(\gamma\gamma)$ cut will be present in
the future trigger menus of CMS and ATLAS, we choose to perform the
analysis in the $\gamma+\MET$ case. We make this choice as an effort
to make the case for the existence of such a trigger strategy for the
future LHC runs. While the diphoton triggers are designed with
specific usages that are already well established, the
$h\rightarrow\gamma\gamma+\MET$ analysis could be viewed as a
benchmark for the $\gamma+\MET$ trigger for three reasons:
\begin{itemize}
\item It is a trigger that is already present at the LHC experiments,
  but can be retuned with a specific analysis as benchmark;
\item A dedicated trigger for this analysis requiring two photons
  might not be as efficient at trigger level, given the soft spectrum
  of the second photon;
\item This trigger can also be used for other exotic searches, such as
  the extension to low energies of the dark matter searches in the
  monophoton channel.
\end{itemize}

\begin{figure}[thbp]
\centering
\includegraphics[height=3.8in]{./WG3/ExoticHiggsDecays/ggmet/triggerplots/Significance_edit.png}\caption{Statistical
  significance corresponding to different trigger scenarios in the gluon
  fusion analysis, for a reference signal branching ratio of
  $\br(\hsm\to\gamma\gamma+\MET) = 10\%$.}
\label{fig:triggers}
\end{figure}



In Figure \ref{fig:branching_5sigma}, on the left, we show the
branching ratio of $h\rightarrow\gamma\gamma+\MET$ needed for a
significance of $5\sigma$, assuming the Standard Model Higgs cross
section, for the ggF analysis (assuming the $\gamma+\MET$
trigger strategy and selection). On the right, we show the branching
ratio $h\rightarrow\gamma\gamma+\MET$ needed for a significance of
$2\sigma$, which represents the $95\%$ confidence level for exclusion,
assuming SM ZH production. For the ZH case, we show the results
for the strategies requiring at least one ($N_\gamma \geq 1$) and at
least two ($N_\gamma \geq 2$)
photons. 


\subsubsection{Systematic Uncertainties}

While the uncertainties in the ZH channel are expected to be
dominated by statistics, the ggF channel is very sensitive to
the systematic uncertainties associated with the background
predictions. We estimate the effect of these uncertainties by
parameterizing the sensitivity as:
%
\begin{equation}
\mathcal{S}_{sys} = \frac{N_\text{Signal}}{\sqrt{N_\text{ Background}}+\sigma_{sys}\times N_\text{Background}},
\label{eqn:syst}
\end{equation}
%
with $\sigma_{sys}$ representing a source of uncertainty that does not scale with the amount of statistics.
%
Figure \ref{fig:branching_5sigma} shows the effect on the $5\sigma$ branching ratios due to the addition of a $10\%$ systematic uncertainty according to Eq. (\ref{eqn:syst}).

\begin{figure}[thbp]
\centering
\includegraphics[height=2.1in]{./WG3/ExoticHiggsDecays/ggmet/finalresults/branchingratio_ggh.pdf}
\includegraphics[height=2.1in]{./WG3/ExoticHiggsDecays/ggmet/finalresults/exclusion_zh.pdf}
\caption{(Left) $5\sigma$ branching ratios for the ggF
  channel, for resonant (in red) and non-resonant (in black) final
  states, using the $\gamma+\MET$ trigger. (Right) Branching ratios
  for 95$\%$ confidence level exclusion in the ZH case, resonant and
  non-resonant topologies, requiring at least one photon ($N_\gamma
  \geq 1$, in green and blue, respectively) and at least two photons
  ($N_\gamma \geq 2$ in black and red, respectively). The shaded areas
  correspond to a variation in systematic uncertainties up to $10\%$. }
\label{fig:branching_5sigma}
\end{figure}

\afterpage{\clearpage}


