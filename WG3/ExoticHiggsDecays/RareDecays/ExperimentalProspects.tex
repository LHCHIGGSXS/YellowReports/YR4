\subsection[Experimental status and prospects]{Experimental status and prospects\SectionAuthor{L.~Caminada, K.~Nikolopoulos}}
\label{ExperimentalProspects}

In the SM, the charged fermions obtain mass ($m_f$) via a direct
Yukawa coupling to the Higgs field, which, following electroweak
symmetry breaking, results in fermion mass terms and a fermion-Higgs boson 
interaction with a strength proportional to $m_f$. This is the
most economic way to generate fermion masses, but it is not imposed by any fundamental symmetry principle. Several
viable models of physics beyond the SM predict modifications of these
couplings, as discussed in Section~\ref{NP}.

To date, the only direct experimental evidence of the Yukawa mechanism
for fermion mass generation is the Higgs boson coupling to third
generation fermions. The ATLAS and CMS Collaborations have reported
evidence for the observation of the Higgs boson decays to a pair of
$\tau$-leptons, $h\to\tau^+\tau^-$, in line with the SM
expectation~\cite{Aad:2015vsa,Chatrchyan:2014nva}.
%
For Higgs boson decays to b-quark pairs, $h\to b\bar{b}$, the Tevatron
experiments have reported evidence~\cite{Aaltonen:2012qt},
complemented by indications at the
LHC~\cite{Aad:2014xzb,Chatrchyan:2013zna}.
%
CMS has also reported the strong evidence for the direct coupling of the 125 GeV Higgs boson to down-type fermions (combining Higgs boson decays to pair of tau leptons and b-quark)~\cite{Chatrchyan:2014vua}.
%
Higgs boson decays to top-quark pairs are kinematically forbidden,
thus the associated production of a Higgs boson with a top-quark pair,
$t\bar{t}h$, is exploited: several final states have been
analysed~\cite{Aad:2015gba,Khachatryan:2014qaa}, and the sensitivity
begins to approach the SM expectation.
%
The muon Yukawa coupling is probed through the $h\to\mu^+\mu^-$ decay,
yielding 95\% confidence level upper limits of 7-10 times the
predicted SM rate~\cite{Aad:2014xva,Khachatryan:2014aep}. Higgs boson
decays to an electron-positron pair have been also searched
for~\cite{Khachatryan:2014aep}, although the resulting bound is many
orders of magnitude larger than the predicted SM value, which is
anticipated to remain out of reach even at the HL-LHC
\cite{Altmannshofer:2015qra}.
%
Flavour-violating Higgs boson interactions have been searched for in
top-quark decays~\cite{Aad:2015pja,Khachatryan:2014jya} and via $h\to e^\pm\mu^\mp$, $h\to \tau^\pm e^\mp$
$h\to\tau^\pm\mu^\mp$~\cite{Aad:2015gha,Khachatryan:2015kon}, yielding
an intriguing excess in the latter.

The Yukawa couplings of the first and second generation
quarks are among the most challenging of the SM couplings to test experimentally.
Indirect
constraints are sparse, with some specifically derived for
flavour-violating interactions via meson-anti-meson
mixing~\cite{Harnik:2012pb,Blankenburg:2012ex}.
%
These Yukawa couplings have been, generally, considered beyond the reach of
the LHC experiments.  Recently, the possibility to probe these
couplings using rare exclusive decays of the Higgs boson to a vector
meson and a photon has resurfaced\footnote{Such rare decays were
considered as a discovery channel for a 
light Higgs boson~\cite{Bander:1978br} but then abandoned.}, initially for the
charm quark, and subsequently for
all the light quarks, including
flavour-violation (see Section~\ref{SMphoton} for details).  
 The rare exclusive decays
of a Higgs boson to a meson and massive vector boson, $h\to MV$, and their
potential in clarifying the nature of the Higgs boson have also been
considered (see Section~\ref{SMmassivegauge}).
These developments prompted significant interest, with theoretical
investigations towards precise estimates of the relevant SM
predictions, as detailed in
Sections~\ref{SMphoton}--\ref{SMmassivegauge}, and phenomenological
sensitivity studies, e.g. Ref.~\cite{Perez:2015lra}.

Currently, these exclusive decays constitute the only available method
to probe the Higgs boson interactions with first and second generation
quarks. Recently, charm-quark specific suggestions regarding the
feasibility of the $h\to c\bar{c}$ channel~\cite{Delaunay:2013pja},
using charm-tagging techniques~\cite{Aad:2015gna}, and of the
associated production of a Higgs boson with a charm
quark~\cite{Brivio:2015fxa}, were made and are under investigation.

Similar exclusive decays of the $W^\pm$ and $Z$
bosons have also attracted
interest~\cite{Grossmann:2015lea,Huang:2014cxa,Achasov:2012zzb},
offering a physics programme in precision quantum chromodynamics
(QCD), electroweak physics, and physics beyond the SM.

Using 20.3~fb$^{-1}$ of 8\,TeV proton-proton collision data, the ATLAS
Collaboration has performed a search for Higgs and $Z$ boson decays to
$J/\psi\,\gamma$ or and $\Upsilon(nS)\,\gamma$
($n=1,2,3$)~\cite{Aad:2015sda}.
%
No significant excess has been observed and 95\% confidence level upper
limits where placed on the respective branching ratios.  In the
$J/\psi\,\gamma$ final state the limits are $1.5\times10^{-3}$ and
$2.6\times10^{-6}$ for the Higgs and $Z$ boson decays, respectively,
while in the $\Upsilon(1S,2S,3S)\,\gamma$ final states the limits are
$(1.3,1.9,1.3)\times10^{-3}$ and $(3.4,6.5,5.4)\times10^{-6}$,
respectively. The CMS Collaboration has placed a 95\% C.L. upper limit
of $1.5\times10^{-3}$ on the $h\to J/\psi\,\gamma$ branching
ratio~\cite{Khachatryan:2015lga}. 
Using 2.7\,fb$^{-1}$ of 13\UTeV\ proton-proton collision data, the ATLAS Collaboration 
has recently performed a search for Higgs and Z boson decays to $\phi\,\gamma$, 
that yielded 95\% confidence level upper limits of $1.4 \times 10^{-3}$ and $8.3 \times 10^{-6}$ 
on the respective branching ratios~\cite{Aaboud:2016rug}. 
In all cases, an SM production rate for the observed Higgs boson is assumed. 
Currently, no other direct experimental constraint on these decays is available.


Looking to the future, the ATLAS Collaboration estimated the expected
sensitivity for Higgs and $Z$ boson decays to a $J/\psi$ and a photon,
assuming up to 3000~fb$^{-1}$ of data collected with the ATLAS
detector at the centre-of-mass energy of
14\,TeV~\cite{ATL-PHYS-PUB-2015-043}, during the operation of the High
Luminosity LHC. The expected sensitivity for the $h\to J/\psi\,\gamma$
branching ratio, assuming 300 and 3000~fb$^{-1}$ at 14 TeV, is
$153\times 10^{-6}$ and $44\times 10^{-6}$,
respectively~\cite{ATL-PHYS-PUB-2015-043}. The corresponding
sensitivities for the $Z\to J/\psi\,\gamma$ branching ratios are
$7\times 10^{-7}$ and $4.4\times 10^{-7}$,
respectively~\cite{ATL-PHYS-PUB-2015-043}.  In this analysis, the same
overall detector performance as in LHC Run~1 is assumed, while an
analysis optimization has been performed and a multivariate
discriminant using the same kinematic information as the published
analysis~\cite{Aad:2015sda} has been introduced.  As the search
sensitivity approaches the SM expectation for the $h\to
J/\psi\,\gamma$ branching ratio, the contribution from $h\to
\mu\mu\gamma$ decays, with a non-resonant dimuon pair, needs to be
included. These can be separated statistically from the $h\to
J/\psi\,\gamma$ signal using dimuon mass information.

\begin{wrapfigure}{R}{0.5\textwidth}
 \centering
\vspace{-0.7cm}
 \includegraphics[width=0.48\textwidth]{./WG3/ExoticHiggsDecays/RareDecays/PhiGamma_TrackPt-eps-converted-to.pdf}
\vspace*{-0.5cm}
\caption{Transverse momentum distribution of decay products in $h\to\phi\gamma\to K^+K^-\gamma$ decays~\cite{phigamma}.\label{fig:KKpt}}
\end{wrapfigure}
Regarding the Higgs boson coupling to the strange-quark, the
$h\to\phi\gamma$ decay is a potential probe. The
subsequent $\phi\to K^+K^-$ decay features a large branching ratio of
about 49\% and gives access to a simple final state of a hard photon
recoiling against two collimated high transverse momentum tracks, as
can be seen in \refF{fig:KKpt}. With the SM branching ratio prediction presented in \refT{table:SM_BRs},  about 6.5
events are expected to be produced with 100 fb$^{-1}$ at 14\,TeV.  For
the first generation quarks, the $h\to\omega\gamma$ and
$h\to\rho\gamma$ are being considered, followed by the
$\omega\to\pi^+\pi^-\pi^0$ and $\rho\to\pi^+\pi^-$ decays, both with
large branching ratios of about 89\% and 100\%, respectively.
The corresponding expected number of events, assuming the SM branching
ratios for these decays, are about 7.6 and 96, respectively.  The
experimental acceptance for these decays, assuming reasonable
geometrical acceptance and transverse momentum requirements, is
expected to range between 40 and 70\%~\cite{phigamma}.

These rare decays to a vector meson and a photon feature very
interesting and experimentally challenging boosted topologies. The
signature is distinct, but the QCD backgrounds require careful
consideration. A primary challenge arises from the trigger
availability to collect the required datasets. In the considered
cases, the decay signature is a photon of large transverse momentum
that is isolated from hadronic activity, recoiling against a narrow
hadronic jet. Triggering on such signatures, especially under pile-up,
will benefit from the upcoming detector upgrades, such as the ATLAS
Fast TracKer (FTK)~\cite{Shochet:1552953} that will provide rapid
track finding and reconstruction in the inner detector for every event
that passes the level-1 trigger.  The search for the final
states $\omega\gamma$ and $\rho\gamma$ is complicated further due to
the large natural width of the $\rho$ meson and the $\omega$-$\rho$
interference.


In the following, as an example of a potentially interesting target
rare decay for the high-statistics proton-proton collision data at a
centre-of-mass energy of 14\,TeV, the search for the Higgs boson decay
to $WD$ is presented in some detail. This is an interesting
experimental target due to its clean experimental signature.
%
The decay signature includes a high transverse momentum lepton from the $W$-boson decay, which gives the trigger for the
event, and a displaced vertex from the charmed meson decay. Since the
branching ratio of the Higgs boson decay to $WD_s^{(*)}$ is more than
an order of magnitude larger than the branching ratio to $WD^{(*)}$ (see \refT{tab:pseudo}),
the discussion here focuses on the former.

The search for this rare Higgs boson decay utilizes the dominant Higgs boson production 
mechanisms at the LHC, namely gluon-gluon fusion (ggF) and
vector boson fusion (VBF). The cross section for VBF Higgs boson production
is about an order of magnitude smaller compared to ggF, but features 
distinct event kinematics, with two jets in the forward regions
of the detector that can be used to tag the event. The main challenge
in the search for $H\rightarrow WD_s^{(*)}$ is to suppress the large
background from $W$ bosons produced in association with charm quarks
fragmenting into $D_s$ mesons. In order to estimate the sensitivity of
the search signal and background events have been produced using the
PYTHIA event generator. The $W$-boson is required to decay
leptonically to either an electron or a muon. Based on the detector
acceptance and trigger requirements, the fiducial region is defined by
lepton $p_{\rm T}>30$\,GeV, $|\eta|<2.5$ and neutrino $p_{\rm
  T}>25$\,GeV. Since the transverse momentum of the $D_s$ meson in
signal events peaks at higher values compared to background events, a
requirement of $D_s$ $p_{\rm T}>30$\,GeV, $|\eta|<2.1$ is applied. The
acceptance of these requirements is 18\% and 22\% for ggF and VBF
Higgs boson production, respectively, while 0.6\% of the background events
fulfil these requirements.

The $D^{(*)}_s$ meson is identified by reconstructing displaced
vertices from its hadronic decay to charged pions or kaon, in
particular $D_s\rightarrow K^{+}K^{-}\pi^{+}(\pi_0)$. The excited
$D^*_s$ state decays almost exclusively to $D_s+\gamma/\pi_0$ and is
tagged by the subsequent $D_s$ decay. The measurements of the
SM $W+c$ production at the LHC~\cite{Aad:2014xca,
  Chatrchyan:2013uja} demonstrate that hadronic charm decay signatures
can be reconstructed in the detector with reasonable efficiency of 30\%
to 40\%. Combinatorial background in the charm reconstruction is
largely rejected by exploiting the charge correlation between the $W$
boson and the charmed meson. However, the major background from $W+c$
production also predominantly yields opposite sign signatures and therefore is
not reduced by this requirement.

The main discriminating variable against the non-resonant $W+D_s$
background is the Higgs boson mass as reconstructed from the $W$-boson and
$D_s$ meson four-momenta. Since an excellent mass resolution of about
1\% is achieved for the $D_s$ meson, the Higgs boson mass resolution
is dominated by the measurement of the missing transverse energy in
the detector which has a resolution of about 10\% to 30\% in the
kinematic region relevant for this study. Requiring the reconstructed
mass to be within a window of 20\,GeV centred around the Higgs boson mass
reduces the background by about a factor of 6.

A further distinct characteristic of the signal events is the
isolation of the $D_s$ meson. In background events the $D_s$ meson
originates from charm-quark fragmentation and is thus seen within
a particle jet in the detector. By applying isolation criteria in
the reconstruction of the $D_s$ meson about 80\% of the background
events are rejected. In order to tag VBF events, two jets with an
invariant mass of $m_{jj}>500$\,GeV and separated by $\Delta \eta>3$
are required.

About 12 signal events and 16000 background events can be expected in
the ggF channel assuming an integrated luminosity of
$\mathcal{L}=3000\,\rm{fb^{-1}}$ and the Standard Model branching
ratio for $H\rightarrow WD_s^{(*)}$. For the VBF channel predicted
numbers of 1 signal event and 120 background events are obtained. Since
the branching ratio of this decay can be significantly enhanced in
many scenarios beyond the Standard Model (see Section~\ref{NP} of this report for a review), setting upper limits on the
decay branching ratio is of considerable interest in exploring the
phase space of these models. Based on these results an upper limit of
$7\times 10^{-4}$ on the branching ratio of $H\rightarrow WD_s^{(*)}$
can be achieved.





