\subsection[Theoretical predictions: photon plus a meson]{Theoretical predictions: photon plus a meson\SectionAuthor{M.~Neubert, F.~Petriello}}
\label{SMphoton}

The SM predictions that the Higgs boson couplings to heavy gauge bosons and fermions are given by $2m_{W,Z}^2/v$ and $m_f/v$, where $v\approx 246$\,GeV is the Higgs vacuum expectation value, have been confirmed within experimental uncertainties for the $W$ and $Z$ bosons and for the third-generation fermions. However, no direct measurements of the Higgs boson couplings to the light fermions of the first two generations are available at present. As discussed in Section~\ref{NP} of this report, in several BSM models these couplings can deviate significantly from those predicted in the SM. Indeed, this is a generic prediction in many models trying to explain the hierarchies seen in the spectrum of fermion masses and mixing angles. Probing the Higgs boson couplings to light fermions is thus of paramount importance. This includes both flavour-diagonal and flavour-changing interactions. 

The measurement of the rare exclusive decays $h\to M\gamma$, where $M$ denotes a vector meson, would allow a unique probe of the Higgs boson coupling to light quarks at the LHC. While the absolute value of the bottom-quark Yukawa coupling can be accessed by measuring $b$-tagged jets in the associated production of the Higgs boson with a $W$ or $Z$ boson, this method becomes progressively more difficult for the lighter-quark couplings.  Advanced charm-tagging techniques may allow some access to the charm-quark Yukawa coupling~\cite{Perez:2015lra}, but no other way of directly measuring even lighter-quark couplings is currently known. The tiny branching ratios for these exclusive decays renders them inaccessible at future $e^+ e^-$ colliders. The program of measuring these decay modes is therefore only a possibility for the LHC and future hadron-collider facilities.  

The possibility of measuring rare exclusive Higgs boson decays was first pointed out in \cite{Bander:1978br,Keung:1983ac} and in more modern discussions in \cite{Bodwin:2013gca,Kagan:2014ila}, and the theoretical framework for their prediction was further developed in~\cite{Bodwin:2014bpa,Grossmann:2015lea,Koenig:2015pha}. Our discussion follows closely the techniques introduced in Refs.~\cite{Bodwin:2013gca,Kagan:2014ila,Bodwin:2014bpa,Grossmann:2015lea,Koenig:2015pha}, and we only summarize the salient features here. We begin our discussion of the theoretical predictions for these modes by introducing the effective Yukawa Lagrangian
%
\begin{equation}
   {\cal L} = - \sum_q\,\kappa_q\,\frac{m_q}{v}\,H\,\bar{q}_L q_R 
    - \sum_{q\ne q'}\,\frac{y_{qq'}}{\sqrt2}\,H\,\bar{q}_L q'_R + h.c. \,,
\end{equation}
%
where in the SM $\kappa_q=1$ while the flavour-changing Yukawa couplings $y_{qq'}$ vanish. The effective Lagrangian leads to two categories of exclusive Higgs boson decays: flavour-conserving decays involving the $\kappa_q$ couplings, where $M=\rho,\omega,\phi,J/\psi,\Upsilon(nS)$, and flavour-violating decays involving the $y_{qq'}$ couplings, where $M=B^{*0}_s,B^{*0}_d,K^{*0},D^{*0}$. In view of the very strong indirect bounds on flavour off-diagonal Higgs boson couplings to light quarks~\cite{Harnik:2012pb}, the flavour-violating decays $h\to M\gamma$ are bound to be very strongly suppressed. We will therefore restrict our discussion here to flavour-conserving processes.

The exclusive decays $H\to M\gamma$ are mediated by two distinct mechanisms, which interfere destructively.
\begin{itemize}
\item 
In the {\em indirect process}, the Higgs boson decays (primarily through loops involving heavy top quarks or weak gauge bosons) to a real photon $\gamma$ and a virtual $\gamma^*$ or $Z^*$ boson, which then converts into the vector meson $M$. This contribution only occurs for the flavour-conserving decay modes. The effect of the off-shellness of the photon and the contribution involving the $h\gamma Z^*$ coupling are suppressed by $m_M^2/m_h^2$, with $m_M$ the mass of the meson, and hence are very small~\cite{Koenig:2015pha}.
\item 
In the {\em direct process}, the Higgs boson decays into a quark and an antiquark, one of which radiates off a photon. This process introduces the dependence of the decay amplitude on the $\kappa_q$ parameters. The formation of the vector meson out of the quark-antiquark pair involves some non-trivial hadronic dynamics.
\end{itemize}
The relevant lowest-order Feynman diagrams contributing to the direct and indirect processes are shown in \refF{figure:diagrams} (left-middle and right panel, respectively).

\begin{figure}[thb]
\includegraphics[width=0.33\textwidth]{./WG3/ExoticHiggsDecays/RareDecays/HtoMGamma3-eps-converted-to.pdf}
\includegraphics[width=0.33\textwidth]{./WG3/ExoticHiggsDecays/RareDecays/HtoMGamma2-eps-converted-to.pdf}
\includegraphics[width=0.29\textwidth]{./WG3/ExoticHiggsDecays/RareDecays/HtoMGamma1-eps-converted-to.pdf}
\caption{\label{figure:diagrams}
Direct (left and centre) and indirect (right) contributions to the $h\to M\gamma$ decay amplitude. The blob represents the non-perturbative meson wave function. The crossed circle in the third diagram denotes the off-shell $h\to\gamma\gamma^*$ and $h\to\gamma Z^*$ amplitudes, which in the SM arise first at one-loop order.}
\end{figure}

We begin by outlining the calculation of the indirect amplitude. The virtual photon or $Z$ boson couples to
the vector meson through the matrix element of a local current, which can be parameterized in terms of a single hadronic parameter: the vector-meson decay constant $f_M$. This quantity can be obtained directly from experimental data. In particular, the leptonic decay rate of the vector meson can be written as
\begin{equation}
   \Gamma(M\to l^+l^-) = \frac{4\pi Q_M^2 f_M^2}{3 m_M} \alpha^2(m_M) \,,
\end{equation}
where $Q_M$ is the relevant combination of quark electric charges. The effective $h\gamma\gamma^{*}$ and $H\gamma Z^*$ vertices, which appear in the indirect amplitude, can be calculated with high accuracy in the SM. The by far dominant contributions involve loop diagrams containing heavy top quarks or $W$ bosons. The two-loop electroweak and QCD corrections to this amplitude are known, and when combined shift the leading one-loop expression by less than 1\% for the measured value of the Higgs boson mass~\cite{Degrassi:2005mc}. However, physics beyond the SM could affect these couplings in a non-trivial way, either through modifications of the $h t\bar t$ and $hW^+W^-$ couplings or by means of loops containing new heavy particles. The measurement of the light-quark couplings to the Higgs should therefore be considered together with the extraction of the effective $h\gamma\gamma$ coupling. As pointed out in~\cite{Koenig:2015pha}, by taking the ratio of the $h\to M\gamma$ and $h\to\gamma\gamma$ branching fractions one can remove this sensitivity to unknown new contributions to the $h\gamma\gamma$ coupling.

We now consider the theoretical prediction for the direct amplitude. This quantity cannot be directly related to data, unlike the indirect amplitude. Two theoretical approaches have been used to calculate this contribution. The hierarchy $m_h\gg m_M$ implies that the vector meson is emitted at very high energy $E_M\gg m_M$ in the Higgs boson rest frame. The partons making up the vector meson can thus be described by energetic particles moving collinear to the direction of $M$. This kinematic hierarchy allows the QCD factorization approach~\cite{Lepage:1980fj,Efremov:1979qk} to be utilized. Up to corrections of order $(\Lambda_{\rm QCD}/m_h)^2$ for light mesons, and of order $(m_M/m_h)^2$ for heavy vector mesons, this method can be used to express the direct contribution to the $h\to M\gamma$ decay amplitude as a perturbatively calculable hard-scattering coefficient convoluted with the leading-twist light-cone distribution amplitude (LCDA) of the vector meson. This approach was pursued in~\cite{Koenig:2015pha}, where the full next-to-leading order (NLO) QCD corrections were calculated and large logarithms of the form $[\alpha_s\ln(m_h/m_M)]^n$ were resummed at NLO, and in~\cite{Kagan:2014ila}, where an initial LO analysis was performed. The dominant theoretical uncertainties remaining after this calculation are parametric uncertainties associated with the non-perturbative LCDAs of the vector mesons. Thanks to the high value $\mu\sim m_h$ of the factorization scale, however, the LCDAs are close to the asymptotic form $\phi_M(x,\mu)=6x(1-x)$ attained for $\mu\to\infty$, and hence the sensitivity to not yet well-known hadronic parameters turns out to be mild. For the heavy vector mesons $M=J/\psi,\Upsilon(nS)$, the quark and antiquark which form the meson are slow-moving in the $M$ rest frame. This allows the non-relativistic QCD framework (NRQCD)~\cite{Bodwin:1994jh} to be employed to facilitate the calculation of the direct amplitude. This approach was pursued in~\cite{Bodwin:2014bpa}, where the NLO corrections in the velocity $v$ of the quarks in the $M$ rest frame, the next-to-leading order corrections in $\alpha_s$, and the leading-logarithmic resummation of collinear logarithms were incorporated into the theoretical predictions. The dominant theoretical uncertainties affecting the results for $h\to J/\psi\,\gamma$ and $h\to\Upsilon(nS)\,\gamma$ after the inclusion of these corrections are the uncalculated ${\cal O}(v^4)$ and ${\cal O}(\alpha_s v^2)$ terms in the NRQCD expansion.

\begin{table}
{\caption{\label{table:SM_BRs} 
Theoretical predictions for the $h\to M\gamma$ branching ratios in the SM, obtained using different theoretical approaches.}}
\begin{center}
\begin{tabular}{c|ccc}
\toprule
Mode & \multicolumn{3}{c}{Branching Fraction [$10^{-6}$]} \\
Method &  ~~NRQCD~\cite{Bodwin:2014bpa}~~ & ~~LCDA LO~\cite{Kagan:2014ila}~~
 & ~~LCDA NLO~\cite{Koenig:2015pha}~~ \\
\midrule
$\br(h\to\rho\gamma)$ & -- & $19.0\pm 1.5$ & $16.8\pm 0.8$ \\
$\br(h\to\omega\gamma)$ & -- & $1.60\pm 0.17$ & $1.48\pm 0.08$ \\
$\br(h\to\phi\gamma)$ & -- & $3.00\pm 0.13$ & $2.31\pm 0.11$ \\
$\br(h\to J/\psi\,\gamma)$ & -- & $2.79\,_{-0.15}^{+0.16}$ & $2.95\pm 0.17$ \\
$\br(h\to\Upsilon(1S)\,\gamma)$ & $(0.61\,_{-0.61}^{+1.74})\cdot 10^{-3}$ & -- 
 & $(4.61\,_{-\,1.23}^{+\,1.76})\cdot 10^{-3}$ \\
$\br(h\to\Upsilon(2S)\,\gamma)$ & $(2.02\,_{-1.28}^{+1.86})\cdot 10^{-3}$ & -- 
 & $(2.34\,_{-\,1.00}^{+\,0.76})\cdot 10^{-3}$ \\
$\br(h\to\Upsilon(3S)\,\gamma)$ & $(2.44\,_{-1.30}^{+1.75})\cdot 10^{-3}$ & -- 
 & $(2.13\,_{-\,1.13}^{+\,0.76})\cdot 10^{-3}$ \\
\bottomrule
\end{tabular}
%\parbox{15.5cm}
\end{center}
\end{table} 

Table~\ref{table:SM_BRs} collects theoretical predictions for the various $h\to M\gamma$ branching fractions in the SM. The inclusion of NLO QCD corrections and resummation help to reduce the theoretical uncertainties. There is in general good agreement between the results obtained by different groups. The $h\to\phi\gamma$ branching ratio obtained in~\cite{Koenig:2015pha} is lower than that found in~\cite{Kagan:2014ila} because of an update of the $\phi$-meson decay constant performed in the former work. Also, in~\cite{Koenig:2015pha} the effects of $\rho$--$\omega$--$\phi$ mixing are taken into account. One observes that the $h\to M\gamma$ branching fractions are typically of order few times $10^{-6}$, which makes them very challenging to observe. The most striking feature of the results shown in the table concerns the $h\to\Upsilon(nS)\,\gamma$ modes, whose branching fractions are very strongly suppressed. This suppression results from an accidental and almost perfect cancellation between the direct and indirect amplitudes. In the case of $h\to\Upsilon(1S)\,\gamma$ the cancellation is so perfect that the small imaginary part of the direct contribution induced by one-loop QCD corrections gives the leading contribution to the decay amplitude. The fact that this imaginary part was neglected in~\cite{Bodwin:2014bpa} explains why a too small branching fraction for this mode was obtained there.

\begin{figure}[thb]
\includegraphics[width=0.45\textwidth]{./WG3/ExoticHiggsDecays/RareDecays/phi_ks-eps-converted-to.pdf} \quad
\includegraphics[width=0.45\textwidth]{./WG3/ExoticHiggsDecays/RareDecays/Jpsi_kc-eps-converted-to.pdf}
\caption{\label{figure:phi_Jpsi}
$h\to\phi\gamma$ and $H\to J/\psi\,\gamma$ branching ratios, normalized to the $h\to\gamma\gamma$ branching fraction, as functions of the real part of $\kappa_{s,c}/\kappa_\gamma$. The SM values are indicated by the red arrows.}
\end{figure}

The main purpose of searching for the decays $h\to M\gamma$ is to use them for probing the light-quark Yukawa couplings. In order to eliminate possible new physics effects in the $h\to\gamma\gamma$ rate, it is of advantage to consider the ratio $R_{M\gamma}=\br(h\to M\gamma)/\br(h\to\gamma\gamma)$~\cite{Koenig:2015pha}, where in the SM $\mbox{BR}(h\to\gamma\gamma)=(2.28\pm 0.11)\cdot 10^{-3}$~\cite{Heinemeyer:2013tqa}. In the limit where the CP-violating contributions to the $h\to\gamma\gamma$ amplitude are neglected (the dominant such contributions would likely arise from the top-quark loop, but Electric Dipole Moment (EDM) constraints limit the imaginary part of $\kappa_t$ to be less than 1\%~\cite{Brod:2013cka}), one finds~\cite{Koenig:2015pha}
\begin{equation}\label{wonderful_ratio}
   R_{M\gamma} = \frac{8\pi\alpha^2(m_M)}{\alpha}\,\frac{Q_M^2 f_M^2}{m_M^2}
    \left( 1 - \frac{m_M^2}{m_h^2} \right)^2 
    \left( \big|1-\Delta_M\big|^2 + \big|\tilde\Delta_M\big|^2 \right) .
\end{equation}
The parameters $\Delta_M$ ($\tilde\Delta_M$) are proportional to the real (imaginary) part of the relevant $\kappa_q$ parameter and can be calculated using the QCD factorization approach, as described earlier. For all mesons other than the $\Upsilon(nS)$ states the interference of the direct amplitude with the dominant indirect one is a small effect, and hence the ratio $R_{M\gamma}$ is to excellent approximation a linear function of the real part of the ratio $\kappa_q/\kappa_\gamma$, where $\kappa_\gamma$ is the new physics modification of the entire $h\to\gamma\gamma$ matrix element. This quantity is known to be close to its SM value~1. \refF{figure:phi_Jpsi} shows theoretical predictions for the ratios $R_{\phi\gamma}$ and $R_{J/\psi\,\gamma}$ (times $10^3$) obtained in~\cite{Koenig:2015pha}. The width of the bands reflects the theoretical uncertainties. The corresponding predictions for the lighter mesons $\rho$ and $\omega$ suffer from significant hadronic uncertainties due to $\rho$--$\omega$--$\phi$ mixing.

\begin{figure}[thb]
\includegraphics[width=0.45\textwidth]{./WG3/ExoticHiggsDecays/RareDecays/Y1s_kb-eps-converted-to.pdf} \quad
\includegraphics[width=0.45\textwidth]{./WG3/ExoticHiggsDecays/RareDecays/Y1S_contour-eps-converted-to.pdf}
\caption{\label{figure:contours}
Ratio $R_{\Upsilon(1S)\gamma}$ as a function of the real and imaginary parts of $\kappa_b/\kappa_\gamma$. In the left plot the imaginary part is set to zero. The right plot shows contour lines of $10^3\,R_{\Upsilon(1S)\gamma}$ in the complex $\kappa_b/\kappa_\gamma$ plane. The black dot and the arrow indicate the SM values. Coupling parameters inside the dashed white circle are preferred by the current LHC data on $h\to b\bar b$.}
\end{figure}

In the case of the $h\to\Upsilon(nS)\,\gamma$ decay modes the SM branching ratios are so small that a discovery at the LHC (or any other conceivable collider) is all but elusive. The direct contributions are no longer a small correction, and hence the quadratic terms in $\kappa_b$ are important. On the other hand, the almost perfect cancellation between the direct and indirect amplitudes no longer holds in the presence of new physics. The left plot in \refF{figure:contours} shows the dependence of the ratio $R_{\Upsilon(1S)\gamma}$ on the real part of $\kappa_b/\kappa_\gamma$, assuming that the CP-violating imaginary part vanishes. It is evident that the SM value accidentally coincides with the minimum of the curve, while significantly larger branching fractions are possible when new physics alters the value of $\kappa_b$. As an interesting benchmark for LHC experiments, we consider the case where $\kappa_b=-1$, while $\kappa_\gamma$ takes its SM value of 1. This benchmark is and will be in great agreement with LHC Higgs boson coupling fits, since Higgs boson coupling measurements cannot probe the sign of $\kappa_b$. We then obtain the branching fractions
\begin{equation}
\begin{aligned}
   \br(h\to\Upsilon(1S)\,\gamma) 
   &= (0.98\pm 0.06) \cdot 10^{-6} \,, \\
   \br(h\to\Upsilon(2S)\,\gamma) 
   &= (0.45\pm 0.03) \cdot 10^{-6} \,, \qquad (\kappa_b=-1) \\
   \br(h\to\Upsilon(3S)\,\gamma) 
   &= (0.33\pm 0.03) \cdot 10^{-6} \,,
\end{aligned}
\end{equation}
more than two orders of magnitude larger than in the SM. The right plot in \refF{figure:contours} shows contours of $10^3\,R_{\Upsilon(1S)\gamma}$ in the complex $\kappa_b/\kappa_\gamma$ plane. The dashed white circle indicates the current upper bound on the combination $\lambda_{b\gamma}=|\kappa_b/\kappa_\gamma|$, which to an excellent approximation measures the deviation of the ratio $\br(h\to b\bar b)/\br(h\to\gamma\gamma)$ from its SM value. The Higgs bosons must be produced via the same production mechanism in both cases, so that possible new physics effects in Higgs boson production cancel out. Since the $h\to b\bar b$ mode is measured at the LHC in the rare $VH$ and $t\bar t H$ associated-production channels, at present no accurate direct measurements of $\lambda_{b\gamma}$ are available. However, from the model-independent global analyses of Higgs boson couplings, in which all couplings to SM particles (including the effective couplings to photons and gluons) are rescaled by corresponding $\kappa_i$ parameters and also invisible Higgs boson decays are allowed, one obtains $\lambda_{b\gamma}=0.63\pm 0.27$ for CMS~\cite{Khachatryan:2014jba} and $\lambda_{b\gamma}=0.67\pm 0.32$ for ATLAS~\cite{Aad:2015gba}. At 95\% CL this implies $\lambda_{b\gamma}<1.3$. Within this allowed region, the $h\to\Upsilon(1S)$ branching ratio varies by more than two orders of magnitude and can take values as large as $1.3\cdot 10^{-6}$, which may be within reach of the high-luminosity run at the LHC.

The decays $h\to\Upsilon(nS)\,\gamma$ provide a golden opportunity to probe new-physics effects on the bottom-quark Yukawa couplings. Any measurement of such a decay would be a clear signal of new physics. A combined measurement of the two ratios $\br(h\to\Upsilon(nS)\,\gamma)/\br(h\to\gamma\gamma)$ and $\mbox{BR}(h\to b\bar b)/\br(h\to\gamma\gamma)$ can provide complementary information on the real and imaginary parts of the $b$-quark Yukawa coupling. We can think of no other way in which one can probe the magnitudes and signs of the real and imaginary parts of $\kappa_b$ separately.
