\subsection[\texorpdfstring{$h \rightarrow V P$}{h to VP} and \texorpdfstring{$h \rightarrow V P^\star$}{H to VP*}]{\texorpdfstring{$h \rightarrow V P$}{h to VP} and \texorpdfstring{$h \rightarrow V P^\star$}{H to VP*}\SectionAuthor{G.~Isidori, M.~Trott}}
\label{SMmassivegauge}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this section, we discuss decays of the form $h \rightarrow V M$
where $V$ is a massive on-shell SM vector boson ($V=Z,W$) and $M$ is
an associated meson (vector or pseudoscalar) produced in the decay of
the $h$ particle.

We will focus on the decay of the Higgs, assuming a narrow width
approximation to factorize the decay and production mechanisms.  The
decays we will discuss are very rare decays in the SM, with extremely
small branching ratios. Despite these small rates, it is still
important to search for such rare decays, to learn experimentally
about the properties of the discovered $h$ state robustly. It was
pointed out in Ref.~\cite{Isidori:2013cla} that such rare exclusive
decays of the $h$ particle with an associated massive vector boson
offer complementary information about the properties of this state,
and how it couples to the SM fields. This information is complementary
to what can be determined experimentally from more inclusive $h$ decay
modes.
Reaching the experimental sensitivity required to observe such
extremely rare decays, with {\it any} associated $V = \{\gamma, Z, W
\}$, is extremely challenging but worth the effort.

Given the strong suppression of such exclusive decay modes,
 the theoretical predictions of the corresponding decay rates
are subject to an irreducible uncertainty due to the limited knowledge  of the $h$ dominant decay modes, both within and, especially, beyond the SM.
In particular, the total uncertainty in the width of the $h$ particle ($\Gamma_h$) feeds into the uncertainty in the predicted $\br(h \rightarrow i)$ as
\bea
\delta BR (h \rightarrow i) = {\rm BR (h \rightarrow i)} \,  \left(\frac{\delta \Gamma_{h \rightarrow i}}{\Gamma_{h \rightarrow i}} + \frac{\delta \Gamma_h}{\Gamma_h}
-2  \frac{{\Delta}_{i \, \Gamma_h}}{\Gamma_{h \rightarrow i} \Gamma_{h}} \right)^{1/2},
\eea
using simple Gaussian error propagation, where $\delta$ indicates a $1 \sigma$ error. Here $\Delta_{i \, \Gamma_h}$ is the covariance of the total width and the decay channel $h \rightarrow i$.
Although possible tests of the decay width---within the SM---have been proposed in the literature~\cite{Kauer:2012hd,Campbell:2013una,Caola:2013yja} and carried out by the experimental collaborations~\cite{Khachatryan:2014iha,Aad:2015xua},
the corresponding constraint on the decay width does not hold in an EFT generalization of the SM~\cite{Englert:2014aca}.
Indeed, as emphasized in Ref.~\cite{Englert:2014aca}, the uncertainty in $\mu_{ggF}$ and $\mu_{ZZ}$ directly feeds into such a measurement.
When we discuss theoretical uncertainties for the rare modes in Tables \ref{tab:pseudo} and \ref{tab:vector}, we report theoretical uncertainties of the form $ \delta \Gamma_{h \rightarrow i}/\Gamma_{h \rightarrow i}$ for the SM,
added in quadrature to the total theoretical width defined as $\Gamma_{h} =4.08~{\rm MeV}$ with a $\pm 3.9\%$ relative error.
We neglect the unknown $\Delta_{i \, \Gamma_h}$ in this estimate.


\subsubsection{SM predictions, dominant electroweak dependence}


 The SM prediction for the decay proceeds dominantly through the diagrams shown in \refF{fig1:exclusivedecay}.
Defining the SM currents coupling to the massive vector bosons as
\bea
\label{currentdefn}
\mathcal{L}_{J} = \frac{e }{ \sqrt{2}\, \sin \theta_W}  J_\mu^{\pm} W^\mu_{\pm}   + \frac{e}{\sin \theta_W \cos \theta_W}  J_\mu^0 Z^\mu,
\eea
and the pseudoscalar ($P$) and vector meson ($P^\star$) decay constants with the following normalizations
\bea
\langle P(q) | J_\mu(q) | 0 \rangle 0 = \frac{1}{2} \, F_P \, q_\mu, \quad \quad \langle P^\star (q) | J_\mu(q) | 0 \rangle  = \frac{1}{2} \, F_P^\star \, m_p^\star \, \epsilon_\mu,
\eea
then the tree-level (\refF{fig1:exclusivedecay}a) SM contribution is \cite{Isidori:2013cla},
\begin{align}
\cB^{\rm SM}(h\to VP)= \frac{m_h^3 \, G_F^2}{8 \, \pi} \frac{\left| C_V F_P \right|^2}{(\Gamma_h)_{SM}} \lambda^3(1, \rho, \hat{q}^2) ,
\label{eq:hVPSM}
\end{align}
where $C_V = \{1/\sqrt{2},1 \}$ for the cases $W$ and $Z$ respectively.
Here $\rho = m_V^2/m_h^2$ and $\hat{q}^2 = m_P^2/m_h^2$ and $\lambda(a, b, c) = \sqrt{a^2 + b^2 + c^2 - 2 (ab + ac + bc)}$. $G_F$ is the fermi constant
and $(\Gamma_h)_{SM}$ is the SM Higgs boson decay width. The case of a decay to a vector meson
through a $Z^\star$, for example $h \rightarrow J/\psi Z$, through \refF{fig1:exclusivedecay}a gives a branching ratio \cite{Gonzalez-Alonso:2014rla}
\begin{align}
\cB^{\rm SM} (h\to VJ/\Psi)=\frac{m_h^3 \, G^2_F}{8 \, \pi} \frac{\left| F_{J/\Psi}^\star \right|^2}{(\Gamma_h)_{SM}} \frac{\sqrt{\lambda(1, \rho, \hat{q}^2)}}{(1 - \hat{q}^2/\rho)^2} \left[(1-\rho)^2\left(1- \frac{\hat{q}^2}{1-\rho}\right)^2 + 8 \,  \hat{q}^2 \, \rho \right].
\end{align}
The tree-level contribution in \refF{fig1:exclusivedecay}a is usually largely dominant but for the case of charmonium vector resonances
($J/\psi$, $\psi'$, $\ldots$). In the latter case the accidental suppression of the $Z$-boson vector coupling to charm makes the
formally subleading (one-loop induced) $h \rightarrow Z \, \gamma$ amplitude (\refF{fig1:exclusivedecay}b) compete with the tree-level one~\cite{Bodwin:2013gca,Gao:2014xlv}.
The full SM expression including this contribution and the interference term can be found in Ref.~\cite{Gao:2014xlv}\footnote{See Ref.~\cite{Alte:2016yuw} for the latest computation using the QCD factorization approach. A future LHC HXSWG note will contain updated predictions.}.
The SM predictions of pseudoscalar decays are given in \refT{tab:pseudo}. We use the value $ C_{Z\gamma}^{SM} = -5.540$ for the Wilson coefficient of this operator, consistent with the normalization of the operator defined in Ref.~\cite{Gao:2014xlv,Korchin:2013ifa}.



\begin{figure}[thb]
\begin{tikzpicture}
\draw[dashed] (0,0) -- (2.0,0) ;
\draw[decorate,decoration=snake] (2.0,0) -- (3.5,1.5) ;
\draw[decorate,decoration=snake] (2.0,0) -- (3,-1.5) ;
\draw [->-] (3,-1.5) -- (4,-1.0);
\draw [-<-] (3,-1.5) -- (4,-2.0);
\draw (4.1,-1.5) ellipse (0.1cm and 0.7cm);
\node [above][ultra thick] at (2.2,0.8) {$Z (W)$};
\node [above][ultra thick] at (1.6,-1.3) {$Z^\star (W^\star)$};
\node [above][ultra thick] at (2.5,-2.8) {$(a)$};
\end{tikzpicture}
\begin{tikzpicture}
\draw[dashed] (0.5,0) -- (2.5,0) ;
\filldraw (2.5,0) circle (0.095);
\draw[decorate,decoration=snake] (2.5,0) -- (4,1.5) ;
\draw[decorate,decoration=snake] (2.5,0) -- (3.5,-1.5) ;
\draw [->-] (3.5,-1.5) -- (4.5,-1.0);
\draw [-<-] (3.5,-1.5) -- (4.5,-2.0);
\draw (4.6,-1.5) ellipse (0.1cm and 0.7cm);
\node [above][ultra thick] at (3,0.8) {$Z$};
\node [above][ultra thick] at (2.5,-1.3) {$\gamma^\star$};
\node [above][ultra thick] at (3,-2.8) {$(b)$};
\end{tikzpicture}
\begin{tikzpicture}
\draw[dashed] (0,0)  (1,2.6) ;
\draw[dashed] (1,2.6)  -- (3,2.6) ;
\draw [->-] (3,2.6) -- (4.9,3.8) ;
\draw [-<-] (3,2.6) -- (3.5,2) ;
\draw [-<-] (3.5,2)  -- (4.9,3.2);
\draw[decorate,decoration=snake] (3.5,2)-- (4.85,1.2) ;
\draw (5,3.5) ellipse (0.1cm and 0.6cm);
\node [above][ultra thick] at (5.5,0.8) {$Z (W)$};
\node [above][ultra thick] at (3,0) {$(c)$};
\end{tikzpicture}
\caption{\label{fig1:exclusivedecay}
Direct contributions to exclusive decay modes of the form $h \rightarrow V P$ and $h \rightarrow V P^\star$. Diagram
(a) is generally the dominant contribution in the SM, while diagram $(b)$ can also contribute significantly for narrow light vector mesons.
Diagram (c) is generally negligible in the SM, but can be significantly enhanced in beyond the SM scenarios.}
\end{figure}



\subsubsection{SM predictions, subdominant Yukawa dependence}

In the SM branching ratios reported in Tables \ref{tab:pseudo} and \ref{tab:vector} the contribution
from \refF{fig1:exclusivedecay}c is neglected, being suppressed by a small Yukawa
coupling.  This is always a good approximation in the SM if the
associated vector meson is a $Z$ or a $W$, i.e.~when there is a
tree-level contribution not suppressed by small Yukawa couplings.

The Yukawa amplitude is not necessarily negligible in the radiative
modes ($V=\gamma$), when the tree-level amplitude is absent.  The
possibility of determining this contribution in the modes $V=\gamma$
has been already discussed in. Section~\ref{SMphoton}.
However, as we briefly illustrate below, this goal is
extremely challenging  for the SM values of $\kappa_q=1$, given that the Yukawa contribution is typically
subleading.

New physics in the Higgs sector at some high scale can induce
large deviations of the light-quark Yukawa couplings from their SM
values even when
the cut-off scale is parametrically greater than the electroweak scale.
Indeed the corresponding amplitudes scale as
%
\bea
A(\bar{\psi} \, \psi \rightarrow W_L \, W_L) = \frac{m_\psi \sqrt{s}}{v^2} (1 - \kappa_\psi \, \kappa_W).
\eea
%
Due to the presence of a small fermion ($\psi$) mass scale, large
deviations in $\kappa_\psi$ still allow the cut-off
scale of the theory to remain parametrically separated from the scale
$v$.  While such enhancements would generally occur along with
 other deviations in Higgs phenomenology,
including deviations in the electroweak couplings present in \refF{fig1:exclusivedecay}a
and \refF{fig1:exclusivedecay}b, some classes of BSM models can predict  parametrically large enhancements to $\kappa_\psi$  while leaving other Higgs boson couplings largely unaffected, as discussed in Section\ref{NP}.  It is important to bear in mind that in BSM models which do yield large deviations in other Higgs boson couplings, the extraction of any
information on the sub-leading Yukawa contribution could be out of reach.

As a specific example, consider the results quoted in Ref.~\cite{Kagan:2014ila}, and discussed in Section~\ref{SMphoton}, for the $\gamma$-tagged decays:
%
\bea
\frac{\br(h \rightarrow \phi \, \gamma)}{\br(h \rightarrow b \bar{b})}  &\simeq&  \frac{\kappa_\gamma \left[(3.0 \pm 0.13) \, \kappa_\gamma  - 0.02 \, \kappa_s\right]}{0.57 \, \kappa_b^2} \, \times 10^{-6}, \nn \\
\frac{\br(h \rightarrow \rho \, \gamma)}{{\br}(h \rightarrow b \bar{b})} &\simeq&  \frac{\kappa_\gamma \left[(1.9 \pm 0.15) \, \kappa_\gamma  - 1 \times 10^{-4} \, \kappa_u  - 1 \times 10^{-4} \, \kappa_d \right]}{0.57 \, \kappa_b^2} \, \times 10^{-5}, \nn \\
\frac{{\br}(h \rightarrow \omega \, \gamma)}{{\br}(h \rightarrow b \bar{b})} &\simeq&  \frac{\kappa_\gamma \left[(1.6 \pm 0.17) \, \kappa_\gamma  - 3 \times 10^{-4} \, \kappa_u  - 3 \times 10^{-4} \kappa_d \right]}{0.57 \, \kappa_b^2} \, \times 10^{-6}.
\label{eq:rarebruncertainty}
\eea
%
The only modification here compared to Ref.~\cite{Kagan:2014ila} is to
utilize $\kappa_q$ factors normalized to the light quark masses,
consistent with standard usage.  Recall that currently $\kappa_\gamma
= 0.92^{+0.12}_{-0.11}$ when $\br_{BSM}$ is assumed to be $0$ in the
ATLAS/CMS coupling fit combination \cite{Khachatryan:2016vau}.
While enhancements of $\kappa_{s,u,d}$ by $\mathcal{O}(100)$ may thus be
 measurable (see Section~\ref{SMphoton}), Eq.~\ref{eq:rarebruncertainty}
 demonstrates that
a very large reduction in the uncertainties of the
tree-level couplings of the $h$ state, and the effective one-loop
couplings of this state (i.e. $\kappa_\gamma$), would be required for
these rare decays to be
sensitive to light quark Yukawa couplings of $\mathcal O(10)$ their SM values or smaller.

\subsubsection{SM Theory errors}
The dominant theoretical errors in the branching ratios are due to the
lattice (or experimental) errors on the meson decay constants $F_P$ (ranging
between few $\%$ and $10\%$), which are combined in quadrature with
the uncertainties on the elements of the Cabibbo-Kobayashi-Maskawa
mixing matrix ($V_{\rm CKM}$).  This uncertainty is given in column
three of the branching ratio tables. This theoretical error, combined
with the theoretical error on the Higgs boson total width ($\sim 4\%$
\cite{Beringer:1900zz}) dictates the error given in the fifth column
of the tables.

%\begin{center}
\begin{table}
\caption{SM branching ratios $\cB^{\rm SM}$  for selected  $h\to VP$ decays. The decay constants are defined as $F_{\pi^0} = f_\pi/\sqrt{2}$, $F_{K^\pm} = V_{us} \, f_K$
$F_{\pi^{\pm}} = V_{ud} f_\pi$, $F_{D^\pm}= V_{cd} f_D$, $F_{D_s} = V_{cs} f_{D_s}$, $F_{B^\pm}= V_{ub} f_B$, $F_{B_c^\pm}= V_{cb} f_B$,  and $F_{\eta_c} = f_{\eta_c}/2$,
where $f_P$ are the standard meson decay constants reported in \cite{Beringer:1900zz,Colangelo:2010et,Davies:2010ip} for $N_f = 2+1$ (when available)
and the CKM parameters are PDG values \cite{Beringer:1900zz}. The theoretical errors quoted are $1 \sigma$ values.}
\label{tab:pseudo}
\centering
\tabcolsep 8pt
\begin{tabular}{c|c|c|c|c}
\toprule
$VP$~mode & $P$ mass & $F_P$ & $\cB^{\rm SM}$  & Th. Error  \\ \hline
 $W^- \pi^+$  & 139.57018 $\pm$ 0.00035 {\rm MeV}  & 126.6 $\pm$ 1.4 {\rm MeV} &  $0.42 \times 10^{-5}$  & $\pm 5 \%$ \\
 $W^- K^+$  & 493.677 $\pm$ 0.016 {\rm MeV}  & 35.2 $\pm$ 0.3 {\rm MeV}  &    $0.33 \times 10^{-6}$ &   $\pm 4 \%$ \\
 $W^- D^+_s$  & 1968.30 $\pm$ 0.11 {\rm MeV}  & 248.6 $\pm$ 2.4 {\rm MeV}  &   $1.6 \times 10^{-5}$ & $\pm 4 \%$  \\
$W^- D^+$  & 1869.61 $\pm$ 0.09 {\rm MeV}  & 47.07 $\pm$ 2.4 {\rm MeV} &  $0.58 \times 10^{-6}$  &  $\pm 11 \%$\\
$W^- B^+$  &  5279.29 $\pm$ 0.15  {\rm MeV}  & 0.79 $\pm$ 0.10 {\rm MeV} &  $1.6 \times 10^{-10}$  & $\pm 26 \%$ \\
$W^- B_c^+$  & 6275.1 $\pm$ 1.0 {\rm MeV}  & 7.82 $\pm$ 0.42 {\rm MeV} &  $1.6 \times 10^{-8}$  & $\pm 11 \%$\\
\midrule
 $Z \pi^0$  &  134.9766 $\pm$ 0.0006 {\rm MeV} & 92.1 $\pm$ 1.0 {\rm MeV}  &  $0.23 \times 10^{-5}$  & $\pm 5 \%$ \\
$Z \eta_c$  & 2984.3 $\pm$ 0.84 {\rm MeV}  & 197.4 $\pm$ 0.30 {\rm MeV}  &  $1.0 \times 10^{-5}$ & $\pm 5 \%$   \\
\bottomrule
\end{tabular}
\end{table}
%\end{center}

%\begin{center}
\begin{table}
\caption{SM branching ratios for selected  $h\to VP^\star$ decays. The normalizations are defined as in the case of the
pseudoscalar mesons. We use $F_B^\star/F_B = 1.02 \pm 0.08$  \cite{Gelhausen:2013wia}. The theoretical errors quoted are $1 \sigma$ values.
Total errors quoted at $\gtrsim 4 \%$  do not have a decay constant theoretical error assigned.}
\label{tab:vector}
\centering
\tabcolsep 8pt
\begin{tabular}{c|c|c|c|c}
\toprule
$VP^\star$~mode & $P^\star$ mass & $F_P^\star/2$ & $\cB^{\rm SM}$  & Th. Error \\ 
\midrule
 $W^- \rho^+$  &  775.26 $\pm$ 0.25 {\rm MeV}  & 210 $\pm$ 5.5 {\rm MeV} \cite{Chen:2015tpa}&  $1.5 \times 10^{-5}$  & $\pm 6 \%$ \\
 $W^- K^{\star +}$  & 891.66 $\pm$ 0.026 {\rm MeV}  & 35.8 $\pm$ 0.3 {\rm MeV}  &   $4.3 \times 10^{-7}$   &  $\pm 4 \%$ \\
 $W^- D^+$  &  2010.26 $\pm$ 0.07 {\rm MeV}  & 61.1 $\pm$ 0.6 {\rm MeV}  &   $1.3 \times 10^{-6}$ & $\pm 6 \%$ \\
$W^- D^{\star \, +}_s$  & 2112.1  $\pm$  0.4 {\rm MeV}  & 320.5 $\pm$ 3.1 {\rm MeV} &  $3.5 \times 10^{-5}$ &  $\pm 6 \%$ \\
$W^- B^{\star +}$  &  5325.2 $\pm$0.4  {\rm MeV}  & 194.3 $\pm$ 15.8 {\rm MeV}  \cite{Gelhausen:2013wia} & $1.3 \times 10^{-5}$ & $\pm 17 \%$  \\
\midrule
$Z J/\Psi(1S)$  &  3096.916 $\pm$ 0.011 {\rm MeV} & 405 $\pm$ - {\rm MeV}  &  $3.2 \times 10^{-6}$ &  $\gtrsim 4 \%$ \\
$Z J/\Psi(2S)$  & 3686.109 $\pm$ 0.013 {\rm MeV}  & 290 $\pm$ - {\rm MeV}  &  $1.5 \times 10^{-6}$  & $\gtrsim 4 \%$ \\
\midrule
$Z \Upsilon(1S)$  & 9460.30 $\pm$ 0.26 {\rm MeV}  & 680 $\pm$ - {\rm MeV}  &  $1.7 \times 10^{-5}$  & $\gtrsim 4 \%$ \\
$Z \Upsilon(2S)$  &10023.26 $\pm$ 0.31 {\rm MeV}  & 485 $\pm$ - {\rm MeV}  &  $8.9 \times 10^{-6}$  & $\gtrsim 4 \%$ \\
$Z \Upsilon(3S)$  & 10355.2 $\pm$ 0.5 {\rm MeV}  & 420 $\pm$ - {\rm MeV}  &  $6.7 \times 10^{-6}$  & $\gtrsim 4 \%$ \\
\midrule
$Z \rho^0$  &  775.26 $\pm$ 0.25 {\rm MeV} & 216 $\pm$ 5.5 {\rm MeV} \cite{Chen:2015tpa} &  $1.4 \times 10^{-5}$ &  $\pm 6 \%$ \\
$Z \omega^0$  & 782.65 $\pm$ 0.12 {\rm MeV} & 216 $\pm$ 5.5 {\rm MeV} &  $1.6 \times 10^{-6}$ &  $\pm 6 \%$ \\
$Z \phi^0$  &  1019.461 $\pm$ 0.019 {\rm MeV} & 233 $\pm$ 5 {\rm MeV} \cite{Becirevic:2003pn} &  $4.2 \times 10^{-6}$ &  $\pm 6 \%$ \\
\bottomrule
\end{tabular}
\end{table}
%\end{center}
