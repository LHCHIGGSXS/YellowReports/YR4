%\newcommand{\Sherpa}{S\protect\scalebox{0.8}{HERPA}\xspace}
\newcommand{\CSS}{C\protect\scalebox{0.8}{SS}\xspace}
\newcommand{\Comix}{C\protect\scalebox{0.8}{OMIX}\xspace}
\newcommand{\Amegic}{A\protect\scalebox{0.8}{MEGIC++}\xspace}
\newcommand{\MCatLO}{M\protect\scalebox{0.8}{C}@L\protect\scalebox{0.8}{O}\xspace}
%\newcommand{\MEPSatNLO}{M\scalebox{0.8}{E}P\scalebox{0.8}{S}@N\protect\scalebox{0.8}{LO}\xspace}
%\newcommand{\Collier}{C\protect\scalebox{0.8}{OLLIER}\xspace}
%\newcommand{\OpenLoops}{O\protect\scalebox{0.8}{PEN}L\protect\scalebox{0.8}{OOPS}\xspace}

In this section, the setup of the \Sherpa{} event generation
framework~\cite{Gleisberg:2008ta} is presented. Two classes of 
results are considered for \Sherpa:
\begin{itemize}
\item {4FS using the \MCatNLO{} matching:}\\
  One set of results is in the 4FS, and based on the
  \MCatNLO technique~\cite{Frixione:2002ik}, as implemented in
  \Sherpa~\cite{Hoeche:2011fd}.  At tree-level the simulation thus starts
  from processes such as $gg\to b\bar{b}H$ and $q\bar{q}\to b\bar{b}H$,
  where no specific cuts are applied on the $b$ quarks.  Their finite
  mass regulates collinear divergences that would appear in
  the massless case.  In most cases, therefore, a $b$ jet actually
  originates from the parton shower evolution and hadronization of a
  $b$ quark. At the NLO, our computation involves contributions proportional 
  to $\yb^2$ and $\yb\yt$, see Eq.~\eqref{sig4FS}.
\item {5FS using the \MEPSatNLO{} multi-jet merging:}\\
  Alternatively, we employ the 5FS with massless  $b$ quarks.  
  In order to account for bins with zero and one $b$ jets, multi-jet
  merging is being employed.  In \Sherpa, the well-established mechanism for
  combining into one inclusive sample towers of matrix elements with
  increasing jet multiplicity at tree level~\cite{Catani:2001cc} has
  recently been extended to next-to leading order matrix elements, in
  a technique dubbed \MEPSatNLO~\cite{Hoeche:2012yf}.  This is the technique
  chosen here.  Merging rests on a jet criterion, applied to the matrix
  elements.  As a result, jets are being produced by the fixed-order
  matrix elements and further evolved by the parton shower.  As a consequence,
  the jet criterion separating the two regimes is typically chosen such
  that the jets produced by the matrix elements are softer than the jets
  entering the analysis.  This is realized here by a cut-off of
  $\mu_{\rm jet}\,=\, 20 $ GeV.  In the \MEPSatNLO{} simulation matrix elements
  for $\bbh$ production in the 5FS up to 2 jets at NLO accuracy have been 
  included, i.e.\ final states for $\phi$, $\phi+j$, and $\phi+jj$ with $\phi$ emitted 
  from a massless bottom-quark line are calculated using the \MCatNLO{} technique, 
  while $\phi+jjj$ matrix elements are accounted for only at the LO, 
  where the jets can be light jets or $b$ jets. 
  For the former, it is of course always possible that a
  light jet originating from, e.g., a gluon, can turn into a $b$ jet through
  a $g\to b\bar{b}$ splitting during the parton shower.  
  We note that only contributions proportional to $\yb^2$ do not vanish in 
  the 5FS computation, see Eq.~\eqref{sig5FS}.
\end{itemize}
In \Sherpa, tree-level cross sections are provided by two matrix
element generators, \Amegic~\cite{Krauss:2001iv} and
\Comix~\cite{Gleisberg:2008fv}, which also implement the automated
infrared subtraction~\cite{Gleisberg:2007md} through the Catani-Seymour
scheme~\cite{Catani:1996vz,Catani:2002hc}.  For parton showering, the
implementation of~\cite{Schumann:2007mg} is employed with the difference
that for $g\to b\bar{b}$ splitting the invariant mass instead of the
transverse momentum are being used as scale.  One-loop matrix elements
are instead obtained from \OpenLoops~\cite{Cascioli:2011va,
Cascioli:2014wya}.

Our central scales, both of perturbative origin ($\mu_R$, $\mu_F$) and relevant 
to the shower ($\mu_Q$), are computed according to the so-called reverse clustering 
algorithm; for further details we refer the reader to \Bref{Hoeche:2012yf}. The residual uncertainties 
are computed through variations by a factor of two from the central scales, where we combine the 
seven-point variation of the renormalization and factorization scales linearly with the uncertainties 
related to $\mu_Q$. We adopt these scale settings and variations for both our 4FS and 5FS setups, 
introduced above. 

%
%\bibliography{bbH}
%@ARTICLE{Catani:1996vz,
%  author = {Catani, S. and Seymour, M. H.},
%  title = "{A general algorithm for calculating jet cross sections in NLO QCD}",
%  journal = {Nucl. Phys.},
%  year = {1997},
%  volume = {B485},
%  pages = {291-419},
%  archiveprefix = {arXiv},
%  eprint = {hep-ph/9605323},
%  file = {:Catani1996vz.ps.gz:PostScript;:Catani1996vz.pdf:PDF},
%  url = {http://www.slac.stanford.edu/spires/find/hep/www?eprint=hep-ph/9605323}
%}
%@ARTICLE{Catani:2001cc,
%  author = {Stefano Catani and Frank Krauss and Ralf Kuhn and Brian R. Webber},
%  title = "{QCD matrix elements + parton showers}",
%  journal = {JHEP},
%  year = {2001},
%  volume = {11},
%  pages = {063},
%  eprint = {hep-ph/0109231},
%  file = {:Catani2001cc.pdf:PDF},
%  url = {http://www.slac.stanford.edu/spires/find/hep/www?eprint=hep-ph/0109231}
%}
%@ARTICLE{Catani:2002hc,
%  author = {Catani, Stefano and Dittmaier, Stefan and Seymour, Michael H. and
%	Trocsanyi, Zoltan},
%  title = "{The dipole formalism for next--to--leading order QCD calculations
%	with massive partons}",
%  journal = {Nucl. Phys.},
%  year = {2002},
%  volume = {B627},
%  pages = {189-265},
%  eprint = {hep-ph/0201036},
%  file = {:Catani2002hc.pdf:PDF},
%  url = {http://www.slac.stanford.edu/spires/find/hep/www?eprint=hep-ph/0201036}
%}
%@ARTICLE{Frixione:2002ik,
%  author = {Frixione, Stefano and Webber, Bryan R.},
%  title = "{Matching NLO QCD computations and parton shower simulations}",
%  journal = {JHEP},
%  year = {2002},
%  volume = {06},
%  pages = {029},
%  eprint = {hep-ph/0204244},
%  file = {:Frixione2002ik.pdf:PDF},
%  url = {http://www.slac.stanford.edu/spires/find/hep/www?eprint=hep-ph/0204244}
%}
%@ARTICLE{Gleisberg:2007md,
%  author = {Gleisberg, Tanju and Krauss, Frank},
%  title = "{Automating dipole subtraction for QCD NLO calculations}",
%  journal = {Eur. Phys. J.},
%  year = {2008},
%  volume = {C53},
%  pages = {501-523},
%  archiveprefix = {arXiv},
%  eprint = {0709.2881},
%  file = {:Gleisberg2007md.pdf:PDF},
%  primaryclass = {hep-ph},
%  url = {http://www.slac.stanford.edu/spires/find/hep/www?eprint=arXiv:0709.2881}
%}
%@ARTICLE{Gleisberg:2008fv,
%  author = {Gleisberg, Tanju and H{\"o}che, Stefan},
%  title = "{Comix, a new matrix element generator}",
%  journal = {JHEP},
%  year = {2008},
%  volume = {12},
%  pages = {039},
%  archiveprefix = {arXiv},
%  doi = {10.1088/1126-6708/2008/12/039},
%  eprint = {0808.3674},
%  file = {:Gleisberg2008fv.pdf:PDF},
%  primaryclass = {hep-ph},
%  slaccitation = {%%CITATION = 0808.3674;%%},
%    url = {http://www.slac.stanford.edu/spires/find/hep/www?rawcmd=f+eprint+0808.3674}
%}
%@ARTICLE{Gleisberg:2008ta,
%  author = {Gleisberg, T. and H{\"o}che, S. and Krauss, F. and Sch\"{o}nherr,
%	M. and Schumann, S. and Siegert, F and Winter, J.},
%  title = "{Event generation with \Sherpa 1.1}",
%  journal = {JHEP},
%  year = {2009},
%  volume = {02},
%  pages = {007},
%  archiveprefix = {arXiv},
%  doi = {10.1088/1126-6708/2009/02/007},
%  eprint = {0811.4622},
%  file = {:Gleisberg2008ta.pdf:PDF},
%  primaryclass = {hep-ph},
%  slaccitation = {%%CITATION = 0811.4622;%%}
%}
%@article{Hoeche:2012yf,
%      author         = "Hoeche, Stefan and Krauss, Frank and Schonherr, Marek and
%                        Siegert, Frank",
%      title          = "{QCD matrix elements + parton showers: The NLO case}",
%      journal        = "JHEP",
%      volume         = "1304",
%      pages          = "027",
%      doi            = "10.1007/JHEP04(2013)027",
%      year           = "2013",
%      eprint         = "1207.5030",
%      archivePrefix  = "arXiv",
%      primaryClass   = "hep-ph",
%      reportNumber   = "SLAC-PUB-15191, IPPP-12-52, DCPT-12-104, LPN12-081,
%                        FR-PHENO-2012-017, MCNET-12-09",
%      SLACcitation   = "%%CITATION = ARXIV:1207.5030;%%",
%}
%@ARTICLE{Krauss:2001iv,
%  author = {Frank Krauss and Ralf Kuhn and Gerhard Soff},
%  title = "{AMEGIC++ 1.0: A Matrix Element Generator In C++}",
%  journal = {JHEP},
%  year = {2002},
%  volume = {02},
%  pages = {044},
%  eprint = {hep-ph/0109036},
%  file = {:Krauss2001iv.pdf:PDF},
%  url = {http://www.slac.stanford.edu/spires/find/hep/www?eprint=hep-ph/0109036}
%}
%@ARTICLE{Schumann:2007mg,
%  author = {Schumann, Steffen and Krauss, Frank},
%  title = "{A parton shower algorithm based on Catani-Seymour dipole factorization}",
%  journal = {JHEP},
%  year = {2008},
%  volume = {03},
%  pages = {038},
%  archiveprefix = {arXiv},
%  eprint = {0709.1027},
%  file = {:Schumann2007mg.pdf:PDF},
%  primaryclass = {hep-ph},
%  url = {http://www.slac.stanford.edu/spires/find/hep/www?eprint=arXiv:0709.1027}
%@article{Cascioli:2011va,
%      author         = "Cascioli, Fabio and Maierhofer, Philipp and Pozzorini,
%                        Stefano",
%      title          = "{Scattering Amplitudes with Open Loops}",
%      journal        = "Phys. Rev. Lett.",
%      volume         = "108",
%      year           = "2012",
%      pages          = "111601",
%      doi            = "10.1103/PhysRevLett.108.111601",
%      eprint         = "1111.5206",
%      archivePrefix  = "arXiv",
%      primaryClass   = "hep-ph",
%      reportNumber   = "ZU-TH-23-11, LPN11-66",
%      SLACcitation   = "%%CITATION = ARXIV:1111.5206;%%"
%}
%@article{Cascioli:2014wya,
%      author         = "Cascioli, F. and Höche, S. and Krauss, F. and
%                        Maierhöfer, P. and Pozzorini, S. and Siegert, F.",
%      title          = "{Automatic one-loop calculations with Sherpa+OpenLoops}",
%      booktitle      = "{Proceedings,  15th International Workshop on Advanced
%                        Computing and Analysis Techniques in Physics Research
%                        (ACAT 2013)}",
%      journal        = "J. Phys. Conf. Ser.",
%      volume         = "523",
%      year           = "2014",
%      pages          = "012058",
%      doi            = "10.1088/1742-6596/523/1/012058",
%      SLACcitation   = "%%CITATION = 00462,523,012058;%%"
%}
%}
