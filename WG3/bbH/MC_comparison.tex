We compare predictions of the different Monte Carlo generators for the simulation of a $\bbh$ signal 
introduced in the preceding section in four- and five-flavour schemes which are at least 
NLO accurate and matched to PS.
Higgs boson decays are not considered. In the \aNLO{} and \POWHEGBOX{} computations 
we employ \PYe~\cite{Sjostrand:2007gs} for the parton-shower matching.
Throughout this section we consider Higgs boson production in association with bottom 
quarks for a SM Higgs boson with mass $\mphi=125$\,GeV at the $13$\,TeV LHC. 
We use a top-quark pole mass of $m_t=172.5$\,GeV relevant to the $\yb\yt$
contribution, see Eq.~(\ref{sig4FS}). The internal bottom-quark mass is set to its pole value of
$m_b = 4.92$\,GeV, while the bottom-quark mass in the Yukawa is renormalized in the 
$\msbar{}$ scheme and set to $m_b(\mu_R)$. The central value $m_b(\mu_R\equiv \mu_0)$ 
is evaluated with $n_f=4$ ($n_f=5$) four-loop running from $m_b(m_b)=4.18$\,GeV in the 4FS (5FS); 
scale variations are done from that central value with two-loop accuracy (which is consistent with the NLO order 
of the computations). Finally, we use the {\tt PDF4LHC15} sets of parton 
distribution functions in its four and five flavour versions where applicable.

In Table\,\ref{tab:brates} and Table\,\ref{tab:jrates} we report predictions of the 
various tools (\aNLO{}, \POWHEGBOX{}, \Sherpa 4FS, \Sherpa 5FS) for total 
rates with requirements on the final-state $b$-jets and jets, respectively. In 
Table\,\ref{tab:brates} we also give the total inclusive cross section. The three 
4FS predictions for the inclusive rate are in rather good agreement at the 1-2\% 
level, which is roughly of the order of the numerical accuracy. Also the uncertainties 
are similarly large, bearing in mind that the central scale for the Sherpa prediction differs 
from the other two 4FS results. We recall that all uncertainties quoted for the predictions 
of each code are given by a $7$-point $\muR$-$\muF$ variation combined linearly with 
the uncertainties coming from the scale related to the respective shower matching procedure
of each code. The latter of course should not have any impact on the total inclusive cross 
section, although unexpectedly the bulk of the Sherpa 4FS uncertainties on this quantity 
originates from variations of the shower starting scale. This issue could not be resolved 
in the course of this comparison.
The 5FS prediction is significantly larger than the 4FS ones and quite far beyond the quoted uncertainties
owing to the positive effect due to the merging of higher multiplicities.
In a full 5FS NNLO computation the cross section is usually  
reduced by the additional two-loop contribution, leading to a far better agreement at the level of the total 
inclusive cross section. 
In any case, the focus throughout this section is on the kinematics and distributions
of the final-state particles, which is why we will rescale the 5FS result, once we 
consider distributions below.

For the total rates with requirements on the $b$ jets, we define a $b$ jets 
as any jet that contains a $B$ hadron, using the anti-$k_T$ algorithm \cite{Cacciari:2008gp} with $R=0.4$, a minimal 
transverse momentum of $25$\,GeV and a rapidity
of $|y|<2.5$. We consider the cross section with a $b$-jet veto ($0j_b$),
one or more $b$ jets ($\ge 1 j_b$), two or more $b$ jets ($\ge 2 j_b$), exactly one $b$ jet ($1 j_b$),
and exactly two $b$ jets ($2 j_b$). To allow a comparison with the 5FS prediction we also 
show the respective acceptances for each code, i.e., the ratio of the prediction within cuts with 
respect to the total rate. The general conclusions that can be drawn from the table are the following:

\begin{itemize}
\item The cross section without any $b$-tagged object in the final state ($b$-jet veto) has the smallest value with the \aNLO{} generator, being 
roughly 20\% below \powheg{} and 10\% below the 4FS result of Sherpa. Given the well-known fact that $\bbH$ production in the 4FS comes 
with rather large ($\sim$20\%) uncertainties even at NLO, due to the logarithmic structure, 
the mutual agreement among the codes is 
still well within scale uncertainties for the jet-vetoed rate.
\item As pointed out before the 5FS is significantly larger due to the different normalization.
Looking at the acceptances, however, we see that the 5FS result is just in between the 4FS predictions by \powheg{} and Sherpa. Due to the very 
similar total rates the conclusions among the 4FS results for the acceptances are, by construction, identical to the ones for the absolute cross sections.
Overall, a large fraction (of the order of $70$\%) of the events have no $b$-tagged objects in the final state.
\item Since the cross section with the requirement of one or more $b$-jets is fully determined by the total and the 
jet-vetoed rate, the general conclusions are identical to the ones for the latter. The uncertainties are, however, larger 
(except for \powheg{}), in particular for the 5FS prediction. Let us point out again that tagging at least one of the $b$ jets
reduces the total rate by about a factor of $3$--$4$, which is rather large. Requiring a second $b$ jet further reduces the
cross section by one order of magnitude. This is consistent with the findings of \Bref{Wiesemann:2014ioa}.
\item The various predictions become increasingly different for higher $b$-jet multiplicities starting at two $b$ jets. While 
\powheg{} and Sherpa 4FS are still quite close to each other, the 5FS results and the \aNLO{} prediction are rather different; 
the former predicting generally a smaller jet activity and the latter a larger one. Still, there is by and large agreement within 
the respective uncertainties, in particular because the 5FS uncertainties are quite sizeable.
\end{itemize}

Considering total rates with requirements on the jets in Table\,\ref{tab:jrates}, we define a jet 
with the anti-$k_T$ algorithm \cite{Cacciari:2008gp} with $R=0.4$ and a minimal transverse momentum of $25$\,GeV.
We consider the same types of rates as in the case of $b$ jets for jets without 
any flavour tagging, and also report the corresponding acceptances in the table.
The general conclusions are very similar as compared to the $b$-jet case and can by summarized as follows:

\begin{itemize}
\item The $0$-jet rates in the 4FS are quite different. In particular the \powheg{} prediction is rather large, which is 25\% 
larger than Sherpa and 45\% than \aNLO{}, having only barely overlapping uncertainties with the latter. As expected, 
the 5FS rate is significantly larger, but the acceptance is quite similar to the one predicted in by Sherpa in the 4FS.
\item The $1$-jet exclusive bin agrees very well among the three 4FS codes and also the acceptances are very close 
among all four predictions.
\item The biggest difference emerges again from higher jet multiplicities (two and more), which is largest in 
the case of \aNLO{} and smallest for \powheg{}. The uncertainties become quite sizeable for the high multiplicities though.
\end{itemize}

We turn now to kinematical distribution of the final-state particles both generated already at 
the hard-matrix element level and by the shower. The figures are all organized according to 
the same pattern: In the main frame the relevant predictions for the different codes are shown 
as cross section per bin (namely, the sum of the contents of the bins is equal to the total cross section, 
possibly within cuts), with 
\aNLO{}+\PYe{} (black, solid), \powheg{}+\PYe{} (red, dotted), \Sherpa{} 4FS (blue, dashed with dots) 
and \Sherpa{} 5FS (green, dash-dotted with open boxes). In the first inset we display the bin-by-bin ratio 
of all the histograms which appear in the main frame over the black solid curve, chosen as a reference.
Finally, in a second inset the bands that represent the fractional scale dependence are given by taking 
the bin-by-bin ratios of the maximum and the minimum of a given simulation over the same central prediction 
that has been used as reference for the ratios of the first inset.
We recall that the \Sherpa{} 5FS normalization is generally much larger than 4FS results and that therefore its curve 
is rescaled to have the identical normalization as the \aNLO{}+\PYe{} result, since we use it as reference also in the ratios.

In \refF{fig:pTH_bbH} we consider the 
transverse-momentum distribution of the Higgs boson $p_T(\phi)$ with different requirements on the 
$b$ jets: inclusive (upper left), no $b$-jets (upper right), one or more $b$ jets 
(lower left) and two or more $b$ jets (lower right). Considering the inclusive 
case first, it is clear that overall there is a reasonable agreement among the predictions of the 
different codes. This conclusion can be drawn from the nicely overlapping uncertainty bands
in the second inset. The size of the uncertainties are also very similar with \powheg{} having 
a slightly smaller scale dependence than the other results. Considering only the shape of the curves 
while ignoring the bands for the moment, as shown in the first inset, some differences emerge 
even though they are not too severe. In the low-$p_T(\phi)$ region the agreement of the two Sherpa 
predictions and \powheg{} is quite remarkable in terms of shape, with \aNLO{} being a bit 
harder than the other predictions. Around $p_T(\phi)\sim 100$\,GeV the 5FS \Sherpa{} prediction 
departs from the other two predictions and gets closer to the \aNLO{} curve, while \powheg{} 
and the 4FS \Sherpa{} result staying rather close to each other over the whole $p_T(\phi)$ range 
that is displayed. One should note, however, that apart from $p_T(\phi)\lesssim 50$\,GeV 
all three 4FS results are very similar in terms of shape.

The $p_T(\phi)$ distribution with a veto on $b$ jets in the upper right panel of \refF{fig:pTH_bbH}, 
shows a quite similar pattern as in the inclusive case, with the relative size of the 
differences among the curves being amplified though. Except for the first bin the \powheg{} and \Sherpa{} 
4FS results are again in very good agreement, while the \aNLO{} prediction is quite much harder 
and up to $\sim 40\%$ away for $p_T(\phi)\lesssim 100$\,GeV. In that region also the 
\Sherpa{} 5FS prediction is very close to the \powheg{} and \Sherpa{} 4FS results, but gets 
significantly harder than all the 4FS predictions in the tail of the distribution. Due to the 
considerably increased size of the uncertainty bands, however, the predictions agree by and large 
within their respective uncertainties.

The picture essentially reverses when considering one or more $b$-tagged objects in the final state: 
the \aNLO{} result now featuring the softest spectrum and \Sherpa{} 5FS the hardest. In this case,
from $p_T(\phi)\gtrsim 30$\,GeV all four predictions (in particular the 4FS ones) are in excellent agreement 
in terms of both normalization and shape, with entirely overlapping uncertainty bands.

Finally, in the case with two or more observed $b$ jets, the biggest difference comes from the fact, that 
the overall normalization, i.e. the rate, is larger
for \aNLO{} as already pointed out in the discussion of Table\,\ref{tab:brates}.
Still, the predictions agree largely within the given uncertainty bands. Considering only the shape, 
the \aNLO{} result is in very good agreement with the \Sherpa{} 5FS curve and also quite similar to 
the one of \powheg{}. The \Sherpa{} 4FS shape in this case is a bit harder than all the other curves.

In conclusion, we find a decent agreement among the various predictions for the Higgs boson transverse-momentum 
spectrum with and without cuts, once the respective uncertainties are taken into consideration.

Let us move now to distributions relevant to the associated jets and $b$ jets 
shown in \refF{fig:b1j1_bbH}. We start by discussing the transverse-momentum 
$p_T(b_1)$  (left panel) and rapidity distributions $y(b_1)$ (right panel) 
of the hardest $b$ jet in the upper panel of \refF{fig:b1j1_bbH}. We first notice 
that the mutual agreement of the central predictions for $p_T(b_1)$ among 
all the codes is very good; none of the curves differs by more 
than $\sim 20\%$ from the others. 
Hence, we observe well overlapping uncertainty bands, which, however, turn 
out to be rather large in the case of \aNLO{} and the \Sherpa{} 5FS prediction.
The agreement is even better in terms of shape. In particular the shapes 
of \powheg{}, \Sherpa{} 5FS and \aNLO{} are essentially identical up to statistical 
fluctuations. For the rapidity $y(b_1)$ the situation is pretty much the same. 
In this case, however, all predictions are in perfect agreement apart from the 
slightly different normalization of \powheg{} and \Sherpa{} 5FS being roughly 
25\% lower than \aNLO{} as already pointed out in the discussion of 
Table\,\ref{tab:brates}. Furthermore, the \aNLO{} uncertainty is much lower 
in case of the $y(b_1)$ distribution and quite similar to the \Sherpa{} 4FS band,
while \powheg{} still has the smallest uncertainty band.

Finally, we can draw similar conclusion for the hardest jet distributions 
in the lower panel of \refF{fig:b1j1_bbH}: The residual uncertainties are rather 
large in case of the $p_T(j_1)$ distribution 
and all predictions agree well within uncertainties. The central 
predictions are quite quite similar in terms of their shape, where again the 
\Sherpa{} 5FS and the \aNLO{} result are essentially identical up to statistical 
fluctuations. For the rapidity of the hardest jet the shapes are quite similar to 
each other except for the one of \aNLO{} which is slightly enhanced in the 
forward region. Nevertheless, we find agreement within the quoted uncertainties 
for the predictions of all codes.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{landscape}

\begin{table}%[h]%[ph]
\caption{\label{tab:brates}
Predictions for the total rates (in pb) of the various Monte-Carlo tools under consideration inclusive and within cuts on the final-state $b$ jets. For comparison, also the 
respective acceptances are given.
}
\begin{center}
%%\begin{small}
\scriptsize
\begin{tabular}{cl|ccccccc}
\toprule
\multicolumn{2}{c|}{\quad} & inclusive & $0j_b$ & $\ge 1j_b$& $\ge 2j_b$ & $1j_b$& $2j_b$\\
\midrule
\multirow{4}{*}{$\sigma$[pb]} & 
{\sc MG5\_aMC}&
$0.369^{+ 19.7 \% }_{-18.8\%}$ &
$0.243^{+ 22.5\%}_{-23.0\%}$ &
$0.126^{+ 32.5\%}_{-28.3\%}$ & 
$0.0160^{+ 47.2\%}_{-39.8\%}$ &
$0.110^{+ 30.4\%}_{-26.7\%}$ & 
$0.0154^{+ 46.0\%}_{-38.9\%}$ \\
&{\sc POWHEG} &
$0.375^{+ 20.3 \% }_{-17.9\%}$ &
$0.281^{+ 21.8\%}_{-18.6\%}$ & 
$0.0943^{+ 16.6\%}_{-16.5\%}$ &
$0.00761^{+ 15.0\%}_{14.8\%}$ & 
$0.0867^{+ 16.8\%}_{-16.7\%}$ &
$0.00754^{+ 14.0\%}_{-14.9\%}$ \\
&{\sc Sherpa 4FS} &
$0.370^{+ 15.4 \% }_{-26.8\%}$ &
$0.264^{+ 11.8\%}_{-26.0\%}$ & 
$0.105^{+ 26.9\%}_{-28.8\%}$ &
$0.00955^{+ 74.9\%}_{-45.4\%}$ & 
$0.0952^{+ 22.2\%}_{-28.6\%}$ &
$0.00934^{+ 70.9\%}_{-44.1\%}$ \\
&{\sc Sherpa 5FS} &
$0.586^{+ 30.4 \% }_{-22.7\%}$ &
$0.423^{+ 20.6\%}_{-15.7\%}$ & 
$0.162^{+ 56.1\%}_{-40.7\%}$ &
$0.00773^{+ 68.9\%}_{-59.7\%}$ & 
$0.155^{+ 55.5\%}_{-40.4\%}$ &
$0.00746^{+ 68.7\%}_{-59.0\%}$ \\
\midrule
\multirow{4}{*}{acceptance} & 
{\sc MG5\_aMC}&
$1$ &
$0.659$ &
$0.342$ & 
$0.0432$ & 
$0.298$ &
$0.0417$ \\
&{\sc POWHEG} &
$1$ &
$0.749$ & 
$0.251$ &
$0.0203$ & 
$0.231$ &
$0.0201$ \\
&{\sc Sherpa 4FS} &
$1$ &
$0.717$ & 
$0.283$ &
$0.0258$ & 
$0.258$ &
$0.0253$ \\
&{\sc Sherpa 5FS} &
$1$ &
$0.723$ & 
$0.277$ &
$0.0132$ & 
$0.264$ &
$0.0127$ \\
\bottomrule
\end{tabular}
%%\end{small}
\end{center}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}%[h]%[ph]
\caption{\label{tab:jrates}
Predictions for the total rates (in pb) of the various Monte-Carlo tools under consideration within cuts on the final-state jets. For comparison, also the 
respective acceptances are given.
}
\begin{center}
%%\begin{small}
\scriptsize
\begin{tabular}{cl|cccccc}
\toprule
\multicolumn{2}{c|}{\quad} & $0j$ & $\ge 1j$& $\ge 2j$ & $1j$& $2j$\\
\midrule
\multirow{4}{*}{$\sigma$[pb]} & 
{\sc MG5\_aMC}&
$0.163^{+ 27.5\%}_{-25.8\%}$ &
$0.206^{+ 31.0\%}_{-30.5\%}$ &
$0.102^{+ 44.4\%}_{-41.3\%}$ &
$0.104^{+ 18.5\%}_{-19.9\%}$ &
$0.0613^{+ 37.9\%}_{-36.5\%}$ \\
&{\sc POWHEG} &
$0.239^{+ 21.6\%}_{-19.3\%}$ &
$0.136^{+ 23.0\%}_{-20.2\%}$ &
$0.0347^{+ 41.7\%}_{-26.7\%}$ &
$0.101^{+ 17.3\%}_{-18.2\%}$ &
$0.0299^{+ 38.7\%}_{-26.2\%}$ \\
&{\sc Sherpa 4FS} &
$0.192^{+ 11.7\%}_{-15.5\%}$ &
$0.177^{+ 32.6\%}_{-48.6\%}$ &
$0.0637^{+ 73.2\%}_{-77.2\%}$ &
$0.113^{+ 9.9\%}_{-38.3\%}$ &
$0.0428^{+ 41.6\%}_{-68.4\%}$ \\
&{\sc Sherpa 5FS} &
$0.328^{+ 34.6\%}_{-28.6\%}$ &
$0.258^{+ 94.2\%}_{-69.4\%}$ &
$0.0851^{+93.2\%}_{-68.6\%}$ &
$0.173^{+ 94.7\%}_{-69.7\%}$ &
$0.068^{+ 90.0\%}_{-65.3\%}$ \\
\midrule
\multirow{4}{*}{acceptance} & 
{\sc MG5\_aMC}&
$0.442$ &
$0.558$ &
$0.276$ &
$0.283$ &
$0.166$  \\
&{\sc POWHEG} &
$0.637$ &
$0.363$ &
$0.0927$ &
$0.270$ &
$0.0798$  \\
&{\sc Sherpa 4FS} &
$0.521$ &
$0.479$ &
$0.172$ &
$0.307$ &
$0.116$  \\
&{\sc Sherpa 5FS} &
$0.559$ &
$0.440$ &
$0.145$ &
$0.295$ &
$0.116$  \\
\bottomrule
\end{tabular}
%%\end{small}
\end{center}
\end{table}

%\end{landscape}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{figure}
\centering
\hspace{-0.2cm}\includegraphics[width=0.46\textwidth]{WG3/bbH/MC_plots/pTH.pdf}\hspace{1cm}
\includegraphics[width=0.46\textwidth]{WG3/bbH/MC_plots/pTH_0b.pdf}\\
\hspace{-0.2cm}\includegraphics[width=0.46\textwidth]{WG3/bbH/MC_plots/pTH_1b.pdf}\hspace{1cm}
\includegraphics[width=0.46\textwidth]{WG3/bbH/MC_plots/pTH_2b.pdf}
\caption{Transverse-momentum distribution of the Higgs boson as predicted by the various codes with requirements on the 
final-state $b$ jets; upper left panel: inclusive; 
upper right panel: with a veto on $b$-tagged jets; lower left panel: with one or more $b$ jets; lower right panel: with at least two observed $b$ jets; see text for details.
\label{fig:pTH_bbH}
}
\end{figure}

\begin{figure}
\centering
\hspace{-0.2cm}\includegraphics[width=0.46\textwidth]{WG3/bbH/MC_plots/pTb1.pdf}\hspace{1cm}
\includegraphics[width=0.46\textwidth]{WG3/bbH/MC_plots/yb1.pdf}
%\caption{ $\pt{}(b_1), y(b_1)$
%\label{fig:b1_bbH}
%}


%\begin{figure}
%\centering
%\hspace{-0.2cm}\includegraphics[width=0.46\textwidth]{WG3/bbH/MC_plots/pTb2.pdf}\hspace{1cm}
%\includegraphics[width=0.46\textwidth]{WG3/bbH/MC_plots/yb2.pdf}
%\caption{ $\pt{}(b_2), y(b_2)$
%\label{fig:b2_bbH}
%}
%\end{figure}


\centering
\hspace{-0.2cm}\includegraphics[width=0.46\textwidth]{WG3/bbH/MC_plots/pTj1.pdf}\hspace{1cm}
\includegraphics[width=0.46\textwidth]{WG3/bbH/MC_plots/yj1.pdf}
\caption{Transverse-momentum (left) and rapidity distributions (right) of the hardest $b$ jet (upper) and 
hardest jet without requirements on its flavour (lower); see text for details.
\label{fig:b1j1_bbH}
}
\end{figure}



%\begin{figure}
%\centering
%\hspace{-0.2cm}\includegraphics[width=0.46\textwidth]{WG3/bbH/MC_plots/pTj2.pdf}\hspace{1cm}
%\includegraphics[width=0.46\textwidth]{WG3/bbH/MC_plots/yj2.pdf}
%\caption{ $\pt{}(j_2), y(j_2)$
%\label{fig:j2_bbH}
%}
%\end{figure}

Due to the limited statistics of the samples under consideration, we refrain from showing 
results relevant to the second hardest jets or $b$ jets. The general conclusions  in these cases 
should be rather similar though to what has been observed so far.


