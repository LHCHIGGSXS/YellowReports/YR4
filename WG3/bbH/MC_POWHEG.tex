%\subsection{$b\bar b H$ in the \POWHEGBOX{}}
\label{sec:powheg-box}
%
In Ref.~\cite{Jager:2015hka} an implementation of the NLO-QCD calculation of Ref.~\cite{Dawson:2003kb} in the framework of the \POWHEGBOX{}~\cite{Nason:2004rx,Frixione:2007vw,Alioli:2010xd} has been presented. 
While the virtual corrections to the $pp \to \bbH$ process have been extracted from the fixed-order calculation of Ref.~\cite{Dawson:2003kb}, the tree-level amplitudes were generated with a tool based on {\tt MadGrap~4}~\cite{Stelzer:1994ta,Alwall:2007st}. The process-independent ingredients of the implementation are provided internally by the \POWHEGBOX{}.  All building blocks have been implemented in the 4FS, i.e.\ no contributions from incoming bottom quarks have been taken into account, and the bottom-quark was always assumed to be massive.

In the virtual corrections, not only diagrams including a $\bbh$ coupling emerge, but also loop diagrams with a $t\bar t \phi$ coupling. Both contributions are fully taken into account in the implementation. However, a switch is provided that allows the user to deactivate the contributions involving a $t\bar t \phi$ coupling. 

By default, the renormalization of the bottom-quark Yukawa coupling is defined in the $\msbar$
renormalization scheme~\cite{Dittmaier:2003ej,Dawson:2003kb,Dawson:2005vi}. In addition, an option for defining the bottom-quark Yukawa coupling in the on-shell renormalization scheme is provided. 

In its default version, the \POWHEGBOX{} code for $pp\to \bbH$ provides three different options for the renormalization and factorization scales: a fixed scale, $\mu_0 = (m_H+2 m_b)/2$, as used for total rates, a dynamical scale defined via the transverse masses $m_T(f)=(m_f^2+p_{T,f}^2)^{1/2}$ of the born-level final state particles $f$ in an event, $\mu_0 = m_T(H)+m_T(b)+m_T(\bar b)$, or the geometrical mean of the transverse masses, $\mu_0 = \left(m_T(H)\, m_T(b)\, m_T(\bar b)\right)^{1/3}$; the latter two being particularly relevant as far as tails of kinematical distributions are concerned. Scaling factors, $\xi_R$ and  $\xi_F$, can be set individually for the factorization and renormalization scales, $\mu_R$ and $\mu_F$, such that the relevant scales are given by $\mu_R = \xi_R \mu_0$ and  $\mu_F = \xi_F \mu_0$. Throughout this section we use the second scale setting in the list above with 
$\xi_R=\xi_F=1/4$, in keeping with the settings quoted for \aNLO{} in the previous section.

In order to assess the intrinsic uncertainties associated with the matching of the NLO-QCD calculation with parton shower programs, the \POWHEGBOX{} offers the possibility to vary the so-called {\tt hdamp} parameter, defined as \cite{Alioli:2010xd}
%
\begin{equation}
D_h= \frac{h^2}{h^2+p_T^2}\,. 
\end{equation}
%
Here, $p_T$ denotes the transverse momentum of the hardest parton in the real emission contributions, and $h$ is a parameter that can be chosen by the user. If no explicit choice is made, the  {\tt hdamp} parameter is set to one. In general, this parameter determines the separation of the cross section in a part at low transverse momentum of the extra
emission, generated mainly with the Sudakov form factor, and a part at
high transverse momentum, generated mainly with the real-emission
diagrams only. The uncertainty of observables simulated at NLO+PS level associated with a variation of the $h$~parameter provides an estimate of the intrinsic matching uncertainty of the  \POWHEGBOX{}. We choose $h=1/4\,(m_H+2\,m_b)$, consistent with the shower scale setting proposed in \Bref{Wiesemann:2014ioa} and used in \aNLO{} throughout.

Residual uncertainties are estimated by performing a seven-point variation of the renormalization and factorization scales by 
a factor of two with respect to their central values. We then combine these uncertainties linearly with the variation of $D_h$ in the same range.

%%%%%%%%%%
%
% references:
%
%%\cite{Jager:2015hka}
%\bibitem{Jager:2015hka}
%  B.~Jager, L.~Reina and D.~Wackeroth,
%  %``Higgs boson production in association with b jets in the POWHEG BOX,''
%  arXiv:1509.05843 [hep-ph].
%  %%CITATION = ARXIV:1509.05843;%%
%
%%\cite{Dawson:2003kb}
%\bibitem{Dawson:2003kb}
%  S.~Dawson, C.~B.~Jackson, L.~Reina and D.~Wackeroth,
%  %``Exclusive Higgs boson production with bottom quarks at hadron colliders,''
%  Phys.\ Rev.\ D {\bf 69} (2004) 074027
%  doi:10.1103/PhysRevD.69.074027
%  [hep-ph/0311067].
%  %%CITATION = doi:10.1103/PhysRevD.69.074027;%%
%  %165 citations counted in INSPIRE as of 23 Dec 2015
%
%%\cite{Frixione:2007vw}
%\bibitem{Frixione:2007vw}
%  S.~Frixione, P.~Nason and C.~Oleari,
%  %``Matching NLO QCD computations with Parton Shower simulations: the POWHEG method,''
%  JHEP {\bf 0711} (2007) 070
%  doi:10.1088/1126-6708/2007/11/070
%  [arXiv:0709.2092 [hep-ph]].
%  %%CITATION = doi:10.1088/1126-6708/2007/11/070;%%
%  %1359 citations counted in INSPIRE as of 23 Dec 2015
%
%%\cite{Nason:2004rx}
%\bibitem{Nason:2004rx}
%  P.~Nason,
%  %``A New method for combining NLO QCD with shower Monte Carlo algorithms,''
%  JHEP {\bf 0411} (2004) 040
%  doi:10.1088/1126-6708/2004/11/040
%  [hep-ph/0409146].
%  %%CITATION = doi:10.1088/1126-6708/2004/11/040;%%
%  %1040 citations counted in INSPIRE as of 23 Dec 2015
%
%%\cite{Alioli:2010xd}
%\bibitem{Alioli:2010xd}
%  S.~Alioli, P.~Nason, C.~Oleari and E.~Re,
%  %``A general framework for implementing NLO calculations in shower Monte Carlo programs: the POWHEG BOX,''
%  JHEP {\bf 1006} (2010) 043
%  doi:10.1007/JHEP06(2010)043
%  [arXiv:1002.2581 [hep-ph]].
%  %%CITATION = doi:10.1007/JHEP06(2010)043;%%
%  %914 citations counted in INSPIRE as of 23 Dec 2015
%
%
%%\cite{Dawson:2005vi}
%\bibitem{Dawson:2005vi}
%  S.~Dawson, C.~B.~Jackson, L.~Reina and D.~Wackeroth,
%  %``Higgs boson production in association with bottom quarks at hadron colliders,''
%  Mod.\ Phys.\ Lett.\ A {\bf 21} (2006) 89
%  doi:10.1142/S0217732306019256
%  [hep-ph/0508293].
%  %%CITATION = doi:10.1142/S0217732306019256;%%
%  %77 citations counted in INSPIRE as of 23 Dec 2015
%
%
%%\cite{Stelzer:1994ta}
%\bibitem{Stelzer:1994ta}
%  T.~Stelzer and W.~F.~Long,
%  %``Automatic generation of tree level helicity amplitudes,''
%  Comput.\ Phys.\ Commun.\  {\bf 81} (1994) 357
%  doi:10.1016/0010-4655(94)90084-1
%  [hep-ph/9401258].
%  %%CITATION = doi:10.1016/0010-4655(94)90084-1;%%
%  %856 citations counted in INSPIRE as of 23 Dec 2015
%
%%\cite{Alwall:2014hca}
%\bibitem{Alwall:2014hca}
%  J.~Alwall {\it et al.},
%  %``The automated computation of tree-level and next-to-leading order differential cross sections, and their matching to parton shower simulations,''
%  JHEP {\bf 1407} (2014) 079
%  doi:10.1007/JHEP07(2014)079
%  [arXiv:1405.0301 [hep-ph]].
%  %%CITATION = doi:10.1007/JHEP07(2014)079;%%
%  %658 citations counted in INSPIRE as of 23 Dec 2015
