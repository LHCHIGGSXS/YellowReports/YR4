%%CG: list of new commands moved to /NewCommands/YR4_NewCommands.tex
%\newcommand\sss{\scriptscriptstyle}
%\newcommand\bbH{b\bar{b}\phi}
%\newcommand\ttH{t\bar{t}\phi}
%\newcommand{\bbh}{\bbH}
%%\newcommand{\tth}{\ttH}
%\providecommand{\tth}{\ttH}
%\newcommand\mb{m_b}
%\newcommand\mH{m_H}
%\newcommand\yt{y_t}
%\newcommand\yb{y_b}
%\newcommand{\kt}{k_{\sss T}}
%\newcommand{\Ht}{H_{\sss T}}
%\newcommand{\ptsyst}{p_{\sss T}^{\rm syst}}
%\newcommand{\LQCD}{\Lambda_{\sss\rm QCD}}
%\newcommand\bb{\bar{b}}
%\def\beq{\begin{equation}}
%\def\eeq{\end{equation}}
%\newcommand\as{\alpha_{\sss S}}
%\newcommand\aNLO{{\sc\small MadGraph5\_aMC@NLO}}
%\renewcommand\MadFKS{{\sc\small MadFKS}}
%\newcommand\CutTools{{\sc\small CutTools}}
%\newcommand\MadLoop{{\sc\small MadLoop}}
%\newcommand\madevent{{\sc\small MadEvent}}
%\newcommand\prompt{{\tt MG5\_aMC>}}
%\newcommand\UFO{{\sc\small UFO}}
%\newcommand\HWpp{{\sc\small Herwig++}}
%\newcommand\PYe{{\sc\small Pythia8}}
%\newcommand{\Qres}{Q_{\text{res}}}
%\newcommand{\Qshow}{Q_{\text{sh}}}
%\newcommand{\Qshowmax}{\Qshow}
%\newcommand{\llog}{{\abbrev LL}}
%\newcommand{\nll}{{\abbrev NLL}}
%\newcommand{\nnll}{{\abbrev NNLL}}
%\newcommand{\os}{{o.s.}}
%\newcommand{\mtop}{m_t}
%\newcommand{\mbottom}{m_b}
%\def\abs#1{\left|#1\right|}

Higgs boson production modes that feature a $\bbH$ vertex at 
tree level are a viable alternative to determine the Higgs-bottom Yukawa 
coupling ($\yb$), since the $H\to b\bb$ decay is problematic from an experimental 
viewpoint: it suffers from the huge $b$-quark QCD background; 
the absolute value of the total width is extremely small;
and the $H\to b\bb$ branching ratio is large,
which renders the determination of the relative partial decay widths at a 
certain accuracy difficult. Besides loop-induced Higgs boson production through 
gluon fusion, that receives a contribution from both top- and bottom-quark 
loops, the direct production of a Higgs boson in association with bottom quarks 
(i.e., tree-level processes that contain a $b$-quark radiating a Higgs boson) 
gives access to the $\bbH$ coupling. 

The associated production of a Higgs boson with bottom quarks ($\bbH$ production) 
is suppressed in the \sm{} by almost two orders of magnitude with respect 
to the gluon-fusion process. 
Furthermore, this inclusive rate strongly decreases when requiring 
realistic $b$-tagging (i.e., minimal transverse momentum and 
centrality requirements on the $b$ jets) in order to render it 
distinguishable from other production mechanisms. However, in theories with 
extended Higgs sector, such as a generic \thdm{} or  
the MSSM, the Higgs boson coupling to bottom quarks can be significantly 
enhanced and the $\bbH$ process can, in fact, become the dominant production mode.
Since experimentally a scalar sector richer than the one of the SM has 
not been ruled out so far, this constitutes a strong motivation for 
precision computations of the total 
rate (see \refS{sec:bbHtotal}), a proper modelling of the $\bbH$ signal 
in Monte Carlo (MC) generators (see \refS{sec:bbHMC}) and the study
of uncertainties related to the experimental acceptance 
(see \refS{sec:bbHacceptance}). We will further report total inclusive cross sections for the 
$c\bar{c}\phi$ production mode (see \refS{sec:ccH}) which may become relevant in specific models with enhanced charm 
Yukawa coupling.
Although all presented predictions are in the SM, they 
are directly applicable to all neutral Higgs bosons ($\phi=h,H,A$) 
in a \thdm{}, by a proper rescaling of the bottom Yukawa; for the MSSM 
this has been shown~\cite{Dittmaier:2006cz,Dawson:2011pe,Dittmaier:2014sva} to be a good 
approximation of the full result.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\begin{center}
    \includegraphics[height=2cm]{./WG3/bbH/diag/gg-bbH.pdf}\hspace*{2cm}
    \includegraphics[height=2cm]{./WG3/bbH/diag/qq-bbH.pdf}\hspace*{2cm}
    \includegraphics[height=2cm]{././WG3/bbH/diag/bb-H.pdf}
	\vspace{0.2cm}
  \caption{Sample of LO Feynman diagrams for $\bbH$ production in
the four-flavour scheme (left, centre) and the five-flavour scheme (right).}
  \label{fig:LO}
\end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

As for all processes that feature $b$ quarks at the level of the hard-scattering
process, there are two viable approaches to compute the $\bbH$ cross section. 
In the four-flavour scheme (4FS), bottom quarks are treated as massive 
particles, hence no bottom quarks can appear in the initial state of the 
partonic scattering process. This is relevant for those cases where
the physical mass of the $b$ quark is considered as a hard scale and implies
that observables with tagged final-state $b$ quarks are well defined (and thus can
be computed) at fixed $\as$ order in perturbation theory. The leading-order 
(LO) partonic processes in the 4FS are (see left and centre diagrams in \refF{fig:LO})
\beq
gg\to b\bb H\,,\;\;\;\;\;\;\;\;
q\bar{q}\to b\bb H\,,
\label{LO4FS}
\eeq
where $q$ denotes a light quark.

At any order in perturbation theory the 4FS involves terms $\sim\as^k\log^k(\mb/Q)$,
where $Q$ is the characteristic scale of the $g\rightarrow b\bar{b}$ splitting. 
These logarithmically 
enhanced terms remain small as long as $Q\sim\mb$, but can spoil the perturbative 
convergence when $Q\gg\mb$. Such terms are generally dealt with by re-organizing 
the perturbative series while resumming them to all orders in $\as$. This is 
precisely achieved by working in second viable approaches to compute the 
$\bbH$ cross section, the five-flavour scheme (5FS), which is particularly 
important when the characteristic of an observable is that of being 
dominated by such logarithms. In this scheme, one assumes massless $b$ quarks 
($\mb\equiv 0$) at the level of the short-distance cross section, which are therefore 
treated at equal footing as the other light quarks and may appear 
as initial state particles; the potentially large logarithms are 
effectively resummed through the DGLAP evolution of the $b$-quark PDFs.
Hence, the LO cross section in the 5FS is simply given by (see right diagram of \refF{fig:LO})
\beq
b\bb\to H\,.
\label{LO5FS}
\eeq

It is clear from the discussion above that 4FS computations do not account for
logarithmic terms beyond the first few, while 5FS results lack 
power-suppressed terms $(\mb/Q)^n$. If either of these properties is 
important the other scheme must be preferred. Being highly observable 
dependent\footnote{This receives an additional complication from the fact that
an observable may be 
associated with different powers of $\as$ in the four- and five-flavour schemes.}, 
at least for inclusive quantities neither resummation nor mass effects 
are dominant and the two approaches lead to generally similar results.
One must bear in mind, however, that reasonable agreement is found only 
by judicious choices of hard scales, i.e., the resummation and factorization 
scales, which must be chosen significantly smaller than 
$\mH$~\cite{Maltoni:2003pn,Boos:2003yi,Harlander:2003ai,Maltoni:2012pa}---the 
hardness one would naively associate with $\bbH$ production.
\footnote{This is also supported by arguments based on collinear dominance
\cite{Maltoni:2012pa} and the small upper $p_T$ limit of the factorizing
part in the $p_T$ distribution \cite{Rainwater:2002hm}.}

For inclusive observables the 5FS process in Eq.\,\eqref{LO5FS} has the 
technical advantage of being much simpler ($2\to 1$ at the LO), which 
renders feasible radiative corrections beyond the NLO (and even beyond the 
NNLO with current technology), while the $2\to 3$ Born-level processes 
of Eq.\,\eqref{LO4FS} in the 4FS
limit perturbative computations in this scheme to NLO.

Considering more exclusive observables, in particular regarding the final-state 
$b$ quarks, which are relevant for a realistic description of the $\bbH$ signal, 
the 5FS loses its advantage mentioned above: The process in Eq.\,\eqref{LO5FS} 
has much more limited information on the final-state kinematics than 
the one in Eq.~\eqref{LO4FS}. Only higher orders in the 5FS recover such 
information, e.g., $1$-$b$ and $2$-$b$ tag observables can be described in the 
5FS only starting from NLO and NNLO, respectively, while the 4FS tree-level process 
in Eq.~\eqref{LO4FS} describes observables involving 
$0$-, $1$-, or $2$-$b$ tags formally already with leading perturbative accuracy.
Furthermore, $b$-tagged objects in the 5FS can not be consistently defined 
at any order in perturbation theory, because the corresponding cross section 
becomes infinite beyond the LO, when the massless $b$ quarks are not considered as
(and clustered into) jets or integrated over. The massive $b$ quarks in the 4FS, 
on the other hand, can be associated with physical objects, allowing for 
realistic $b$ tagging with arbitrary selection cuts on the $b$ quark kinematics\footnote{Note that 
fragmentation effects when turning the $b$ quarks into $B$ hadrons are 
assumed to be moderate and neglected in general at fixed order.}, whereas too small 
$\pt$ cuts directly lead to a divergence in the 5FS. The problems related to a 
consistent definition of $b$-tagged objects in the 5FS can be alleviated by matching 
the fixed-order computation to parton showers (PS). Due to the backward evolution of the initial-state 
$b$ quarks the shower will generate $b$-flavoured hadrons already at the LO, 
rendering realistic any $b$-tagging requirements. However, one must
not forget that the backward evolution in the Monte Carlos is not trivial and 
has only leading logarithmic (\llog{}) accuracy. Furthermore, the kinematic 
reshuffling of massless into massive $b$ quarks can have sizeable effects 
on the $B$ hadron kinematics. Both come at the price of an additional 
uncertainty. As far as the 4FS is concerned, the PS matching particularly 
improves the Sudakov-suppressed small-$\pt$ initial-state radiation, although the
impact of the PS is less crucial than in the 5FS. In both schemes, 
the PS introduces 
additional power-suppressed contributions due to long-distance phenomena.

Before considering phenomenological predictions, let us discuss 
the general coupling structure in the two schemes. Being a $2\to 3$ 
process the lowest order cross section in the 4FS starts at 
${\cal O}(\as^2)$. In the 5FS each bottom PDF can be considered 
as being of ${\cal O}(\as)$ with respect to the gluon and hence the 
LO features no power of $\as$. In both schemes the LO 
cross section is proportional to $\yb^2$, since the Higgs boson is always 
coupled to a $b$ quark. Considering higher order corrections, the 
coupling structure becomes more involved, in particular in the 4FS.
In this case virtual diagrams with a $\bbH$ final state 
may involve a top quark circulating in the loop, which couples 
to the Higgs boson (e.g., see \refF{fig:4FSyt}), and thus are 
proportional to the Higgs-top coupling ($\yt$). Such diagrams 
are generally attributed to the gluon-fusion Higgs boson production mode
(their square enters the NNLO gluon-fusion cross section), but their 
interference with diagrams proportional to $\yb$ must be carefully 
accounted for in the cross section of the $\bbH$ production mode. 
Such contributions will be 
generically referred to as $\yb\yt$ terms. Including such interference effects, 
but neglecting all contributions that already appear in the gluon-fusion process, 
we may thus express the $\bbH$ cross section in the two schemes as follows:
\begin{align}
\sigma_{\bbh}^{\text{4FS}}&=
\underbrace{\as^2\,\yb^2\,\Delta^{(0)}_{\yb^2}+
\as^3\,\Big(\yb^2\,\Delta^{(1)}_{\yb^2}}_{\equiv \sigma_{\yb^2}}+
\underbrace{\yb\,\yt\,\Delta^{(1)}_{\yb\yt}}_{\equiv \sigma_{\yb\yt}/\as^3}\Big)+
\as^4\left(\yb^2\,\Delta^{(2)}_{\yb^2} + \yb\,\yt\,\Delta^{(2)}_{\yb\yt}\right)+\mathcal{O}(\alpha_s^5)\,.
\label{sig4FS}\\
\sigma_{\bbh}^{\text{5FS}}&=
\yb^2\,\left(\Delta^{(0)}_{\yb^2}+
\as\,\Delta^{(1)}_{\yb^2}+
\as^2\,\Delta^{(2)}_{\yb^2} 
+\as^3 \Delta^{(3)}_{\yb^2} + \mathcal{O}(\alpha_s^4)\right).
\label{sig5FS}
\end{align}
The 4FS cross section at NLO (being the current state-of-the-art) can 
be decomposed in terms proportional to $\yb^2$ ($\sigma_{\yb^2}$) and 
$\yb\yt$ ($\sigma_{\yb\yt}$). Any component with a $\bbh$ final state, but 
proportional to $\yt^2$, must originate from a squared gluon-fusion amplitude 
(i.e., with a Higgs radiated from a closed top-quark loop) and can be 
incoherently added to the $\bbh$ cross section above.\footnote{Note that 
starting from the NNLO in both schemes such squared gluon-fusion diagrams 
contribute also to the $\yb^2$ and $\yb\yt$ components of the $\bbh{}$ 
cross section, which, as stated above and being common practice, are not 
attributed to the $\bbh$ cross section, but to the gluon-fusion one.}
In the 5FS, on the other hand, interference terms (proportional to $\yb\yt$) 
between the gluon-fusion and $\bbh$ processes exactly vanish order by 
order in perturbative QCD, since they involve a helicity flip of the
bottom quarks that leads to a vanishing interference term with the
generic 5FS amplitudes in the massless limit.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\begin{center}
%\hspace{-0.46cm}
    \includegraphics[height=2.15cm]{./WG3/bbH/diag/gg-bbH1loop2tb_4F.pdf}\hspace{1.2cm}
    \includegraphics[height=2.15cm]{./WG3/bbH/diag/gg-bbH1loop5tb.pdf}\hspace{1.2cm}
%    \includegraphics[width=0.24\textwidth]{./WG3/bbH/diag/gg-bbH1loop_4F.pdf}
%\\
%    \includegraphics[width=0.28\textwidth]{./WG3/bbH/diag/gg-bbH1loop4_4F.pdf}
    \includegraphics[height=2.15cm]{./WG3/bbH/diag/qq-bbH1loop2tb.pdf}
%    \includegraphics[width=0.28\textwidth]{./WG3/bbH/diag/qq-bbH1loop_4F.pdf}
  \caption{Sample of one-loop Feynman diagrams for $\bbH$ production in the
	four-flavour scheme featuring a $\yt$ coupling.}
  \label{fig:4FSyt}
\end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{figure}
%\begin{center}
%\hspace{-0.5cm}
%    \includegraphics[width=0.3\textwidth]{./WG3/bbH/diag/gg-bbgH3.pdf}
%    \includegraphics[width=0.3\textwidth]{./WG3/bbH/diag/gg-bbgH4.pdf}
%\\
%    \includegraphics[width=0.25\textwidth]{./WG3/bbH/diag/qg-qbbH3.pdf}
%    \includegraphics[width=0.3\textwidth]{./WG3/bbH/diag/qq-bbgH2.pdf}
%  \caption{Sample of real-emission Feynman diagrams for $\bbH$ production in
%the four-flavour scheme.}
%  \label{fig:4FSreal}
%\end{center}
%\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%
%
%
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{figure}
%\begin{center}
%    
%    \includegraphics[width=0.28\textwidth]{././WG3/bbH/diag/bb-H1loop.pdf}
%\\
%    \includegraphics[width=0.28\textwidth]{./WG3/bbH/diag/bb-gH.pdf}
%    \includegraphics[width=0.28\textwidth]{././WG3/bbH/diag/bg-bH.pdf}
%  \caption{A sample of Feynman diagrams for $\bbh{}$ production in
%   the five-flavour scheme: (a) \lo{}; (b) one-loop; (c-d) real emission.}
%  \label{fig:5FS}
%\end{center}
%\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%






