The first NLO simulation matched to parton showers of the $\bbH$ signal in the
4FS has been performed in \Bref{Wiesemann:2014ioa}. This computation has been treated as 
a special case in the automated framework of \aNLO{} due to the necessity of 
a $\msbar{}$ renormalization of the bottom-quark Yukawa coupling, which 
can not be handled by the public version so far. Therefore, dedicated 
process folders have been provided\footnote{See 
\url{https://cp3.irmp.ucl.ac.be/projects/madgraph/wiki/bbH}.}, which were also used for 
all NLO+PS simulations with \aNLO{} throughout this chapter.

\aNLO{} allows for the computation of LO and NLO cross sections both with and
without matching to parton showers. NLO results not matched to parton showers
are obtained by adopting the FKS method~\cite{Frixione:1995ms,Frixione:1997np}
automated in the module \MadFKS~\cite{Frederix:2009yq}, and the OPP
integral-reduction procedure~\cite{Ossola:2006us} for the computation 
of the one-loop matrix elements (automated in the module 
\MadLoop~\cite{Hirschi:2011pa}, which makes use of
\CutTools~\cite{Ossola:2007ax} and of an in-house implementation 
of the optimizations proposed in \Bref{Cascioli:2011va} (\OpenLoops)).
Matching with parton showers is achieved
by means of the MC@NLO formalism~\cite{Frixione:2002ik}.
\aNLO\ is maximally automated.


The default treatment of Yukawa couplings in the \aNLO{} code, however, 
is that of an on-shell scheme 
renormalization, which is not optimal in the case of $\bbH$ production, 
where the $\msbar{}$ scheme has to be preferred~\cite{Braaten:1980yq}.
The advantage of an $\msbar{}$ renormalized bottom Yukawa $\overline{y}_b(\muR)$ 
is the resummation of potentially large logarithms
of \mbox{$\mH/\mb$}, when \mbox{$\muR\sim\mH$} is chosen.
An additional complication emerges from the fact that
$\yb$ enters at different powers in the $\sigma_{\yb^2}$ and 
$\sigma_{\yb\yt}$ terms introduced in eq.~(\ref{sig4FS}). 
At the moment the implementation does not warrant a completely 
general and automated solution. However, since such complication 
is recurrent in the mixed-coupling expansion as for EW corrections, 
we have included a general $\overline{y}_b(\muR)$ implementation applicable 
not only to $\bbH$ production in this context, which will be made available in the future.
This will allow any user to generate the $\bbH$ process in the general \aNLO{} 
interface and replace the current necessity of using the dedicated 
process folders.

The inputs are coordinated to be the same as the ones for the \POWHEGBOX{} 
described in the upcoming section
to warrant a consistent comparison of the results, starting from a reasonable 
agreement in the normalization, i.e., the total rate. We set the
renormalization and factorization scales to the sum of the transverse masses
of all Born-level particles, divided by a factor of four, which in the soft/collinear limit is in 
keeping with the scales used for the total 4FS rate (see Sect.\,\ref{sec:santander}):
\beq
\muR=\muF=\mu_0=\frac{\Ht^{\rm Born}}{4}\equiv 
\frac{1}{4}\sum_{i\in \{b,\bar{b},\phi\}}\sqrt{m_i^2+\pt^2(i)}\,.
\label{scref}
\eeq
The respective scale uncertainties are estimated by the independent variation
$0.5\,\mu_0\le\muR,\muF\le 2\,\mu_0$ with the constraint $0.5\le\muR/\muF\le 2$.

The additional factor of $1/4$ in the scale settings of the 4FS reflects the
fact that the optimal values for the hard scales that enter the $\bbH$ calculation
appear to be significantly smaller than the hardness of the process
would suggest. As pointed out in \Bref{Wiesemann:2014ioa} another 
hard scale is affected by this choice, when considering simulations matched to parton showers, 
namely the shower scale $\Qshow$, which loosely 
speaking can be identified with the largest hardness accessible to the shower.
It is the MC that determines, event-by-event, the value of $\Qshow$, by choosing it
so as to maximize the kinematic population of the phase-space due to
shower radiation, without overstretching the approximations upon which 
the MC is based. 

In \aNLO\ one is given the possibility of
setting the upper value\footnote{If the MC-determined $\Qshow$ value 
is lower than that set by the user, the latter is ignored. Also
bear in mind that the physical meaning of $\Qshow$ depends on the
specific MC employed -- see \Bref{Alwall:2014hca}.}
of $\Qshow$; this value is actually picked up at random in a 
user-defined range:
\beq
\alpha f_1\sqrt{s_0}\le\Qshowmax\le\alpha f_2\sqrt{s_0}\,,
\label{murange}
\eeq
so as to avoid possible numerical inaccuracies due to the presence 
of sharp thresholds.\footnote{More details can be found in Sect.~2.4.4 of
\Bref{Alwall:2014hca} (see in particular Eq.~(2.113) and the
related discussion).} $s_0$ is the Born-level
partonic centre of mass energy squared, and $\alpha$, $f_1$, and $f_2$ are numerical
constants whose default inputs are $1$, $0.1$, and $1$, respectively. 
The way, in which $\Qshowmax$ is generated, results in a 
distribution peaked at values slightly larger than 
\mbox{$\alpha(f_1+f_2)\sqrt{\langle s_0\rangle}/2$}.
As argued in \Bref{Wiesemann:2014ioa} the typical shower 
scales in the default setup are rather large as compared to the 
factorization scale, even though their origin is quite similar being both 
based on the soft/collinear approximation. By setting
$\alpha=1/4$ the two scales become significantly closer.
\Bref{Wiesemann:2014ioa} further studied the distribution of the 
Born-level ``system'' ($\ptsyst$), which showed a strongly improved 
matching behaviour of the NLO+PS curve with the NLO curve in the 
high-$\ptsyst$ tail for smaller values of $\alpha$. 
In conclusion a reduced shower scale by setting 
$\alpha=1/4$ has to be preferred over the default choice in \aNLO\ and will
be the default choice fo all numerical results produced with \aNLO\
throughout this section.

Additionally, we assign a theoretical uncertainty to the shower scale choice 
by varying $\alpha\in[1/(4\sqrt{2}),\sqrt{2}/4]$, which is added linearly to 
the $\muR$-$\muF$ scale uncertainties.




