\cleardoublepage
\phantomsection
\addcontentsline{toc}{part}{Introduction}

\chapter*{Introduction\markboth{Introduction}{Introduction}} 
%\label{chapter:intro}
\ChapterAuthor{D.~de~Florian, C.~Grojean, F.~Maltoni, C.~Mariotti, A.~Nikitenko,\\ M.~Pieri, P.~Savard, M.~Schumacher, R.~Tanaka}

  The observation by the ATLAS and CMS collaborations in 2012 of a new particle with properties compatible with the Higgs boson predicted by the Standard Model~\cite{Aad:2012tfa,Chatrchyan:2012xdj} was a major breakthrough in particle physics and an unprecedented advance in the understanding of the dynamics at the origin of the breaking of the electroweak symmetry.   Following the end of data-taking in 2012, a vast programme aimed at characterizing the new particle was undertaken: the full LHC Run~1 datasets collected by both experiments were reanalysed with updated detector calibrations, and improved reconstruction and analysis techniques.  The published Run~1 results cover the main production and decay channels expected of the SM Higgs boson, along with the spin and CP properties of the new particle~\cite{Aad:2013xqa,Chatrchyan:2012jja}, and precision measurements of its mass~\cite{Aad:2015zhl}.  More recently, a combination of the ATLAS and CMS measurements was performed~\cite{Khachatryan:2016vau}.  The combined results feature clear observations of the Higgs boson decay to the bosonic channels, the observation of the decay to tau leptons, and of the weak boson production mode.  Overall, the results are consistent with the predictions of the Standard Model, see \refF{fig:intro-RunI-legacy}.  In parallel, searches for an extended Higgs sector were performed covering many Beyond the Standard Model (BSM) scenarios.  No significant evidence of a signal was observed.

   In 2015, the LHC started to collide protons at the higher centre of mass energy of 13\,TeV.  The cross sections for the SM production modes increase by a factor of approximately two to four, depending on the process, and the anticipated integrated luminosity for Run~2  which is scheduled to end in 2018 is of the order of 120\,fb${}^{-1}$ per experiment.  Such a dataset will allow ATLAS and CMS to reduce the current experimental uncertainties significantly, motivating the need for improved theoretical predictions.   For BSM Higgs physics, the higher collision energy will increase substantially the reach of searches for BSM Higgs bosons in the high mass regime and the larger dataset will improve the sensitivity of searches for exotic decays of the 125\,GeV Higgs boson.  New benchmarks and updated calculations will facilitate the exploration of this newly accessible BSM parameter space.

   This report presents improved predictions for the production and decay of the Standard Model Higgs boson~\cite{Dittmaier:2011ti,Dittmaier:2012vm,Heinemeyer:2013tqa}.  These include a gluon fusion production cross section at N3LO, updated and improved Parton Distribution Functions and correlation studies following the PDF4LHC prescriptions, fully differential VBF/VH NNLO QCD and NLO electroweak calculations, NLO electroweak corrections to the ttH processes in addition to studies of the ttV backgrounds.
Differential NNLO+NNLL QCD calculations for the HH process are now available as well as the first NLO results with  full top-quark mass dependence.   
Also first 2-loop NLO calculations for $gg\to VV$ below top-threshold with a massive quark have been achieved and they will be helpful for the study of the off-shell production of the Higgs boson.

   For Higgs boson property measurements, the Interim framework for the analysis of Higgs couplings ``kappa framework"~\cite{LHCHiggsCrossSectionWorkingGroup:2012nn} was used by the ATLAS and CMS experiments to report Higgs boson coupling related results extracted from the LHC Run 1 data~\cite{Khachatryan:2016vau}. The update information for Run 2 is provided elsewhere\footnote{\url{https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWG2KAPPA}}. With the additional statistical power of the Run~2  dataset, the experiments will be able to measure more precisely the kinematic properties of the 125 GeV Higgs and use these measurements to probe for possible deviations induced by new phenomena.  To do this, several strategies have been devised to maximize the sensitivity to BSM physics across channels and as a function of the integrated luminosity.  These strategies will allow the experiments to extract more information from the data compared to the ``kappa framework". To do this, pseudo-observables are defined as a possible alternative to more direct template, fiducial and differential cross section measurements. They will eventually be used as inputs to interpret the data in terms of Effective Field Theories (EFTs).  Several Monte Carlo tools necessary to undertake this effort are being developed and  made available, as described in this report.

  In the search for new physics in the Higgs sector, this report proposes new benchmarks for the exploration of a BSM Higgs sector along with improved and extended calculations in various scenarios including the MSSM, the NMSSM, and more generic models featuring charged and neutral Higgs bosons.  In addition, a new chapter on rare and exotic Higgs boson decays has been added and it covers rare mesonic decays, flavour-violating decays, prompt decays with and without missing energy, and decays into 
long-lived particles or with displaced vertices,  and it provides recommendations on searches for exotic decays of the 125\,GeV Higgs boson.

The updated and improved calculations of SM and BSM Higgs boson production and decay processes as reported here
provide a solid theoretical reference for experimental studies of the early Run~2  data
that are expected to decipher the properties of the 125\,GeV Higgs boson and the nature of the full Higgs sector.  
In addition, they pave the way towards the theoretical developments and improvements in precision   that will be needed in the future in order to fully exploit the potential of the complete Run~2  dataset. 

\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]{./Introduction/RunI-legacy.pdf}
\end{center}
\caption{Best fit results for the production (left) and decay (right) signal strengths for the combination of ATLAS and CMS data~\cite{Khachatryan:2016vau}. The error bars indicate the 1$\sigma$ (thick lines) and 2$\sigma$ (thin lines) intervals. The combined results show a remarkable agreement with the SM prediction (normalized to $\mu=1$).}
\label{fig:intro-RunI-legacy}
\end{figure}


 

 

